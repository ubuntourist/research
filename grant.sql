-- Last modified by Kevin Cole <kevin.cole@gallaudet.edu> 2011.01.24
--
-- 2008.11.30 KJC - Initial release
-- 2011.01.24 KJC - Added update permissions to all sequences 
--
-- Risky business, I'm sure... django or apache complains about
-- permissions on django_session.  Let's open the floodgates.
--
-- The original permissions were:
--
-- GRANT ALL ON SCHEMA public TO postgres;
-- GRANT ALL ON SCHEMA public TO PUBLIC;
--
-- GRANT ALL ON TABLE mhd_country TO kjcole;
-- GRANT SELECT ON TABLE mhd_country TO "apache";
-- GRANT SELECT ON TABLE mhd_country TO reader;
--
-- GRANT ALL ON TABLE mhd_state TO kjcole;
-- GRANT SELECT ON TABLE mhd_state TO "apache";
-- GRANT SELECT ON TABLE mhd_state TO reader;
--

GRANT ALL ON TABLE mhd_state TO "apache";
GRANT ALL ON TABLE auth_group TO "apache";
GRANT ALL ON TABLE auth_group_permissions TO "apache";
GRANT ALL ON TABLE auth_message TO "apache";
GRANT ALL ON TABLE auth_permission TO "apache";
GRANT ALL ON TABLE auth_user TO "apache";
GRANT ALL ON TABLE auth_user_groups TO "apache";
GRANT ALL ON TABLE auth_user_user_permissions TO "apache";
GRANT ALL ON TABLE books_author TO "apache";
GRANT ALL ON TABLE books_book TO "apache";
GRANT ALL ON TABLE books_book_authors TO "apache";
GRANT ALL ON TABLE books_publisher TO "apache";
GRANT ALL ON TABLE django_admin_log TO "apache";
GRANT ALL ON TABLE django_content_type TO "apache";
GRANT ALL ON TABLE django_session TO "apache";
GRANT ALL ON TABLE django_site TO "apache";
GRANT ALL ON TABLE mhd_country TO "apache";
GRANT ALL ON TABLE mhd_dirty TO "apache";
GRANT ALL ON TABLE mhd_provider TO "apache";
GRANT ALL ON TABLE mhd_state TO "apache";

-- The sequences

GRANT UPDATE ON SEQUENCE auth_group_id_seq                 TO "apache";
GRANT UPDATE ON SEQUENCE auth_group_permissions_id_seq     TO "apache";
GRANT UPDATE ON SEQUENCE auth_message_id_seq               TO "apache";
GRANT UPDATE ON SEQUENCE auth_permission_id_seq            TO "apache";
GRANT UPDATE ON SEQUENCE auth_user_groups_id_seq           TO "apache";
GRANT UPDATE ON SEQUENCE auth_user_id_seq                  TO "apache";
GRANT UPDATE ON SEQUENCE auth_user_user_permissions_id_seq TO "apache";
GRANT UPDATE ON SEQUENCE books_author_id_seq               TO "apache";
GRANT UPDATE ON SEQUENCE books_book_authors_id_seq         TO "apache";
GRANT UPDATE ON SEQUENCE books_book_id_seq                 TO "apache";
GRANT UPDATE ON SEQUENCE books_publisher_id_seq            TO "apache";
GRANT UPDATE ON SEQUENCE dirty_id_seq                      TO "apache";
GRANT UPDATE ON SEQUENCE django_admin_log_id_seq           TO "apache";
GRANT UPDATE ON SEQUENCE django_content_type_id_seq        TO "apache";
GRANT UPDATE ON SEQUENCE django_site_id_seq                TO "apache";
GRANT UPDATE ON SEQUENCE mhd_provider_id_seq               TO "apache";
GRANT UPDATE ON SEQUENCE mhd_state_id_seq                  TO "apache";
