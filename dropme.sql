-- Cleans the rsca database
-- Written by Kevin Cole <kjcole@gallaudet.edu> 2012.03.19
--

DROP TABLE focus;
DROP TABLE funding;
DROP TABLE investigator;
DROP TABLE product;
DROP TABLE project;
DROP TABLE agency;
DROP TABLE classification;
DROP TABLE department;
DROP TABLE funder;
DROP TABLE priority;
DROP TABLE profession;
DROP TABLE program;
DROP TABLE person;
