CREATE TABLE mhd_city (
    id serial primary key,
    state smallint,
    name character varying(30)
);

ALTER TABLE public.mhd_city OWNER TO kjcole;

COMMENT ON TABLE mhd_city IS 'Cities and their coresponding states';
COMMENT ON COLUMN mhd_city.state IS 'State or province';
COMMENT ON COLUMN mhd_city.name IS 'City name';

ALTER TABLE ONLY mhd_city
    ADD CONSTRAINT state_fk FOREIGN KEY (state) REFERENCES mhd_state(id);

REVOKE ALL ON TABLE mhd_city FROM PUBLIC;
REVOKE ALL ON TABLE mhd_city FROM kjcole;
GRANT ALL ON TABLE mhd_city TO kjcole;
GRANT ALL ON TABLE mhd_city TO "apache";
GRANT SELECT ON TABLE mhd_city TO reader;

COPY mhd_city (state, name) FROM stdin;
1	Birmingham
1	Huntsville
1	Mobile
1	Montgomery
2	Anchorage
3	Tempe
3	Tucson
4	Little Rock
5	Berkeley
5	Burlingame
5	Chino
5	Hayward
5	La Jolla
5	Long Beach
5	Los Angeles
5	Oakland
5	Riverside
5	San Diego
5	San Francisco
5	San Jose
5	Santa Monica
5	Santa Rosa
6	Denver
7	Bridgeport
9	Washington
10	Miami
10	Riverview
11	Decatur
12	Honolulu
14	Chicago
14	Dekalb
14	Springfield
15	Indianapolis
17	Olathe
18	Bowling Green
18	Florence
18	Lexington
18	Louisville
18	Paducah
20	Augusta
20	Portland
21	Arnold
21	Baltimore
21	Burtonsville
21	Frederick
21	Landover Hills
21	Laurel
21	N. Bethesda
21	North Potomac
21	Owings Mills
21	Rockville
21	Salisbury
21	Silver Spring
22	E. Boston
22	Holyoke
22	North Quincy
22	Waltham
22	Westborough
23	Ann Arbor
23	Berkley
23	Farmington Hills
23	Redford
24	Minneapolis
24	St. Paul
26	Jefferson City
32	Santa Fe
33	Hicksville
33	New York
33	Rochester
33	Valley Cottage
34	Charlotte
34	Fayetteville
34	Greensboro
34	Morganton
34	Raleigh
34	Wilmington
34	Wilson
36	Akron
36	Cincinnati
36	Columbus
36	Dayton
36	Grove City
38	Portland
38	Salem
39	Bala Cynwyd
39	Glenside
39	Narberth
39	Pittsburgh
40	Warren
41	Simpsonville
43	Knoxville
44	Austin
44	Tyler
45	Taylorsville
46	Brattleboro
47	Alexandria
47	Charlottesville
47	Fairfax
47	Falls Church
47	McLean
47	Springfield
47	Staunton
48	Seattle
50	Madison
50	Milwaukee
60	Milton
60	Toronto
63	Regina
63	Saskatoon
\.
