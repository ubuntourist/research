# Written by Kevin Cole <kjcole@gallaudet.edu> 2013.12.19
#
# A snippet to help interactively diagnose pagination issues with the
# International Visitor Request Form.
#

from django.http           import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts      import render_to_response, get_object_or_404
from django.db.models      import Q
from django.template       import RequestContext
from django.forms.models   import inlineformset_factory
from django.core.paginator import Paginator
from django.template       import Context, loader

from visitors.models       import *  # My models
from visitors.forms        import *  # My forms

countries = Country.objects.all()                          # List of countries
html      = loader.get_template("visitors/visitors.html")  # Visitor Template

inquiry   = get_object_or_404(Inquiry, pk=6)
VisitorFormset = inlineformset_factory(Inquiry, Visitor, 
                                       form=VisitorForm,
                                       extra=inquiry.visitors) # Six slots

visitor_formset = VisitorFormset(instance=inquiry)
len(visitor_formset)
visitor_formset

p = Paginator(visitor_formset,1)
p.count
p.page_range
page1 = p.page(1)
page1
page1.object_list
page1.object_list[0]

render_to_response("visitors/visitors.html",locals())
filler = Context(locals())
html.render(filler)
save = html.render(filler)
wtf = open("wtf.html","wb")
wtf.write(save)
wtf.close()

##############################################################################

# p
# page1 = p.page(1)
# vform = page1.object_list[0]
# vform

# vform.as_ul() 
#    ... generates the HTML with the "visitor_set-##-field"
#        and "id_visitor_set-##-field" attribute values ...
#
#    ... So... We need pass vform AND know where it stores it's page
#        or pass the page id in another way. Then manually recreate
#        the "id_visitor_set-##-field" and "visitor_set-##-field"
#        attribute values.
#
#    ... GOT IT! vform.prefix returns "visitor_set-0",
#        "visitor_set-1", etc.
#

# vform.fields
# surname = vform.fields["surname"]
# surname.label
# inquiry = vform.fields["inquiry"]
# inquiry
# inquiry.help_text
# inquiry.label
# inquiry.parent_instance
# parent = inquiry.parent_instance
# parent.dep_date

#In [12]: surname.
#surname.__class__               surname.clean
#surname.__deepcopy__            surname.creation_counter
#surname.__delattr__             surname.default_error_messages
#surname.__dict__                surname.default_validators
#surname.__doc__                 surname.error_messages
#surname.__format__              surname.help_text
#surname.__getattribute__        surname.hidden_widget
#surname.__hash__                surname.initial
#surname.__init__                surname.label
#surname.__module__              surname.localize
#surname.__new__                 surname.max_length
#surname.__reduce__              surname.min_length
#surname.__reduce_ex__           surname.prepare_value
#surname.__repr__                surname.required
#surname.__setattr__             surname.run_validators
#surname.__sizeof__              surname.show_hidden_initial
#surname.__slotnames__           surname.to_python
#surname.__str__                 surname.validate
#surname.__subclasshook__        surname.validators
#surname.__weakref__             surname.widget
#surname.bound_data              surname.widget_attrs

#In [12]: surname.widget
#Out[12]: <django.forms.widgets.TextInput object at 0x2b90f90>
#In [13]: surname.widget.
#surname.widget.__class__             surname.widget.__str__
#surname.widget.__deepcopy__          surname.widget.__subclasshook__
#surname.widget.__delattr__           surname.widget.__weakref__
#surname.widget.__dict__              surname.widget._format_value
#surname.widget.__doc__               surname.widget._has_changed
#surname.widget.__format__            surname.widget.attrs
#surname.widget.__getattribute__      surname.widget.build_attrs
#surname.widget.__hash__              surname.widget.id_for_label
#surname.widget.__init__              surname.widget.input_type
#surname.widget.__metaclass__         surname.widget.is_hidden
#surname.widget.__module__            surname.widget.is_localized
#surname.widget.__new__               surname.widget.is_required
#surname.widget.__reduce__            surname.widget.media
#surname.widget.__reduce_ex__         surname.widget.needs_multipart_form
#surname.widget.__repr__              surname.widget.render
#surname.widget.__setattr__           surname.widget.subwidgets
#surname.widget.__sizeof__            surname.widget.value_from_datadict
#surname.widget.__slotnames__         
