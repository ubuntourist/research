# Written by Kevin Cole <kjcole@gallaudet.edu> 2013.12.19
#
# A snippet to help interactively diagnose pagination issues with the
# International Visitor Request Form.
#

from django.http           import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts      import render_to_response, get_object_or_404
from django.db.models      import Q
from django.template       import RequestContext
from django.forms.models   import inlineformset_factory
from django.core.paginator import Paginator
from django.template       import Context, loader

from visitors.models       import *  # My models
from visitors.forms        import *  # My forms

countries = Country.objects.all()                          # List of countries
html      = loader.get_template("visitors/visitors.html")  # Visitor Template

inquiry   = get_object_or_404(Inquiry, pk=6)
VisitorFormset = inlineformset_factory(Inquiry, Visitor, 
                                       form=VisitorForm,
                                       extra=inquiry.visitors) # Six slots

visitor_formset = VisitorFormset(instance=inquiry)
p = Paginator(visitor_formset,1)
page1 = p.page(1)
vform = page1.object_list[0]
#    ... GOT IT! vform.prefix returns "visitor_set-0",
#        "visitor_set-1", etc.
