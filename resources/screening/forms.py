from models                              import *
from django                              import forms
from django.contrib.localflavor.us.forms import USPhoneNumberField
from django.forms.widgets                import TextInput, CheckboxInput, RadioSelect
from django.forms.widgets                import DateInput, TimeInput, DateTimeInput
#from django.forms.widgets               import CheckboxSelectMultiple

class EmailInput(TextInput):
    input_type = "email"             # HTML5 enhanced

class NumberInput(TextInput):
    input_type = "number"            # HTML5 enhanced

class TelephoneInput(TextInput):
    input_type = "tel"               # HTML5 enhanced

class DateInput(DateInput):
    input_type = "date"              # HTML5 enhanced

class DateTimeInput(DateTimeInput):
    input_type = "datetime"          # HTML5 enhanced

class TimeInput(TimeInput):
    input_type = "time"              # HTML5 enhanced

PARTICIPATION = ((2,"Yes, please do so."),
                 (1,"Yes, but someone else must approve participation."),
                 (0,"No, I respectfully decline."))

class StateForm(forms.ModelForm):
    class Meta:
        model = State
#       widgets = {"priorities": CheckboxSelectMultiple,}

class AgencyForm(forms.ModelForm):
    is_source_iep   = forms.BooleanField(required=False)
    is_source_ifsp  = forms.BooleanField(required=False)
    is_source_504   = forms.BooleanField(required=False)
    is_source_ada   = forms.BooleanField(required=False)
    is_source_other = forms.BooleanField(required=False)

    contact_name    = forms.CharField(max_length=60)
    contact_title   = forms.CharField(max_length=60)
    contact_email   = forms.EmailField(max_length=60)
    contact_phone   = USPhoneNumberField(max_length=25)

    participation   = forms.TypedChoiceField(coerce=int, choices=PARTICIPATION)

    class Meta:
        model   = Agency
        fields  = ("is_source_iep", "is_source_ifsp", "is_source_504", 
                   "is_source_ada", "is_source_other", 
                   "contact_name", "contact_title", "contact_email", 
                   "contact_phone", "participation",)
        widgets = {"contact_name":  TextInput(attrs={"size":"60"}),
                   "contact_title": TextInput(attrs={"size":"60"}),
                   "contact_email": TextInput(attrs={"size":"60"}),
                   "contact_phone": TextInput(attrs={"size":"25"}),
                   "participation": RadioSelect(),}

class PrivateForm(forms.ModelForm):
    students_deaf = forms.IntegerField(required=False,
                                       min_value=0, max_value=99999)

    is_source_iep   = forms.BooleanField(required=False)
    is_source_ifsp  = forms.BooleanField(required=False)
    is_source_504   = forms.BooleanField(required=False)
    is_source_ada   = forms.BooleanField(required=False)
    is_source_other = forms.BooleanField(required=False)

    contact_name    = forms.CharField(max_length=60)
    contact_title   = forms.CharField(max_length=60)
    contact_email   = forms.EmailField(max_length=60)
    contact_phone   = USPhoneNumberField(max_length=25)

    participation   = forms.TypedChoiceField(coerce=int, choices=PARTICIPATION)

    class Meta:
        model   = Private_School
        fields  = ("students_deaf", 
                   "is_source_iep", "is_source_ifsp", "is_source_504", 
                   "is_source_ada", "is_source_other", 
                   "contact_name", "contact_title", "contact_email", 
                   "contact_phone", "participation",)
        widgets = {"students_deaf": TextInput(attrs={"style":"font-size:150%; text-align:right; width:6em;",
                                                       "size":"6",
                                                       "maxlength":"6"}),
                   "contact_name":  TextInput(attrs={"size":"60"}),
                   "contact_title": TextInput(attrs={"size":"60"}),
                   "contact_email": TextInput(attrs={"size":"60"}),
                   "contact_phone": TextInput(attrs={"size":"25"}),
                   "participation": RadioSelect(),}

class PublicForm(forms.ModelForm):
    students_deaf = forms.IntegerField(required=False,
                                       min_value=0, max_value=99999)

    class Meta:
        model   = Public_School
        fields  = ("students_deaf",)
        widgets = {"students_deaf": TextInput(attrs={"style":"font-size:150%; text-align:right; width:6em;",
                                                       "size":"6",
                                                       "maxlength":"6"}),}

#_____________________________________________________________________________

# These generic forms contain the only fields we're actually interested in.

class CountsForm(forms.Form):
    deaf   = forms.IntegerField(min_value=0, max_value=99999,
                                required=False,
                                widget=TextInput(attrs={"style":"font-size:150%; text-align:right; width:6em;",
                                                          "size":"6",
                                                          "maxlength":"6"}))

class SourcesForm(forms.Form):
    iep   = forms.BooleanField(required=False)
    ifsp  = forms.BooleanField(required=False)
    s504  = forms.BooleanField(required=False)
    ada   = forms.BooleanField(required=False)
    other = forms.BooleanField(required=False)

class ContactForm(forms.Form):
    name  = forms.CharField(max_length=60,
                            widget=TextInput(attrs={"size":"60"}))
    title = forms.CharField(max_length=60,
                            widget=TextInput(attrs={"size":"60"}))
    email = forms.EmailField(max_length=60,
                             widget=TextInput(attrs={"size":"60"}))
    phone = USPhoneNumberField(max_length=25,
                               widget=TextInput(attrs={"size":"25"}))

class ParticipationForm(forms.Form):
    participation = forms.TypedChoiceField(coerce=int,
                                           widget=RadioSelect(),
                                           choices=PARTICIPATION)

#_____________________________________________________________________________

class AgencySourcesForm(forms.ModelForm):
    is_source_iep   = forms.BooleanField(required=False)
    is_source_ifsp  = forms.BooleanField(required=False)
    is_source_504   = forms.BooleanField(required=False)
    is_source_ada   = forms.BooleanField(required=False)
    is_source_other = forms.BooleanField(required=False)

    class Meta:
        model   = Agency
        fields  = ("is_source_iep",
                   "is_source_ifsp",
                   "is_source_504", 
                   "is_source_ada",
                   "is_source_other",)

class AgencyContactForm(forms.ModelForm):
    contact_name  = forms.CharField(max_length=60,
                          widget=TextInput(attrs={"size":"60"}))
    contact_title = forms.CharField(max_length=60,
                          widget=TextInput(attrs={"size":"60"}))
    contact_email = forms.EmailField(max_length=60,
                          widget=TextInput(attrs={"size":"60"}))
    contact_phone = USPhoneNumberField(max_length=25,
                          widget=TextInput(attrs={"size":"25"}))

    class Meta:
        model   = Agency
        fields  = ("contact_name",
                   "contact_title",
                   "contact_email", 
                   "contact_phone",)

class AgencyParticipationForm(forms.ModelForm):
    participation = forms.TypedChoiceField(coerce=int,
                          choices=PARTICIPATION,
                          widget=RadioSelect())

    class Meta:
        model   = Agency
        fields  = ("participation",)

##############################################################################

class PrivateCountsForm(forms.ModelForm):
    students_deaf = forms.IntegerField(required=False,
                          min_value=0, max_value=99999,
                          widget=TextInput(attrs={"style":"font-size:150%; text-align:right; width:6em;",
                                                    "size":"6",
                                                    "maxlength":"6"}))

    class Meta:
        model   = Private_School
        fields  = ("students_deaf",)

class PrivateSourcesForm(forms.ModelForm):
    is_source_iep   = forms.BooleanField(required=False)
    is_source_ifsp  = forms.BooleanField(required=False)
    is_source_504   = forms.BooleanField(required=False)
    is_source_ada   = forms.BooleanField(required=False)
    is_source_other = forms.BooleanField(required=False)

    class Meta:
        model   = Private_School
        fields  = ("is_source_iep",
                   "is_source_ifsp",
                   "is_source_504", 
                   "is_source_ada",
                   "is_source_other",)

class PrivateContactForm(forms.ModelForm):
    contact_name  = forms.CharField(max_length=60,
                          widget=TextInput(attrs={"size":"60"}))
    contact_title = forms.CharField(max_length=60,
                          widget=TextInput(attrs={"size":"60"}))
    contact_email = forms.EmailField(max_length=60,
                          widget=TextInput(attrs={"size":"60"}))
    contact_phone = USPhoneNumberField(max_length=25,
                          widget=TextInput(attrs={"size":"25"}))

    class Meta:
        model   = Private_School
        fields  = ("contact_name",
                   "contact_title",
                   "contact_email", 
                   "contact_phone",)

class PrivateParticipationForm(forms.ModelForm):
    participation = forms.TypedChoiceField(coerce=int,
                          choices=PARTICIPATION,
                          widget=RadioSelect())

    class Meta:
        model   = Private_School
        fields  = ("participation",)

class PublicCountsForm(forms.ModelForm):
    students_deaf = forms.IntegerField(required=False,
                          min_value=0, max_value=99999,
                          widget=TextInput(attrs={"style":"font-size:150%; text-align:right; width:6em;",
                                                    "size":"6",
                                                    "maxlength":"6"}))

    class Meta:
        model   = Public_School
        fields  = ("students_deaf",)
