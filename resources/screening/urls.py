# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2013.07.16
#
# 2013.07.16 KJC - Switching to class-based generic views from function-based
#

from django.conf.urls.defaults import *
from resources.screening.views import *
from django.views.generic.base import TemplateView

urlpatterns = patterns("",
#   (r"^.*$",       TemplateView.as_view(template_name="screening/closed.html")),
    (r"^$",         TemplateView.as_view(template_name="screening/index.html")),
#   (r"^credits/$", TemplateView.as_view(template_name="screening/credits.html")),
#   (r"^consent/$", TemplateView.as_view(template_name="screening/consent.html")),
#   (r"^stop/$",    TemplateView.as_view(template_name="screening/stop.html")),
    (r"^sesame/$",   "django.contrib.auth.views.login", 
                                        {"template_name": "screening/sesame.html"}),
#   (r"^begin/$",                    winnow),
#   (r"^winnow_confirm/$",           winnow_confirm),
#   (r"^public_confirm/(\d{1,7})/$", public_confirm),
#   (r"^private_confirm/(\w{8})/$",  private_confirm),
#   (r"^public_collect/(\d{1,7})/$", public_collect),
#   (r"^private_collect/$",          private_collect),
#   (r"^public_students/$",          public_students),
#   (r"^public_counts/$",            public_counts),
#   (r"^public_sources/$",           public_sources),
#   (r"^public_contact/$",           public_contact),
#   (r"^private_contact/$",          private_contact),
#   (r"^public_participation/$",     public_participation),
#   (r"^private_participation/$",    private_participation),
    (r"^track/$",                    track),
    (r"^track_agency/(\d{1,7})/$",   track_agency),
    (r"^track_private/(\w{8})/$",    track_private),
)
