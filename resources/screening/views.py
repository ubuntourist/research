# Written by Kevin Cole <kjcole@gri.gallaudet.edu> 2012.04.19
#
# 2012.03.11 KJC - Began development in earnest
# 2012.03.30 KJC - Switching to generic views for static pages
# 2012.04.19 KJC - Considering and toying with introducing forms
# 2012.05.08 KJC - Ross is moving the finish line...
#
# This contains the functions to which URL's are mapped by 
# urls.py.
#

import random                                         # DEBUG
from django.http                import HttpResponse   # DEBUG

from django.shortcuts           import render_to_response, get_object_or_404
from django.db.models           import Q
from django.template            import RequestContext
from django.forms.models        import inlineformset_factory
from django.contrib.auth        import authenticate, login

from resources.screening.models import State, Agency,                \
                                       Private_School, Public_School
from resources.screening.forms  import StateForm, AgencyForm,                \
                                       PrivateForm, PublicForm,              \
                                       CountsForm, ContactForm, SourcesForm, \
                                       ParticipationForm

from resources.screening.forms  import AgencySourcesForm,        \
                                       AgencyContactForm,        \
                                       AgencyParticipationForm,  \
                                       PrivateCountsForm,         \
                                       PrivateSourcesForm,       \
                                       PrivateContactForm,       \
                                       PrivateParticipationForm, \
                                       PublicCountsForm

def winnow(request):
    states  = State.objects.all()            # Get a list of all states
    st      = request.GET.get("usps",  "")   # State selected
    funding = request.GET.get("funding", "") # Public or Private?
    if st and funding:
        states = State.objects.get(usps=st) # Get the full state name
        if   funding == "public":
            agency_type = "Local Education Agency (LEA) / Public School"
        elif funding == "private":
            agency_type ="Private School"
        else:
            agency_type =""
        request.session["state"] = st
        request.session["type"]  = funding
        return render_to_response("screening/winnow_confirm.html", 
                                  {"states":      states,
                                   "agency_type": agency_type})
    else:
        return render_to_response("screening/winnow.html", 
                                  {"states":   states})

def winnow_confirm(request):
    st      = request.session["state"]    # State selected
    funding = request.session["type"]     # Public or Private?
    states  = State.objects.get(usps=st)  # Get the full state name
    if   funding == "public":
        leas   = Agency.objects.filter(state=st)
        leas   = leas.filter(in_sample=True)   \
                     .filter(as_contact=False) \
                     .filter(visited=False)
        return render_to_response("screening/public_selector.html", 
                                  {"states": states, 
                                   "leas":   leas})
    elif funding == "private":
        privates = Private_School.objects.filter(state=st)
        privates = privates.filter(in_sample=True) \
                           .filter(visited=False)
        return render_to_response("screening/private_selector.html", 
                                  {"states":   states, 
                                   "privates": privates})

def public_confirm(request,lea):
    """public_confirm: Offer the option to set the gun down & back away slowly"""
    agency =  get_object_or_404(Agency, pk=lea)
    request.session["entity"] = unicode(lea)
    return render_to_response("screening/public_confirm.html", locals(),
                              context_instance=RequestContext(request))

def public_collect(request,lea):
    """public_collect: collects data for a specific LEA"""
    agency  = get_object_or_404(Agency, pk=lea)
    schools = Public_School.objects.filter(agency=lea)

    agency.visited = True
    agency.save()

    for school in schools:
        school.visited = True
        school.save()

    PublicCountsFormSet = inlineformset_factory(Agency, Public_School,
                                               form=PublicCountsForm,
                                               max_num=1,
                                               can_delete=False,
                                               fields=("students_deaf",))
    formset = PublicCountsFormSet(instance=agency)
    return render_to_response("screening/public_counts.html", locals(),
                              context_instance=RequestContext(request))

def public_counts(request):
    lea     = int(request.session["entity"])
    agency  = Agency.objects.get(id=lea)
    schools = Public_School.objects.filter(agency=lea)

    PublicCountsFormSet = inlineformset_factory(Agency, Public_School,
                                               form=PublicCountsForm,
                                               max_num=1,
                                               can_delete=False,
                                               fields=("students_deaf",))

    if request.method == "POST": # If the form has been submitted...
        formset = PublicCountsFormSet(request.POST,instance=agency)
        if formset.is_valid(): # ...instantiate w/ posted data & check for validity
            formset.save()

            agency.visited = True
            agency.save()

            for school in schools:
                school.visited = True
                school.save()

            # Debuggery
            data = []
            for form in formset:
                deaf   = form.cleaned_data["students_deaf"]
                school = form.cleaned_data["id"]
                if deaf:
                    request.session[school] = deaf
                    data.append({"key":school, "value":deaf})

            form = AgencySourcesForm(instance=agency)
            return render_to_response("screening/public_sources.html",
                                      {"form": form, "agency":agency, "data":data},
                                      context_instance=RequestContext(request))

    else:                                            # If form NOT submitted...
        formset = PublicCountsForm(instance=agency)  # ...instantiate empty form

    return render_to_response("screening/public_counts.html", locals(),
                              context_instance=RequestContext(request))

def public_students(request):
    """public_students: saves the data gathered by public_collect, continues to contact view"""
    submitted = request.POST

    lea = int(request.session["entity"])
    agency = Agency.objects.get(id=lea)

    agency.visited = True
    agency.save()

    for kee in submitted.keys():
        if submitted[kee]:
            if kee[0:3] == "ps.":
                ps = int(kee[3:])
                school = Public_School.objects.get(id=ps)
                school.students_deaf = int(submitted[kee])
                school.visited = True
                school.save()
                request.session[kee] = submitted[kee]

    # Debuggery
    data = []
    for kee in submitted.keys():
        if submitted[kee] and kee != "csrfmiddlewaretoken":
            data.append({"key": kee, "value":submitted[kee]})

    form = AgencySourcesForm(instance=agency)
    return render_to_response("screening/public_sources.html",
                              {"form": form, "agency":agency, "data":data},
                              context_instance=RequestContext(request))

def public_sources(request):
    """public_sources: collects sources data for a Local Education Agency"""
    lea    = int(request.session["entity"])
    agency = Agency.objects.get(id=lea)

    if request.method == "POST": # If the form has been submitted...
        form = AgencySourcesForm(request.POST, instance=agency)
        if form.is_valid():   # ...instantiate w/ posted data & check for validity
            agency.visited = True
            agency.save()

            # Debuggery
            iep   = form.cleaned_data["is_source_iep"] 
            ifsp  = form.cleaned_data["is_source_ifsp"]
            s504  = form.cleaned_data["is_source_504"]
            ada   = form.cleaned_data["is_source_ada"]
            other = form.cleaned_data["is_source_other"]
            request.session["iep"]   = iep
            request.session["ifsp"]  = ifsp
            request.session["s504"]  = s504
            request.session["ada"]   = ada
            request.session["other"] = other
            data = [{"key":"iep",   "value":iep},
                    {"key":"ifsp",  "value":ifsp},
                    {"key":"s504",  "value":s504},
                    {"key":"ada",   "value":ada},
                    {"key":"other", "value":other}]

            form = AgencyContactForm(instance=agency)
            return render_to_response("screening/public_contact.html",
                                      {"form": form, "agency":agency, "data":data},
                                      context_instance=RequestContext(request))

    else:                                            # If form NOT submitted...
        form = AgencySourcesForm(instance=agency) # ...instantiate empty form

    return render_to_response("screening/public_sources.html",
                              {"form":form, "agency":agency},
                              context_instance=RequestContext(request))

def public_contact(request):
    lea = int(request.session["entity"])
    agency = Agency.objects.get(id=lea)

    if request.method == "POST": # If the form has been submitted...
        form = AgencyContactForm(request.POST, instance=agency)
        if form.is_valid():      # ...instantiate w/ posted data & check for validity
            agency.visited = True
            agency.save()

            # Debuggery
            name  = form.cleaned_data["contact_name"]
            title = form.cleaned_data["contact_title"]
            email = form.cleaned_data["contact_email"]
            phone = form.cleaned_data["contact_phone"]
            request.session["name"]  = name
            request.session["title"] = title
            request.session["email"] = email
            request.session["phone"] = phone

            data  = [{"key":"lea",   "value":lea},
                     {"key":"name",  "value":name},
                     {"key":"title", "value":title},
                     {"key":"email", "value":email},
                     {"key":"phone", "value":phone}]

            form = AgencyParticipationForm(instance=agency)
            return render_to_response("screening/public_participation.html",
                                      locals(),
                                      context_instance=RequestContext(request))
    else:                                         # If form NOT submitted...
        form = AgencyContactForm(instance=agency) # ...instantiate empty form

    return render_to_response("screening/public_contact.html", 
                              {"form":form, "agency":agency},
                              context_instance=RequestContext(request))

def public_participation(request):
    lea = int(request.session["entity"])
    agency = Agency.objects.get(id=lea)

    if request.method == "POST": # If the form has been submitted...
        form = AgencyParticipationForm(request.POST, instance=agency)
        if form.is_valid():      # ...instantiate w/ posted data & check for validity
            agency.visited = True
            agency.save()

            # Debuggery
            participation = form.cleaned_data["participation"]
            request.session["participation"] = participation
            data = [{"key":"participation", "value":participation}]

            return render_to_response("screening/thanks.html", locals())
    else:                                               # If form NOT submitted...
        form = AgencyParticipationForm(instance=agency) # ...instantiate empty form

    return render_to_response("screening/public_participation.html", 
                              {"form":form, "agency":agency},
                              context_instance=RequestContext(request))

def private_confirm(request,private):
    """private_confirm: Offer the option to set the gun down & back away slowly"""
    school =  get_object_or_404(Private_School, pk=private)
    request.session["entity"] = private
    return render_to_response("screening/private_confirm.html", locals(),
                              context_instance=RequestContext(request))

def private_collect(request):
    """private_collect: collects data for a specific private school"""
    private = request.session["entity"]
    school  = Private_School.objects.get(id=private)
    data = []
    okay = 0

    school.visited = True
    school.save()

    if request.method == "POST": # If the form has been submitted...
        sources = PrivateSourcesForm(request.POST, instance=school)
        if sources.is_valid():   # ...instantiate w/ posted data & check for validity
            school.visited = True
            school.save()

            okay += 1

            # Debuggery
            iep   = sources.cleaned_data["is_source_iep"] 
            ifsp  = sources.cleaned_data["is_source_ifsp"]
            s504  = sources.cleaned_data["is_source_504"]
            ada   = sources.cleaned_data["is_source_ada"]
            other = sources.cleaned_data["is_source_other"]
            request.session["iep"]   = iep
            request.session["ifsp"]  = ifsp
            request.session["s504"]  = s504
            request.session["ada"]   = ada
            request.session["other"] = other
            data.append({"key":"iep",   "value":iep})
            data.append({"key":"ifsp",  "value":ifsp})
            data.append({"key":"s504",  "value":s504})
            data.append({"key":"ada",   "value":ada})
            data.append({"key":"other", "value":other})

        counts = PrivateCountsForm(request.POST, instance=school)
        if counts.is_valid(): # ...instantiate w/ posted data & check for validity
            school.visited = True
            school.save()

            okay += 1

            # Debuggery
            deaf = counts.cleaned_data["students_deaf"]
            request.session["deaf"] = deaf
            data.append({"key":"deaf",   "value":deaf})

        if okay == 2:
            form = PrivateContactForm(instance=school)
            return render_to_response("screening/private_contact.html",
                                      {"form": form, "school":school, "data":data},
                                      context_instance=RequestContext(request))

    else:                                             # If form NOT submitted...
        sources = PrivateSourcesForm(instance=school) # ...instantiate empty form
        counts  = PrivateCountsForm(instance=school)  # ...instantiate empty form

    return render_to_response("screening/private_collect.html", locals(),
                              context_instance=RequestContext(request))

def private_contact(request):
    private = request.session["entity"]
    school  = Private_School.objects.get(id=private)

    if request.method == "POST": # If the form has been submitted...
        form = PrivateContactForm(request.POST, instance=school)
        if form.is_valid():      # ...instantiate w/ posted data & check validity
            form.save()

            school.visited = True
            school.save()

            # Debuggery
            name  = form.cleaned_data["contact_name"]
            title = form.cleaned_data["contact_title"]
            email = form.cleaned_data["contact_email"]
            phone = form.cleaned_data["contact_phone"]
            request.session["name"]  = name
            request.session["title"] = title
            request.session["email"] = email
            request.session["phone"] = phone
            data  = [{"key":"private", "value":private},
                     {"key":"name",    "value":name},
                     {"key":"title",   "value":title},
                     {"key":"email",   "value":email},
                     {"key":"phone",   "value":phone}]

            form = PrivateParticipationForm(instance=school)
            return render_to_response("screening/private_participation.html",
                                      locals(),
                                      context_instance=RequestContext(request))
    else:                                          # If form NOT submitted...
        form = PrivateContactForm(instance=school) # ...instantiate empty form

    return render_to_response("screening/private_contact.html", 
                              {"form":form, "school":school},
                              context_instance=RequestContext(request))

def private_participation(request):
    private = request.session["entity"]
    school  = Private_School.objects.get(id=private)

    if request.method == "POST": # If the form has been submitted...
        form = PrivateParticipationForm(request.POST, instance=school)
        if form.is_valid():      # ...instantiate w/ posted data & check for validity
            school.visited = True
            school.save()

            # Debuggery
            participation = form.cleaned_data["participation"]
            request.session["participation"] = participation
            data = [{"key":"participation", "value":participation}]

            return render_to_response("screening/thanks.html", locals())
    else:                                                # If form NOT submitted...
        form = PrivateParticipationForm(instance=school) # ...instantiate empty form

    return render_to_response("screening/private_participation.html", 
                              {"form":form, "school":school},
                              context_instance=RequestContext(request))

def track(request):
    user = request.user
    if user.is_authenticated():
        agencies = Agency.objects.filter(visited=True).order_by("state","agency_name")
        privates = Private_School.objects.filter(visited=True).order_by("state","school_name")

        return render_to_response("screening/track.html", locals())
    else:
        return render_to_response("screening/index.html")

def track_agency(request,lea):
    agency  = get_object_or_404(Agency, pk=lea)
    schools = Public_School.objects.filter(agency=lea).order_by("school_name")
    schools = schools.filter(students_deaf__gte=0)
    return render_to_response("screening/public_report.html", locals())

def track_private(request,private):
    school =  get_object_or_404(Private_School, pk=private)
    return render_to_response("screening/private_report.html", locals())

