# Models for the Preliminary Screening Survey for the Supplemental
# Survey of the Annual Survey. What a mouthful.
#
# Last modified by Kevin Cole <kjcole@gallaudet.edu>
#

from django.db import models

PARTICIPATION = ((2,"Yes, please do so."),
                 (1,"Yes, but someone else must approve participation."),
                 (0,"No, I respectfully decline."))

class State (models.Model):
    usps = models.CharField(max_length=2, editable=False)
    name = models.CharField(max_length=50, editable=False)

    def __unicode__(self):
        return "%2s (%s)" % (self.usps, self.name)

    class Meta:
        ordering = ["usps"]

class Agency (models.Model):
    id               = models.IntegerField(primary_key=True, editable=False)
    password         = models.CharField(max_length=8, editable=False)
    changed          = models.DateTimeField("last changed", auto_now=True, editable=False)
    in_annual_survey = models.BooleanField("in Annual Survey?", editable=False)
    in_sample        = models.BooleanField("in sample?", editable=False)
    as_contact       = models.BooleanField("contacted re AS?", editable=False)
    frequency        = models.PositiveSmallIntegerField(editable=False)
    agency_name      = models.CharField(max_length=60, editable=False)
    street           = models.CharField(max_length=30, editable=False)
    city             = models.CharField(max_length=22, editable=False)
    county           = models.CharField(max_length=30, editable=False)
    state            = models.CharField(max_length=2, editable=False)
    zip              = models.PositiveIntegerField(editable=False)
    zip4             = models.PositiveSmallIntegerField(null=True, editable=False)
    phone            = models.CharField(max_length=14, editable=False)
    enrollment       = models.IntegerField(editable=False)
    agency_type      = models.PositiveSmallIntegerField(editable=False)
    lowest           = models.CharField(max_length=2, editable=False)
    highest          = models.CharField(max_length=2, editable=False)
    school_count     = models.PositiveIntegerField(editable=False)
    students_deaf    = models.PositiveSmallIntegerField(null=True)
    is_source_iep    = models.NullBooleanField("Individual Education Program (IEP)")
    is_source_ifsp   = models.NullBooleanField("Individual Family Service Plan (IFSP)")
    is_source_504    = models.NullBooleanField("Section 504 Plan")
    is_source_ada    = models.NullBooleanField("Americans with Disabilities Act (ADA) Title II Plan")
    is_source_other  = models.NullBooleanField("other source of identification")
    contact_name     = models.CharField(max_length=255,  null=True)
    contact_title    = models.CharField(max_length=255,  null=True)
    contact_email    = models.EmailField(max_length=255, null=True)
    contact_phone    = models.CharField(max_length=255,  null=True)
    participation    = models.PositiveSmallIntegerField(null=True,
                                                        choices=PARTICIPATION)
    visited          = models.BooleanField("Form visited?", editable=False)

    def __unicode__(self):
        return self.agency_name

    class Meta:
        ordering = ["state", "agency_name"]
        get_latest_by = "changed"
        verbose_name_plural = "agencies"

class Private_School (models.Model):
    id                = models.CharField(max_length=8, primary_key=True, editable=False)
    password          = models.CharField(max_length=8, editable=False)
    changed           = models.DateTimeField("last changed", auto_now=True, editable=False)
    in_annual_survey  = models.BooleanField("in Annual Survey?", editable=False)
    in_sample         = models.BooleanField("in sample?", editable=False)
    as_contact        = models.BooleanField("contacted re AS?", editable=False)
    frequency         = models.PositiveSmallIntegerField(editable=False)
    school_name       = models.CharField(max_length=50, editable=False)
    street            = models.CharField(max_length=40, null=True, editable=False)
    city              = models.CharField(max_length=22, editable=False)
    fips_county_code  = models.PositiveSmallIntegerField(editable=False)
    county            = models.CharField(max_length=20, editable=False)
    fips_state_code   = models.PositiveSmallIntegerField(editable=False)
    state             = models.CharField(max_length=2, editable=False)
    zip               = models.PositiveIntegerField(editable=False)
    zip4              = models.PositiveSmallIntegerField(null=True, editable=False)
    phone             = models.CharField(max_length=10, null=True, editable=False)
    students_total    = models.PositiveSmallIntegerField(null=True, editable=False)
    students_pk       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_k        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_01       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_02       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_03       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_04       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_05       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_06       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_07       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_08       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_09       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_10       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_11       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_12       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_ug       = models.PositiveSmallIntegerField(null=True, editable=False)
    students_native   = models.PositiveSmallIntegerField(null=True, editable=False)
    students_asian    = models.PositiveSmallIntegerField(null=True, editable=False)
    students_latino   = models.PositiveSmallIntegerField(null=True, editable=False)
    students_black    = models.PositiveSmallIntegerField(null=True, editable=False)
    students_white    = models.PositiveSmallIntegerField(null=True, editable=False)
    students_pacific  = models.PositiveSmallIntegerField(null=True, editable=False)
    students_multi    = models.PositiveSmallIntegerField(null=True, editable=False)
    students_deaf     = models.PositiveSmallIntegerField(null=True)
    is_source_iep     = models.NullBooleanField("Individual Education Program (IEP)")
    is_source_ifsp    = models.NullBooleanField("Individual Family Service Plan (IFSP)")
    is_source_504     = models.NullBooleanField("Section 504 Plan")
    is_source_ada     = models.NullBooleanField("Americans with Disabilities Act (ADA) Title II Plan")
    is_source_other   = models.NullBooleanField("other source of identification")
    contact_name      = models.CharField(max_length=255,  null=True)
    contact_title     = models.CharField(max_length=255,  null=True)
    contact_email     = models.EmailField(max_length=255, null=True)
    contact_phone     = models.CharField(max_length=255,  null=True)
    participation     = models.PositiveSmallIntegerField(null=True,
                                                         choices=PARTICIPATION)
    visited           = models.BooleanField("Form visited?", editable=False)

    def __unicode__(self):
        return self.school_name

    class Meta:
        ordering = ["state", "school_name"]
        get_latest_by = "changed"

class Public_School (models.Model):
    id                 = models.BigIntegerField(primary_key=True, editable=False)
    changed            = models.DateTimeField("last changed", auto_now=True, editable=False)
    in_annual_survey   = models.BooleanField("in Annual Survey?", editable=False)
    agency             = models.ForeignKey(Agency, editable=False)
    agency_name        = models.CharField(max_length=60, editable=False)
    school_id          = models.PositiveIntegerField(editable=False)
    school_name        = models.CharField(max_length=50, editable=False)
    street             = models.CharField(max_length=30, editable=False)
    city               = models.CharField(max_length=25, editable=False)
    county_code        = models.PositiveIntegerField(null=True, editable=False)
    county             = models.CharField(max_length=33, null=True, editable=False)
    state              = models.CharField(max_length=2, editable=False)
    zip                = models.PositiveIntegerField(editable=False)
    zip4               = models.PositiveSmallIntegerField(null=True, editable=False)
    phone              = models.CharField(max_length=10, null=True, editable=False)
    students_total     = models.PositiveSmallIntegerField(null=True, editable=False)
    students_pk        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_k         = models.PositiveSmallIntegerField(null=True, editable=False)
    students_01        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_02        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_03        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_04        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_05        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_06        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_07        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_08        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_09        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_10        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_11        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_12        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_ug        = models.PositiveSmallIntegerField(null=True, editable=False)
    students_native    = models.PositiveSmallIntegerField(null=True, editable=False)
    students_asian     = models.PositiveSmallIntegerField(null=True, editable=False)
    students_latino    = models.PositiveSmallIntegerField(null=True, editable=False)
    students_black     = models.PositiveSmallIntegerField(null=True, editable=False)
    students_white     = models.PositiveSmallIntegerField(null=True, editable=False)
    students_pacific   = models.PositiveSmallIntegerField(null=True, editable=False)
    students_multi     = models.PositiveSmallIntegerField(null=True, editable=False)
    total_ethnic       = models.PositiveSmallIntegerField(null=True, editable=False)
    free_lunch         = models.PositiveSmallIntegerField(null=True, editable=False)
    reduced_lunch      = models.PositiveSmallIntegerField(null=True, editable=False)
    total_lunch        = models.PositiveSmallIntegerField(null=True, editable=False)
    state_school_id    = models.CharField(max_length=20, editable=False)
    state_agency_id    = models.CharField(max_length=14, editable=False)
    congressional_code = models.PositiveIntegerField(null=True, editable=False)
    students_deaf      = models.PositiveSmallIntegerField(null=True)
    is_source_iep      = models.NullBooleanField("Individual Education Program (IEP)")
    is_source_ifsp     = models.NullBooleanField("Individual Family Service Plan (IFSP)")
    is_source_504      = models.NullBooleanField("Section 504 Plan")
    is_source_ada      = models.NullBooleanField("Americans with Disabilities Act (ADA) Title II Plan")
    is_source_other    = models.NullBooleanField("other source of identification")
    contact_name       = models.CharField(max_length=255,  null=True)
    contact_title      = models.CharField(max_length=255,  null=True)
    contact_email      = models.EmailField(max_length=255, null=True)
    contact_phone      = models.CharField(max_length=255,  null=True)
    participation      = models.PositiveSmallIntegerField(null=True,
                                                          choices=PARTICIPATION)
    visited            = models.BooleanField("Form visited?", editable=False)

    def __unicode__(self):
        return self.school_name

    class Meta:
        ordering = ["state", "school_name"]
        get_latest_by = "changed"
