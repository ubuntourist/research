# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2012.05.24
#
# Extracted, sort of, from models.py (and from mhd which begat ragu).
#

from django.contrib             import admin
from resources.screening.models import Agency, Private_School, Public_School

class PublicAdmin(admin.ModelAdmin):
    """PublicAdmin: display/edit Public School info"""
    actions         = None
    list_display    = ("school_name","state")
    list_filter     = ("agency", "state")
    ordering        = ("agency", "state")
    readonly_fields = ("id", "changed", "in_annual_survey", "agency",
                       "agency_name", "school_id", "school_name", "street",
                       "city", "county_code", "county", "state", "zip", "zip4",
                       "phone", "students_total", "students_pk", "students_k",
                       "students_01", "students_02", "students_03",
                       "students_04", "students_05", "students_06",
                       "students_07", "students_08", "students_09",
                       "students_10", "students_11", "students_12",
                       "students_ug",
                       "students_native", "students_asian", "students_latino",
                       "students_black",  "students_white",  "students_pacific",
                       "students_multi",  "total_ethnic",
                       "free_lunch", "reduced_lunch", "total_lunch",
                       "state_school_id", "state_agency_id", "congressional_code",
                       "students_deaf",
                       "is_source_iep", "is_source_ifsp", "is_source_504",
                       "is_source_ada", "is_source_other",
                       "contact_name", "contact_title", "contact_email",
                       "contact_phone", "participation", "visited",)
    search_fields   = ("school_name", "agency")
    fieldsets = (("Data from CCD databases", 
                  {"classes": ("collapse",),
                   "fields":  ("id", ("agency_name", "agency",),
                               ("school_name", "school_id",),
                               "street",
                               ("city", "state", "zip", "zip4",),
                               ("county", "county_code",),
                               "phone", 
                               "students_total", "students_pk", "students_k",
                               "students_01", "students_02", "students_03",
                               "students_04", "students_05", "students_06",
                               "students_07", "students_08", "students_09",
                               "students_10", "students_11", "students_12",
                               "students_ug",
                               "students_native", "students_asian",
                               "students_latino", "students_black",
                               "students_white",  "students_pacific",
                               "students_multi",  "total_ethnic",
                               "free_lunch", "reduced_lunch", "total_lunch",
                               "state_school_id", "state_agency_id",
                               "congressional_code",)}),

                 ("Sampling criteria",
                  {"classes": ("collapse",),
                   "fields":  ("changed", "in_annual_survey",)}),

                 ("Survey data",
                  {"fields": ("students_deaf", "visited",)}),)

class PublicInline(admin.TabularInline):
    model           = Public_School
    max_num         = 1
    actions         = None
    ordering        = ("school_name",)
    readonly_fields = ("students_deaf", "visited",)
    fields          = ("students_deaf", "visited",)

class AgencyAdmin(admin.ModelAdmin):
    """AgencyAdmin: display/edit Local Education Agency info"""
    actions         = None
    list_display    = ("agency_name","state")
    list_filter     = ("visited", "participation", "state",)
    ordering        = ("state",)
    readonly_fields = ("id", "password", "changed", "in_annual_survey",
                       "in_sample", "as_contact", "frequency", "agency_name",
                       "street", "city", "county", "state", "zip", "zip4",
                       "phone", "enrollment", "agency_type", "lowest",
                       "highest", "school_count", "students_deaf",
                       "is_source_iep", "is_source_ifsp", "is_source_504",
                       "is_source_ada", "is_source_other", "contact_name",
                       "contact_title", "contact_email", "contact_phone",
                       "participation", "visited",)
    search_fields   = ("agency_name",)
    fieldsets       = (("Data from CCD databases", 
                        {"classes": ("collapse",),
                         "fields":  ("id","agency_name","street",
                                     ("city", "state", "zip", "zip4",), "county",
                                     "phone", "enrollment", "agency_type",
                                     "lowest", "highest", "school_count",)}),
    
                       ("Sampling criteria",
                        {"classes": ("collapse",),
                         "fields":  ("password", "changed", "in_annual_survey",
                                     "in_sample", "as_contact", "frequency",)}),
    
                       ("Survey data",
                        {"fields":  ("is_source_iep", "is_source_ifsp",
                                     "is_source_504", "is_source_ada", 
                                     "is_source_other",
                                     "contact_name",  "contact_title",
                                     "contact_email", "contact_phone",
                                     "participation", "visited",)}))
    inlines = (PublicInline,)

class PrivateAdmin(admin.ModelAdmin):
    """PrivateAdmin: display/edit Private School info"""
    actions         = None
    list_display    = ("school_name","state")
    list_filter     = ("visited", "participation", "state",)
    ordering        = ("state",)
    readonly_fields = ("id", "password", "changed", "in_annual_survey",
                       "in_sample", "as_contact", "frequency", "school_name",
                       "street", "city", "fips_county_code", "county",
                       "fips_state_code", "state", "zip", "zip4", "phone",
                       "students_total", "students_pk", "students_k",
                       "students_01", "students_02", "students_03",
                       "students_04", "students_05", "students_06",
                       "students_07", "students_08", "students_09",
                       "students_10", "students_11", "students_12",
                       "students_ug",
                       "students_native", "students_asian", "students_latino",
                       "students_black",  "students_white", "students_pacific",
                       "students_multi",
                       "students_deaf",
                       "is_source_iep", "is_source_ifsp", "is_source_504",
                       "is_source_ada", "is_source_other",
                       "contact_name", "contact_title", "contact_email",
                       "contact_phone", "participation", "visited",)
    search_fields   = ("school_name",)
    fieldsets = (("Data from CCD databases", 
                  {"classes": ("collapse",),
                   "fields":  ("id", "school_name", "street",
                               ("city", "state", "zip", "zip4",),
                               "county", "fips_county_code",
                               "fips_state_code", "phone", "students_total",
                               "students_pk", "students_k",
                               "students_01", "students_02", "students_03",
                               "students_04", "students_05", "students_06",
                               "students_07", "students_08", "students_09",
                               "students_10", "students_11", "students_12",
                               "students_ug",
                               "students_native", "students_asian",
                               "students_latino", "students_black",
                               "students_white",  "students_pacific",
                               "students_multi",)}),

                 ("Sampling criteria",
                  {"classes": ("collapse",),
                   "fields":  ("password", "changed", "in_annual_survey",
                               "in_sample", "as_contact", "frequency",)}),

                 ("Survey data",
                  {"fields": ("students_deaf", "is_source_iep",
                              "is_source_ifsp", "is_source_504",
                              "is_source_ada",  "is_source_other",
                              "contact_name",  "contact_title",
                              "contact_email", "contact_phone",
                              "participation", "visited",)}))

admin.site.register(Agency,         AgencyAdmin)
admin.site.register(Public_School,  PublicAdmin)
admin.site.register(Private_School, PrivateAdmin)
