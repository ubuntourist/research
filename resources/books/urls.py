# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2008.11.30
#

from django.conf.urls.defaults import *

from resources.books.views import search, contact, add_publisher

urlpatterns = patterns('',
    (r'^search/$', search),
    (r'^contact/$', contact),
    (r'^addpub/$', add_publisher),
)
