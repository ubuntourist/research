# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2008.07.20
#
# Extracted, sort of, from models.py

# Everything's commented out.  Uncommenting it will add the generic
# admin interfaces for the Book, Author and Publisher tables.

#from django.contrib import admin
#from resources.books.models import Book, Author, Publisher

#admin.site.register(Book)
#admin.site.register(Author)
#admin.site.register(Publisher)
