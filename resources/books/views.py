# Books views from Chapter 7

from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.mail import send_mail
from resources.books.models import Book
from resources.books.forms import ContactForm, PublisherForm

def search(request):
    query = request.GET.get("q", "")
    if query:
        qset = (
            Q(title__icontains=query) |
            Q(authors__first_name__icontains=query) |
            Q(authors__last_name__icontains=query)
        )
        results = Book.objects.filter(qset).distinct()
    else:
        results = []
    return render_to_response("books/search.html", 
                              {"results": results,
                               "query":   query})

def contact(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            topic = form.cleaned_data["topic"]
            message = form.cleaned_data["message"]
            sender = form.cleaned_data["sender"]
            send_mail("Feedback from your site, topic: %s" % (topic,),
                      message, sender, ["kjcole@gri.gallaudet.edu"])
            return HttpResponseRedirect("/books/contact/thanks/")
    else:
        form = ContactForm()
    return render_to_response("books/contact.html", {"form": form})

def add_publisher(request):
    if request.method == "POST":
        form = PublisherForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/books/add_publisher/thanks")
    else:
        form = PublisherForm()
    return render_to_response("books/add_publisher.html", {"form": form})
