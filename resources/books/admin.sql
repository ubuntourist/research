-- Generated from "pg_dump -s ... django" (2008.04.07)
-- These are the authorization and adminstration tables for django

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;

ALTER TABLE auth_group ALTER COLUMN id 
    SET DEFAULT nextval('auth_group_id_seq'::regclass);

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key
    UNIQUE (name);

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey
    PRIMARY KEY (id);

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER SEQUENCE auth_group_permissions_id_seq
    OWNED BY auth_group_permissions.id;

ALTER TABLE auth_group_permissions ALTER COLUMN id
    SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_key
    UNIQUE (group_id, permission_id);

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey
    PRIMARY KEY (id);

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_fkey
    FOREIGN KEY (group_id) REFERENCES auth_group(id)
    DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey
    FOREIGN KEY (permission_id) REFERENCES auth_permission(id)
    DEFERRABLE INITIALLY DEFERRED;

CREATE TABLE auth_message (
    id integer NOT NULL,
    user_id integer NOT NULL,
    message text NOT NULL
);

CREATE SEQUENCE auth_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER SEQUENCE auth_message_id_seq
    OWNED BY auth_message.id;

ALTER TABLE auth_message ALTER COLUMN id
    SET DEFAULT nextval('auth_message_id_seq'::regclass);

ALTER TABLE ONLY auth_message
    ADD CONSTRAINT auth_message_pkey
    PRIMARY KEY (id);

CREATE INDEX auth_message_user_id ON auth_message
    USING btree (user_id);

ALTER TABLE ONLY auth_message
    ADD CONSTRAINT user_id_refs_id_650f49a6
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    DEFERRABLE INITIALLY DEFERRED;

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);

CREATE SEQUENCE auth_permission_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER SEQUENCE auth_permission_id_seq
    OWNED BY auth_permission.id;

ALTER TABLE auth_permission ALTER COLUMN id
    SET DEFAULT nextval('auth_permission_id_seq'::regclass);

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_key
    UNIQUE (content_type_id, codename);

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey
    PRIMARY KEY (id);

CREATE INDEX auth_permission_content_type_id ON auth_permission
    USING btree (content_type_id);

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT content_type_id_refs_id_728de91f
    FOREIGN KEY (content_type_id) REFERENCES django_content_type(id)
    DEFERRABLE INITIALLY DEFERRED;

CREATE TABLE auth_user (
    id integer NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    "password" character varying(128) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    is_superuser boolean NOT NULL,
    last_login timestamp with time zone NOT NULL,
    date_joined timestamp with time zone NOT NULL
);

CREATE SEQUENCE auth_user_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER SEQUENCE auth_user_id_seq
    OWNED BY auth_user.id;

ALTER TABLE auth_user ALTER COLUMN id
    SET DEFAULT nextval('auth_user_id_seq'::regclass);

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey
    PRIMARY KEY (id);

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key
    UNIQUE (username);

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER SEQUENCE auth_user_groups_id_seq
    OWNED BY auth_user_groups.id;

ALTER TABLE auth_user_groups ALTER COLUMN id
    SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey
    PRIMARY KEY (id);

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_key
    UNIQUE (user_id, group_id);

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_fkey
    FOREIGN KEY (group_id) REFERENCES auth_group(id)
    DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_fkey
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    DEFERRABLE INITIALLY DEFERRED;

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER SEQUENCE auth_user_user_permissions_id_seq
    OWNED BY auth_user_user_permissions.id;

ALTER TABLE auth_user_user_permissions ALTER COLUMN id
    SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey
    PRIMARY KEY (id);

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_key
    UNIQUE (user_id, permission_id);

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_permission_id_fkey
    FOREIGN KEY (permission_id) REFERENCES auth_permission(id)
    DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_fkey
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    DEFERRABLE INITIALLY DEFERRED;

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER SEQUENCE django_admin_log_id_seq
    OWNED BY django_admin_log.id;

ALTER TABLE django_admin_log ALTER COLUMN id
    SET DEFAULT nextval('django_admin_log_id_seq'::regclass);

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey
    PRIMARY KEY (id);

CREATE INDEX django_admin_log_content_type_id ON django_admin_log
    USING btree (content_type_id);

CREATE INDEX django_admin_log_user_id ON django_admin_log
    USING btree (user_id);

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);

CREATE SEQUENCE django_content_type_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER SEQUENCE django_content_type_id_seq
    OWNED BY django_content_type.id;

ALTER TABLE django_content_type ALTER COLUMN id
    SET DEFAULT nextval('django_content_type_id_seq'::regclass);

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_key
    UNIQUE (app_label, model);

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey
    PRIMARY KEY (id);

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey
    PRIMARY KEY (session_key);

CREATE TABLE django_site (
    id integer NOT NULL,
    "domain" character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);

CREATE SEQUENCE django_site_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

ALTER SEQUENCE django_site_id_seq
    OWNED BY django_site.id;

ALTER TABLE django_site ALTER COLUMN id
    SET DEFAULT nextval('django_site_id_seq'::regclass);

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey
    PRIMARY KEY (id);

