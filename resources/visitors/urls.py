# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2014.03.06
#
# 2013.07.16 KJC - Switching to class-based generic views from function-based
# 2014.03.06 KJC - index is now oldindex and testicle is now index.
#

from django.conf.urls.defaults import *
from django.views.generic.base import TemplateView
from resources.visitors.views  import *

urlpatterns = patterns("",
    (r"^$",                          index),
#   (r"^test/$",                     testicle),
    (r"^api/feed/$",                 feed),
    (r"^bios/(\d{1,6})/$",           bios),
    (r"^thanks/(\d{1,6})/$",         thanks),
)
