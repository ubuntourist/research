#!/usr/bin/env python
# Copied by Kevin Cole <kjcole@gallaudet.edu> 2013.11.10
# from http://www.masnun.com/2010/01/01/sending-mail-via-postfix-a-perfect-python-example.html
#

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import os

def sendMail(sender, to, subject, text, files=[], server="localhost"):
    assert type(to)==list
    assert type(files)==list

    msg = MIMEMultipart()
    msg["From"] = sender
    msg["To"] = COMMASPACE.join(to)
    msg["Date"] = formatdate(localtime=True)
    msg["Subject"] = subject

    msg.attach( MIMEText(text) )

    for file in files:
        part = MIMEBase("application", "octet-stream")
        part.set_payload( open(file,"rb").read() )
        Encoders.encode_base64(part)
        part.add_header("Content-Disposition", "attachment; filename=\"%s\""
                       % os.path.basename(file))
        msg.attach(part)

    smtp = smtplib.SMTP(server)
    smtp.sendmail(sender, to, msg.as_string() )
    smtp.close()

# Example:
if (__name__ == "__main__"):
    sendMail("Django Unchained <gri.gateway@gallaudet.edu>",
             ["Kevin Cole <dc.loco@gmail.com>",
              "Kevin Cole <kevin.cole@gallaudet.edu>"],
             "Hello from Python!",
             "Heya buddy! Say hello to Python! :)",
              ["sendit.py","outline.html"])
