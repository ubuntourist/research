# Admin interface for the International Visitors Request Form
# Last modified by Kevin Cole <kjcole@gallaudet.edu> 2013.12.01
#
# Extracted, sort of, from models.py
#

from django.contrib            import admin
from django.forms.widgets      import TextInput
from django.db                 import models
from resources.visitors.models import Language, Country, Inquiry, Visitor

class LanguageAdmin(admin.ModelAdmin):
    """Languages of the world"""
    actions         = None
    list_display    = ("name",)
    readonly_fields = ("id","iso2","iso3","name",)
    search_fields   = ("name",)

class CountryAdmin(admin.ModelAdmin):
    """Countries of the world"""
    actions         = None
    list_display    = ("name",)
    readonly_fields = ("id","iso2","iso3","name",)
    search_fields   = ("name",)

class VisitorInline(admin.TabularInline):
    model = Visitor
    extra = 0
#   readonly_fields = ("surname", "givenname", "middlename", 
#                      "occupation", "gender", "dob", 
#                      "citizenship", "native", "residence",)

class CountriesInline(admin.TabularInline):
    Inquiry.countries.through.__unicode__ = lambda x: "Countries of visitors"
    model = Inquiry.countries.through
    extra = 0
    verbose_name = "country"
    verbose_name_plural = "countries"

class InquiryAdmin(admin.ModelAdmin):
    """Inquiry general information"""
    inlines = [CountriesInline, VisitorInline,]
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'80'})}
    }
    list_display    = ("arr_date",)
    list_filter     = ("arr_date","arr_time","dep_date","dep_time",
                       "requester","organization","letters",)
    ordering        = ("arr_date",)
    search_fields   = ("arr_date",)
    fieldsets = (("Arrival and Departure", 
                  {"classes": ("collapse",),
                   "fields":  (("arr_date", "arr_time",),
                               ("dep_date", "dep_time",),)}),

                 ("Requester",
                  {"classes": ("collapse",),
                   "fields":  ("requester","organization","email","phone",)}),

                 ("Purpose of visit",
                  {"classes": ("collapse",),
                   "fields":  ("purpose",)}),

                 ("Number of visitors",
                  {"classes": ("collapse",),
                   "fields":  ("visitors","signers","speakers",)}),

                 ("Needs of visitors",
                  {"classes": ("collapse",),
                   "fields":  ("letters",)}),)

class VisitorAdmin(admin.ModelAdmin):
    """Visitor information"""
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'80'})}
    }
    list_display    = ("surname","givenname",)
    list_filter     = ("citizenship",)
    ordering        = ("citizenship","surname","givenname",)
    search_fields   = ("citizenship","surname","givenname",)

class InquiriesAdmin(admin.ModelAdmin):
    inlines = [CountriesInline,]
    exclude = ("countries",)

##############################################################################

# This is how we sort of got multiple schools within a school district.
# It should be used to get multiple countries within a request...
#
#class PublicInline(admin.TabularInline):
#    model           = Public_School
#    max_num         = 1
#    actions         = None
#    ordering        = ("school_name",)
#    readonly_fields = ("students_deaf", "visited",)
#    fields          = ("students_deaf", "visited",)

#class AgencyAdmin(admin.ModelAdmin):
#    """AgencyAdmin: display/edit Local Education Agency info"""
#    inlines = (PublicInline,)

admin.site.register(Language, LanguageAdmin)
admin.site.register(Country,  CountryAdmin)
admin.site.register(Inquiry,  InquiryAdmin)
admin.site.register(Visitor,  VisitorAdmin)
