CREATE TABLE inquiry(
  id             SERIAL      PRIMARY KEY,
  arr_date       DATE        NOT NULL,
  arr_time       TIME        NOT NULL,
  dep_date       DATE        NOT NULL,
  dep_time       TIME        NOT NULL,
  fullname       VARCHAR(80) NOT NULL,
  organization   VARCHAR(80) NOT NULL,
  email          VARCHAR(80) NOT NULL,
  phone          VARCHAR(80) NOT NULL,
  visitors       INTEGER     NOT NULL,
  speakers       INTEGER     NOT NULL,
  signers        INTEGER     NOT NULL,
  purpose        TEXT        NOT NULL,
  interpreter    BOOLEAN     NOT NULL,
  letters        BOOLEAN     NOT NULL,
);

-- CREATE TABLE languages (
-- many2many
-- );

-- CREATE TABLE countries (
-- many2many
-- );
