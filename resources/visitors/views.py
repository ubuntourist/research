# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2014.03.06
#
# This contains the functions to which URL's are mapped by 
# urls.py.
#
# 2014.03.06 KJC - index is now oldindex and testicle is now index.
#

import random              # DEBUG

import json                # JSON for jQuery AJAX autocomplete
from   datetime            import timedelta, datetime, date

from django.http           import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts      import render_to_response, get_object_or_404
from django.db.models      import Q
from django.template       import RequestContext
from django.forms.models   import inlineformset_factory
from django.contrib.auth   import authenticate, login
from django.core.paginator import *

from django.core.mail      import send_mail, EmailMultiAlternatives
from django.template       import Context, loader

from resources.visitors.models import Language, Country, Inquiry, Visitor
from resources.visitors.forms  import InquiryForm, VisitorForm

#from resources.screening.forms import AgencySourcesForm,        \
#                                      AgencyContactForm,        \
#                                      AgencyParticipationForm,  \
#                                      PrivateCountsForm,        \
#                                      PrivateSourcesForm,       \
#                                      PrivateContactForm,       \
#                                      PrivateParticipationForm, \
#                                      PublicCountsForm

months    = ("January","February","March","April","May","June","July",
             "August","September","October","November","December",)
instant   = datetime.now()
today     = date.today()
#yesterday = today - timedelta(1)
#year_ago  = today - timedelta(365)
#thisyear  = today.year
#lastyear  = thisyear - 1
#fiscal    = today.year + (today.month // 10)    # Now in fiscal year "fiscal"
#years     = range(thisyear-50,thisyear+10)
#lastfy    = (date(lastyear,10,1), date(lastyear,9,30)) # Last fiscal year range

##############################################################################
#
# The following function was copied on 2012.08.10 from
#
#   http://www.redrobotstudios.com/blog/2009/02/18/securing-django-with-ssl/
#
# Securing individual pages...  To use, add "@secure_required"
# immediately above any function that requires an https connection.
#
# Note: The original version of this function, on the web site above,
# checks for HTTPS_SUPPORT = True in settings.py.  However, could not
# find such a setting anywhere in the Debian package nor in the
# official Django documentation. So this check has been removed here.
#
##############################################################################

def secure_required(view_func):
    """Decorator makes sure URL is accessed over https."""
    def _wrapped_view_func(request, *args, **kwargs):
        if not request.is_secure():
            request_url = request.build_absolute_uri(request.get_full_path())
            secure_url  = request_url.replace('http://', 'https://')
            return HttpResponseRedirect(secure_url)
        return view_func(request, *args, **kwargs)
    return _wrapped_view_func

# End copied code

##############################################################################

def feed(request):
    """Auto-complete country names"""
    if request.is_ajax():
        term = request.GET.get("term","")
        countries = Country.objects.filter(name__istartswith=term)
        if countries.count() == 0:
            countries = Country.objects.filter(name__icontains=term)
        results = []
        for country in countries:
            country_json = {}
            country_json["label"] = country.name
            country_json["value"] = country.name
            country_json["id"]    = country.id
            results.append(country_json)
        data = json.dumps(results)         # Convert to JSON string
    else:                                  # No results returned!
        data = "fail"                      # ERROR!
    mimetype = "application/json"
    return HttpResponse(data, mimetype)    # Send JSON back to the web page

def oldindex(request):
    countries = Country.objects.all()
    if request.method == "POST":
        inquiry_form = InquiryForm(request.POST)
        if inquiry_form.is_valid():
            inquiry = inquiry_form.save()
#           return HttpResponseRedirect("/resources/visitors/bios/%s" % (inquiry.id,))
            origins = int(request.POST["origins"])
            for origin in range(1,origins+1):
                try:
                    country_id = int(request.POST["country.%s" % origin])
                    if country_id:
                        country = Country.objects.get(pk=country_id)
                        inquiry.countries.add(country)
                except ValueError:
                    pass
            return HttpResponseRedirect("/resources/visitors/thanks/%s/" % (inquiry.id,))
        else:
           return render_to_response("visitors/index.html", locals(),
                              context_instance=RequestContext(request))
    else:
        inquiry_form = InquiryForm()
    return render_to_response("visitors/index.html", locals(),
                              context_instance=RequestContext(request))

##############################################################################

def thanks(request,inquiry_id):
    inquiry   = get_object_or_404(Inquiry, pk=int(inquiry_id))
    countries = inquiry.countries.all().order_by("id")
    visitors  = Visitor.objects.filter(inquiry=inquiry)

    today     = date.today()
    subject   = "Re: Visit Inquiry - "
    subject  += countries[0].name.upper() + " ("
    subject  += inquiry.arr_date.strftime("%B %d, %Y") + " to "
    subject  += inquiry.dep_date.strftime("%B %d, %Y")  + ")"
    sender    = "International Visitor Form <gri.gateway@gallaudet.edu>"
    to        = ["%s <%s>" % (inquiry.requester,inquiry.email,),]
    bcc       = ["intl.visit@gallaudet.edu",]
    extras    = {"Reply-To": "intl.visit@gallaudet.edu",}
#                "MIME-Version": "1.0",
#                "Content-type": "text/html; charset=iso-8859-1",}
    body      = "Your request has been received."
    filler    = Context(locals())
    html      = loader.get_template("visitors/deliver.html")
    msg = EmailMultiAlternatives(subject, body, sender, to, bcc,
                                 headers=extras)
    msg.attach_alternative(html.render(filler), "text/html")
##  msg.attach("save_as.png", png_raw_data, "image/png")
#   msg.attach_file("/var/www/html/images/logos/Gallaudet-750.png",
#                   "image/png")
    html = loader.get_template("visitors/receipt.html")
    msg.attach("receipt.html", html.render(filler), "text/html")
    SHIFT     = {1:"Morning", 2:"Midday", 3:"Evening"}
    NEEDED    = {True:"Needed", False:""}
    tsv       = "Countries\tArrival\tDeparture\tRequester\tOrganization\t"
    tsv      += "Email\tPhone\tVisitors\tSpeakers\tSigners\tLetters\n"
    tsv      += "%s\t"         % (inquiry.countries.all()[0],)
    tsv      += "%s %s\t"      % (inquiry.arr_date, SHIFT[inquiry.arr_time],)
    tsv      += "%s %s\t"      % (inquiry.dep_date, SHIFT[inquiry.dep_time],)
    tsv      += "%s\t%s\t"     % (inquiry.requester, inquiry.organization,)
    tsv      += "%s\t%s\t"     % (inquiry.email, inquiry.phone,)
    tsv      += "%s\t%s\t%s\t" % (inquiry.visitors, inquiry.speakers, inquiry.signers,)
    tsv      += "%s\n"         % (NEEDED[inquiry.letters],)
    if len(inquiry.countries.all()) > 1:
        for country in range(1,len(inquiry.countries.all())):
            tsv += "%s\n"      % (inquiry.countries.all()[country],)
    if inquiry.letters:
        tsv  += "\n\tLast Name\tFirst Name\tMiddle Name\tOccupation\tGender\t"
        tsv  += "DOB\tCitizenship\tBirth\tResidence\n"
        for visitor in visitors:
            tsv += "\t%s\t"   % (visitor.surname,)
            tsv += "%s\t"     % (visitor.givenname,)
            tsv += "%s\t"     % (visitor.middlename,)   
            tsv += "%s\t"     % (visitor.occupation,)
            tsv += "%s\t%s\t" % (visitor.gender, visitor.dob,)
            tsv += "%s\t"     % (visitor.citizenship,)
            tsv += "%s\t"     % (visitor.native,)
            tsv += "%s\n"     % (visitor.residence,)
    msg.attach("request.tsv", tsv, "text/tab-separated-values")
    msg.send()
    return render_to_response("visitors/thanks.html", locals())

##############################################################################

def bios(request,inquiry_id):
    countries = Country.objects.all()
    inquiry   = get_object_or_404(Inquiry, pk=int(inquiry_id))
    VisitorFormset = inlineformset_factory(Inquiry, Visitor,
                                           form=VisitorForm,
                                           extra=inquiry.visitors)

    if request.method == "POST":
        visitor_formset = VisitorFormset(request.POST, instance=inquiry)
        if visitor_formset.is_valid():
            visitor = visitor_formset.save()
            return HttpResponseRedirect("/resources/visitors/thanks/%s/" % (inquiry.id,))
        else:
            pass
    else:
        visitor_formset = VisitorFormset(instance=inquiry)
        visitor_forms   = Paginator(visitor_formset,1)
#       page = request.GET.get("page")
#       try:
#           visitor_form = visitor_forms.page(page)
#       except PageNotAnInteger:
#           visitor_form = visitor_forms.page(1)
#       except EmptyPage:
#           visitor_form = visitor_forms.page(visitor_forms.num_pages)
#       details = visitor_form.object_list[0]
#       prefix = details.prefix

    return render_to_response("visitors/visitors.html", locals(),
                              context_instance=RequestContext(request))

##############################################################################

def index(request):
    countries = Country.objects.all()
    if request.method == "POST":
        inquiry_form = InquiryForm(request.POST)
        if inquiry_form.is_valid():
            inquiry = inquiry_form.save()
            origins = int(request.POST["origins"])
            for origin in range(1,origins+1):
                try:
                    country_id = int(request.POST["country.%s" % origin])
                    if country_id:
                        country = Country.objects.get(pk=country_id)
                        inquiry.countries.add(country)
                except ValueError:
                    pass
            if request.POST["letters"] == "True":
                return HttpResponseRedirect("/resources/visitors/bios/%s" % (inquiry.id,))
            else:
                return HttpResponseRedirect("/resources/visitors/thanks/%s/" % (inquiry.id,))
        else:
           return render_to_response("visitors/index.html", locals(),
                              context_instance=RequestContext(request))
    else:
        inquiry_form = InquiryForm()
    return render_to_response("visitors/index.html", locals(),
                              context_instance=RequestContext(request))

##############################################################################
