CREATE TABLE visitor (
  id          SERIAL       PRIMARY KEY,
  surname     VARCHAR(80)  NOT NULL,
  givenname   VARCHAR(80)  NOT NULL,
  middlename  VARCHAR(80),
  occupation  VARCHAR(80)  NOT NULL,
  gender      CHARACTER(1) NOT NULL,
  dob         DATE         NOT NULL,
  citizenship VARCHAR(80)  NOT NULL,
  native      VARCHAR(80)  NOT NULL,
  residence   VARCHAR(80)  NOT NULL,
  passport    VARCHAR(80)
);
