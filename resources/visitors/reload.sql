ALTER TABLE public.visitors_country OWNER TO apache;
ALTER TABLE public.visitors_language OWNER TO apache;
ALTER TABLE public.visitors_language_id_seq OWNER TO apache;
ALTER SEQUENCE visitors_language_id_seq OWNED BY visitors_language.id;
SELECT pg_catalog.setval('visitors_language_id_seq', 1, false);
ALTER TABLE ONLY visitors_language ALTER COLUMN id SET DEFAULT nextval('visitors_language_id_seq'::regclass);

COPY visitors_country (id, name, iso3, iso2) FROM stdin;
4	Afghanistan	AFG	AF
8	Albania	ALB	AL
12	Algeria	DZA	DZ
16	American Samoa	ASM	AS
20	Andorra	AND	AD
24	Angola	AGO	AO
660	Anguilla	AIA	AI
10	Antarctica	ATA	AQ
28	Antigua and Barbuda	ATG	AG
32	Argentina	ARG	AR
248	&Aring;land Islands	ALA	AX
51	Armenia	ARM	AM
533	Aruba	ABW	AW
36	Australia	AUS	AU
40	Austria	AUT	AT
31	Azerbaijan	AZE	AZ
44	Bahamas	BHS	BS
48	Bahrain	BHR	BH
50	Bangladesh	BGD	BD
52	Barbados	BRB	BB
112	Belarus	BLR	BY
56	Belgium	BEL	BE
84	Belize	BLZ	BZ
204	Benin	BEN	BJ
60	Bermuda	BMU	BM
64	Bhutan	BTN	BT
68	Bolivia	BOL	BO
70	Bosnia and Herzegovina	BIH	BA
72	Botswana	BWA	BW
74	Bouvet Island	BVT	BV
76	Brazil	BRA	BR
86	British Indian Ocean Territory	IOT	IO
92	British Virgin Islands	VGB	VG
96	Brunei Darussalam	BRN	BN
100	Bulgaria	BGR	BG
854	Burkina Faso	BFA	BF
108	Burundi	BDI	BI
116	Cambodia	KHM	KH
120	Cameroon	CMR	CM
124	Canada	CAN	CA
132	Cape Verde	CPV	CV
136	Cayman Islands	CYM	KY
140	Central African Republic	CAF	CF
148	Chad	TCD	TD
152	Chile	CHL	CL
156	China	CHN	CN
162	Christmas Island	CXR	CX
384	C&ocirc;te d'Ivoire	CIV	CI
166	Cocos (Keeling) Islands	CCK	CC
170	Colombia	COL	CO
174	Comoros	COM	KM
178	Congo	COG	CG
180	Congo, Democratic Republic of the	COD	CD
184	Cook Islands	COK	CK
188	Costa Rica	CRI	CR
191	Croatia	HRV	HR
192	Cuba	CUB	CU
196	Cyprus	CYP	CY
203	Czech Republic	CZE	CZ
208	Denmark	DNK	DK
262	Djibouti	DJI	DJ
212	Dominica	DMA	DM
214	Dominican Republic	DOM	DO
218	Ecuador	ECU	EC
818	Egypt	EGY	EG
222	El Salvador	SLV	SV
226	Equatorial Guinea	GNQ	GQ
232	Eritrea	ERI	ER
233	Estonia	EST	EE
231	Ethiopia	ETH	ET
234	Faeroe Islands	FRO	FO
238	Falkland Islands (Malvinas)	FLK	FK
242	Fiji	FJI	FJ
246	Finland	FIN	FI
250	France	FRA	FR
254	French Guiana	GUF	GF
258	French Polynesia	PYF	PF
260	French Southern Territories	ATF	TF
266	Gabon	GAB	GA
270	Gambia	GMB	GM
268	Georgia	GEO	GE
276	Germany	DEU	DE
288	Ghana	GHA	GH
292	Gibraltar	GIB	GI
300	Greece	GRC	GR
304	Greenland	GRL	GL
308	Grenada	GRD	GD
312	Guadeloupe	GLP	GP
316	Guam	GUM	GU
320	Guatemala	GTM	GT
831	Guernsey	GGY	GG
624	Guinea-Bissau	GNB	GW
324	Guinea	GIN	GN
328	Guyana	GUY	GY
332	Haiti	HTI	HT
334	Heard Island and McDonald Islands	HMD	HM
336	Holy See (Vatican City)	VAT	VA
340	Honduras	HND	HN
344	Hong Kong Special Administrative Region of China	HKG	HK
348	Hungary	HUN	HU
352	Iceland	ISL	IS
356	India	IND	IN
360	Indonesia	IDN	ID
364	Iran, Islamic Republic of	IRN	IR
368	Iraq	IRQ	IQ
372	Ireland	IRL	IE
833	Isle of Man	IMN	IM
376	Israel	ISR	IL
380	Italy	ITA	IT
388	Jamaica	JAM	JM
392	Japan	JPN	JP
832	Jersey	JEY	JE
400	Jordan	JOR	JO
398	Kazakhstan	KAZ	KZ
404	Kenya	KEN	KE
296	Kiribati	KIR	KI
408	Korea, Democratic People's Republic of	PRK	KP
410	Korea, Republic of	KOR	KR
414	Kuwait	KWT	KW
417	Kyrgyzstan	KGZ	KG
418	Lao People's Democratic Republic	LAO	LA
428	Latvia	LVA	LV
422	Lebanon	LBN	LB
426	Lesotho	LSO	LS
430	Liberia	LBR	LR
434	Libyan Arab Jamahiriya	LBY	LY
438	Liechtenstein	LIE	LI
440	Lithuania	LTU	LT
442	Luxembourg	LUX	LU
446	Macao Special Administrative Region of China	MAC	MO
807	Macedonia, The former Yugoslav Republic of	MKD	MK
450	Madagascar	MDG	MG
454	Malawi	MWI	MW
458	Malaysia	MYS	MY
462	Maldives	MDV	MV
466	Mali	MLI	ML
470	Malta	MLT	MT
584	Marshall Islands	MHL	MH
474	Martinique	MTQ	MQ
478	Mauritania	MRT	MR
480	Mauritius	MUS	MU
175	Mayotte	MYT	YT
484	Mexico	MEX	MX
583	Micronesia, Federated States of	FSM	FM
498	Moldova, Republic of	MDA	MD
492	Monaco	MCO	MC
496	Mongolia	MNG	MN
499	Montenegro	MNE	ME
500	Montserrat	MSR	MS
504	Morocco	MAR	MA
508	Mozambique	MOZ	MZ
104	Myanmar	MMR	MM
516	Namibia	NAM	NA
520	Nauru	NRU	NR
524	Nepal	NPL	NP
530	Netherlands Antilles	ANT	AN
528	Netherlands	NLD	NL
540	New Caledonia	NCL	NC
554	New Zealand	NZL	NZ
558	Nicaragua	NIC	NI
566	Nigeria	NGA	NG
562	Niger	NER	NE
570	Niue	NIU	NU
574	Norfolk Island	NFK	NF
580	Northern Mariana Islands	MNP	MP
578	Norway	NOR	NO
275	Occupied Palestinian Territory	PSE	PS
512	Oman	OMN	OM
586	Pakistan	PAK	PK
585	Palau	PLW	PW
591	Panama	PAN	PA
598	Papua New Guinea	PNG	PG
600	Paraguay	PRY	PY
604	Peru	PER	PE
608	Philippines	PHL	PH
612	Pitcairn	PCN	PN
616	Poland	POL	PL
620	Portugal	PRT	PT
630	Puerto Rico	PRI	PR
634	Qatar	QAT	QA
638	R&eacute;union	REU	RE
642	Romania	ROU	RO
643	Russian Federation	RUS	RU
646	Rwanda	RWA	RW
654	Saint Helena	SHN	SH
659	Saint Kitts and Nevis	KNA	KN
662	Saint Lucia	LCA	LC
666	Saint Pierre and Miquelon	SPM	PM
670	Saint Vincent and the Grenadines	VCT	VC
882	Samoa	WSM	WS
674	San Marino	SMR	SM
678	Sao Tome and Principe	STP	ST
682	Saudi Arabia	SAU	SA
686	Senegal	SEN	SN
688	Serbia	SRB	RS
690	Seychelles	SYC	SC
694	Sierra Leone	SLE	SL
702	Singapore	SGP	SG
703	Slovakia	SVK	SK
705	Slovenia	SVN	SI
90	Solomon Islands	SLB	SB
706	Somalia	SOM	SO
710	South Africa	ZAF	ZA
239	South Georgia and the South Sandwich Islands	SGS	GS
724	Spain	ESP	ES
144	Sri Lanka	LKA	LK
736	Sudan	SDN	SD
740	Suriname	SUR	SR
744	Svalbard and Jan Mayen Islands	SJM	SJ
748	Swaziland	SWZ	SZ
752	Sweden	SWE	SE
756	Switzerland	CHE	CH
760	Syrian Arab Republic	SYR	SY
158	Taiwan, Province of China	TWN	TW
762	Tajikistan	TJK	TJ
834	Tanzania, United Republic of	TZA	TZ
764	Thailand	THA	TH
626	Timor-Leste	TLS	TL
768	Togo	TGO	TG
772	Tokelau	TKL	TK
776	Tonga	TON	TO
780	Trinidad and Tobago	TTO	TT
788	Tunisia	TUN	TN
792	Turkey	TUR	TR
795	Turkmenistan	TKM	TM
796	Turks and Caicos Islands	TCA	TC
798	Tuvalu	TUV	TV
800	Uganda	UGA	UG
804	Ukraine	UKR	UA
784	United Arab Emirates	ARE	AE
826	United Kingdom of Great Britain and Northern Ireland	GBR	GB
581	United States Minor Outlying Islands	UMI	UM
840	United States of America	USA	US
850	United States Virgin Islands	VIR	VI
858	Uruguay	URY	UY
860	Uzbekistan	UZB	UZ
548	Vanuatu	VUT	VU
862	Venezuela, Bolivarian Republic of	VEN	VE
704	Viet Nam	VNM	VN
876	Wallis and Futuna Islands	WLF	WF
732	Western Sahara	ESH	EH
887	Yemen	YEM	YE
894	Zambia	ZMB	ZM
716	Zimbabwe	ZWE	ZW
\.

COPY visitors_language (id, iso3, iso2, name) FROM stdin;
1	aar	aa	Afar
2	abk	ab	Abkhazian
3	ace	\N	Achinese
4	ach	\N	Acoli
5	ada	\N	Adangme
6	ady	\N	Adyghe; Adygei
7	afa	\N	Afro-Asiatic languages
8	afh	\N	Afrihili
9	afr	af	Afrikaans
10	ain	\N	Ainu
11	aka	ak	Akan
12	akk	\N	Akkadian
13	ale	\N	Aleut
14	alg	\N	Algonquian languages
15	alt	\N	Southern Altai
16	amh	am	Amharic
17	ang	\N	English, Old (ca.450-1100)
18	anp	\N	Angika
19	apa	\N	Apache languages
20	ara	ar	Arabic
21	arc	\N	Official Aramaic (700-300 BCE); Imperial Aramaic (700-300 BCE)
22	arg	an	Aragonese
23	arn	\N	Mapudungun; Mapuche
24	arp	\N	Arapaho
25	art	\N	Artificial languages
26	arw	\N	Arawak
27	asm	as	Assamese
28	ast	\N	Asturian; Bable; Leonese; Asturleonese
29	ath	\N	Athapascan languages
30	aus	\N	Australian languages
31	ava	av	Avaric
32	ave	ae	Avestan
33	awa	\N	Awadhi
34	aym	ay	Aymara
35	aze	az	Azerbaijani
36	bad	\N	Banda languages
37	bai	\N	Bamileke languages
38	bak	ba	Bashkir
39	bal	\N	Baluchi
40	bam	bm	Bambara
41	ban	\N	Balinese
42	bas	\N	Basa
43	bat	\N	Baltic languages
44	bej	\N	Beja; Bedawiyet
45	bel	be	Belarusian
46	bem	\N	Bemba
47	ben	bn	Bengali
48	ber	\N	Berber languages
49	bho	\N	Bhojpuri
50	bih	bh	Bihari languages
51	bik	\N	Bikol
52	bin	\N	Bini; Edo
53	bis	bi	Bislama
54	bla	\N	Siksika
55	bnt	\N	Bantu languages
56	bod	bo	Tibetan
57	bos	bs	Bosnian
58	bra	\N	Braj
59	bre	br	Breton
60	btk	\N	Batak languages
61	bua	\N	Buriat
62	bug	\N	Buginese
63	bul	bg	Bulgarian
64	byn	\N	Blin; Bilin
65	cad	\N	Caddo
66	cai	\N	Central American Indian languages
67	car	\N	Galibi Carib
68	cat	ca	Catalan; Valencian
69	cau	\N	Caucasian languages
70	ceb	\N	Cebuano
71	cel	\N	Celtic languages
72	ces	cs	Czech
73	cha	ch	Chamorro
74	chb	\N	Chibcha
75	che	ce	Chechen
76	chg	\N	Chagatai
77	chk	\N	Chuukese
78	chm	\N	Mari
79	chn	\N	Chinook jargon
80	cho	\N	Choctaw
81	chp	\N	Chipewyan; Dene Suline
82	chr	\N	Cherokee
83	chu	cu	Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic
84	chv	cv	Chuvash
85	chy	\N	Cheyenne
86	cmc	\N	Chamic languages
87	cop	\N	Coptic
88	cor	kw	Cornish
89	cos	co	Corsican
90	cpe	\N	Creoles and pidgins, English based
91	cpf	\N	Creoles and pidgins, French-based
92	cpp	\N	Creoles and pidgins, Portuguese-based
93	cre	cr	Cree
94	crh	\N	Crimean Tatar; Crimean Turkish
95	crp	\N	Creoles and pidgins
96	csb	\N	Kashubian
97	cus	\N	Cushitic languages
98	cym	cy	Welsh
99	dak	\N	Dakota
100	dan	da	Danish
101	dar	\N	Dargwa
102	day	\N	Land Dayak languages
103	del	\N	Delaware
104	den	\N	Slave (Athapascan)
105	deu	de	German
106	dgr	\N	Dogrib
107	din	\N	Dinka
108	div	dv	Divehi; Dhivehi; Maldivian
109	doi	\N	Dogri
110	dra	\N	Dravidian languages
111	dsb	\N	Lower Sorbian
112	dua	\N	Duala
113	dum	\N	Dutch, Middle (ca.1050-1350)
114	dyu	\N	Dyula
115	dzo	dz	Dzongkha
116	efi	\N	Efik
117	egy	\N	Egyptian (Ancient)
118	eka	\N	Ekajuk
119	ell	el	Greek, Modern (1453-)
120	elx	\N	Elamite
121	eng	en	English
122	enm	\N	English, Middle (1100-1500)
123	epo	eo	Esperanto
124	est	et	Estonian
125	eus	eu	Basque
126	ewe	ee	Ewe
127	ewo	\N	Ewondo
128	fan	\N	Fang
129	fao	fo	Faroese
130	fas	fa	Persian
131	fat	\N	Fanti
132	fij	fj	Fijian
133	fil	\N	Filipino; Pilipino
134	fin	fi	Finnish
135	fiu	\N	Finno-Ugrian languages
136	fon	\N	Fon
137	fra	fr	French
138	frm	\N	French, Middle (ca.1400-1600)
139	fro	\N	French, Old (842-ca.1400)
140	frr	\N	Northern Frisian
141	frs	\N	Eastern Frisian
142	fry	fy	Western Frisian
143	ful	ff	Fulah
144	fur	\N	Friulian
145	gaa	\N	Ga
146	gay	\N	Gayo
147	gba	\N	Gbaya
148	gem	\N	Germanic languages
149	gez	\N	Geez
150	gil	\N	Gilbertese
151	gla	gd	Gaelic; Scottish Gaelic
152	gle	ga	Irish
153	glg	gl	Galician
154	glv	gv	Manx
155	gmh	\N	German, Middle High (ca.1050-1500)
156	goh	\N	German, Old High (ca.750-1050)
157	gon	\N	Gondi
158	gor	\N	Gorontalo
159	got	\N	Gothic
160	grb	\N	Grebo
161	grc	\N	Greek, Ancient (to 1453)
162	grn	gn	Guarani
163	gsw	\N	Swiss German; Alemannic; Alsatian
164	guj	gu	Gujarati
165	gwi	\N	Gwich'in
166	hai	\N	Haida
167	hat	ht	Haitian; Haitian Creole
168	hau	ha	Hausa
169	haw	\N	Hawaiian
170	heb	he	Hebrew
171	her	hz	Herero
172	hil	\N	Hiligaynon
173	him	\N	Himachali languages; Western Pahari languages
174	hin	hi	Hindi
175	hit	\N	Hittite
176	hmn	\N	Hmong; Mong
177	hmo	ho	Hiri Motu
178	hrv	hr	Croatian
179	hsb	\N	Upper Sorbian
180	hun	hu	Hungarian
181	hup	\N	Hupa
182	hye	hy	Armenian
183	iba	\N	Iban
184	ibo	ig	Igbo
185	ido	io	Ido
186	iii	ii	Sichuan Yi; Nuosu
187	ijo	\N	Ijo languages
188	iku	iu	Inuktitut
189	ile	ie	Interlingue; Occidental
190	ilo	\N	Iloko
191	ina	ia	Interlingua (International Auxiliary Language Association)
192	inc	\N	Indic languages
193	ind	id	Indonesian
194	ine	\N	Indo-European languages
195	inh	\N	Ingush
196	ipk	ik	Inupiaq
197	ira	\N	Iranian languages
198	iro	\N	Iroquoian languages
199	isl	is	Icelandic
200	ita	it	Italian
201	jav	jv	Javanese
202	jbo	\N	Lojban
203	jpn	ja	Japanese
204	jpr	\N	Judeo-Persian
205	jrb	\N	Judeo-Arabic
206	kaa	\N	Kara-Kalpak
207	kab	\N	Kabyle
208	kac	\N	Kachin; Jingpho
209	kal	kl	Kalaallisut; Greenlandic
210	kam	\N	Kamba
211	kan	kn	Kannada
212	kar	\N	Karen languages
213	kas	ks	Kashmiri
214	kat	ka	Georgian
215	kau	kr	Kanuri
216	kaw	\N	Kawi
217	kaz	kk	Kazakh
218	kbd	\N	Kabardian
219	kha	\N	Khasi
220	khi	\N	Khoisan languages
221	khm	km	Central Khmer
222	kho	\N	Khotanese; Sakan
223	kik	ki	Kikuyu; Gikuyu
224	kin	rw	Kinyarwanda
225	kir	ky	Kirghiz; Kyrgyz
226	kmb	\N	Kimbundu
227	kok	\N	Konkani
228	kom	kv	Komi
229	kon	kg	Kongo
230	kor	ko	Korean
231	kos	\N	Kosraean
232	kpe	\N	Kpelle
233	krc	\N	Karachay-Balkar
234	krl	\N	Karelian
235	kro	\N	Kru languages
236	kru	\N	Kurukh
237	kua	kj	Kuanyama; Kwanyama
238	kum	\N	Kumyk
239	kur	ku	Kurdish
240	kut	\N	Kutenai
241	lad	\N	Ladino
242	lah	\N	Lahnda
243	lam	\N	Lamba
244	lao	lo	Lao
245	lat	la	Latin
246	lav	lv	Latvian
247	lez	\N	Lezghian
248	lim	li	Limburgan; Limburger; Limburgish
249	lin	ln	Lingala
250	lit	lt	Lithuanian
251	lol	\N	Mongo
252	loz	\N	Lozi
253	ltz	lb	Luxembourgish; Letzeburgesch
254	lua	\N	Luba-Lulua
255	lub	lu	Luba-Katanga
256	lug	lg	Ganda
257	lui	\N	Luiseno
258	lun	\N	Lunda
259	luo	\N	Luo (Kenya and Tanzania)
260	lus	\N	Lushai
261	mad	\N	Madurese
262	mag	\N	Magahi
263	mah	mh	Marshallese
264	mai	\N	Maithili
265	mak	\N	Makasar
266	mal	ml	Malayalam
267	man	\N	Mandingo
268	map	\N	Austronesian languages
269	mar	mr	Marathi
270	mas	\N	Masai
271	mdf	\N	Moksha
272	mdr	\N	Mandar
273	men	\N	Mende
274	mga	\N	Irish, Middle (900-1200)
275	mic	\N	Mi'kmaq; Micmac
276	min	\N	Minangkabau
277	mis	\N	Uncoded languages
278	mkd	mk	Macedonian
279	mkh	\N	Mon-Khmer languages
280	mlg	mg	Malagasy
281	mlt	mt	Maltese
282	mnc	\N	Manchu
283	mni	\N	Manipuri
284	mno	\N	Manobo languages
285	moh	\N	Mohawk
286	mon	mn	Mongolian
287	mos	\N	Mossi
288	mri	mi	Maori
289	msa	ms	Malay
290	mul	\N	Multiple languages
291	mun	\N	Munda languages
292	mus	\N	Creek
293	mwl	\N	Mirandese
294	mwr	\N	Marwari
295	mya	my	Burmese
296	myn	\N	Mayan languages
297	myv	\N	Erzya
298	nah	\N	Nahuatl languages
299	nai	\N	North American Indian languages
300	nap	\N	Neapolitan
301	nau	na	Nauru
302	nav	nv	Navajo; Navaho
303	nbl	nr	Ndebele, South; South Ndebele
304	nde	nd	Ndebele, North; North Ndebele
305	ndo	ng	Ndonga
306	nds	\N	Low German; Low Saxon; German, Low; Saxon, Low
307	nep	ne	Nepali
308	new	\N	Nepal Bhasa; Newari
309	nia	\N	Nias
310	nic	\N	Niger-Kordofanian languages
311	niu	\N	Niuean
312	nld	nl	Dutch; Flemish
313	nno	nn	Norwegian Nynorsk; Nynorsk, Norwegian
314	nob	nb	Bokm&aring;l, Norwegian; Norwegian Bokm&aring;l
315	nog	\N	Nogai
316	non	\N	Norse, Old
317	nor	no	Norwegian
318	nqo	\N	N'Ko
319	nso	\N	Pedi; Sepedi; Northern Sotho
320	nub	\N	Nubian languages
321	nwc	\N	Classical Newari; Old Newari; Classical Nepal Bhasa
322	nya	ny	Chichewa; Chewa; Nyanja
323	nym	\N	Nyamwezi
324	nyn	\N	Nyankole
325	nyo	\N	Nyoro
326	nzi	\N	Nzima
327	oci	oc	Occitan (post 1500)
328	oji	oj	Ojibwa
329	ori	or	Oriya
330	orm	om	Oromo
331	osa	\N	Osage
332	oss	os	Ossetian; Ossetic
333	ota	\N	Turkish, Ottoman (1500-1928)
334	oto	\N	Otomian languages
335	paa	\N	Papuan languages
336	pag	\N	Pangasinan
337	pal	\N	Pahlavi
338	pam	\N	Pampanga; Kapampangan
339	pan	pa	Panjabi; Punjabi
340	pap	\N	Papiamento
341	pau	\N	Palauan
342	peo	\N	Persian, Old (ca.600-400 B.C.)
343	phi	\N	Philippine languages
344	phn	\N	Phoenician
345	pli	pi	Pali
346	pol	pl	Polish
347	pon	\N	Pohnpeian
348	por	pt	Portuguese
349	pra	\N	Prakrit languages
350	pro	\N	Proven&ccedil;al, Old (to 1500);Occitan, Old (to 1500)
351	pus	ps	Pushto; Pashto
353	que	qu	Quechua
354	raj	\N	Rajasthani
355	rap	\N	Rapanui
356	rar	\N	Rarotongan; Cook Islands Maori
357	roa	\N	Romance languages
358	roh	rm	Romansh
359	rom	\N	Romany
360	ron	ro	Romanian; Moldavian; Moldovan
361	run	rn	Rundi
362	rup	\N	Aromanian; Arumanian; Macedo-Romanian
363	rus	ru	Russian
364	sad	\N	Sandawe
365	sag	sg	Sango
366	sah	\N	Yakut
367	sai	\N	South American Indian languages
368	sal	\N	Salishan languages
369	sam	\N	Samaritan Aramaic
370	san	sa	Sanskrit
371	sas	\N	Sasak
372	sat	\N	Santali
373	scn	\N	Sicilian
374	sco	\N	Scots
375	sel	\N	Selkup
376	sem	\N	Semitic languages
377	sga	\N	Irish, Old (to 900)
378	sgn	\N	Sign Languages
379	shn	\N	Shan
380	sid	\N	Sidamo
381	sin	si	Sinhala; Sinhalese
382	sio	\N	Siouan languages
383	sit	\N	Sino-Tibetan languages
384	sla	\N	Slavic languages
385	slk	sk	Slovak
386	slv	sl	Slovenian
387	sma	\N	Southern Sami
388	sme	se	Northern Sami
389	smi	\N	Sami languages
390	smj	\N	Lule Sami
391	smn	\N	Inari Sami
392	smo	sm	Samoan
393	sms	\N	Skolt Sami
394	sna	sn	Shona
395	snd	sd	Sindhi
396	snk	\N	Soninke
397	sog	\N	Sogdian
398	som	so	Somali
399	son	\N	Songhai languages
400	sot	st	Sotho, Southern
401	spa	es	Spanish; Castilian
402	sqi	sq	Albanian
403	srd	sc	Sardinian
404	srn	\N	Sranan Tongo
405	srp	sr	Serbian
406	srr	\N	Serer
407	ssa	\N	Nilo-Saharan languages
408	ssw	ss	Swati
409	suk	\N	Sukuma
410	sun	su	Sundanese
411	sus	\N	Susu
412	sux	\N	Sumerian
413	swa	sw	Swahili
414	swe	sv	Swedish
415	syc	\N	Classical Syriac
416	syr	\N	Syriac
417	tah	ty	Tahitian
418	tai	\N	Tai languages
419	tam	ta	Tamil
420	tat	tt	Tatar
421	tel	te	Telugu
422	tem	\N	Timne
423	ter	\N	Tereno
424	tet	\N	Tetum
425	tgk	tg	Tajik
426	tgl	tl	Tagalog
427	tha	th	Thai
428	tig	\N	Tigre
429	tir	ti	Tigrinya
430	tiv	\N	Tiv
431	tkl	\N	Tokelau
432	tlh	\N	Klingon; tlhIngan-Hol
433	tli	\N	Tlingit
434	tmh	\N	Tamashek
435	tog	\N	Tonga (Nyasa)
436	ton	to	Tonga (Tonga Islands)
437	tpi	\N	Tok Pisin
438	tsi	\N	Tsimshian
439	tsn	tn	Tswana
440	tso	ts	Tsonga
441	tuk	tk	Turkmen
442	tum	\N	Tumbuka
443	tup	\N	Tupi languages
444	tur	tr	Turkish
445	tut	\N	Altaic languages
446	tvl	\N	Tuvalu
447	twi	tw	Twi
448	tyv	\N	Tuvinian
449	udm	\N	Udmurt
450	uga	\N	Ugaritic
451	uig	ug	Uighur; Uyghur
452	ukr	uk	Ukrainian
453	umb	\N	Umbundu
454	und	\N	Undetermined
455	urd	ur	Urdu
456	uzb	uz	Uzbek
457	vai	\N	Vai
458	ven	ve	Venda
459	vie	vi	Vietnamese
460	vol	vo	Volap&uuml;k
461	vot	\N	Votic
462	wak	\N	Wakashan languages
463	wal	\N	Wolaitta; Wolaytta
464	war	\N	Waray
465	was	\N	Washo
466	wen	\N	Sorbian languages
467	wln	wa	Walloon
468	wol	wo	Wolof
469	xal	\N	Kalmyk; Oirat
470	xho	xh	Xhosa
471	yao	\N	Yao
472	yap	\N	Yapese
473	yid	yi	Yiddish
474	yor	yo	Yoruba
475	ypk	\N	Yupik languages
476	zap	\N	Zapotec
477	zbl	\N	Blissymbols; Blissymbolics; Bliss
478	zen	\N	Zenaga
479	zgh	\N	Standard Moroccan Tamazight
480	zha	za	Zhuang; Chuang
481	zho	zh	Chinese
482	znd	\N	Zande languages
483	zul	zu	Zulu
484	zun	\N	Zuni
485	zxx	\N	No linguistic content; Not applicable
486	zza	\N	Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki
\.
