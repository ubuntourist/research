# Models for the International Visitors Request Form
# Last modified by Kevin Cole <kjcole@gallaudet.edu> 2013.12.01
#

from django.db import models

SHIFT   = ((1, "Morning"),
           (2, "Midday" ),
           (3, "Evening"))

YEA_NAY = ((True,  "Yes"),
           (False, "No" ))

class Language (models.Model):
    iso3 = models.CharField("ISO 3-letter",max_length=3,editable=False)
    iso2 = models.CharField("ISO 2-letter",max_length=2,editable=False,null=True)
    name = models.CharField(max_length=80, editable=False)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = "languages"

class Country (models.Model):
    id   = models.IntegerField(primary_key=True, editable=False)
    name = models.CharField(max_length=80, editable=False)
    iso3 = models.CharField("ISO 3-letter",max_length=3,editable=False)
    iso2 = models.CharField("ISO 2-letter",max_length=2,editable=False)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = "countries"

class Inquiry (models.Model):
    changed      = models.DateTimeField("last changed", auto_now=True, editable=False)
    arr_date     = models.DateField("arrival date")
    arr_time     = models.PositiveSmallIntegerField("arrival time",null=True,choices=SHIFT)
    dep_date     = models.DateField("departure date")
    dep_time     = models.PositiveSmallIntegerField("departure time",null=True,choices=SHIFT)
    requester    = models.CharField(max_length=80)
    organization = models.CharField(max_length=80)
    email        = models.CharField(max_length=80)
    phone        = models.CharField(max_length=80)
    visitors     = models.PositiveSmallIntegerField()
    speakers     = models.PositiveSmallIntegerField()
    signers      = models.PositiveSmallIntegerField()
    purpose      = models.TextField()
    letters      = models.BooleanField("letters?", choices=YEA_NAY)
    countries    = models.ManyToManyField(Country)

    def _interpreter_needed(self):
        """Build a computed pseudo-field named "interpreter"."""
        if self.signers < self.visitors: return True
        else:                            return False
    interpreter = property(_interpreter_needed)

    def __unicode__(self):
        return "%s - %s" % (self.arr_date, self.organization,)

    class Meta:
        ordering = ["arr_date", "organization"]
        get_latest_by = "changed"
        verbose_name_plural = "inquiries"

class Visitor (models.Model):
    changed     = models.DateTimeField("last changed",auto_now=True,editable=False)
    inquiry     = models.ForeignKey(Inquiry)
    surname     = models.CharField(max_length=80)
    givenname   = models.CharField("given name",max_length=80)
    middlename  = models.CharField(max_length=80, null=True)
    occupation  = models.CharField(max_length=80)
    gender      = models.CharField(max_length=1)   # Should this be PosSmallInt?
    dob         = models.DateField("date of birth")
    citizenship = models.ForeignKey(Country, related_name="citizens")
    native      = models.ForeignKey(Country, related_name="natives")
    residence   = models.ForeignKey(Country, related_name="residents")
    passport    = models.CharField(max_length=80, null=True)

    def __unicode__(self):
        return "%s, %s" % (self.surname, self.givenname,)

    class Meta:
        ordering = ["citizenship", "surname", "givenname"]
        get_latest_by = "changed"
        verbose_name_plural = "visitors"
