from models                              import *
from django                              import forms
from django.contrib.localflavor.us.forms import USPhoneNumberField
from django.forms.widgets                import TextInput, Textarea, Select
from django.forms.widgets                import CheckboxInput, RadioSelect
from django.forms.widgets                import DateInput, TimeInput, DateTimeInput
from django.forms.widgets                import CheckboxSelectMultiple
from django.forms.widgets                import NullBooleanSelect
from django.forms.extras.widgets         import SelectDateWidget
from django.utils.safestring             import mark_safe
from django.forms.models                 import inlineformset_factory

from datetime                            import timedelta, datetime, date

today     = date.today()
fourweeks = today + timedelta(28)        # Four weeks from now

class EmailInput(TextInput):
    input_type = "email"             # HTML5 enhanced

class NumberInput(TextInput):
    input_type = "number"            # HTML5 enhanced

class TelephoneInput(TextInput):
    input_type = "tel"               # HTML5 enhanced

class DateInput(DateInput):
    input_type = "date"              # HTML5 enhanced

class DateTimeInput(DateTimeInput):
    input_type = "datetime"          # HTML5 enhanced

class TimeInput(TimeInput):
    input_type = "time"              # HTML5 enhanced

class HorizRadioRenderer(forms.RadioSelect.renderer):
    """ From the University of Texas wiki
        https://wikis.utexas.edu/display/~bm6432/Django-Modifying+RadioSelect+Widget+to+have+horizontal+buttons
    """
    def render(self):
            """Outputs radios"""
            return mark_safe(u"\n".join([u"%s\n" % w for w in self]))

SHIFT   = ((1, "Morning"),
           (2, "Midday"),
           (3, "Evening"))

YEA_NAY = ((True,  "Yes"),
           (False, "No" ))

class LanguageForm(forms.ModelForm):
    class Meta:
        model = Language
#       widgets = {"priorities": CheckboxSelectMultiple,}

class CountryForm(forms.ModelForm):
    class Meta:
        model = Country
#       widgets = {"priorities": CheckboxSelectMultiple,}

class InquiryForm(forms.ModelForm):
    arr_date     = forms.DateField(label="Arrival date",
                     initial=fourweeks,
                     widget=TextInput(attrs={"class":"datepicker",}))
    arr_time     = forms.TypedChoiceField(label="Arrival time",
                     coerce=int,
                     choices=SHIFT,
                     widget=Select())
    dep_date     = forms.DateField(label="Departure date",
                     initial=fourweeks,
                     widget=TextInput(attrs={"class":"datepicker",}))
    dep_time     = forms.TypedChoiceField(label="Departure time",
                     coerce=int,
                     choices=SHIFT,
                     widget=Select())
    requester    = forms.CharField(label="Name of requester",
                     max_length=80,
                     widget=TextInput(attrs={"size":"58",}))
    organization = forms.CharField(label="Requester's organization",
                     max_length=80,
                     widget=TextInput(attrs={"size":"58"}))
    email        = forms.CharField(label="Requester's e-mail address",
                     max_length=80,
                     widget=TextInput(attrs={"size":"58"}))
    phone        = forms.CharField(label="Requester's phone number",
                     max_length=80,
                     widget=TextInput(attrs={"size":"58"}))
    visitors     = forms.IntegerField(label="Number of visitors",
                     min_value=1, max_value=99,
                     widget=TextInput(attrs={"style":"text-align:right; width:2em;",
                                             "class":"spinner",}))
    speakers     = forms.IntegerField(label="How many are fluent English speakers?",
                     min_value=0, max_value=99,
                     widget=TextInput(attrs={"style":"text-align:right; width:2em;",
                                             "class":"spinner",}))
    signers      = forms.IntegerField(label="How many are fluent ASL users?",
                     min_value=0, max_value=99,
                     widget=TextInput(attrs={"style":"text-align:right; width:2em;",
                                             "class":"spinner",}))
    purpose      = forms.CharField(label="Purpose of visit",
                     widget=Textarea(attrs={"rows":"20", "cols":"58"}))

    letters      = forms.TypedChoiceField(label="Will you need invitation letters?",
                     coerce=lambda x: x == "True",
                     choices=YEA_NAY,
                     widget=RadioSelect(renderer=HorizRadioRenderer))

    class Meta:
        model   = Inquiry
        fields  = ("arr_date",
                   "arr_time",
                   "dep_date",
                   "dep_time",
                   "requester",
                   "organization",
                   "email",
                   "phone",
                   "visitors",
                   "speakers",
                   "signers",
                   "purpose",
                   "letters",)

class VisitorForm(forms.ModelForm):
    surname     = forms.CharField(label="Family name",
                    max_length=80,
                    widget=TextInput(attrs={"size":"58"}))
    givenname   = forms.CharField(label="Given name",
                    max_length=80,
                    widget=TextInput(attrs={"size":"58"}))
    middlename  = forms.CharField(label="Middle name(s)",
                    max_length=80, required=False,
                    widget=TextInput(attrs={"size":"58"}))
    occupation  = forms.CharField(label="Occupation",
                    max_length=80,
                    widget=TextInput(attrs={"size":"58"}))
    gender      = forms.CharField(label="Gender",
                    max_length=1,   # Should this be PosSmallInt?
                    widget=TextInput(attrs={"size":"58"}))
    dob         = forms.DateField(label="Date of birth",
                     widget=TextInput(attrs={"class":"datepicker",}))
    citizenship = forms.ModelChoiceField(label="Country of citizenship",
                    queryset=Country.objects.all())
    native      = forms.ModelChoiceField(label="Country of birth",
                    queryset=Country.objects.all())
    residence   = forms.ModelChoiceField(label="Country of current residence",
                    queryset=Country.objects.all())
    passport    = forms.CharField(label="Passport number",
                    max_length=80, required=False,
                    widget=TextInput(attrs={"size":"58"}))

    class Meta:
        model   = Visitor
        fields  = ("surname",
                   "givenname",
                   "middlename",
                   "occupation",
                   "gender",
                   "dob",
                   "citizenship",
                   "native",
                   "residence",
                   "passport",)

#VisitorFormset = inlineformset_factory(Inquiry,Visitor)
