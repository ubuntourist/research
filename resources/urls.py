# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2012.03.12
#
# For help with all the now undocumented updates, see:
#    http://code.djangoproject.com/wiki/NewFormsAdminBranch
#
# For help with the images and css, see:
#    http://www.djangoproject.com/documentation/static_files/
#
# See also?
#    http://code.djangoproject.com/wiki/IrcFAQ#MediaURL
#    http://www.djangoproject.com/documentation/templates_python/#subclassing-context-requestcontext
#    http://www.djangoproject.com/documentation/settings/#media-root
#    http://www.djangoproject.com/documentation/settings/#media-url
#
# 2008.11.30 KJC - DEPLOY! Added "django/" to all patterns (See p. 279)
# 2008.12.01 KJC - Oops. Changed "^django/" to "^". See updated info at:
#                  http://docs.djangoproject.com/en/dev/howto/deployment/modpython/
# 2008.12.01 KJC - Split into multiple urls.py files
# 2009.09.18 KJC - Changed ^admin/ to upcoming Django 1.3 format
# 2012.02.22 KJC - Refactored / merged from /psite/ into this "instance"
# 2012.03.11 KJC - Adding in Research at Gallaudet (rag)
# 2012.03.12 KJC - Changing it to Research at Gallaudet University (ragu)
# 2012.05.02 KJC - Adding in Preliminary Screening Survey (screening)
# 2013.11.10 KJC - Adding an International Visitors form...

from django.conf.urls.defaults import patterns, include, url
from django.contrib            import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #   url(r'^$',          'resources.views.home', name='home'),
    #   url(r'^resources/', include('resources.foo.urls')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/',     include(admin.site.urls)),

    url(r'^books/',     include('resources.books.urls')),
    url(r'^mhd/',       include('resources.mhd.urls')),
    url(r'^ragu/',      include('resources.ragu.urls')),
    url(r'^screening/', include('resources.screening.urls')),
    url(r'visitors/',   include('resources.visitors.urls')),

    # Probably now a better way to handle the following two lines...
    #url(r'^images/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/home/kjcole/Django/images'}),
    #url(r'^css/(?P<path>.*)$',    'django.views.static.serve', {'document_root': '/home/kjcole/Django/css'}),
)
