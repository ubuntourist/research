﻿<h3>Students engaged in research</h3>

<p>Research at Gallaudet University strives to involve students in
ways that benefits their minds and also advances the pursuit of new
understandings and knowledge. Of the 180 research projects reported
herein, 82 graduate and undergraduate students are involved in 68
projects. From serving as assistants for faculty investigators to the
carrying out of their own study, students are major contributors in
the vitality of campus research scholarship. With scientific inquiry
often provoking ever more and more questions, and thereby revealing
the complexity of our world, self-motivated students learn to
challenge received information. Accordingly, many academic programs
have classes that require research projects as a final project or as
the focus of the entire course. Through active inquiry, students get a
chance to apply theories and knowledge from their classes in a way
that helps them to make connections to real situations and practice.
Such critical approach to thinking can propel a deeper insight into
their chosen field&mdash;and solidify their foundation for a promising
career in the knowledge-based fields.</p>

<p>Engaging students in research benefits not only them, but the
professional fields as well. Young minds may approach problems in new
ways. Gaining the insight of younger Deaf and hard of hearing people
is essential to many topics of concern to Gallaudet in particular.
Student research assistants play vital roles in collecting responses
from diverse participants, analyzing raw data, and presenting
findings. Across the university, there are a growing number of "hot
spots" of student researchers working on studies from the physical
sciences to social sciences to deafness-related disciplines. The
university encourages student involvement in research activity through
graduate assistantships, hiring under external grants, and direct
funding of student research. One promising development is the extent
of student-initiated, student-led research activity. In FY 2012, 44
small research grants were awarded to students conducting their own
research or who are working under faculty members. At the pinnacle of
student contribution to knowledge is the doctoral dissertation; a list
of dissertations completed by Gallaudet students in FY 2012 is shown
below.</p>
