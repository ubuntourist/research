<?php require("/var/www/include/template.php"); ?>

<?php gu_metadata("Whateva!"); ?>
<meta name="description"
      content="Gallaudet Research Institute (GRI)" />
<meta name="keywords"
      content="research, deafness, deaf, ASL, American Sign Language,
               cochlear implants, hearing aids, assistive technology,
               listening devices, literacy, academic performace" />

<script type="text/javascript">
function OnSubmitForm() {
  var form = document.getElementById("general");
  if      (form.letters[0].checked == true) form.action = "letters.php";
  else if (form.letters[1].checked == true) form.action = "deliver.php";
  return true;
}
</script>

<style type="text/css">
/* This is a CSS comment. It is ignored by the browser. */

body     {font-family: Verdana, Arial, Helvetica, sans-serif;
          font-size:   16px;}

fieldset {margin:           10px;
          background-color: #dfffff;              /* sort of cyan */
          border:           thin solid #ff7f7f;}  /* pink         */

legend   {background-color: #ffffc0;              /* light yellow */
          border:           thin solid #ff7f7f;   /* pink         */
          color:            #ff3f3f;              /* light red    */
          font-weight:      bold;}

input,
select, option,
textarea {border:      1px solid black;
          font-family: fixed;
          font-size:   16px;
          vertical-align: top;}

.center  {margin:           1em auto;
          text-align:       center;}

table, tbody, tr, th, td { border: none; }
</style>

<?php gu_scaffold(); ?>

<h1 class="center">INTERNATIONAL VISITOR REQUEST FORM</h1>

<p>Please provide <strong>ALL</strong> the requested information.</p>

<form id="general" name="general" 
      action="letters.php" method="post"
      onsubmit="return OnSubmitForm();">

<fieldset>
  <legend>General information</legend>

  <table style="width:100%;">
  <colgroup>
    <col span="1" style="width:40%;" />
    <col span="1" style="width:15%;" />
    <col span="1" style="width:45%;" />
  </colgroup>

  <tbody style="vertical-align:top;">

  <tr>
  <td colspan="3"><div style="margin:10px 50px auto 50px;">
  You are welcome to visit Gallaudet for as long
  as you wish. However, our office can only provide up to two (2) days
  support for your visit.  If you need a longer stay please contact
  &lt;<a href="mailto:intl.visit@gallaudet.edu">intl.visit@gallaudet.edu</a>&gt;
  directly. You can find information about visiting Gallaudet at <a
  href="http://www.gallaudet.edu/international_relations/visitor_information.html">http://www.gallaudet.edu/international_relations/visitor_information.html</a>.
  </div></td>
  </tr>

  <tr>
  <td style="text-decoration:underline; font-weight:bold; height:50px; vertical-align:bottom;">Date(s)
      of visit:</td><td colspan="2"> </td>
  </tr>

  <tr>
  <td style="text-align:right;"><label
      for="arr_date"><strong>Arrival on campus:</strong></label></td>
  <td><input id="arr_date" name="arr_date" type="date"
             required="required" /></td>
  <td><label for="arr_time">Time:</label>
  <select id="arr_time" name="arr_time" title="time">
    <option value="morning">morning</option>
    <option value="midday">midday</option>
    <option value="evening">evening</option>
  </select></td>

  </tr>
  <tr>
  <td style="text-align:right;"><label
      for="dep_date"><strong>Departure from campus:</strong></label></td>
  <td><input id="dep_date" name="dep_date" type="date"
             required="required" /></td>
  <td><label for="dep_time">Time:</label>
  <select id="dep_time" name="dep_time" title="time">
    <option value="morning">morning</option>
    <option value="midday">midday</option>
    <option value="evening">evening</option>
  </select></td>
  </tr>

  <tr>
  <td style="text-decoration:underline; font-weight:bold; height:50px; vertical-align:bottom;">Requester Information</td>
  <td colspan="2"> </td>
  </tr>

  <tr>
  <td style="text-align:right;"><label for="requester_name">Requester's
      name:</label></td>
  <td colspan="2"><input id="requester_name" name="requester_name"
                         type="text" size="70" autocomplete="off"
                         required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label for="requester_org">Requester's
      organization:</label></td>
  <td colspan="2"><input id="requester_org" name="requester_org"
                         type="text" size="70" autocomplete="off"
                         required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label for="requester_email">Requester's
      email address:</label></td>
  <td colspan="2"><input id="requester_email" name="requester_email"
                         type="email" size="70" autocomplete="off"
                         required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label for="requester_phone">Requester's
      phone number:</label></td>
  <td colspan="2"><input id="requester_phone" name="requester_phone"
                         type="tel" size="20" autocomplete="off"
                         required="required" /></td>
  </tr>

  <tr>
  <td style="text-decoration:underline; font-weight:bold; height:50px; vertical-align:bottom;">Visitors Information</td>
  <td colspan="2"> </td>
  </tr>

  <tr>
  <td style="text-align:right;"><label for="country">Country
      of visitor(s):</label></td>
  <td colspan="2"><input id="country" name="country"
                         type="text" size="70" autocomplete="off"
                         required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label for="visitors">Number of
      visitor(s):</label></td>
  <td colspan="2"><input id="visitors" name="visitors"
                         type="number" min="1" max="99"
                         autocomplete="off"
                         style="text-align:right;"
                         required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right"><label for="purpose">Purpose of
      visit:</label></td>
  <td colspan="2"><textarea name="purpose" id="purpose"
                            rows="4" cols="70"
                            required="required"></textarea></td>
  </tr>

  <tr>
  <td style="text-decoration:underline; font-weight:bold; height:50px; vertical-align:bottom;">Interpreting</td>
  <td colspan="2"> </td>
  </tr>

  <tr>
  <td colspan="3">The main modes of communication at Gallaudet
  University are <strong>American Sign Language (ASL)</strong> and
  <strong>English</strong>.<br />
  <br />How many members of your group:</td>
  </tr>

  <tr>
  <td style="text-align:right;"><label
      for="speakers">speak English fluently? </label></td>
  <td colspan="2"><input id="speakers" name="speakers"
                         type="number" min="0" max="99"
                         autocomplete="off"
                         style="text-align:right;"
                         required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label
      for="signers">use American Sign Language fluently? </label></td>
  <td colspan="2"><input id="signers" name="signers"
                         type="number" min="0" max="99"
                         autocomplete="off"
                         style="text-align:right;"
                         required="required" /></td>
  </tr>

  <tr>
  <td colspan="3">We will contact you about interpreting needs and 
  costs, if applicable. </>
  </tr>

  <tr>
  <td style="text-decoration:underline; font-weight:bold; height:50px; vertical-align:bottom;">Invitation letters</td>
  <td colspan="2"> </td>
  </tr>

  <tr>
  <td colspan="3">Do you require an invitation letter?
  <input id="letters_yes" name="letters" type="radio"
         value="Y" required="required" />
  <label for="letters_yes">Yes</label>
  <input id="letters_no"  name="letters" type="radio"
         value="N" required="required" />
  <label for="letters_no">No</label></td>
  </tr>

  </tbody>
  </table>

</fieldset>

<!--
<fieldset>
  <legend>A radio example (choose one)</legend>

  <input id="input2" name="city" type="radio" value="1" />
      <label for="input2">Washington, DC</label><br />

  <input id="input3" name="city" type="radio" value="2" />
                <label for="input3">Hyattsville, MD</label><br />

  <input id="input4" name="city" type="radio" value="3" />
                    <label for="input4">Falls Church, VA</label>

</fieldset>

<fieldset>
  <legend>A checkbox example (choose many)</legend>

  <input id="input5" name="feature1" type="checkbox" value="1" />
  <label for="input5">Four-wheel drive</label><br />

  <input id="input6" name="feature2" type="checkbox" value="1" />
  <label for="input6">GPS</label><br />

  <input id="input7" name="feature3" type="checkbox" value="1" />
  <label for="input7">Air Conditioning</label>

</fieldset>

<fieldset>
  <legend>A select (pull-down) example</legend>

  <select id="input8" name="mb" title="month">
      <option value="???"> </option>
    <optgroup label="1st quarter">
      <option value="Jan">January</option>
      <option value="Feb">February</option>
      <option value="Mar">March</option>
    </optgroup>
    <optgroup label="2nd quarter">
      <option value="Apr">April</option>
      <option value="May">May</option>
      <option value="Jun">June</option>
    </optgroup>
  </select>

  <select id="input9" name="yb" title="year">
    <option value="????"> </option>
    <option value="1990">1990</option>
    <option value="1991">1991</option>
    <option value="1992">1992</option>
    <option value="1993">1993</option>
    <option value="1994">1994</option>
    <option value="1995">1995</option>
  </select>

</fieldset>

<p class="center">
  <button type="button"
          onclick="alert('Hey!');">Click me!</button></p>

<p class="center">
  <button type="button"
          onclick="alert('You typed: ' + document.getElementById('requester_name').value);">Show text</button>
</p>
-->

<p class="center">
  <button type="submit">Next &rarr;</button>
</p>

</form>

<p style="text-align:center; font-size:x-small;">[Last modified:
<?php echo date("Y.m.d H:i:s.", getlastmod()); ?> by <a
href="http://www.gallaudet.edu/Faculty-Staff/Gallaudet_Research_Institute/Cole_Kevin.html">Kevin
Cole</a>]</p>

<?php gufooter(); ?>
