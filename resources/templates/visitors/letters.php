<?php require("/var/www/include/template.php"); ?>

<?php gu_metadata("Whateva!"); ?>
<meta name="description"
      content="Gallaudet Research Institute (GRI)" />
<meta name="keywords"
      content="research, deafness, deaf, ASL, American Sign Language,
               cochlear implants, hearing aids, assistive technology,
               listening devices, literacy, academic performace" />

<style type="text/css">
/* This is a CSS comment. It is ignored by the browser. */

body     {font-family: Verdana, Arial, Helvetica, sans-serif;
          font-size:   16px;}

fieldset {margin:           35px 10px;
          background-color: #dfffff;              /* sort of cyan */
          border:           thin solid #ff7f7f;}  /* pink         */

legend   {background-color: #ffffc0;              /* light yellow */
          border:           thin solid #ff7f7f;   /* pink         */
          color:            #ff3f3f;              /* light red    */
          font-weight:      bold;}

input,
textarea {border:      1px solid black;
          font-family: fixed;
          font-size:   16px;
          vertical-align: top;}

.center  {margin:           1em auto;
          text-align:       center;}

table, tbody, tr, th, td { border: none; }
</style>

<?php gu_scaffold(); ?>

<?php
function bio($visitor) {
$fieldset =<<<KJCVISITOR
<fieldset>
  <legend>Visitor #{$visitor}</legend>
  <table style="width:100%;">
  <colgroup>
    <col span="1" style="width:30%;" />
    <col span="1" style="width:70%;" />
  </colgroup>
  <tbody style="vertical-align:top;">

  <tr>
  <td style="text-align:right;"><label 
      for="visitor_{$visitor}_surname">Family name(s):</label></td>
  <td><input id="visitor_{$visitor}_surname"
             name="visitor_{$visitor}_surname"
             type="text" size="70" autocomplete="off"
             required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label
      for="visitor_{$visitor}_givenname">Given name(s):</label></td>
  <td><input id="visitor_{$visitor}_givenname" 
             name="visitor_{$visitor}_givenname"
             type="text" size="70" autocomplete="off"
             required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label
      for="visitor_{$visitor}_middlename">Middle name(s):</label></td>
  <td><input id="visitor_{$visitor}_middlename"
             name="visitor_{$visitor}_middlename"
             type="text" size="70" autocomplete="off" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label 
      for="visitor_{$visitor}_occupation">Occupation:</label></td>
  <td><input id="visitor_{$visitor}_occupation"
             name="visitor_{$visitor}_occupation"
             type="text" size="70" autocomplete="off"
             required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;">Gender:</td>
  <td><input id="visitor_{$visitor}_gender_f"
             name="visitor_{$visitor}_gender"
             type="radio" value="F"
             required="required" />
      <label for="visitor_{$visitor}_gender_f">Female</label>
      <input id="visitor_{$visitor}_gender_m"
             name="visitor_{$visitor}_gender"
             type="radio" value="M"
             required="required" />
      <label for="visitor_{$visitor}_gender_m">Male</label></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label 
      for="visitor_{$visitor}_dob">Date of birth:</label></td>
  <td><input id="visitor_{$visitor}_dob"
             name="visitor_{$visitor}_dob"
             type="date" autocomplete="off"
             required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label 
      for="visitor_{$visitor}_citizenship">Country of citizenship:</label></td>
  <td><input id="visitor_{$visitor}_citizenship"
             name="visitor_{$visitor}_citizenship"
             type="text" size="70" autocomplete="off"
             required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label 
      for="visitor_{$visitor}_native">Country of birth:</label></td>
  <td><input id="visitor_{$visitor}_native"
             name="visitor_{$visitor}_native"
             type="text" size="70" autocomplete="off"
             required="required" /></td>
  </tr>

  <tr>
  <td style="text-align:right;"><label 
      for="visitor_{$visitor}_residence">Country of legal residence:</label></td>
  <td><input id="visitor_{$visitor}_residence"
             name="visitor_{$visitor}_residence"
             type="text" size="70" autocomplete="off"
             required="required" /></td>
  </tr>
  </tbody>
  </table>
</fieldset>
KJCVISITOR;
echo $fieldset;
}
?>

<h1 class="center">INTERNATIONAL VISITOR REQUEST FORM</h1>

<p>Please provide <strong>ALL</strong> the requested information. (We
will contact you directly in order to obtain passport numbers.)</p>

<table style="border: 1px solid #00007f; border-collapse:collapse;">
<tbody style="border: 1px solid #00007f; border-collapse:collapse;">
<?php
$bring = null;
$visitors = intval($_POST["visitors"], 10);
$speakers = intval($_POST["speakers"], 10);
$signers  = intval($_POST["signers"],  10);
$fluent   = $speakers + $signers;
if ($signers < $visitors) { $bring = true;  }
else                      { $bring = false; }

echo '<tr><td>Arrival date:</td><td>' . $_POST["arr_date"] . '</td></tr>';
echo '<tr><td>Arrival time:</td><td>' . $_POST["arr_time"] . '</td></tr>';
echo '<tr><td>Departure date:</td><td>' . $_POST["dep_date"] . '</td></tr>';
echo '<tr><td>Departure time:</td><td>' . $_POST["dep_time"] . '</td></tr>';
echo '<tr><td>Requester:</td><td>' . $_POST["requester_name"] . '</td></tr>';
echo '<tr><td>Organization:</td><td>' . $_POST["requester_org"] . '</td></tr>';
echo '<tr><td>Email address:</td><td>' . $_POST["requester_email"] . '</td></tr>';
echo '<tr><td>Phone number:</td><td>' . $_POST["requester_phone"] . '</td></tr>';
echo '<tr><td>Country:</td><td>' . $_POST["country"] . '</td></tr>';
echo '<tr><td>Number of visitors:</td><td>' . $_POST["visitors"] . '</td></tr>';
echo '<tr><td>Number of English speakers:</td><td>' . $_POST["speakers"] . '</td></tr>';
echo '<tr><td>Number of ASL users:</td><td>' . $_POST["signers"] . '</td></tr>';
if ($bring) {
  echo '<tr><td colspan="2"><strong>Requester';
  echo ' <span style="text-decoration:underline;">MUST</span>';
  echo ' bring interpreter(s).</strong></td></tr>';
}
echo '<tr><td>Purpose of visit:</td><td>' . $_POST["purpose"] . '</td></tr>';
echo '<tr><td>Need letters?:</td><td>' . $_POST["letters"] . '</td></tr>';
?>
</tbody>
</table>

<form action="deliver.php" method="post">

<?php 
$visitors = $_POST["visitors"];
for ($visitor = 1; $visitor < $visitors+1; ++$visitor) { bio($visitor); }

$previously  = '<input name="arr_date" id="arr_date"';
$previously .= '  type="hidden" value="' . $_POST["arr_date"] . '" />';
$previously .= '<input name="arr_time" id="arr_time"';
$previously .= '  type="hidden" value="' . $_POST["arr_time"] . '" />';
$previously .= '<input name="dep_date" id="dep_date"';
$previously .= '  type="hidden" value="' . $_POST["dep_date"] . '" />';
$previously .= '<input name="dep_time" id="dep_time"';
$previously .= '  type="hidden" value="' . $_POST["dep_time"] . '" />';
$previously .= '<input name="requester_name" id="requester_name"';
$previously .= '  type="hidden" value="' . $_POST["requester_name"] . '" />';
$previously .= '<input name="requester_org" id="requester_org"';
$previously .= '  type="hidden" value="' . $_POST["requester_org"] . '" />';
$previously .= '<input name="requester_email" id="requester_email"';
$previously .= '  type="hidden" value="' . $_POST["requester_email"] . '" />';
$previously .= '<input name="requester_phone" id="requester_phone"';
$previously .= '  type="hidden" value="' . $_POST["requester_phone"] . '" />';
$previously .= '<input name="country" id="country"';
$previously .= '  type="hidden" value="' . $_POST["country"] . '" />';
$previously .= '<input name="visitors" id="visitors"';
$previously .= '  type="hidden" value="' . $_POST["visitors"] . '" />';
$previously .= '<input name="speakers" id="speakers"';
$previously .= '  type="hidden" value="' . $_POST["speakers"] . '" />';
$previously .= '<input name="signers" id="signers"';
$previously .= '  type="hidden" value="' . $_POST["signers"] . '" />';
if ($bring) {
$previously .= '<input name="bring" id="bring"';
$previously .= '  type="hidden" value="true" />';
}
$previously .= '<input name="purpose" id="purpose"';
$previously .= '  type="hidden" value="' . $_POST["purpose"] . '" />';
$previously .= '<input name="letters" id="letters"';
$previously .= '  type="hidden" value="' . $_POST["letters"] . '" />';
echo $previously;
?>


<!-- 
<p class="center">
  <button type="button"
          onclick="alert('Hey!');">Click me!</button></p>

<p class="center">
  <button type="button"
          onclick="alert('You typed: ' + document.getElementById('requester_name').value);">Show text</button>
</p>
 -->

<p class="center">
  <button type="submit">Next</button>
</p>

</form>

<p style="text-align:center; font-size:x-small;">[Last modified:
<?php echo date("Y.m.d H:i:s.", getlastmod()); ?> by <a 
href="http://www.gallaudet.edu/Faculty-Staff/Gallaudet_Research_Institute/Cole_Kevin.html">Kevin
Cole</a>]</p>

<?php gufooter(); ?>
