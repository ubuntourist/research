<?php require("/var/www/include/template.php"); ?>

<?php
$sfs = "margin:35px 10px;background-color:#dfffff;border:thin solid #ff7f7f;";
$sl  = "background-color:#ffffc0;border:thin solid #ff7f7f;color:#ff3f3f;font-weight:bold;";

  function bio($visitor) {
    global $sfs, $sl;
    $fieldset .= "<fieldset style='" . $sfs . "'>\r\n";
    $fieldset .= "  <legend style='" . $sl . "'>Visitor #" . $visitor . "</legend>\r\n";
    $fieldset .= "  <table style='width:100%;'>\r\n";
    $fieldset .= "  <colgroup>\r\n";
    $fieldset .= "    <col span='1' style='width:30%;' />\r\n";
    $fieldset .= "    <col span='1' style='width:70%;' />\r\n";
    $fieldset .= "  </colgroup>\r\n";
    $fieldset .= "  <tbody style='vertical-align:top;'>\r\n";
    $fieldset .= "  <tr>\r\n";
    $fieldset .= "  <td>Family name(s):</td>\r\n";
    $fieldset .= "  <td>" . $_POST["visitor_{$visitor}_surname"] . "</td>\r\n";
    $fieldset .= "  </tr>\r\n";
    $fieldset .= "  <tr>\r\n";
    $fieldset .= "  <td>Given name(s):</td>\r\n";
    $fieldset .= "  <td>" . $_POST["visitor_{$visitor}_givenname"] . "</td>\r\n";
    $fieldset .= "  </tr>\r\n";
    $fieldset .= "  <tr>\r\n";
    $fieldset .= "  <td>Middle name(s):</td>\r\n";
    $fieldset .= "  <td>" . $_POST["visitor_{$visitor}_middlename"] . "</td>\r\n";
    $fieldset .= "  </tr>\r\n";
    $fieldset .= "  <tr>\r\n";
    $fieldset .= "  <td>Occupation:</td>\r\n";
    $fieldset .= "  <td>" . $_POST["visitor_{$visitor}_occupation"] . "</td>\r\n";
    $fieldset .= "  </tr>\r\n";
    $fieldset .= "  <tr>\r\n";
    $fieldset .= "  <td>Gender:</td>\r\n";
    $fieldset .= "  <td>" . $_POST["gender"] . "</td>\r\n";
    $fieldset .= "  </tr>\r\n";
    $fieldset .= "  <tr>\r\n";
    $fieldset .= "  <td>Date of birth:</td>\r\n";
    $fieldset .= "  <td>" . $_POST["visitor_{$visitor}_dob"] . "</td>\r\n";
    $fieldset .= "  </tr>\r\n";
    $fieldset .= "  <tr>\r\n";
    $fieldset .= "  <td>Country of citizenship:</td>\r\n";
    $fieldset .= "  <td>" . $_POST["visitor_{$visitor}_citizenship"] . "</td>\r\n";
    $fieldset .= "  </tr>\r\n";
    $fieldset .= "  <tr>\r\n";
    $fieldset .= "  <td>Country of birth:</td>\r\n";
    $fieldset .= "  <td>" . $_POST["visitor_{$visitor}_native"] . "</td>\r\n";
    $fieldset .= "  </tr>\r\n";
    $fieldset .= "  <tr>\r\n";
    $fieldset .= "  <td>Country of legal residence:</td>\r\n";
    $fieldset .= "  <td>" . $_POST["visitor_{$visitor}_residence"] . "</td>\r\n";
    $fieldset .= "  </tr>\r\n";
    $fieldset .= "  </tbody>\r\n";
    $fieldset .= "  </table>\r\n";
    $fieldset .= "</fieldset>\r\n";

    return $fieldset;
  }
?>

<?php gu_metadata("Whateva!"); ?>
<meta name="description"
      content="Gallaudet Research Institute (GRI)" />
<meta name="keywords"
      content="research, deafness, deaf, ASL, American Sign Language,
               cochlear implants, hearing aids, assistive technology,
               listening devices, literacy, academic performace" />

<style type="text/css">
/* This is a CSS comment. It is ignored by the browser. */

body     {font-family: Verdana, Arial, Helvetica, sans-serif;
          font-size:   16px;}

fieldset {margin:           35px 10px;
          background-color: #dfffff;              /* sort of cyan */
          border:           thin solid #ff7f7f;}  /* pink         */

legend   {background-color: #ffffc0;              /* light yellow */
          border:           thin solid #ff7f7f;   /* pink         */
          color:            #ff3f3f;              /* light red    */
          font-weight:      bold;}

input,
textarea {border:      1px solid black;
          font-family: fixed;
          vertical-align: top;}

.center  {margin:           1em auto;
          text-align:       center;}

table, tbody, tr, th, td { border: none; }
</style>

<?php gu_scaffold(); ?>

<?php
if ($_POST) {
  $from     = "International Visitor Form <gri.gateway@gallaudet.edu>";
  $cc       = "intl.visit@gallaudet.edu";
  $replyto  = "intl.visit@gallaudet.edu";
  $mimevers = "1.0";
  $mimetype = "text/html; charset=iso-8859-1";
  $fullname = '"' . $_POST["requester_name"] . '"';  // "Kevin Cole"
  $email    = $_POST["requester_email"];             // kevin.cole@gallaudet.edu
  $arriving = $_POST["arr_date"];
  $leaving  = $_POST["dep_date"];
  $style    = "color:red; text-decoration:underline; font-weight:bold;";

  $headers  = "From: "         . $from     . "\r\n";
  $headers .= "Reply-To: "     . $replyto  . "\r\n";
  $headers .= "MIME-Version: " . $mimevers . "\r\n";
  $headers .= "Cc: "           . $cc       . "\r\n";
  $headers .= "Content-type: " . $mimetype . "\r\n";

  $to       = $fullname . " <" . $email . ">";
  $subject  = "Re: Visit Inquiry - ";
  $subject .= $_POST["country"] . " (";
  $subject .= date("F j, Y", strtotime($arriving)) . " to ";
  $subject .= date("F j, Y", strtotime($leaving))  . ")";

  $message  = "<!DOCTYPE html>\r\n";
  $message .= "<html lang='en-US'>\r\n";
  $message .= " <head>\r\n";
  $message .= "  <meta charset='UTF-8' />\r\n";
  $message .= "  <meta name='generator'\r\n";
  $message .= "        content='Linux, Emacs and me' />\r\n";
  $message .= "  <meta name='author'\r\n";
  $message .= "        content='Kevin Cole' />\r\n";
  $message .= "  <link rel='license'\r\n";
  $message .= "        title='[Kevin Cole]'\r\n";
  $message .= "        href='http://research.gallaudet.edu/~kjcole/license/notice.php' />\r\n";
  $message .= "  <link rel='author'\r\n";
  $message .= "        title='[Kevin Cole]'\r\n";
  $message .= "        href='http://research.gallaudet.edu/~kjcole/' />\r\n";
  $message .= "<title>" . $subject . "</title>\r\n";
  $message .= "<style type='text/css'>\r\n";
  $message .= "body     {font-family: Verdana, Arial, Helvetica, sans-serif;\r\n";
  $message .= "          font-size:   16px;}\r\n";
  $message .= "fieldset {margin:           10px;\r\n";
  $message .= "          background-color: #dfffff;\r\n";
  $message .= "          border:           thin solid #ff7f7f;}\r\n";
  $message .= "legend   {background-color: #ffffc0;\r\n";
  $message .= "          border:           thin solid #ff7f7f;\r\n";
  $message .= "          color:            #ff3f3f;\r\n";
  $message .= "          font-weight:      bold;}\r\n";
  $message .= ".center  {margin:           1em auto;\r\n";
  $message .= "          text-align:       center;}\r\n";
  $message .= "table, tbody, tr, th, td { border: none; }\r\n";
  $message .= "</style>\r\n";
  $message .= "<head>\r\n";
  $message .= "<body itemscope itemtype='http://schema.org/WebPage'>\r\n";
  $message .= "<article id='essence' role='main'\r\n";
  $message .= "         style='margin:0px auto;'>\r\n";

  $message .= "<div style='border:1px solid black; width:715px; padding:20px;'>\r\n";
  $message .= "<div><img\r\n";
  $message .= " src='http://research.gallaudet.edu/images/logos/Gallaudet-750.png'\r\n";
  $message .= " alt='Gallaudet University' width='281' height='159'\r\n";
  $message .= " style='display:block; margin:auto; width:281px; height:150px' />\r\n";
  $message .= "</div>\r\n";

  $message .= "<section style='page-break-after:always;'>\r\n";
  $message .= "<p style='text-align:right;'>" . date("F j, Y") . "</p>\r\n";
  $message .= "<p>Dear " . $_POST["requester_name"] . ",</p>\r\n";
  $message .= "<p>Your request for special support while visiting";
  $message .= " Gallaudet University has been received.  Please print this";
  $message .= " receipt. We will inform you soon as to whether our office";
  $message .= " can support your visit by arranging for speakers and class";
  $message .= " observations and such. If we can support your visit, we will";
  $message .= " provide up to two (2) days support.</p>\r\n";
  $message .= "<p>Send all correspondence to email &lt;<a";
  $message .= " href='mailto:intl.visit@gallaudet.edu'>intl.visit@gallaudet.edu</a>&gt;.</p>\r\n";
  if ($_POST["bring"] == "true") {
    $message .= "<p>Based upon the information you have provided, it";
    $message .= " appears that you need sign language interpreters";
    $message .= " (<em>American Sign Language &larr;&rarr;";
    $message .= " spoken English, your spoken language or sign";
    $message .= " language</em>).";
    $message .= " <strong>You must bring these interpreters or we can hire";
    $message .= " for you at a cost to be determined.  We can provide two";
    $message .= " hours of interpreting without charge.</p>\r\n";
  }
  $message .= "<p>Of course, you may visit Gallaudet University on";
  $message .= " your own without our support. You can find information";
  $message .= " about visiting the campus at";
  $message .= " <a href='http://www.gallaudet.edu/international_relations/visitor_information.html'>http://www.gallaudet.edu/international_relations/visitor_information.html</a></p>\r\n";
  $message .= "<p>At this website, you can make your own reservations";
  $message .= " for housing on campus.</p>\r\n";
  $message .= "<p>The full record of your request appears below,";
  $message .= " at the end of this messsage.</p>\r\n";
  $message .= "<p>Thank you for your interest in Gallaudet University.</p>\r\n";
  $message .= "<p>--&nbsp;<br />\r\n";
  $message .= "The Office of International Programs &amp; Services<br />\r\n";
  $message .= "Dawes House, Gallaudet University</p>\r\n";
  $message .= "</section>\r\n";

  $message .= "<section style='page-break-after:always;'>\r\n";
  $message .= "<h1 style='text-align:center;'>Receipt</h1>\r\n";
  $message .= "<fieldset style='" . $sfs . "'>\r\n";
  $message .= "<legend style='" . $sl . "'>General Information</legend>\r\n";
  $message .= "<table style='table-layout:fixed; width:640px;'>\r\n";
  $message .= "<tbody>\r\n";
  $message .= "<tr><td style='width:30%;'>Arrival date:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["arr_date"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Arrival time:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["arr_time"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Departure date:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["dep_date"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Departure time:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["dep_time"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Requester:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["requester_name"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Organization:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["requester_org"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Email address:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["requester_email"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Phone number:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["requester_phone"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Country:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["country"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Number of visitors:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["visitors"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Number&nbsp;of&nbsp;English&nbsp;speakers:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["speakers"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Number&nbsp;of&nbsp;ASL&nbsp;users:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["signers"] . "</td></tr>\r\n";
  if ($_POST["bring"] == "true") {
    $message .= "<tr><td colspan='2'><strong>You";
    $message .= " <span style='text-decoration:underline'>MUST</span>";
    $message .= " bring your own interpreter(s).</strong></td></tr>";
  }
  $message .= "<tr><td style='width:30%;'>Purpose of visit:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["purpose"] . "</td></tr>\r\n";
  $message .= "<tr><td style='width:30%;'>Need letters?:</td>\r\n";
  $message .= "<td style='width:70%;'>";
  $message .= $_POST["letters"] . "</td></tr>\r\n";
  $message .= "</tbody>\r\n";
  $message .= "</table>\r\n";
  $message .= "</fieldset>\r\n";
  $message .= "</section>\r\n";

  if ($_POST["letters"] == "Y") {
    $message .= "<section style='page-break-after:always;'>\r\n";
    $visitors = $_POST["visitors"];
    for ($visitor = 1; $visitor < $visitors+1; ++$visitor) {
      $message .= bio($visitor);
    }
    $message .= "</section>\r\n";
  }

  $message .= "<section style='page-break-after:always;'>\r\n";
  $message .= "<p";
  $message .= " style='font-size:10pt; font-weight:bold; color:#00447C; text-align:center; vertical-align:top; padding-top:6px;'>";
  $message .= "Dawes House &bull; ";
  $message .= "800 Florida Avenue, NE &bull; ";
  $message .= "Washington, DC 20002-3695\r\n<br />";
  $message .= "202-651-5575 &bull; ";
  $message .= "intl.visit@gallaudet.edu &bull; ";
  $message .= "www.gallaudet.edu\r\n";
  $message .= "</p>\r\n";
  $message .= "</section>\r\n";

  $message .= "</div>";
  $message .= "</article>\r\n";
  $message .= "</body>\r\n";
  $message .= "</html>\r\n";

  mail($to, $subject, $message, $headers);
}
?>

<h1 class="center">INTERNATIONAL VISITOR REQUEST FORM</h1>

<p>Your request is on its way!</p>

<p style="text-align:center; font-size:x-small;">[Last modified:
<?php echo date("Y.m.d H:i:s.", getlastmod()); ?> by <a 
href="http://www.gallaudet.edu/Faculty-Staff/Gallaudet_Research_Institute/Cole_Kevin.html">Kevin
Cole</a>]</p>

<?php gufooter(); ?>
