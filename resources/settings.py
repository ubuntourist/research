# Django settings for resources project.
# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2012.05.30
#
# 2010.03.09 KJC - Modernized the format to match 1.2.0 beta
# 2011.03.28 KJC - Added a username and password to the databases default...
#                  but I don't understand why it used to work...
# 2011.05.06 KJC - Added "django.db.backends." prefix to ENGINE
# 2012.02.22 KJC - Merged /psite/settings.py with this document.
# 2012.03.09 KJC - Set DEBUG to False finally
# 2012.03.19 KJC - Installing Research at Gallaudet University (ragu) app
# 2012.05.02 KJC - Installing Preliminary Screening Survey (screening) app
# 2012.05.30 KJC - Installing EXPERIMENTAL WYSIWYG editor (django-wysiwyg)
#                  See http://pypi.python.org/pypi/django-wysiwyg
# 2012.08.29 KJC - Adding django-extensions
# 2013.12.02 KJC - Added international visitors registration
# 2013.12.07 KJC - Changed EMAIL_HOST from smtp.gmail.com to localhost
# 2013.12.07 KJC - Commented out the other EMAIL_* settings
#

import os

SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Kevin Cole', 'kevin.cole@gallaudet.edu'),
    ('Kevin Cole', 'kjcole@research.gallaudet.edu'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME':     'django',        # Or path to database file if using sqlite3.
        'USER':     'apache',      # Not used with sqlite3.
        'PASSWORD': 'PeterParker',   # Not used with sqlite3.
        'HOST':     '',              # Set to empty string for localhost. Not used with sqlite3.
        'PORT':     '',              # Set to empty string for default. Not used with sqlite3.
    }
}

# 2008.04.15 KJC - Override email defaults from /usr/lib/django/conf/global_settings.py
# 2013.12.07 KJC - Redo EMAIL settings in light of system-wide handling
#

EMAIL_HOST      = 'localhost'  # was "smtp.gmail.com"
#EMAIL_USE_TLS   = True
#EMAIL_PORT      = 587
#EMAIL_HOST_USER = 'kevin.cole@gallaudet.edu'
#EMAIL_HOST_PASSWORD = ''

# Local time zone for this installation. Choices can be found here:
#
#     http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
#
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.

# If running in a Windows environment this must be set to the same as
# your system time zone.
#
TIME_ZONE = 'America/New_York'

# Language code for this installation. All choices can be found here:
#
#     http://www.i18nguy.com/unicode/language-identifiers.html
#
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
#
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
#
USE_L10N = True

# Absolute path to the directory will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/"
#
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
#
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
#
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
#
STATIC_URL = '/static/'

# Additional locations of static files
#
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
#
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#   'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
#
SECRET_KEY = 'l#o%cnd-7uxm#d+r2ipdohl^=g9o98-m=pdxokujls!+ekitdb'

# List of callables that know how to import templates from various sources.
#
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#   'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'resources.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = "resources.wsgi.application"

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates"
    # or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(SITE_ROOT, "templates"),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django_wysiwyg',
    'django_extensions',
    'resources.books',
    'resources.mhd',
    'resources.ragu',
    'resources.screening',
    'resources.visitors',
)

DJANGO_WYSIWYG_FLAVOR = "ckeditor"

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
