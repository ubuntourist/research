# Written by Kevin Cole <kjcole@gri.gallaudet.edu> 2008.07.14
#

from models       import Provider
from django       import forms

class ProviderForm(forms.ModelForm):

    class Meta:
        model = Provider
