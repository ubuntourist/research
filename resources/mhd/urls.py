# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2013.07.16
#
# 2012.03.30 KJC - Switching to generic views for static pages
# 2013.07.16 KJC - Switching to class-based generic views from function-based
#

from django.conf.urls.defaults import *
from resources.mhd.views       import *
from django.views.generic.base import TemplateView

urlpatterns = patterns('',
    (r"^$",                   TemplateView.as_view(template_name="mhd/index.html")),
    (r"^copyright/$",         TemplateView.as_view(template_name="mhd/copyright.html")),
    (r"^preface/$",           TemplateView.as_view(template_name="mhd/preface.html")),
    (r"^introduction/$",      TemplateView.as_view(template_name="mhd/introduction.html")),
    (r"^howto/$",             TemplateView.as_view(template_name="mhd/howto.html")),
    (r'^listings/$',          listings),
    (r'^search/$',            search),
    (r'^details/(\d{1,3})/$', details),
    (r"^updates/$",           TemplateView.as_view(template_name="mhd/updates.html")),
    (r"^communication/$",     TemplateView.as_view(template_name="mhd/communication.html")),
    (r"^solicitation/$",      TemplateView.as_view(template_name="mhd/solicitation.html")),
    (r"^conference/$",        TemplateView.as_view(template_name="mhd/conference.html")),
    (r'^add/provider/$',      add_provider),
)
