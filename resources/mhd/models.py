# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2009.01.12
#
# 2008.03.25 KJC - Auto-generated this file
# 2008.04.08 KJC - Added in __unicode__ order and admin details, as well
#                  as modifying some field type guesses.  Commented
#                  out the map shape models (geoshapes, usmap). Added
#                  docstrings.
# 2008.07.20 KJC - REMOVED all the admin stuff (now in admin.py)
# 2008.10.31 KJC - Allow nulls (and blanks) in most fields
# 2008.12.01 KJC - Commentd out "id" fields. Django wants to do it.
# 2009.01.12 KJC - Added state index and service_context index.
# 2009.04.12 KJC - Changed country and state in Provider model to
#                  foreign keys referencing Country and State models,
#                  added unique constraints and indexes too.
# 2009.04.12 KJC - Rearranged the model order to eliminate forward
#                  references caused by adding the foreign keys.
#
# This is an auto-generated Django model module created by the command:
#
# $ python manage.py inspectdb > generated.py
#
# See Chapter 16, page 236 for details.
#
# You'll have to do the following manually to clean this up:
#
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
#
# Feel free to rename the models, but don't rename db_table values or 
# field names.
#
# Also note: You'll have to insert the output of 
#
#     'django-admin.py sqlcustom [appname]'
#
# into your database.

from django.db import models

STATUS_CHOICES = ((0,"Draft"),
                  (1,"Pending"),
                  (2,"Published"),
                  (3,"Rejected"),
                  (4,"Obsolete"),)

ENTRY_TYPES = ((1,"Private practitioner"),
               (2,"Program"),)

COUNTRIES = (("US","United States"),
             ("CA","Canada"),)

PHONE_TYPES = ((1,"Voice"),
               (2,"TDD"),
               (3,"Video"),
               (4,"Cell (SMS)"),
               (5,"Pager"),)

YORN = ((False,"No"),
        (True,"Yes"),)

CONTEXTS = ((1,"Private Practice"),
            (2,"Psychiatric Hospital"),
            (3,"Residential Treatment Center for Emotionally Disturbed Children"),
            (4,"Outpatient Mental Health Clinic or Agency"),
            (5,"Alcohol/Substance Abuse Treatment Program"),
            (6,"Mental Health Day/Night Facility"),
            (7,"Multi-service Mental Health Facility"),
            (8,"Vocational/Rehabilitation/ILS Program"),
            (9,"School/University Program"),
            (10,"Other"),)

OVERSEERS = ((1,"State"),
             (2,"City"),
             (3,"County"),
             (4,"City-County"),
             (5,"State and County"),
             (6,"Hospital District or Authority"),
             (7,"Veterans Administration"),
             (8,"For Profit: Individual"),
             (9,"For Profit: Partnership"),
             (10,"For Profit: Corporation"),
             (11,"Nonprofit: Church-related"),
             (12,"Not-for-profit Corporation"),
             (13,"Other Nonprofit"),
             (14,"Other"),)

ACCESSIBLES = ((1,"All"),
               (2,"Some"),
               (3,"None"),)

class Country(models.Model):
    """ISO country names, alpha abbreviations, and numeric id's"""
    id   = models.PositiveSmallIntegerField(primary_key=True)
    name = models.CharField(max_length=70)
    iso3 = models.CharField(max_length=3, unique=True)
    iso2 = models.CharField(max_length=2, unique=True)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = u"mhd_country"
        ordering = ["name"]

class State(models.Model):
    """State names, USPS abbreviations, and owner country"""
    id      = models.PositiveSmallIntegerField(primary_key=True)
    country = models.ForeignKey(Country, db_column="country")
    usps    = models.CharField(max_length=2, unique=True)
    name    = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = u"mhd_state"
        ordering = ["name"]

class City(models.Model):
    """Cities and their coresponding owner states"""
    id    = models.AutoField(primary_key=True)
    state = models.ForeignKey(State, db_column="state")
    name  = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = u"mhd_city"
        ordering = ["name"]

class Provider(models.Model):
    """Mental Health Providers data"""
#   id                   = models.PositiveSmallIntegerField("ID",
#                                                           primary_key=True,
#                                                           editable=False)
    entered              = models.DateTimeField("Date entered",
                                                help_text="Date entered",
                                                auto_now=True,
                                                editable=False)
    status               = models.PositiveSmallIntegerField("Status",
                                                            default=2,
                                                            help_text="Publication status",
                                                            choices=STATUS_CHOICES)
    sort_key             = models.PositiveSmallIntegerField("Sort key",
                                                            null=True, blank=True,
                                                            help_text="Sort key",
                                                            editable=False)
    entry_type           = models.PositiveSmallIntegerField("Entry type",
                                                            help_text="Program or Practitioner?",
                                                            choices=ENTRY_TYPES)
    practitioner_ln      = models.CharField("Practitioner last name",
                                            max_length=30,
                                            blank=True,
                                            help_text="Last name ONLY")
    practitioner_fn      = models.CharField("Practitioner first name",
                                            max_length=30,
                                            blank=True,
                                            help_text="First name ONLY")
    practitioner_mi      = models.CharField("Practitioner middle initial",
                                            max_length=1,
                                            blank=True,
                                            help_text="Middle initial ONLY")
    practitioner_deg     = models.CharField("Practitioner degrees",
                                            max_length=30,
                                            blank=True,
                                            help_text="DO NOT use periods. Separate w/ commas: 'PhD, LSW' not 'Ph.D; L.S.W.")
    program              = models.CharField("Program",
                                            max_length=70,
                                            blank=True,
                                            help_text="Program or department")
    institution          = models.CharField("Host Institution",
                                            max_length=70,
                                            blank=True,
                                            help_text="Institution or agency")
    street               = models.CharField("Street address",
                                            max_length=70,
                                            help_text="Street address")
    suite                = models.CharField("Suite, building or room",
                                            max_length=70,
                                            blank=True,
                                            help_text="Suite, building, or room")
    city                 = models.CharField("City",
                                            max_length=70,
                                            help_text="City")
    state                = models.ForeignKey(State, db_column="state", to_field="usps", 
                                             db_index=True,
                                             verbose_name="State or Province",
                                             help_text="State or Province")
    zip                  = models.CharField("Postal Code",
                                            help_text="Zip or Postal code",
                                            max_length=10)
    country              = models.ForeignKey(Country, db_column="country", to_field="iso2", 
                                             db_index=False,
                                             verbose_name="Country",
                                             help_text="US or CA",
                                             limit_choices_to = {"iso2__in": ["US","CA"]})
    phone_voice          = models.CharField("Phone (voice)",
                                            max_length=30,
                                            blank=True,
                                            help_text="999-999-9999 ext. 9999")
    phone_tdd            = models.CharField("Phone (TDD)",
                                            max_length=30,
                                            blank=True,
                                            help_text="999-999-9999 ext. 9999")
    phone_fax            = models.CharField("Phone (FAX)",
                                            max_length=30,
                                            blank=True,
                                            help_text="999-999-9999 ext. 9999")
    phone_video          = models.CharField("Phone (video)",
                                            max_length=30,
                                            blank=True,
                                            help_text="999-999-9999 ext. 9999")
    url                  = models.URLField("Site URL",
                                           max_length=80,
                                           blank=True,
                                           help_text="http://host.domain.tld/...")
    contact1_ln          = models.CharField("Contact 1 last name",
                                            max_length=30,
                                            blank=True,
                                            help_text="Last name ONLY")
    contact1_fn          = models.CharField("Contact 1 first name",
                                            max_length=30,
                                            blank=True,
                                            help_text="First name ONLY")
    contact1_mi          = models.CharField("Contact 1 middle initial",
                                            max_length=1,
                                            blank=True,
                                            help_text="Middle initial ONLY")
    contact1_deg         = models.CharField("Contact 1 degrees",
                                            max_length=30,
                                            blank=True,
                                            help_text="DO NOT use periods. Separate w/ commas: 'PhD, LSW' not 'Ph.D; L.S.W.")
    contact1_job         = models.CharField("Contact 1 job title",
                                            max_length=70,
                                            blank=True,
                                            help_text="Job title or position")
    contact1_eml         = models.EmailField("Contact 1 e-mail",
                                             max_length=70,
                                             blank=True,
                                             help_text="username@host.domain.tld")
    contact2_ln          = models.CharField("Contact 2 last name",
                                            max_length=30,
                                            blank=True,
                                            help_text="Last name ONLY")
    contact2_fn          = models.CharField("Contact 2 first name",
                                            max_length=30,
                                            blank=True,
                                            help_text="First name ONLY")
    contact2_mi          = models.CharField("Contact 2 middle initial",
                                            max_length=1,
                                            blank=True,
                                            help_text="Middle initial ONLY")
    contact2_deg         = models.CharField("Contact 2 degrees",
                                            max_length=30,
                                            blank=True,
                                            help_text="DO NOT use periods. Separate w/ commas: 'PhD, LSW' not 'Ph.D; L.S.W.")
    contact2_job         = models.CharField("Contact 2 job title",
                                            max_length=70,
                                            blank=True,
                                            help_text="Job title or position")
    contact2_eml         = models.EmailField("Contact 2 e-mail",
                                             max_length=70,
                                             blank=True,
                                             help_text="username@host.domain.tld")
    weekday_hours        = models.CharField("Weekday hours",
                                            max_length=60,
                                            blank=True,
                                            help_text="Weekday hours")
    weekend_hours        = models.CharField("Weekend hours",
                                            max_length=60,
                                            blank=True,
                                            help_text="Weekend hours")
    emergency_contact    = models.CharField("Emergency contact information",
                                            max_length=70,
                                            blank=True,
                                            help_text="Name, phone or e-mail")
    service_context      = models.PositiveSmallIntegerField("Service context",
                                                            db_index=True,
                                                            null=True, blank=True,
                                                            help_text="Primary service provided",
                                                            choices=CONTEXTS)
    other_context        = models.CharField("Specify:",
                                            max_length=70,
                                            blank=True,
                                            help_text="Other service context")

    oversight            = models.PositiveSmallIntegerField("Oversight agency",
                                                            null=True, blank=True,
                                                            help_text="Reviews and funding provider",
                                                            choices=OVERSEERS)
    other_oversight      = models.CharField("Specify:",
                                            max_length=30,
                                            blank=True,
                                            help_text="Other organization")
    accreditation        = models.TextField("Agency accreditation",
                                            blank=True)
    licensure            = models.TextField("Licensure",
                                            blank=True,
                                            help_text="Certification / Licensure")
    established          = models.PositiveSmallIntegerField("Year deaf program established",
                                                            null=True, blank=True,
                                                            help_text="Four-digit year ONLY")
    private              = models.BooleanField("Private Practice",
                                               default="f")
    inpatient            = models.BooleanField("Inpatient Treatment",
                                               default="f")
    beds                 = models.PositiveSmallIntegerField("Number of beds",
                                                            null=True, blank=True)
    outpatient           = models.BooleanField("Outpatient Treatment",
                                               default="f")
    hospitalization      = models.BooleanField("Other Partial Hospitalization",
                                               default="f")
    day_treatment        = models.BooleanField("Day Treatment",
                                               default="f")
    addiction_recovery   = models.BooleanField("Addiction Recovery",
                                               default="f")
    halfway_house        = models.BooleanField("Halfway House",
                                               default="f")
    supported_living     = models.BooleanField("Supported Independent Living",
                                               default="f")
    vocational           = models.BooleanField("Day Program/Vocational Program",
                                               default="f")
    emergency            = models.BooleanField("Emergency Services",
                                               default="f")
    videophone           = models.BooleanField("Videophone Service",
                                               default="f")
    fee                  = models.BooleanField("Fee?",
                                               default="f",
                                               help_text="Is there a fee for deaf clients?",
                                               choices=YORN)
    fee_covered          = models.BooleanField("Fee covered?",
                                               default="f",
                                               help_text="Is the fee covered by a 3rd party?",
                                               choices=YORN)
    sliding_scale        = models.BooleanField("Sliding scale?",
                                               default="f",
                                               help_text="Is there an adjustable fee?",
                                               choices=YORN)
    staff                = models.PositiveSmallIntegerField("Total staff",
                                                            null=True, blank=True,
                                                            help_text="Total number of staff")
    signers              = models.PositiveSmallIntegerField("Signing staff",
                                                            null=True, blank=True,
                                                            help_text="How many are fluent signers?")
    deaf                 = models.BooleanField("Deaf staff?",
                                               default="f")
    hearing              = models.BooleanField("Hearing staff?",
                                               default="f")
    hoh                  = models.BooleanField("Hard of hearing staff?",
                                               default="f")
    coda                 = models.BooleanField("CODA staff?",
                                               default="f")
    nah                  = models.BooleanField("No staff??",
                                               default="f")
    interpreting         = models.BooleanField("Interpreting provided?",
                                               default="f",
                                               choices=YORN)
    terp_details         = models.TextField("Interpreting details",
                                            blank=True)
    devices_tdd          = models.BooleanField("TTY/TDD?",
                                               default="f",
                                               choices=YORN)
    devices_amp          = models.BooleanField("Amplified phone?",
                                               default="f",
                                               choices=YORN)
    devices_flasher      = models.BooleanField("Flashing phone/doorbell?",
                                               default="f",
                                               choices=YORN)
    devices_alarm        = models.BooleanField("Visual alarm?",
                                               default="f",
                                               choices=YORN)
    devices_none         = models.BooleanField("No devices?",
                                               default="f",
                                               choices=YORN)
    wheelchair           = models.BooleanField("Wheelchair accessiblity",
                                               default="f",
                                               help_text="Facilities accessible by wheelchair?",
                                               choices=YORN)
    transit              = models.BooleanField("Public transit",
                                               default="f",
                                               help_text="Facilities accessible by public transit?",
                                               choices=YORN)
    comments             = models.TextField("Additional comments",
                                            blank=True)
    passphrase           = models.CharField("Unused",
                                            max_length=70,
                                            blank=True,
                                            help_text="Future passphrase")
    garbage              = models.TextField("Left over garbage",
                                            blank=True,
                                            help_text="Bad data moved from other fields")

    def __unicode__(self):
        """__unicode__: represent instance as either practitioner or institution"""
        if len(self.practitioner_ln) > 0:
            display = self.practitioner_ln
            if len(self.practitioner_fn) > 0:
                display += ", %s" % (self.practitioner_fn,)
            if len(self.practitioner_mi) > 0:
                display += " %s." % (self.practitioner_mi,)
        else:
            display = self.institution
        return display

    def indexer(self):
        """indexer: a pseudo-field for sort and display"""
        if len(self.practitioner_ln) > 0:
            display = self.practitioner_ln
            if len(self.practitioner_fn) > 0:
                display += ", %s" % (self.practitioner_fn,)
            if len(self.practitioner_mi) > 0:
                display += " %s." % (self.practitioner_mi,)
        else:
            display = self.institution
        return display
    indexer.short_description = 'Provider'

    class Meta:
        db_table = u"mhd_provider"
        ordering = ["-country","state","city","sort_key"]
