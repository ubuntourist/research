# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2008.07.20
#
# Extracted, sort of, from models.py

from django.contrib import admin
from resources.mhd.models import Provider

class ProviderAdmin(admin.ModelAdmin):
    """ProviderAdmin: display/edit MHD provider info in admin interface"""
    list_display = ("indexer", "city", "state", "country",)
    list_filter = ("country", "state",)
    ordering = ("-country", "state",)
    search_fields = ("practitioner_ln","program","institution",)
    radio_fields = {"entry_type":      admin.HORIZONTAL,
                    "service_context": admin.VERTICAL,
                    "oversight":       admin.VERTICAL}

    fieldsets = (("ADDRESS INFORMATION", 
                     {"classes": ("collapse",),
                      "fields": ("entry_type",
                                 ("practitioner_ln","practitioner_fn",
                                  "practitioner_mi",),
                                 "practitioner_deg",
                                 "program","institution",
                                 "street", "suite",
                                 ("city","state","zip",),"country",)}),
                 ("PHONE NUMBERS",
                     {"classes": ("collapse",),
                      "fields": ("phone_voice","phone_tdd","phone_fax",
                                 "phone_video",
                                 "url",)}),
                 ("PERSONNEL", 
                     {"classes": ("collapse",),
                      "fields": (("contact1_ln","contact1_fn",
                                  "contact1_mi"), "contact1_eml",
                                 "contact1_deg", "contact1_job",
                                 ("contact2_ln","contact2_fn",
                                  "contact2_mi"), "contact2_eml",
                                 "contact2_deg", "contact2_job",)}),
                 ("HOURS / EMERGENCY",
                     {"classes": ("collapse",),
                      "fields": ("weekday_hours","weekend_hours",
                                 "emergency_contact",)}),
                 ("PROGRAM TYPE",
                     {"classes": ("collapse",),
                      "fields": ("service_context","other_context",)}),
                 ("ACCREDITATION, LICENSING, and OVERSIGHT",
                     {"classes": ("collapse",),
                      "fields": ("oversight", "other_oversight",
                                 "accreditation",
                                 "licensure","established",)}),
                 ("OTHER SERVICES",
                     {"classes": ("collapse",),
                      "fields": (("private","inpatient","outpatient",
                                  "hospitalization","day_treatment",
                                  "addiction_recovery",),
                                 ("halfway_house","supported_living",
                                  "vocational","emergency","videophone",),
                                 "beds",)}),
                 ("FEES",
                     {"classes": ("collapse",),
                      "fields": ("fee","fee_covered","sliding_scale",)}),
                 ("SERVICE PROVIDERS",
                     {"classes": ("collapse",),
                      "fields": (("staff","signers",),
                                 ("deaf","hoh","hearing","coda","nah",),)}),
                 ("INTERPRETING and ASSISTIVE DEVICES",
                     {"classes": ("collapse",),
                      "fields": (("interpreting","terp_details",),
                                 "devices_tdd",
                                 "devices_amp","devices_flasher",
                                 "devices_alarm","devices_none",)}),
                 ("ACCESSIBILTY",
                     {"classes": ("collapse",),
                      "fields": ("wheelchair","transit",)}),
                 ("Miscellaneous",
                     {"classes": ("collapse",),
                      "fields": ("comments","passphrase","garbage",)}),
                 )

admin.site.register(Provider, ProviderAdmin)
