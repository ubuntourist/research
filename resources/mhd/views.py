# Written by Kevin Cole <kjcole@gri.gallaudet.edu> 2012.03.30
#
# 2008.17.14 KJC - Initial attempts
# 2012.03.30 KJC - Switching to generic views for static pages
#
# This contains the functions to which URL's are mapped by 
# urls.py.
#

from django.db.models     import Q
from django.http          import HttpResponseRedirect, Http404
from django.shortcuts     import render_to_response
from django.core.mail     import send_mail
from resources.mhd.models import Provider, Country, State
from resources.mhd.forms  import ProviderForm
#from resources.books.forms import ContactForm, PublisherForm

def add_provider(request):
    """add_provider: a fantasy."""
    if request.method == "POST":
        form = ProviderForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/mhd/add_provider/confirm")
    else:
        form = ProviderForm()
    return render_to_response("mhd/add_provider.html", {"form": form})

def search(request):
    """search - returns a city/state/provider list for  all records where state = query"""
    query = request.GET.get("state", "")       # Get either "ST" or an empty string.
    if query:                                  # If it's not an empty string...
        qset = (
            Q(state__usps__iexact=query)
#           Q(state__icontains=query) |
#           Q(authors__first_name__icontains=query) |
#           Q(authors__last_name__icontains=query)
        )                                      # ...query mhd_provider for that state
        results = Provider.objects.filter(qset).distinct()
    else:                                      # Otherwise...
        results = []                           # ...don't do squat
    return render_to_response("mhd/search.html",
                              {"results": results,
                               "query":   query})

def sos(request):
    """Son of Search"""
    query = request.GET.get("state", "")       # Get either "ST" or an empty string.
    if query:                                  # If it's not an empty string...
        qset = (
            Q(state__usps__iexact=query)
#           Q(state__icontains=query) |
#           Q(authors__first_name__icontains=query) |
#           Q(authors__last_name__icontains=query)
        )                                      # ...query mhd_provider for that state
        results = Provider.objects.filter(qset).distinct()
    else:                                      # Otherwise...
        results = []                           # ...don't do squat
    return render_to_response("mhd/search.html",
                              {"results": results,
                               "query":   query})

def details(request, record_id):
    """details: returns, in a detail view, all fields for a single record"""
    qset = (Q(id=record_id))
    results = Provider.objects.filter(qset).distinct()
    if results.count() == 0:       # No record found
        raise Http404
#   googlemap = "street city state zip country (practitioner institution)"
#   render_to_response wants second argument to be a context dictionary?
#   c = Context({"name": "Stephane"})
    entity = ""
    if results[0].practitioner_ln:
        entity = results[0].practitioner_ln
        if results[0].practitioner_fn:
            entity += ", %s" % (results[0].practitioner_fn,)
        if results[0].practitioner_mi:
            entity += " %s." % (results[0].practitioner_mi,)
    else:
        entity = results[0].institution
    google = (("%s "*5) + "(%s)") % (results[0].street,
                                     results[0].city, results[0].state,
                                     results[0].zip,  results[0].country,
                                     entity)
    google = google.replace(" ","%20")
    return render_to_response("mhd/details.html",
                              {"results": results,
                               "google":  google})

def listings(request):
    """listings: sorted list of links to detail listings"""
    providers = Provider.objects.select_related().all()
    return render_to_response("mhd/listings.html",
                              {"providers": providers})
