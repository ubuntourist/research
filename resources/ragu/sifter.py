#!/usr/bin/env python
# Written by Kevin Cole <kjcole@gallaudet.edu> 2012.10.09
#
# This is an attempt to write an HTML interpreter that strips
# tags out and converts entities to ... Unicode?  This will give
# us something consistently sortable. I hope.
#

import re
from   BeautifulSoup import BeautifulSoup, BeautifulStoneSoup

def strain(html):
    """Expects an HTML string. Returns a plaintext UTF-8 encoded string."""
    soup = BeautifulSoup(html,
                         convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
    plain = "".join(soup.findAll(text=re.compile(".*")))
    return plain.encode("utf-8")

def dearticle(item):
    """Returns a lowercase string with initial articles removed"""
    whateva = strain(item.lower())     # Lower case, Unicode, no HTML
    if   whateva[:4] == "the ": whateva = whateva[4:]
    elif whateva[:3] == "an ":  whateva = whateva[3:]
    elif whateva[:2] == "a ":   whateva = whateva[2:]
    return whateva

if __name__ == "__main__":

    strings = ["Andrew is an android",
               "What a quartet",
               "<em>Theo</em> is not an android",
               "The android is Andrew",
               "Sally is an animate lifeform",
               "an inanimate android, Sally is not",
               "&Aacute;ndr&eacute; spells his name weird",
               "A Droid for Andrew, Theo and Sally",
               "&Igrave;ngred also cannot spell"]


    pairs = [(1,"Andrew is an android",),
             (2,"What a quartet",),
             (3,"<em>Theo</em> is not an android",),
             (4,"The android is Andrew",),
             (5,"Sally is an animate lifeform",),
             (6,"an inanimate android, Sally is not",),
             (7,"&Aacute;ndr&eacute; spells his name weird",),
             (8,"A Droid for Andrew, Theo and Sally",),
             (9,"&Igrave;ngred also cannot spell",)]

    print "%s\n" % ("_"*78,)

    for string in strings:
        print string
    print "%s\n" % ("_"*78,)

    strings.sort(key=dearticle)      # Use 2nd of pair as sort key

    for string in strings:
        print string
    print "%s\n" % ("_"*78,)

    for string in strings:
        print strain(string)
    print "%s\n" % ("_"*78,)

    newpairs = sorted(pairs, key=lambda record: dearticle(record[1]))

    for pair in pairs:
        print pair
    print "%s\n" % ("_"*78,)

    for pair in newpairs:
        print pair
    print "%s\n" % ("_"*78,)
