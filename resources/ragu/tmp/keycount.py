#!/usr/bin/env python
# keycount.py
# Written by Kevin Cole <kjcole@gallaudet.edu> 2012.12.10
#
# This is just to diagnose which keys are contained in the LDAP records,
# and in what quantity.

keez   = {}
oldval = ""

fin = open("ldap2.log","r")
for line in fin:
    kee, crap, newval = line.partition(": ")
    if kee in keez:
        keez[kee][0] += 1
        if newval != oldval:
            keez[kee][1] += 1
            oldval = newval
    else:
        keez[kee] = [1,1]
fin.close()

fout = open("ldap3.log", "w")
fout.write(" keys  uniq keyname\n")
fout.write("%5s %5s %64s\n" % ("-"*5, "-"*5, "-"*64,))
for kee in keez:
    fout.write("%5d %5d %s\n" % (keez[kee][0], keez[kee][1], kee,))
fout.close()

fin = open("ldap2.log","r")
valyouz = {}
for line in fin:
    kee, crap, newval = line.partition(": ")
    if (kee in keez) and (keez[kee][1] < 250):
        if kee in valyouz:
            valyouz[kee].append(newval)
        else:
            valyouz[kee] = [newval]
fin.close()
for valyou in valyouz:
    valyouz[valyou].sort()

keez = valyouz.keys()
keez.sort()

fout = open("ldap4.log", "w")
for kee in keez:
    fout.write("%s:\n" % (kee,))
    oldval = ""
    for valyou in valyouz[kee]:
        if valyou != oldval:
            fout.write("....%s\n" % (valyou[:110],))
            oldval = valyou
fout.close()
