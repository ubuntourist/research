-- Written by Kevin Cole <kjcole@gallaudet.edu> 2012.11.13
--
-- Switch the foreign key pointer in the project table from the
-- department table (a many to one relationship) to the stakeholder
-- table (a many to many relationship) with benefits.

ALTER TABLE ONLY public.ragu_shareholder
  DROP CONSTRAINT ragu_shareholder_project_id_fkey;
ALTER TABLE ONLY public.ragu_shareholder
  DROP CONSTRAINT ragu_shareholder_editor_id_fkey;
ALTER TABLE ONLY public.ragu_shareholder
  DROP CONSTRAINT ragu_shareholder_department_id_fkey;
DROP INDEX public.ragu_shareholder_project_id;
DROP INDEX public.ragu_shareholder_editor_id;
DROP INDEX public.ragu_shareholder_department_id;
ALTER TABLE ONLY public.ragu_shareholder
  DROP CONSTRAINT ragu_shareholder_project_id_department_id_key;
ALTER TABLE ONLY public.ragu_shareholder
  DROP CONSTRAINT ragu_shareholder_pkey;
ALTER TABLE public.ragu_shareholder ALTER COLUMN id
  DROP DEFAULT;
DROP SEQUENCE public.ragu_shareholder_id_seq;
DROP TABLE public.ragu_shareholder;

-- Generated from the following model in models.py by manage sqlall:
--
-- class Shareholder(models.Model):
--     """Mapping projects to departments with a share"""
--     changed  = models.DateTimeField("last changed",
--                                     auto_now=True, editable=False)
--     project    = models.ForeignKey(Project)
--     department = models.ForeignKey(InternalDepartment)
--     listed_under = models.BooleanField("Listed under?")
--     editor     = models.ForeignKey(Editor)
-- 
--     def __unicode__(self):
--         return "%s (%s)" % (self.project, self.department,)
-- 
--     class Meta:
--         unique_together = (("project","department"),)
--         get_latest_by = "changed"
--         verbose_name_plural = "shareholders"
-- 
-- ##########################################################################
--
BEGIN;
CREATE TABLE ragu_shareholder (
    id            serial    NOT NULL PRIMARY KEY,
    changed       timestamp with time zone NOT NULL,
    project_id    integer   NOT NULL REFERENCES ragu_project (id)
                            DEFERRABLE INITIALLY DEFERRED,
    department_id integer   NOT NULL REFERENCES ragu_department (id)
                            DEFERRABLE INITIALLY DEFERRED,
    listed_under  boolean   NOT NULL,
    editor_id     integer   NOT NULL REFERENCES ragu_editor (id)
                            DEFERRABLE INITIALLY DEFERRED,
    UNIQUE (project_id, department_id)
);

CREATE INDEX ragu_shareholder_project_id ON ragu_shareholder (project_id);
CREATE INDEX ragu_shareholder_department_id ON ragu_shareholder (department_id);
CREATE INDEX ragu_shareholder_editor_id ON ragu_shareholder (editor_id);
COMMIT;

-- Take care of table and sequence ownership and permissions:
--
ALTER TABLE public.ragu_activity OWNER TO kjcole;
ALTER TABLE public.ragu_activity_id_seq OWNER TO kjcole;
ALTER SEQUENCE ragu_activity_id_seq OWNED BY ragu_activity.id;
REVOKE ALL ON TABLE ragu_activity FROM PUBLIC;
REVOKE ALL ON TABLE ragu_activity FROM kjcole;
GRANT ALL ON TABLE ragu_activity TO kjcole;
GRANT ALL ON TABLE ragu_activity TO apache;
REVOKE ALL ON SEQUENCE ragu_activity_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ragu_activity_id_seq FROM kjcole;
GRANT ALL ON SEQUENCE ragu_activity_id_seq TO kjcole;
GRANT ALL ON SEQUENCE ragu_activity_id_seq TO apache;

-- Populate it
--
INSERT INTO ragu_shareholder
 SELECT nextval('ragu_shareholder_id_seq'), current_timestamp,
        id, department_id, 't', editor_id 
 FROM ragu_project;

------------------------------------------------------------------------------
-- BREAK the foreign key relationship in the project table.
--
ALTER TABLE ONLY public.ragu_project 
  DROP CONSTRAINT ragu_project_department_id_fkey;

