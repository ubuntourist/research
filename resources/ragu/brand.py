#!/usr/bin/env python
# Written by Kevin Cole <kjcole@gallaudet.edu> 2012.08.06
#
# Create "yearstamps" for projects and products.
#

import pgdb

# FY 2011 (stage = vetted by author, vetted by reviewer, reported in 2011,
#          the "live" version, and an actual project, not merely products.)

query  = "select ragu_project.id"
query += "  from ragu_project, ragu_report"
query += "  where     ragu_project.id = ragu_report.project_id"
query += "  and       ragu_project.stage = 2"
query += "  and       ragu_project.is_live"
query += "  and   not ragu_project.product_only"
query += "  and       ragu_report.reported = 2011"
query += "  order by ragu_project.id;"

# older? (stage = vetted by author, vetted by reviewer, NOT this
#         fiscal year, and an actual project, not merely products.)

query  = "select ragu_project.id"
query += "  from ragu_project, ragu_report"
query += "  where     ragu_project.id = ragu_report.project_id"
query += "  and       ragu_project.stage = 2"
query += "  and       ragu_project.is_live"
query += "  and   not ragu_project.product_only"
query += "  and       ragu_report.reported < 2011"
query += "  order by ragu_project.id;"


# complex query that will come in handy later?  This gem lists
# projects not in this fiscal year, sorted by department, together
# with the department's name.
#

query  = "select ragu_project.id, ragu_department.name, ragu_project.title"
query += "  from ragu_project, ragu_department, ragu_report"
query += "  where     ragu_project.id = ragu_report.project_id"
query += "  and       ragu_project.stage = 2"
query += "  and       ragu_project.is_live"
query += "  and   not ragu_project.product_only"
query += "  and       ragu_report.reported < 2011"
query += "  and       ragu_project.department_id = ragu_department.id"
query += "  order by  ragu_department.name, ragu_project.title;"
