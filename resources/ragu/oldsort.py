#!/usr/bin/env python
# Written by Kevin Cole <kjcole@gallaudet.edu> 2012.01.03
#
# A general outline of what we're trying to do here:
#
# 1. Get all of the HTML product descriptions into a list.
# 2. Make a copy and strip away the HTML, converting entities to Unicode.
# 3. Put tuples in a new list [(HTML,plaintext), (HTML,plaintext)...].
# 4. Sort the list keyed on the second element of each tuple.
# 5. For each tuple in the sorted new list, update the table, setting
#    sort_by = (new list index + 1) * 10.  The * 10 might not be
#    necessary, if we resort nightly, as long as the default value for
#    new inserts is ridiculous... maybe a negative number...
#

import pgdb
from   operator     import itemgetter  # For sort. Gets item from an iterable
from   parsimonious import strain

descriptions = []

rsca    = pgdb.connect("localhost:rsca:kjcole:In4$in4#.")
product = rsca.cursor()

query  = """select id, description         """
query += """  from "product"                  """
query += """  where description is not null;"""

product.execute(query)
print "%s records found." % (product.rowcount,)
for row in product.fetchall():
     id        = row[0]                   # Unique PostgreSQL internal ID
     html      = row[1]                   # Product description, HTML encoded
     plaintext = strain(html.lower())     # Lower case, Unicode, no HTML
     descriptions.append([id,plaintext])  # Store pairs to be sorted

descriptions.sort(key=itemgetter(1))      # Use 2nd of pair as sort key

# There's probably a fiendishly clever way to do the following with
# enumerate() and list() revolving around some variant of
#
#     list(enumerate(descriptions, start=1))
#
# But I could waste a lifetime figuring it out, or I could "get it done".

update  = """update product     """
update += """  set sort_by = %s """
update += """  where id = %s;  """

sort_by = 0
for row in descriptions:
    sort_by += 1
    id, description = row
#   product.execute(update % (sort_by,id))
    print sort_by, id, description[:70]     # DEBUG: Verify sort order
#rsca.commit()

# for row in results:
#    descriptions.append(row[0])

# """update "product"
#      set sort_by = %s
#      where description = '%s';""" % ((key * 10), product_des)
