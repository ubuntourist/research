#!/usr/bin/env python
#

import sys,ldap,ldap.resiter,ldap.sasl
from   getpass   import getpass # Non-echoing password prompter

class MyLDAPObject(ldap.ldapobject.LDAPObject,ldap.resiter.ResultProcessor):
  pass

server = "ldaps://dc01.ad.gallaudet.edu/"     # LDAP w/  SSL
server = "ldap://gallaudet-rr.gallaudet.edu/" # LDAP w/o SSL
con    = MyLDAPObject(server)
user   = "kevin.cole"
passwd = getpass()
token  = ldap.sasl.digest_md5(user,passwd)     # Make encrypted token
try:
    con.sasl_interactive_bind_s("",token)      # Secure synchronous bind
except:
    print "Fail!"

base_dn  = "OU=people,dc=ad,dc=gallaudet,dc=edu" # Base distinguished name
scope    = ldap.SCOPE_SUBTREE                    # Walk the subtree
fields   = ["sn","givenName","mail","description","title","department",]
lastname = ""

while lastname != "quit":
  lastname = raw_input("Last name: ")
  screen  = "(sn=%s*)" % (lastname,)
  msg_id = con.search(base_dn,scope,screen,fields) # Asynchronous search method

  for res_type,res_data,res_msgid,res_controls in con.allresults(msg_id):
    for dn,entry in res_data:
      # process dn and entry
      print dn,entry['sn']

con.unbind()                                # Unbind / close the connection
