-- Written by Kevin Cole <kjcole@gallaudet.edu> 2012.11.15
--
-- Creates a numeric sort key for a string by sorting the string
-- alphabetically while ignoring an initial article! Hot damn!
-- 

SELECT ROW_NUMBER() OVER (ORDER BY
  (CASE
     WHEN title ~* '^The ' THEN substring(title from 5)
     WHEN title ~* '^An '  THEN substring(title from 4)
     WHEN title ~* '^A '   THEN substring(title from 3)
     ELSE title
  END) ASC)*10 AS sortkey, title
  FROM ragu_project where is_live;
