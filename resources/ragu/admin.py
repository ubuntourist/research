# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2008.07.20
#
# Extracted, sort of, from models.py

from django                import forms
from django.contrib        import admin
from resources.ragu.models import Priority, Focus, Classification,        \
                                  Agency, Department, Program,            \
                                  InternalDepartment, ExternalDepartment, \
                                  ResearchCenter, NonCenter,       \
                                  Person, Profession, Investigator,       \
                                  Staff, InternalStaff, ExternalStaff,    \
                                  Funder, Funding, Report,                \
                                  Project, Product, Editor

from resources.ragu.sifter import * # Parse HTML to plaintext and sort

def mark_draft (modeladmin, request, queryset):
    queryset.update(stage=0)
mark_draft.short_description = "Mark as draft"

def mark_submitted (modeladmin, request, queryset):
    queryset.update(stage=1)
mark_submitted.short_description = "Mark as awaiting proofread"

def mark_approved (modeladmin, request, queryset):
    queryset.update(stage=2)
mark_approved.short_description = "Mark as approved for web"

def mark_accepted (modeladmin, request, queryset):
    queryset.update(stage=3, is_worthy=True)
mark_accepted.short_description = "Mark as ACCEPTED for book"

def mark_rejected (modeladmin, request, queryset):
    queryset.update(stage=3, is_worthy=False)
mark_rejected.short_description = "Mark as REJECTED for book"

def mark_undecided (modeladmin, request, queryset):
    queryset.update(stage=2, is_worthy=None)
mark_undecided.short_description = "Mark as UNDECIDED for book"

def mark_reapprove (modeladmin, request, queryset):
    queryset.update(stage=4)
mark_reapprove.short_description = "Mark as updated from book"

##############################################################################

class PriorityAdmin(admin.ModelAdmin):
    def priority(self):
        return self.summary
    priority.allow_tags = True

    list_display   = (priority,)
    date_hierarchy = "changed"

##############################################################################

class ClassificationAdmin(admin.ModelAdmin):
    def category(self):
        return self.name
    category.allow_tags = True   # HTML-ize

    list_display   = (category,)
    ordering       = ("name",)
    date_hierarchy = "changed"

##############################################################################

class AgencyAdmin(admin.ModelAdmin):
    def agency(self):
        return self.name
    agency.allow_tags = True   # HTML-ize

    list_display   = (agency,)
    ordering       = ("name",)
    search_fields  = ("name",)
    date_hierarchy = "changed"

##############################################################################

class DepartmentForm(forms.ModelForm):
    class Meta:
        model = Department

    def clean_homepage(self):
        return self.cleaned_data["homepage"] or None

    def clean_description(self):
        return self.cleaned_data["description"] or None

class DepartmentAdmin(admin.ModelAdmin):
    def department(self):
        return self.name
    department.allow_tags = True   # HTML-ize

    form           = DepartmentForm
    list_display   = (department,)
    list_filter    = ("is_current","is_center",)
    ordering       = ("name",)
    search_fields  = ("name",)
    date_hierarchy = "changed"

##############################################################################

class ProgramForm(forms.ModelForm):
    class Meta:
        model = Program

    def clean_homepage(self):
        return self.cleaned_data["homepage"] or None

class ProgramAdmin(admin.ModelAdmin):
    def program(self):
        return self.name
    program.allow_tags = True   # HTML-ize

    form           = ProgramForm
    list_display   = (program,)
    ordering       = ("name",)
    search_fields  = ("name",)
    date_hierarchy = "changed"

##############################################################################

class PersonForm(forms.ModelForm):
    class Meta:
        model = Person

    def clean_email(self):
        return self.cleaned_data["email"] or None

class PersonAdmin(admin.ModelAdmin):
    def person(self):
        return "%s, %s" % (self.last_name, self.first_name,)
    person.allow_tags = True   # HTML-ize

    form           = PersonForm
    list_display   = (person,)
    ordering       = ("last_name",)
    search_fields  = ("last_name","first_name",)
    date_hierarchy = "changed"

##############################################################################

class ProfessionAdmin(admin.ModelAdmin):
    def profession(self):
        return self.name
    profession.allow_tags = True   # HTML-ize

    list_display   = (profession,)
    ordering       = ("name",)
    search_fields  = ("name",)
    date_hierarchy = "changed"

##############################################################################

class EditorAdmin(admin.ModelAdmin):
    list_display   = ("username","responsibility",)
    list_filter    = ("responsibility",)
    ordering       = ("username",)
    search_fields  = ("username",)
    date_hierarchy = "changed"

##############################################################################

class StaffForm(forms.ModelForm):
    class Meta:
        model = Staff

    def clean_homepage(self):
        return self.cleaned_data["homepage"] or None

class StaffAdmin(admin.ModelAdmin):
    def person(self):
        return self.person
    person.allow_tags = True   # HTML-ize

    def profession(self):
        return self.profession
    profession.allow_tags = True   # HTML-ize

    def department(self):
        return self.department
    department.allow_tags = True   # HTML-ize

    def program(self):
        return self.program
    program.allow_tags = True   # HTML-ize

    def agency(self):
        return self.agency
    agency.allow_tags = True   # HTML-ize

    def htmlize(self):
        return "<em>%s</em> (%s)<br />%s (%s), %s" % (self.person,
                                                      self.profession,
                                                      self.department,
                                                      self.program,
                                                      self.agency,)
    htmlize.allow_tags = True   # HTML-ize

    form           = StaffForm
#   list_display   = (htmlize,)
    list_display   = (person,department,agency,)
    list_filter    = ("department","program","profession",)
    ordering       = ("person",)
    search_fields  = ("person__last_name","agency__name","department__name",)
    date_hierarchy = "changed"

##############################################################################

class FunderForm(forms.ModelForm):
    class Meta:
        model = Funder

    def clean_acronym(self):
        return self.cleaned_data["acronym"] or None

    def clean_homepage(self):
        return self.cleaned_data["homepage"] or None

class FunderAdmin(admin.ModelAdmin):
    def funder(self):
        return self.name
    funder.allow_tags = True   # HTML-ize

    form           = FunderForm
    list_display   = (funder,)
    list_filter    = ("is_internal",)
    ordering       = ("name",)
    search_fields  = ("name","acronym",)
    date_hierarchy = "changed"

##############################################################################

class ProjectAdmin(admin.ModelAdmin):
    def project(self):
        return self.title
    project.allow_tags = True   # HTML-ize

    list_display   = (project,)
    list_filter    = ("report__reported",
                     "is_live","status","begin_date","end_date",
                      "stage","is_worthy",
                      "product_only","department",
                      "investigator__staff__person","funding__funder",
                      "version",)
    ordering       = ("title",)
    search_fields  = ("title","investigator__staff__person__last_name",
                      "funding__funder__name",)
    actions        = (mark_draft,mark_submitted,mark_approved,
                      mark_accepted,mark_rejected,mark_undecided,
                      mark_reapprove,)
    date_hierarchy = "changed"

##############################################################################

class ProductAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "project":
            kwargs["queryset"] = Project.objects.filter(is_live=True)
        return super(ProductAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

#   def product(self):
#       return "<em>%s</em>..." % (self.description[:120],)
#   product.allow_tags = True   # HTML-ize

    def product(self, obj):
        return strain(obj.description)
    product.admin_order_field = "description"

    list_display  = ("product",)
    list_filter   = ("is_live","reported","stage","is_worthy","peer_reviewed",
                     "classification",)
    ordering      = ("description",)
    search_fields = ("description","project__title",)
    actions       = (mark_draft,mark_submitted,mark_approved,
                     mark_accepted,mark_rejected,mark_undecided,)
    date_hierarchy = "changed"

##############################################################################

class InvestigatorAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "project":
            kwargs["queryset"] = Project.objects.filter(is_live=True)
        return super(InvestigatorAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def investigator(self):
        return "<em>%s</em><br />(%s)" % (self.staff, self.project,)
    investigator.allow_tags = True   # HTML-ize

    list_display   = (investigator,)
    list_filter    = ("staff__person",)
    ordering       = ("staff__person__last_name",)
    search_fields  = ("staff__person__last_name","project__title",)
    date_hierarchy = "changed"

##############################################################################

class FocusAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "project":
            kwargs["queryset"] = Project.objects.filter(is_live=True)
        return super(FocusAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def focus(self):
        return "<em>%s</em><br />(%s)" % (self.project, self.priority,)
    focus.allow_tags = True   # HTML-ize

    list_display   = (focus,)
    ordering       = ("project",)
    search_fields  = ("priority__summary","project__title",)
    date_hierarchy = "changed"

##############################################################################

class FundingAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "project":
            kwargs["queryset"] = Project.objects.filter(is_live=True)
        return super(FundingAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def funding(self):
        return "<em>%s</em><br />(%s)" % (self.project, self.funder,)
    funding.allow_tags = True   # HTML-ize

    list_display   = (funding,)
    list_filter    = ("funder",)
    ordering       = ("project",)
    search_fields  = ("funder__name","project__title",)
    date_hierarchy = "changed"

##############################################################################

class ReportAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "project":
            kwargs["queryset"] = Project.objects.filter(is_live=True)
        return super(ReportAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def report(self):
        return "%4.4d &bullet; %s" % (self.reported, self.project)
    report.allow_tags = True

    list_display   = (report,)
    list_filter    = ("reported","project__is_live","project__stage",
                      "project__status",
                      "project__begin_date","project__end_date",
                      "project__department",
                      "project__investigator__staff__person",
                      "project__funding__funder",)
    ordering       = ("project",)
    search_fields  = ("reported","project__title",)
    date_hierarchy = "changed"

##############################################################################

#class ProviderAdmin(admin.ModelAdmin):
#    """ProviderAdmin: display/edit MHD provider info in admin interface"""
#    list_display = ("indexer","city","state","country",)
#    list_filter = ("country","state",)
#    ordering = ("state",)
#    search_fields = ("practitioner_ln","program","institution",)
#    radio_fields = {"entry_type":      admin.HORIZONTAL,
#                    "service_context": admin.VERTICAL,
#                    "oversight":       admin.VERTICAL}
#
#    fieldsets = (("ADDRESS INFORMATION",
#                     {"classes": ("collapse",),
#                      "fields": ("entry_type",
#                                 ("practitioner_ln","practitioner_fn",
#                                  "practitioner_mi",),
#                                 "practitioner_deg",
#                                 "program","institution",
#                                 "street","suite",
#                                 ("city","state","zip",),"country",)}),
#                 ("PHONE NUMBERS",
#                     {"classes": ("collapse",),
#                      "fields": ("phone_voice","phone_tdd","phone_fax",
#                                 "phone_video",
#                                 "url",)}),
#                 ("PERSONNEL",
#                     {"classes": ("collapse",),
#                      "fields": (("contact1_ln","contact1_fn",
#                                  "contact1_mi"),"contact1_eml",
#                                 "contact1_deg","contact1_job",
#                                 ("contact2_ln","contact2_fn",
#                                  "contact2_mi"),"contact2_eml",
#                                 "contact2_deg","contact2_job",)}),
#                 ("HOURS / EMERGENCY",
#                     {"classes": ("collapse",),
#                      "fields": ("weekday_hours","weekend_hours",
#                                 "emergency_contact",)}),
#                 ("PROGRAM TYPE",
#                     {"classes": ("collapse",),
#                      "fields": ("service_context","other_context",)}),
#                 ("ACCREDITATION,LICENSING,and OVERSIGHT",
#                     {"classes": ("collapse",),
#                      "fields": ("oversight","other_oversight",
#                                 "accreditation",
#                                 "licensure","established",)}),
#                 ("OTHER SERVICES",
#                     {"classes": ("collapse",),
#                      "fields": (("private","inpatient","outpatient",
#                                  "hospitalization","day_treatment",
#                                  "addiction_recovery",),
#                                 ("halfway_house","supported_living",
#                                  "vocational","emergency","videophone",),
#                                 "beds",)}),
#                 ("FEES",
#                     {"classes": ("collapse",),
#                      "fields": ("fee","fee_covered","sliding_scale",)}),
#                 ("SERVICE PROVIDERS",
#                     {"classes": ("collapse",),
#                      "fields": (("staff","signers",),
#                                 ("deaf","hoh","hearing","coda","nah",),)}),
#                 ("INTERPRETING and ASSISTIVE DEVICES",
#                     {"classes": ("collapse",),
#                      "fields": (("interpreting","terp_details",),
#                                 "devices_tdd",
#                                 "devices_amp","devices_flasher",
#                                 "devices_alarm","devices_none",)}),
#                 ("ACCESSIBILTY",
#                     {"classes": ("collapse",),
#                      "fields": ("wheelchair","transit",)}),
#                 ("Miscellaneous",
#                     {"classes": ("collapse",),
#                      "fields": ("comments","passphrase","garbage",)}),
#                 )
#

##############################################################################

admin.site.register(Priority,           PriorityAdmin)
admin.site.register(Classification,     ClassificationAdmin)
admin.site.register(Agency,             AgencyAdmin)
admin.site.register(Department,         DepartmentAdmin)
admin.site.register(InternalDepartment, DepartmentAdmin)
admin.site.register(ExternalDepartment, DepartmentAdmin)
admin.site.register(ResearchCenter,     DepartmentAdmin)
admin.site.register(NonCenter,          DepartmentAdmin)
admin.site.register(Program,            ProgramAdmin)
admin.site.register(Person,             PersonAdmin)
admin.site.register(Profession,         ProfessionAdmin)
#admin.site.register(Staff,             StaffAdmin)
admin.site.register(InternalStaff,      StaffAdmin)
admin.site.register(ExternalStaff,      StaffAdmin)
admin.site.register(Funder,             FunderAdmin)
admin.site.register(Project,            ProjectAdmin)
admin.site.register(Product,            ProductAdmin)
admin.site.register(Investigator,       InvestigatorAdmin)
admin.site.register(Focus,              FocusAdmin)
admin.site.register(Funding,            FundingAdmin)
admin.site.register(Report,             ReportAdmin)
admin.site.register(Editor,             EditorAdmin)
