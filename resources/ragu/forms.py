from django                  import forms
from django.forms.widgets    import TextInput, CheckboxInput, RadioSelect
from django.forms.widgets    import DateInput, TimeInput, DateTimeInput
from django.forms.widgets    import CheckboxSelectMultiple, Textarea
from django.forms.widgets    import NullBooleanSelect
from django.utils.safestring import mark_safe

from models                  import *
from datetime                import datetime, date

STATUS = ((1,"Ongoing"),
          (2,"Completed"),)

PEER   = ((True,  "Peer-reviewed"),
          (False, "Not peer-reviewed"),
          (None,  "I do not know"),)

instant  = datetime.now()
thisyear = date.today().year

##############################################################################

class EditorForm(forms.ModelForm):
    username       = forms.CharField(max_length=255)
    responsibility = forms.IntegerField(min_value=1)

    class Meta:
        model   = Editor
        fields  = ("username", "responsibility",)
        widgets = {"username":       TextInput(attrs={"size":"60"}),
                   "responsibility": TextInput(attrs={"size":"4"}),}

##############################################################################

class PriorityForm(forms.ModelForm):
    summary     = forms.CharField(max_length=255)
    description = forms.CharField()

    class Meta:
        model   = Priority
        fields  = ("summary", "description",)
        widgets = {"summary":     TextInput(attrs={"size":"60"}),
                   "description": Textarea(attrs={"rows":"20", "cols":"60"}),}

##############################################################################

class ClassificationForm(forms.ModelForm):
    name = forms.CharField(max_length=255)

    class Meta:
        model   = Classification
        fields  = ("name",)
        widgets = {"name": TextInput(attrs={"size":"60"}),}

##############################################################################

class AgencyForm(forms.ModelForm):
    name     = forms.CharField(max_length=255)
    homepage = forms.URLField(required=False)

    class Meta:
        model   = Agency
        fields  = ("name", "homepage",)
        widgets = {"name":     TextInput(attrs={"size":"60"}),
                   "homepage": TextInput(attrs={"size":"60"}),}

    def clean_homepage(self):
        return self.cleaned_data["homepage"] or None

##############################################################################

class DepartmentForm(forms.ModelForm):
    name        = forms.CharField(max_length=255)
    homepage    = forms.URLField(required=False)
    is_current  = forms.BooleanField()
    is_internal = forms.BooleanField()
    is_center   = forms.BooleanField()
    description = forms.CharField(required=False)

    class Meta:
        model   = Department
        fields  = ("name","homepage","description","is_current","is_internal","is_center",)
        widgets = {"name":        TextInput(attrs={"size":"60"}),
                   "homepage":    TextInput(attrs={"size":"60"}),
                   "description": Textarea(attrs={"rows":"20", "cols":"60"}),}

    def clean_description(self):
        return self.cleaned_data["description"] or None

    def clean_homepage(self):
        return self.cleaned_data["homepage"] or None

##############################################################################

class ProgramForm(forms.ModelForm):
    name     = forms.CharField(max_length=255)
    homepage = forms.URLField(required=False)

    class Meta:
        model   = Program
        fields  = ("name","homepage",)
        widgets = {"name":     TextInput(attrs={"size":"60"}),
                   "homepage": TextInput(attrs={"size":"60"}),}

    def clean_homepage(self):
        return self.cleaned_data["homepage"] or None

##############################################################################

class EmployeeForm(forms.ModelForm):
    caste       = forms.CharField(max_length=7)
    department  = forms.CharField(max_length=255)
    last_name   = forms.CharField(max_length=255)
    first_name  = forms.CharField(max_length=255)
    middle_name = forms.CharField(max_length=255, required=False)
    email       = forms.EmailField(max_length=255)

    class Meta:
        model   = Employee
        fields  = ("caste","department",
                   "last_name","first_name","middle_name","email",)
        widgets = {"caste":       TextInput(attrs={"size":"10"}),
                   "department":  TextInput(attrs={"size":"30"}),
                   "last_name":   TextInput(attrs={"size":"30"}),
                   "first_name":  TextInput(attrs={"size":"30"}),
                   "middle_name": TextInput(attrs={"size":"30"}),
                   "email":       TextInput(attrs={"size":"90"}),}

    def clean_middle_name(self):
        return self.cleaned_data["middle_name"] or None

##############################################################################

class PersonForm(forms.ModelForm):
    last_name  = forms.CharField(max_length=255)
    first_name = forms.CharField(max_length=255,  required=False)
    email      = forms.EmailField(max_length=255, required=False)

    class Meta:
        model   = Person
        fields  = ("last_name", "first_name", "email",)
        widgets = {"last_name":  TextInput(attrs={"size":"60"}),
                   "first_name": TextInput(attrs={"size":"60"}),
                   "email":      TextInput(attrs={"size":"60"}),}

    def clean_first_name(self):
        return self.cleaned_data["first_name"] or None

    def clean_email(self):
        return self.cleaned_data["email"] or None

##############################################################################

class ProfessionForm(forms.ModelForm):
    name = forms.CharField(max_length=255)

    class Meta:
        model   = Profession
        fields  = ("name",)
        widgets = {"name": TextInput(attrs={"size":"60"}),}

##############################################################################

class StaffForm(forms.ModelForm):
    person       = forms.ModelChoiceField(queryset=Person.objects.all())
    profession   = forms.ModelChoiceField(queryset=Profession.objects.all(),
                                          required=False)
    department   = forms.ModelChoiceField(queryset=Department.objects.all(),
                                          required=False)
    program      = forms.ModelChoiceField(queryset=Program.objects.all(),
                                          required=False)
    agency       = forms.ModelChoiceField(queryset=Agency.objects.all())
    homepage     = forms.URLField(required=False)

    class Meta:
        model   = Staff
        fields  = ("person", "profession",
                   "department", "program", "agency", "homepage",)
        widgets = {"homepage": TextInput(attrs={"size":"60"}),}

    def clean_homepage(self):
        return self.cleaned_data["homepage"] or None

##############################################################################

class FunderForm(forms.ModelForm):
    is_internal = forms.BooleanField()
    name        = forms.CharField(max_length=255)
    acronym     = forms.CharField(max_length=40, required=False)
    homepage    = forms.URLField(required=False)

    class Meta:
        model   = Funder
        fields  = ("is_internal", "name", "acronym", "homepage",)
        widgets = {"name":     TextInput(attrs={"size":"60"}),
                   "acronym":  TextInput(attrs={"size":"10"}),
                   "homepage": TextInput(attrs={"size":"60"}),}

    def clean_acronym(self):
        return self.cleaned_data["acronym"] or None

    def clean_homepage(self):
        return self.cleaned_data["homepage"] or None

##############################################################################

class ProjectForm(forms.ModelForm):
    title              = forms.CharField(max_length=255,
                               widget=TextInput(attrs={"size":"60"}))
    department         = forms.ModelChoiceField(queryset=InternalDepartment.objects.all())
    product_only       = forms.BooleanField(initial=False, required=False)
    is_student_project = forms.BooleanField(initial=False, required=False)
    is_clerc_project   = forms.BooleanField(initial=False, required=False)
    status             = forms.TypedChoiceField(coerce=int,
                                                widget=RadioSelect(),
                                                choices=STATUS)
    begin_date         = forms.DateField(required=False)
    end_date           = forms.DateField(required=False)
    description        = forms.CharField(
                               widget=Textarea(attrs={"rows":"20", "cols":"60"}))

    class Meta:
        model   = Project
        fields  = ("title", "department",
                   "product_only", "is_student_project", "is_clerc_project",
                   "status",
                   "begin_date", "end_date",
                   "description",)

    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.fields["department"].choices = ((department.id, mark_safe(department.name)) for department in InternalDepartment.objects.all())

##############################################################################

class ProductForm(forms.ModelForm):
#   project        = forms.ModelChoiceField(queryset=Project.objects.all())
    reported       = forms.IntegerField(min_value=2010, initial=thisyear,
                           widget=TextInput(attrs={"size":"4"}))
    classification = forms.ModelChoiceField(queryset=Classification.objects.all(),
                           widget=RadioSelect())
    peer_reviewed  = forms.TypedChoiceField(coerce=bool, required=False,
                               widget=RadioSelect(),
                               choices=PEER)
    description    = forms.CharField(
                           widget=Textarea(attrs={"rows":"5", "cols":"60"}))

    class Meta:
        model   = Product
        fields  = ("reported","classification","peer_reviewed","description",)

##############################################################################

class InvestigatorForm(forms.ModelForm):
#   project      = forms.ModelChoiceField(queryset=Project.objects.all())
    staff        = forms.ModelChoiceField(queryset=Staff.objects.all())
    is_principal = forms.BooleanField(required=False)
    is_leader    = forms.BooleanField(required=False)

    class Meta:
        model  = Investigator
        fields = ("staff",
                  "is_principal", "is_leader",)

##############################################################################

class FocusForm(forms.ModelForm):
#   project    = forms.ModelChoiceField(queryset=Project.objects.all())
    priorities = forms.ModelMultipleChoiceField(queryset=Priority.objects.all(),
                   required=False,
                   widget=CheckboxSelectMultiple())

    class Meta:
        model  = Focus
        fields = ("priorities",)

##############################################################################

class FundingForm(forms.ModelForm):
#   project = forms.ModelChoiceField(queryset=Project.objects.all())
    funders = forms.ModelMultipleChoiceField(queryset=Funder.objects.all())

    class Meta:
        model  = Funding
        fields = ("funders",)

##############################################################################

class ReportForm(forms.ModelForm):
#   project  = forms.ModelChoiceField(queryset=Project.objects.all())
    reported = forms.IntegerField(min_value=1968)

    class Meta:
        model  = Report
        fields = ("reported",)

##############################################################################
