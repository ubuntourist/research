#!/usr/bin/env python
# ### el-dappery ###
# Written by Kevin Cole <kjcole@gri.gallaudet.edu> 2012.04.19
#
# This is an attempt to make Python do what ldapsearch does, and then
# some.
#
# The starting points for this little exercise are the Packt's tutorial
# "Python LDAP Applications: Part 1 - Installing and Configuring the
# Python-LDAP Library and Binding to an LDAP Directory" and the known
# working command below:
#
#   $ ldapsearch -H ldaps://dc01.ad.gallaudet.edu/ \
#                -s sub                            \
#                -Y DIGEST-MD5                     \
#                -U kevin.cole                     \
#                -b "OU=people,dc=ad,dc=gallaudet,dc=edu"
#
#  Tests at the end of this show (as of 2012.12.09):
#
#    5310 entries
#      5310 last names       (ln)
#      5298 first names      (fn)
#      5288 descriptions     (desc)
#      5003 e-mail addresses (email)
#      1331 titles           (title)
#      1331 departments      (dept)
#
# So... the only guarantee is last name, with coverage of titles and
# descriptions being pretty damned bad.
#

import ldap, ldap.sasl
from   getpass   import getpass # Non-echoing password prompter

def dapperize(user, passwd):
    """Create an LDAP connection instance and bind to it"""
    server = "ldaps://dc01.ad.gallaudet.edu/"      # w/  Secure Socket Layer (SSL)
    server = "ldap://gallaudet-rr.gallaudet.edu/"  # w/o Secure Socket Layer (SSL)
    token  = ldap.sasl.digest_md5(user,passwd)     # Make encrypted token
    con    = ldap.initialize(server)               # Make connection object
    try:
        con.sasl_interactive_bind_s("",token)      # Secure synchronous bind
    except:
        return False                               # Nay! Return False
    return con                                     # Yea! Return connection

def check_name(con, lastname, firstname):
    """Check name against LDAP database. False if no match. True otherwise."""
    base_dn = "OU=people,dc=ad,dc=gallaudet,dc=edu" # Base distinguished name
    scope   = ldap.SCOPE_SUBTREE                    # Walk the subtree
    screen  = "(&(sn=%s)(givenName=%s))" % (lastname, firstname)
    fields  = ["sn","givenName","mail","description","title","department",]
    found   = con.search_s(base_dn, scope, screen, fields)
    if len(found) == 0: return False  # No one by that name in LDAP
    else:               return True   # At LEAST one (possibly more!) found

def fetch_names(con, partial):
    """Return a list of matching names"""
    if partial:
        if "," in partial:
            lname, fname = [name.strip() for name in partial.split(",")]
            screen = "(&(sn=%s)(givenName=%s*))" % (lname, fname)
        else:
            screen = "(sn=%s*)" % (partial,)
    else:
        screen = "(sn=*)"
    base_dn = "OU=people,dc=ad,dc=gallaudet,dc=edu" # Base distinguished name
    scope   = ldap.SCOPE_SUBTREE                    # Walk the subtree
    fields  = ["sn","givenName","mail","description","title","department",]
    found   = con.search_s(base_dn, scope, screen, fields)

    # found looks like: [("CN=csv,csv,csv", {key=["value"], "key"=["value"]}),
    #                    ("CN=csv,csv,csv", {key=["value"], "key"=["value"]}),
    #                    ("CN=csv,csv,csv", {key=["value"], "key"=["value"]}),
    #                    ...
    #                    ("CN=csv,csv,csv", {key=["value"], "key"=["value"]})]
    #
    # We want the values from the one-element "lists" in the dictionaries.

    directory = []
    if len(found) != 0:
        for find in found:
            if len(find) != 2:
                raise ValueError("Entry NOT a pair! Length = %s" % \
                                 (len(find),))
            entry = find[1]
            for kee in entry:
                if len(entry[kee]) != 1:
                    raise ValueError("""Entry["%s"] has %s values!""" % \
                                     (kee, len(entry[kee]),))
            bio = {}
            if "sn"          in entry: bio["ln"]    = entry["sn"][0]
            if "givenName"   in entry: bio["fn"]    = entry["givenName"][0]
            if "mail"        in entry: bio["email"] = entry["mail"][0]
            if "department"  in entry: bio["dept"]  = entry["department"][0]
            if "description" in entry: bio["desc"]  = entry["description"][0]
            if "title"       in entry: bio["title"] = entry["title"][0]
            directory.append(bio)
        return directory   # At LEAST one (possibly more!) found

def dedapperize(con):
    con.unbind()                                # Unbind / close the connection

if __name__ == "__main__":
    username  = "kevin.cole"
    password  = getpass()
    firstname = raw_input("First name: ")
    lastname  = raw_input("Last  name: ")
    con = dapperize(username, password)
    if con:
        check = check_name(con, lastname, firstname)
        print check
        directory = fetch_names(con, lastname)
        print directory
        x = raw_input("\nContinue?")
        print
        for thing in directory:
            print thing
        dedapperize(con)
        x = raw_input("\nContinue?")
        print "\n%s entries" % (len(directory),)
        keez = {}
        for entry in directory:
            for kee in entry:
                if kee in keez:
                    keez[kee] += 1
                else:
                    keez[kee] = 1
        for kee in keez:
            print "%s %s" % (keez[kee], kee,)
    else:
        print "Login failure"
