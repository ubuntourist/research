# Written by Kevin Cole <kjcole@gallaudet.edu> 2012.11.14
#
# This searches the ARA database finding "product only" projects that
# in fact, have NO products.
#

from ragu.models import *
ponies = Project.objects.filter(product_only=True)  # Product only projects
ponies = ponies.filter(is_live=True)                # Only the live ones
for pony in ponies:
    products = pony.product_set.all()  # How many products?
    if products.count() == 0:          # None??? for a product_only???
        print pony                     # Who/What the f' is it?
