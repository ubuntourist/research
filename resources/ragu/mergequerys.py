# Written by Kevin Cole <kjcole@gallaudet.edu> 2012.11.14
#
# A hard-won example of merging two querysets into one, taken from
# an iPython interactive session
#

from ragu.models import *
fy = 2012

reported   = Report.objects.filter(reported=int(fy)).values("project_id")
department = ResearchCenter.objects.get(pk=58)

staff = InternalStaff.objects.all()
staff = staff.filter(department=58)

investigators = Investigator.objects.all()
investigators = investigators.filter(staff__in=staff)
investigators = investigators.filter(is_principal=True)
investigating = investigators.values("project_id")

pro1 = Project.objects.filter(id__in=investigating)    # Queryset 1
pro2 = Project.objects.filter(department=58)           # Queryset 2

pro3 = pro1 | pro2                                     # Merged queryset
pro3 = pro3.filter(is_live=True)
pro3 = pro3.filter(pk__in=reported)
pro3 = pro3.filter(stage=3)
pro3 = pro3.filter(is_worthy=True)
pro3 = pro3.filter(product_only=False)
pro3 = pro3.order_by("sort_order")

pro3.count()

for pro in pro3:
    print pro.title
