#!/usr/bin/env python
# cleaner.py
# Written by Kevin Cole <kjcole@gallaudet.edu> 2012.08.01
#
# cleaner isn't part of the Django proper. It goes "old-school" into
# the database and does mass cleanings of various fields.

import pgdb, re               # Postgresql interface & regular expression
from   getpass import getpass # Non-echoing password prompter

password = getpass()

django = pgdb.connect("localhost:django:kjcole:"+password)
projects = django.cursor()

query  = "select id, title, description"
query += "  from ragu_project"

projects.execute(query)
print("%s records found." % (projects.rowcount,))
for row in projects.fetchall():
    id          = row[0]
    title       = row[1]
    description = row[2]

    ### description
    # - strip all "\r" and "\t"
    # - replace all "<p>\n" with "\n<p>"
    # - remove initial "\n" and trailing "\n"
    # - replace "&nbsp;" with " "
    # - collapse multiple consecutive spaces with a single space
    # - replace "&lsquo" and "&rsquo" with '
    # - replace "&ldquo" and "&rdquo" with "
    # - replace " <br />" with "<br />"
    # - replace " <p>" with "<p>"
    # - replace " </p>" with "</p>"
    # - replace "<p> " with "<p>"
    # - replace "</p> " with "</p>"
    # - replace "<br /><br />" with "</p>\n\n<p>"
    # - flag all remaining "<br />" as "suspicious"
