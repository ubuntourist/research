create table ragu_pool (
  id serial primary key,
  caste varchar(255),
  department varchar(255),
  last_name varchar(255),
  first_name varchar(255),
  middle_name varchar(255),
  email varchar(255),
  editor_id integer
);

COPY ragu_pool (caste,department,last_name,first_name,middle_name,email, editor_id) FROM stdin;
Staff	Academic Advising	Duhon	Jessica	Whitney	jessica.duhon@gallaudet.edu	1
Staff	Academic Advising	Murgel	Cheryl	Renee	cheryl.murgel@gallaudet.edu	1
Staff	Academic Advising	O'Brien	Kathleen	A	kathleen.obrien@gallaudet.edu	1
Staff	Academic Advising	Peters	Bruce	W	bruce.peters@gallaudet.edu	1
Staff	Academic Advising	Schroeder	Thelma	I	thelma.schroeder@gallaudet.edu	1
Staff	Academic Div. Special Project	Andersen	Catherine	F	catherine.andersen@gallaudet.edu	1
Faculty	Administration &amp; Supervision	Duffy	Francis	M	francis.duffy@gallaudet.edu	1
Faculty	Administration &amp; Supervision	Marshall	William	J. A.	william.marshall@gallaudet.edu	1
Faculty	Administration &amp; Supervision	Vasishta	Madan	Mohan	madan.vasishta@gallaudet.edu	1
Staff	Admissions	Baker	Patrick	John	patrick.baker@gallaudet.edu	1
Staff	Admissions	Bjarnason	Kristinn	Jon	kristinn.bjarnason@gallaudet.edu	1
Staff	Admissions	Church	Rebecca	Lynn	rebecca.church@gallaudet.edu	1
Staff	Admissions	Cox	Daphne	Elaine	daphne.cox@gallaudet.edu	1
Staff	Admissions	DeMarco	Jeffrey	L	jeffrey.demarco@gallaudet.edu	1
Staff	Admissions	Goodwin	Melba	Kaye	melba.goodwin@gallaudet.edu	1
Staff	Admissions	Henriquez	Anibelka	\N	anibelka.henriquez@gallaudet.edu	1
Staff	Admissions	Jacques	Tabitha	L.	tabitha.jacques@gallaudet.edu	1
Staff	Admissions	King	David	Musa	david.king@gallaudet.edu	1
Staff	Admissions	Ramirez	Mark	Anthony	mark.ramirez@gallaudet.edu	1
Staff	Admissions	Santimyer	Tami	Lee	tami.santimyer@gallaudet.edu	1
Staff	Admissions	Schooley-Fernandez	Trina	\N	trina.schooley@gallaudet.edu	1
Staff	Admissions	Tossman	David	Steven	david.tossman@gallaudet.edu	1
Staff	Admissions	Whetstone	Vickie	Z	vickie.whetstone@gallaudet.edu	1
Staff	Admissions	Williams	Derrick	Stuart	derrick.williams@gallaudet.edu	1
Staff	Alcohol&amp;Drug SanctionEducData	Maloney	Phyllis	C	phyllis.maloney@gallaudet.edu	1
Staff	Alumni Relations	Drake	Abigail	Rebecca	abigail.drake@gallaudet.edu	1
Staff	Alumni Relations	Graves	Mark	Dennis	mark.graves@gallaudet.edu	1
Staff	Alumni Relations	Sonnenstrahl	Samuel	\N	samuel.sonnenstrahl@gallaudet.edu	1
Staff	Alumni Relations	Tarantino	Thomas	A	thomas.tarantino@gallaudet.edu	1
Staff	Alumni Relations	Ward	Susan	Elizabeth	susan.ward@gallaudet.edu	1
Faculty	Art	Carollo	Scott	Michael	scott.carollo@gallaudet.edu	1
Faculty	Art	Glass	Marguerite	A	marguerite.glass@gallaudet.edu	1
Faculty	Art	Grindstaff	Johnston	Bell	johnston.grindstaff@gallaudet.edu	1
Faculty	Art	Johnston	Paul	\N	paul.johnston@gallaudet.edu	1
Faculty	Art	Kazemzadeh	Max	B	max.kazemzadeh@gallaudet.edu	1
Faculty	Art	McAuliffe	Michelle	\N	michelle.mcauliffe@gallaudet.edu	1
Staff	Art	Pellerin	Andre	P	andre.pellerin@gallaudet.edu	1
Faculty	Art	Salaway	Tracey	\N	tracey.salaway@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Arellano	Gabriel	\N	gabriel.arellano@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Bahan	Benjamin	J	benjamin.bahan@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Bauman	H-Dirksen	L.	h-dirksen.bauman@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Bienvenu	Martina	J	martina.bienvenu@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Cagle	Keith	Martin	keith.cagle@gallaudet.edu	1
Staff	ASL &amp; Deaf Studies Program	Dunn	Lindsay	\N	lindsay.dunn@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Durand	Stephanie	Elizabeth	stephanie.durand@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Ewan	Darlene	Leila	darlene.ewan@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Gardner	James	\N	james.gardner@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Garrow	William	George	william.garrow@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Giordano	Tyrone	\N	tyrone.giordano@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Griffin	Christena	Lorraine	christena.griffin@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Griffin	Frank	Anthony	frank.griffin@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Harris	Raychelle	Leigh	raychelle.harris@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Kelly	Arlene	B	arlene.kelly@gallaudet.edu	1
Staff	ASL &amp; Deaf Studies Program	Lewis	Benjamin	J	benjamin.lewis@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Loeffler	Summer	Crider	summer.loeffler@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	McCaskill	Carolyn	Deloris	carolyn.mccaskill@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Miller	Don	Arnette	don.miller@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Mirus	Eugene	Roberts	eugene.mirus@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Murray	Joseph	J	joseph.murray@gallaudet.edu	1
Staff	ASL &amp; Deaf Studies Program	Osbrink	Rory	Howard	rory.osbrink@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Paludnevicius	Zilvinas	\N	zilvinas.paludnevicius@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Riddle	Wanda	Ann	wanda.riddle@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Shaw	Emily	Potter	emily.shaw@gallaudet.edu	1
Faculty	ASL &amp; Deaf Studies Program	Sirvage	Robert	Thomas	robert.sirvage@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Ammons	Paula	Jean	paula.ammons@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Arellano	Leticia	\N	leticia.arellano@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Armato	Dianne	Marie	dianne.armato@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Brizendine	Albert	Brian	albert.brizendine@gallaudet.edu	1
Faculty	ASL Diagnostic and Eval Svcs	Claussen	Roger	Dale	roger.claussen@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Golightly	Jane	Beverly	jane.golightly@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Gordon	Jean	Marie	jean.gordon@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Gunn	Karla	Ann	karla.gunn@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Kanekuni	Jacqueline	Ann	jacqueline.kanekuni@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Kellner	Marti	Edelman	marti.kellner@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Kelly	Ryan	Cloyd	ryan.kelly@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Kovacs-Houlihan	Marika	\N	marika.kovacs-houlihan@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Roult	Loretta	Sue	loretta.roult@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Roult	Paul	Ian	paul.roult@gallaudet.edu	1
Staff	ASL Diagnostic and Eval Svcs	Shannon	Robin	Stafford	robin.shannon@gallaudet.edu	1
Staff	ASL Programs	Duren	Deborah	Deanne	deborah.duren@gallaudet.edu	1
Staff	ASL Programs	Massey	Carroll	Robin	carroll.massey@gallaudet.edu	1
Staff	ASL Programs	Pudans-Smith	Kimberly	K	kimberly.pudans-smith@gallaudet.edu	1
Staff	Athletics	Arp	Vanessa	M	vanessa.arp@gallaudet.edu	1
Staff	Athletics	Atkinson	John-Samuel	Griffith	john-samuel.atkinson@gallaudet.edu	1
Staff	Athletics	Barber	John	Benjamin	john.barber@gallaudet.edu	1
Staff	Athletics	Blumstein	Stephen	D	stephen.blumstein@gallaudet.edu	1
Staff	Athletics	Boren	Lynn	R	lynn.boren@gallaudet.edu	1
Staff	Athletics	Burke	Mark	Patrick	mark.burke@gallaudet.edu	1
Staff	Athletics	Carpenter	Harry	Everett	harry.carpenter@gallaudet.edu	1
Staff	Athletics	Cassel	Christopher	David	christopher.cassel@gallaudet.edu	1
Staff	Athletics	Davis	John	H	john.davis@gallaudet.edu	1
Faculty	Athletics	Delgado	Jesus	Manuel	jesus.delgado@gallaudet.edu	1
Staff	Athletics	Doleac	Steven	G	steven.doleac@gallaudet.edu	1
Staff	Athletics	Eck	Brian	James	brian.eck@gallaudet.edu	1
Staff	Athletics	Goldstein	Charles	S	charles.goldstein@gallaudet.edu	1
Staff	Athletics	Gould	Kris	O	kris.gould@gallaudet.edu	1
Staff	Athletics	Gumina	Sarah	Rose	sarah.gumina@gallaudet.edu	1
Staff	Athletics	Hall	Anna	S	anna.hall@gallaudet.edu	1
Staff	Athletics	Healey	Stephon	Charles	stephon.healey@gallaudet.edu	1
Staff	Athletics	Hudson	Mikaela	Gardner	mikaela.hudson@gallaudet.edu	1
Staff	Athletics	Huntington	Brian	W	brian.huntington@gallaudet.edu	1
Staff	Athletics	Jimenez	Bregitt	\N	bregitt.jimenez@gallaudet.edu	1
Staff	Athletics	Johnson	Karina	S	karina.johnson@gallaudet.edu	1
Staff	Athletics	Kittredge	Jennifer	Lynn	jennifer.kittredge@gallaudet.edu	1
Staff	Athletics	Kolcun	Joseph	Michael	joseph.kolcun@gallaudet.edu	1
Staff	Athletics	Liang	Olivia	Xiaoxun	olivia.liang@gallaudet.edu	1
Staff	Athletics	Lipman	Joshua	\N	joshua.lipman@gallaudet.edu	1
Staff	Athletics	Payne	Brittainy	Nicole	brittainy.payne@gallaudet.edu	1
Staff	Athletics	Perkins	Russell	\N	russell.perkins@gallaudet.edu	1
Staff	Athletics	Pride	Curtis	\N	curtis.pride@gallaudet.edu	1
Staff	Athletics	Schaeffer	Isaac	\N	isaac.schaeffer@gallaudet.edu	1
Staff	Athletics	Starkman	Lauren	Nicole	lauren.starkman@gallaudet.edu	1
Staff	Athletics	Stern	Brendan	Udkovich	brendan.stern@gallaudet.edu	1
Staff	Athletics	Stevens	Stephanie	Rae	stephanie.stevens@gallaudet.edu	1
Staff	Athletics	Trofimenkoff	Valerie	Joan	valerie.trofimenkoff@gallaudet.edu	1
Staff	Athletics	Weinstock	Michael	H	michael.weinstock@gallaudet.edu	1
Faculty	Audiology	Ackley	Robert	Steven	robert.ackley@gallaudet.edu	1
Staff	Audiology	Allen	Antoinette	S	antoinette.allen@gallaudet.edu	1
Faculty	Audiology	Bakke	Matthew	H.	matthew.bakke@gallaudet.edu	1
Staff	Audiology	Cotton	Karen	\N	karen.cotton@gallaudet.edu	1
Faculty	Audiology	Covington	Melissa	\N	melissa.covington@gallaudet.edu	1
Staff	Audiology	Devlin	Lisa	C	lisa.devlin@gallaudet.edu	1
Faculty	Audiology	Garrido	Karen	Gae Santos	karen.garrido@gallaudet.edu	1
Staff	Audiology	Goffen	Robin	L	robin.goffen@gallaudet.edu	1
Faculty	Audiology	Gross	Jennifer	Hommert	jennifer.gross@gallaudet.edu	1
Staff	Audiology	Handscomb	Andrea	\N	andrea.handscomb@gallaudet.edu	1
Faculty	Audiology	Hanks	Wendy	\N	wendy.hanks@gallaudet.edu	1
Faculty	Audiology	Henry	Kenneth	G	kenneth.henry@gallaudet.edu	1
Faculty	Audiology	Jaiswal	Sanyukta	\N	sanyukta.jaiswal@gallaudet.edu	1
Faculty	Audiology	Jones-Oleson	Linda	\N	linda.jones-oleson@gallaudet.edu	1
Faculty	Audiology	Karch	Stephanie	Jean	stephanie.karch@gallaudet.edu	1
Staff	Audiology	King	Victoria	L.	victoria.king@gallaudet.edu	1
Faculty	Audiology	Kwon	Bomjun	\N	bomjun.kwon@gallaudet.edu	1
Faculty	Audiology	LaSasso	Carol	J	carol.lasasso@gallaudet.edu	1
Staff	Audiology	Malta	Michelle	\N	michelle.malta@gallaudet.edu	1
Faculty	Audiology	Medwetsky	Larry	\N	larry.medwetsky@gallaudet.edu	1
Staff	Audiology	Ng	Ka-Wai	\N	ka-wai.ng@gallaudet.edu	1
Faculty	Audiology	Robinson	Tommie	L	tommie.robinson@gallaudet.edu	1
Staff	Audiology	Roush	Kristin	Cholewa	kristin.roush@gallaudet.edu	1
Faculty	Audiology	Seal	Brenda	\N	brenda.seal@gallaudet.edu	1
Faculty	Audiology	Sitcovsky	Jessica	\N	jessica.sitcovsky@gallaudet.edu	1
Faculty	Audiology	Tamaki	Chizuko	\N	chizuko.tamaki@gallaudet.edu	1
Faculty	Biology	Arnos	Kathleen	S	kathleen.arnos@gallaudet.edu	1
Faculty	Biology	Braun	Derek	C.	derek.braun@gallaudet.edu	1
Staff	Biology	Hines	Daniel	Edward	daniel.hines@gallaudet.edu	1
Faculty	Biology	Merritt	Raymond	Clyde	raymond.merritt@gallaudet.edu	1
Faculty	Biology	Morrow	Ava	P	ava.morrow@gallaudet.edu	1
Faculty	Biology	Ogunjirin	Adebowale	E	adebowale.ogunjirin@gallaudet.edu	1
Staff	Biology	Peebles	Simone	\N	simone.peebles@gallaudet.edu	1
Faculty	Biology	Solomon	Caroline	Miller	caroline.solomon@gallaudet.edu	1
Staff	Bookstore	Cooper	Ross	Jacob	ross.cooper@gallaudet.edu	1
Staff	Bookstore	Erlandson	Janna	Sue	janna.erlandson@gallaudet.edu	1
Staff	Bookstore	Hubbard	Darlene	Ellon	darlene.hubbard@gallaudet.edu	1
Staff	Bookstore	Klewin	Nathan	Allan	nathan.klewin@gallaudet.edu	1
Staff	Bookstore	Lantieri	Anthony	Nicholas	anthony.lantieri@gallaudet.edu	1
Staff	Bookstore	Lemery	Rock	\N	rock.lemery@gallaudet.edu	1
Staff	Bookstore	O'Donnell	Priscilla	K	priscilla.odonnell@gallaudet.edu	1
Staff	Bookstore	Williams	Iva	V	iva.williams@gallaudet.edu	1
Staff	Budget	Wells	Monika	\N	monika.wells@gallaudet.edu	1
Staff	Business Operations	Aller	Gary	B.	gary.aller@gallaudet.edu	1
Staff	Business Operations	Fleishell	Sherri	E	sherri.fleishell@gallaudet.edu	1
Staff	Campus Activities	Buchanan	Beverly	\N	beverly.buchanan@gallaudet.edu	1
Staff	Campus Activities	Keane	Mary	Louise	mary.keane@gallaudet.edu	1
Staff	Campus Activities	Kononenko	Melissa	Sue	melissa.kononenko@gallaudet.edu	1
Staff	Career Center	Amissah	Kojo	\N	kojo.amissah@gallaudet.edu	1
Staff	Career Center	Cook	Karen	B	karen.cook@gallaudet.edu	1
Staff	Career Center	Desai-Margolin	Anjali	\N	anjali.desai-margolin@gallaudet.edu	1
Staff	Career Center	Garvin	Monica	Venise Staples	monica.garvin@gallaudet.edu	1
Staff	Career Center	Keller	Beth	Ann	beth.keller@gallaudet.edu	1
Staff	Career Center	Moore	Deborah	Roberta	deborah.moore@gallaudet.edu	1
Staff	Career Center	Walden	Stephanie	C	stephanie.walden@gallaudet.edu	1
Staff	Chemistry	Cargo	Sharron	Elizabeth Anne	sharron.cargo@gallaudet.edu	1
Faculty	Chemistry	Lundberg	Daniel	James	daniel.lundberg@gallaudet.edu	1
Faculty	Chemistry	Sabila	Paul	Sali	paul.sabila@gallaudet.edu	1
Faculty	Chemistry	Snyder	Henry	D	henry.snyder@gallaudet.edu	1
Faculty	Chemistry	Sorensen	Charlene	\N	charlene.sorensen@gallaudet.edu	1
Staff	Chief Information Officer	King	Cynthia	M	cynthia.king@gallaudet.edu	1
Staff	Child Development Center	An	Jung-Sun	\N	jung-sun.an@gallaudet.edu	1
Staff	Child Development Center	Arevalo	Yvonne	Margarita	yvonne.arevalo@gallaudet.edu	1
Staff	Child Development Center	Ferguson	Mary	N	mary.ferguson@gallaudet.edu	1
Staff	Child Development Center	Guion	Sodartha	V	sodartha.guion@gallaudet.edu	1
Staff	Child Development Center	Harrison	Ethel	R	ethel.harrison@gallaudet.edu	1
Staff	Child Development Center	Langford	Rochelle	Anastasia	rochelle.langford@gallaudet.edu	1
Staff	Child Development Center	Pacheco	Dustina	M	dustina.pacheco@gallaudet.edu	1
Staff	Clerc Center Interpreting	Sussman	Cheri	L	cheri.sussman@gallaudet.edu	1
Staff	Cochlear Implant Education Ctr	Nussbaum	Debra	\N	debra.nussbaum@gallaudet.edu	1
Staff	Cochlear Implant Education Ctr	Scott	Susanne	M	susanne.scott@gallaudet.edu	1
Staff	Communication Arts Research	Tucker	Paula	E	paula.tucker@gallaudet.edu	1
Faculty	Communication Arts Research	Vogler	Christian	\N	christian.vogler@gallaudet.edu	1
Staff	Community Services	Terhune	Karen	Leigh	karen.terhune@gallaudet.edu	1
Staff	Commuter Programs	Williams	Roger	Keith	roger.williams@gallaudet.edu	1
Staff	Contracts and Purchasing	Harewood	Maxine	D	maxine.harewood@gallaudet.edu	1
Staff	Contracts and Purchasing	Hopkins	Patricia	\N	patricia.hopkins@gallaudet.edu	1
Staff	Contracts and Purchasing	McClelland	Pamela	S	pamela.mcclelland@gallaudet.edu	1
Faculty	Counseling Department	Batiano	Joseph	Evaristo	Joseph.Batiano@Gallaudet.Edu	1
Faculty	Counseling Department	Beach	Roger	L	roger.beach@gallaudet.edu	1
Faculty	Counseling Department	Hufnell	Mary	C	mary.hufnell@gallaudet.edu	1
Faculty	Counseling Department	Lewis	Jeffrey	W	jeffrey.lewis@gallaudet.edu	1
Faculty	Counseling Department	Lytle	Linda	\N	linda.lytle@gallaudet.edu	1
Faculty	Counseling Department	Santomen	Lisa	Ann	lisa.santomen@gallaudet.edu	1
Faculty	Counseling Department	Smith	Kendra	Lyn	kendra.smith@gallaudet.edu	1
Faculty	Counseling Department	Smith	Marilyn	Jean	marilyn.smith@gallaudet.edu	1
Staff	Counseling Department	Smith	Renee'	R.	renee.smith@gallaudet.edu	1
Faculty	Counseling Department	Wu	Cheryl	Lynn	cheryl.wu@gallaudet.edu	1
Staff	CPSO Program Support	Bradford	Nicole	Powell	nicole.bradford@gallaudet.edu	1
Staff	CPSO Program Support	Kempton	Tracey	Marie	tracey.kempton@gallaudet.edu	1
Staff	CPSO Program Support	Laba	Charlene	R	charlene.laba@gallaudet.edu	1
Staff	CPSO Program Support	Shafiq	Zeshan	T	zeshan.shafiq@gallaudet.edu	1
Staff	CPSO Program Support	Stamper	Linda	\N	linda.stamper@gallaudet.edu	1
Staff	CPSO Program Support	Wiatrowski	Wendy	Christine	wendy.wiatrowski@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Baker	Cinbrella	Bell	cinbrella.baker@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Biega	Darlene	Anne	darlene.biega@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Bradley	Ronnie	Alex	ronnie.bradley@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Brown	Frances	A	frances.brown@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Cholmondeley	Tracey	Elizabeth	tracey.cholmondeley@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Clapp	Larissa	Elizabeth	larissa.clapp@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Dunn	Florence	L.	florence.dunn@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Ellsworth	Mary	\N	mary.ellsworth@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Elsner	Christopher	\N	christopher.elsner@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Grymes	Christine	Louise	christine.grymes@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Harbaugh	Jennifer	\N	jennifer.harbaugh@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Hollywood	Lisa	J	lisa.hollywood@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Jones	Delbert	\N	delbert.jones@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Kaika	Eric	Michael	eric.kaika@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Klatt	Walter	M	walter.klatt@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Loggins	Aarron	Al'uarius	aarron.loggins@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Padden	Kami	Hye-Mee	kami.padden@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Pedersen	Stacey	Dawn	stacey.pedersen@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Perry	Carolyn	S	carolyn.perry@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Scott	Cynthia	S.	cynthia.scott@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Spicer	David	Lee	david.spicer@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Thompson	Sally	Jane	sally.thompson@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Trapani	Debra	Ann	Debra.Trapani@Gallaudet.Edu	1
Staff	Curriculum &amp; Instruction	Varner	Naytasha	Capri	naytasha.varner@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Weinstock	Janet	S	janet.weinstock@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Weinstock	Joshua	Matthew	joshua.weinstock@gallaudet.edu	1
Staff	Curriculum &amp; Instruction	Yanke	Myra	S.	myra.yanke@gallaudet.edu	1
Staff	Custodial Services	Adames	Nurys	\N	nurys.adames@gallaudet.edu	1
Staff	Custodial Services	Allen	Vianela	\N	vianela.allen@gallaudet.edu	1
Staff	Custodial Services	Balbuena	Maria	\N	maria.balbuena@gallaudet.edu	1
Staff	Custodial Services	Blanco	Bernarda	L	bernarda.blanco@gallaudet.edu	1
Staff	Custodial Services	Blanco	Edenilsa	\N	edenilsa.blanco@gallaudet.edu	1
Staff	Custodial Services	Blanco	Silvia	\N	silvia.blanco@gallaudet.edu	1
Staff	Custodial Services	Bradley	Gary	L	gary.bradley@gallaudet.edu	1
Staff	Custodial Services	Cavanaugh	Overton	C	overton.cavanaugh@gallaudet.edu	1
Staff	Custodial Services	Cork	Clide	\N	clide.cork@gallaudet.edu	1
Staff	Custodial Services	Curtis	Deborah	\N	deborah.curtis@gallaudet.edu	1
Staff	Custodial Services	Duckett	Ronald	K	ronald.duckett@gallaudet.edu	1
Staff	Custodial Services	Ebendeng	Francoise	Z.	francoise.ebendeng@gallaudet.edu	1
Staff	Custodial Services	Gates	Carolyn	\N	carolyn.gates@gallaudet.edu	1
Staff	Custodial Services	Gray	Michael	J	michael.gray@gallaudet.edu	1
Staff	Custodial Services	Guerra	Rosa	\N	rosa.guerra@gallaudet.edu	1
Staff	Custodial Services	Hernandez	Maximiliano	\N	maximiliano.hernandez@gallaudet.edu	1
Staff	Custodial Services	Hernandez	Noemis	\N	noemis.hernandez@gallaudet.edu	1
Staff	Custodial Services	Inniss	Gloria	A	gloria.inniss@gallaudet.edu	1
Staff	Custodial Services	Jackson	Donald	M	donald.jackson@gallaudet.edu	1
Staff	Custodial Services	Jones	Inez	M.	inez.jones@gallaudet.edu	1
Staff	Custodial Services	Knight	Ellen	E.	ellen.knight@gallaudet.edu	1
Staff	Custodial Services	Larkins	Gerald	F	gerald.larkins@gallaudet.edu	1
Staff	Custodial Services	Lemma	Kidist	Belachen	kidist.lemma@gallaudet.edu	1
Staff	Custodial Services	Llerena	Nelson	\N	nelson.llerena@gallaudet.edu	1
Staff	Custodial Services	Loggins	Sue	A	sue.loggins@gallaudet.edu	1
Staff	Custodial Services	Martinez	Xiomara	A	xiomara.martinez@gallaudet.edu	1
Staff	Custodial Services	McKinney	Vern	M	vern.mckinney@gallaudet.edu	1
Staff	Custodial Services	Miguel	Maria	A	maria.miguel@gallaudet.edu	1
Staff	Custodial Services	Rosario	Olga	M	olga.rosario@gallaudet.edu	1
Staff	Custodial Services	Sanders	David	\N	david.sanders@gallaudet.edu	1
Staff	Custodial Services	Shell	Sandra	\N	sandra.shell@gallaudet.edu	1
Staff	Custodial Services	Smith	Leslie	A	leslie.smith@gallaudet.edu	1
Staff	Custodial Services	Thomas	Lester	M	lester.thomas@gallaudet.edu	1
Staff	Custodial Services	Tomlinson	Shirley	\N	shirley.tomlinson@gallaudet.edu	1
Staff	Custodial Services	Ventura	Jose	Julio Dubon	jose.ventura@gallaudet.edu	1
Staff	Custodial Services	Walker	Elmira	\N	elmira.walker@gallaudet.edu	1
Staff	Custodial Services	Walton	Tina	B	tina.walton@gallaudet.edu	1
Staff	Custodial Services	Wesley	Roy	L	roy.wesley@gallaudet.edu	1
Staff	Custodial Services	White	Surmenta	L	surmenta.white@gallaudet.edu	1
Staff	Custodial Services	Williams	Pearlie	M	pearlie.williams@gallaudet.edu	1
Faculty	Dean, CLAST	Adams	Elizabeth	Bourque	elizabeth.adams@gallaudet.edu	1
Faculty	Dean, CLAST	Agboola	Isaac	O	isaac.agboola@gallaudet.edu	1
Faculty	Dean, CLAST	DeAndrea-Lazarus	Ian	Anthony	ian.deandrea-lazarus@gallaudet.edu	1
Staff	Dean, CLAST	Gardner	Cherisse	Sebrina	cherisse.gardner@gallaudet.edu	1
Faculty	Dean, CLAST	Hicks	Joseph	\N	joseph.hicks@gallaudet.edu	1
Staff	Dean, CLAST	Hill	Patricia	B	patricia.hill@gallaudet.edu	1
Faculty	Dean, CLAST	Kennedy	Rhea	D.Y.	rhea.kennedy@gallaudet.edu	1
Staff	Dean, CLAST	McCubbin	Mona	B	mona.mccubbin@gallaudet.edu	1
Faculty	Dean, CLAST	McLaughlin	Erin	Brook	erin.mclaughlin@gallaudet.edu	1
Staff	Dean, CLAST	Mounty	Judith	L	judith.mounty@gallaudet.edu	1
Staff	Dean, CLAST	Musa	Lawrence	Gbele	lawrence.musa@gallaudet.edu	1
Faculty	Dean, CLAST	Myers	Lynda	R	lynda.myers@gallaudet.edu	1
Staff	Dean, CLAST	Officer	Cindy	Ebeling	cindy.officer@gallaudet.edu	1
Faculty	Dean, CLAST	Pucci	Concetta	\N	concetta.pucci@gallaudet.edu	1
Faculty	Dean, CLAST	Schaefer-Salins	Ellen	\N	ellen.schaefer-salins@gallaudet.edu	1
Faculty	Dean, CLAST	Snyder-Gardner	Laura	Jeanne	laura.snyder-gardner@gallaudet.edu	1
Faculty	Dean, CLAST	St Cyr	Carrie	Lynn	carrie.st.cyr@gallaudet.edu	1
Faculty	Dean, CLAST	Towers	Troy	Alexander	troy.towers@gallaudet.edu	1
Faculty	Dean, CLAST	Weiner	Mary	T	mary.weiner@gallaudet.edu	1
Staff	Dean, CPSO	Allen	Melissa	R	melissa.allen@gallaudet.edu	1
Faculty	Dean, CPSO	Nystroem	Angelina	Sofia	angelina.nystroem@gallaudet.edu	1
Faculty	Dean, Graduate Sch &amp; Prof Prog	Allen	Thomas	E	thomas.allen@gallaudet.edu	1
Staff	Dean, Graduate Sch &amp; Prof Prog	Blount	Jacqueline	\N	jacqueline.blount@gallaudet.edu	1
Staff	Dean, Graduate Sch &amp; Prof Prog	Carroll	Nancy	P	nancy.carroll@gallaudet.edu	1
Staff	Dean, Graduate Sch &amp; Prof Prog	Erting	Carol	J	carol.erting@gallaudet.edu	1
Staff	Dean, Graduate Sch &amp; Prof Prog	Mathur	Gaurav	\N	gaurav.mathur@gallaudet.edu	1
Staff	Dean, Graduate Sch &amp; Prof Prog	O'Brien	Catherine	Ann	catherine.obrien@gallaudet.edu	1
Faculty	Dean, Graduate Sch &amp; Prof Prog	Petitto	Laura-Ann	\N	laura-ann.petitto@gallaudet.edu	1
Staff	Dean, Graduate Sch &amp; Prof Prog	Simmons	Bonnie	L.	bonnie.simmons@gallaudet.edu	1
Staff	Dean, Graduate Sch &amp; Prof Prog	Spiegel	Katherine	Ann	katherine.spiegel@gallaudet.edu	1
Staff	Dean, Graduate Sch &amp; Prof Prog	Wang	Wei	\N	wei.wang@gallaudet.edu	1
Faculty	Dean's Special Activities	Henderson	Murdock	McDonald	murdock.henderson@gallaudet.edu	1
Faculty	Dean's Special Activities	Simmons	Paul	Kingsley	paul.simmons@gallaudet.edu	1
Faculty	Dean's Special Activities	Watson	Martreece	\N	martreece.watson@gallaudet.edu	1
Faculty	Department of Business	Alkoby	Karen	Lynne	karen.alkoby@gallaudet.edu	1
Faculty	Department of Business	Baldridge	Thomas	\N	thomas.baldridge@gallaudet.edu	1
Faculty	Department of Business	Chanin	Jonathan	Paul	jonathan.chanin@gallaudet.edu	1
Faculty	Department of Business	Chukwuma	Emilia	A	emilia.chukwuma@gallaudet.edu	1
Faculty	Department of Business	Gershwind	Reed	\N	reed.gershwind@gallaudet.edu	1
Faculty	Department of Business	Janger	Michael	\N	michael.janger@gallaudet.edu	1
Staff	Department of Business	Jenoure	Chardae	N	chardae.jenoure@gallaudet.edu	1
Staff	Department of Business	Kearney	Christopher	\N	christopher.kearney@gallaudet.edu	1
Faculty	Department of Business	Kinyon	Kari	Kristine	kari.kinyon@gallaudet.edu	1
Staff	Department of Business	Lopes	Georgette	Dorothy	georgette.lopes@gallaudet.edu	1
Faculty	Department of Business	Ng	Mon	Ching	mon.ng@gallaudet.edu	1
Faculty	Department of Business	Ogork	Marie	Shook	marie.ogork@gallaudet.edu	1
Faculty	Department of Business	Rashid	Khadijat	K	khadijat.rashid@gallaudet.edu	1
Faculty	Department of Business	Recht	Scott	Alan	scott.recht@gallaudet.edu	1
Faculty	Department of Business	Sampson	Nwokoma	\N	nwokoma.sampson@gallaudet.edu	1
Faculty	Department of Business	Sloboda	William	P	william.sloboda@gallaudet.edu	1
Faculty	Department of Business	Wang	Qi	\N	qi.wang@gallaudet.edu	1
Faculty	Department of Business	Warigon	Slemo	D	slemo.warigon@gallaudet.edu	1
Faculty	Department of Interpretation	Collins	Steven	D	steven.collins@gallaudet.edu	1
Faculty	Department of Interpretation	Dively	Valerie	L	valerie.dively@gallaudet.edu	1
Staff	Department of Interpretation	Faith	Jeanelle	C.H.	jeanelle.faith@gallaudet.edu	1
Faculty	Department of Interpretation	Mathers	Carla	M	carla.mathers@gallaudet.edu	1
Faculty	Department of Interpretation	Metzger	Melanie	\N	melanie.metzger@gallaudet.edu	1
Faculty	Department of Interpretation	Nicodemus	Brenda	S	brenda.nicodemus@gallaudet.edu	1
Faculty	Department of Interpretation	Roy	Cynthia	B	cynthia.roy@gallaudet.edu	1
Faculty	Department of Interpretation	Shaw	Risa	S	risa.shaw@gallaudet.edu	1
Faculty	Department of Interpretation	Thumann	Mary	Agnes	mary.thumann@gallaudet.edu	1
Faculty	Department of Linguistics	Dudis	Paul	G	paul.dudis@gallaudet.edu	1
Faculty	Department of Linguistics	Hochgesang	Julie	Ann	julie.hochgesang@gallaudet.edu	1
Faculty	Department of Linguistics	Lucas	Ceil	\N	ceil.lucas@gallaudet.edu	1
Faculty	Department of Linguistics	Mather	Susan	M	susan.mather@gallaudet.edu	1
Staff	Department of Linguistics	McKenzie	Jayne	A	jayne.mckenzie@gallaudet.edu	1
Faculty	Department of Linguistics	Mulrooney	Kristin	Jean	kristin.mulrooney@gallaudet.edu	1
Faculty	Department of Linguistics	Pichler	Deborah	Chen	deborah.pichler@gallaudet.edu	1
Faculty	Department of Linguistics	Rankin	Miako	Noel Polson	miako.rankin@gallaudet.edu	1
Staff	Department of Public Safety	Baran	Theodore	A	theodore.baran@gallaudet.edu	1
Staff	Department of Public Safety	Bauer	Daniel	E	daniel.bauer@gallaudet.edu	1
Staff	Department of Public Safety	Butler	Pierre	O.	pierre.butler@gallaudet.edu	1
Staff	Department of Public Safety	Cai	Hui	\N	hui.cai@gallaudet.edu	1
Staff	Department of Public Safety	Collson	Fabienne	\N	fabienne.collson@gallaudet.edu	1
Staff	Department of Public Safety	Cornejo-Adrianzen	Victor	Jose	victor.cornejo-adrianzen@gallaudet.edu	1
Staff	Department of Public Safety	Craft	Jeramee	\N	jeramee.craft@gallaudet.edu	1
Staff	Department of Public Safety	Cunningham	Jacqueline	L	jacqueline.cunningham@gallaudet.edu	1
Staff	Department of Public Safety	Dashuwar	Dayak	Tentukur	dayak.dashuwar@gallaudet.edu	1
Staff	Department of Public Safety	Dean	Richard	E	richard.dean@gallaudet.edu	1
Staff	Department of Public Safety	Edwards	Ricardo	Anthony	ricardo.edwards@gallaudet.edu	1
Staff	Department of Public Safety	Eley	Tracey	\N	tracey.eley@gallaudet.edu	1
Staff	Department of Public Safety	Fedor	Virginia	M.	virginia.fedor@gallaudet.edu	1
Staff	Department of Public Safety	Figert	Joseph	M	joseph.figert@gallaudet.edu	1
Staff	Department of Public Safety	Galanter	Simantha	\N	simantha.galanter@gallaudet.edu	1
Staff	Department of Public Safety	Garcia	Jose	Angel	jose.garcia@gallaudet.edu	1
Staff	Department of Public Safety	Hoover	Akilah	Renee	akilah.hoover@gallaudet.edu	1
Staff	Department of Public Safety	Hunter	Maurice	\N	maurice.hunter@gallaudet.edu	1
Staff	Department of Public Safety	Ithier	Jose	A.	jose.ithier@gallaudet.edu	1
Staff	Department of Public Safety	Jones	Michael	Allen	michael.jones@gallaudet.edu	1
Staff	Department of Public Safety	Kelly-Bryant	Sheila	\N	sheila.kelly-bryant@gallaudet.edu	1
Staff	Department of Public Safety	Knapp	Harold	\N	harold.knapp@gallaudet.edu	1
Staff	Department of Public Safety	McCaskill	Jamel	LaShaun	jamel.mccaskill@gallaudet.edu	1
Staff	Department of Public Safety	Moore	Brian	Casey	brian.moore@gallaudet.edu	1
Staff	Department of Public Safety	Potts	Wendy	\N	wendy.potts@gallaudet.edu	1
Staff	Department of Public Safety	Rader	Patrick	W	patrick.rader@gallaudet.edu	1
Staff	Department of Public Safety	Simms	Sherita	\N	sherita.simms@gallaudet.edu	1
Staff	Department of Public Safety	Slater	Anthony	\N	anthony.slater@gallaudet.edu	1
Staff	Department of Public Safety	Smith	Brad	M	brad.smith@gallaudet.edu	1
Staff	Department of Public Safety	Ware	Darrell	C.	darrell.ware@gallaudet.edu	1
Staff	Department of Public Safety	White	Laria	Elaine	laria.white@gallaudet.edu	1
Staff	Department of Public Safety	White	Teres	Lynn Hargraves	teres.white@gallaudet.edu	1
Staff	Department of Public Safety	Wilson	Robert	Glenn	robert.wilson@gallaudet.edu	1
Staff	Department of Public Safety	Wright	Andrew	\N	andrew.wright@gallaudet.edu	1
Faculty	Dept Of Communication Studies	Benedict	Beth	S	beth.benedict@gallaudet.edu	1
Faculty	Dept Of Communication Studies	Foley	Patricia	C	patricia.foley@gallaudet.edu	1
Faculty	Dept Of Communication Studies	Harrison	Robert	D	robert.harrison@gallaudet.edu	1
Staff	Dept Of Communication Studies	Hutchinson	Nicole	Briana	nicole.hutchinson@gallaudet.edu	1
Faculty	Dept Of Communication Studies	Norman	Jane	\N	jane.norman@gallaudet.edu	1
Staff	Dept Of Communication Studies	Powers	Jennifer	\N	jennifer.powers@gallaudet.edu	1
Faculty	Dept Of Communication Studies	Ransom	Lillie	Sharon	lillie.ransom@gallaudet.edu	1
Faculty	Dept Of Communication Studies	Whetter	Jevon	R	jevon.whetter@gallaudet.edu	1
Faculty	Dept of Family &amp; Child Studies	Krichbaum	Deborah	\N	deborah.krichbaum@gallaudet.edu	1
Faculty	Dept of Family &amp; Child Studies	Weber	Samuel	L	samuel.weber@gallaudet.edu	1
Staff	Development Office	Carter	Lynda	\N	lynda.carter@gallaudet.edu	1
Staff	Development Office	Castle	Brittany	\N	brittany.castle@gallaudet.edu	1
Staff	Development Office	Dark	Chandra	F	chandra.dark@gallaudet.edu	1
Staff	Development Office	Davis	Felicia	A.	felicia.davis@gallaudet.edu	1
Staff	Development Office	Fleming	Anita	P	anita.fleming@gallaudet.edu	1
Staff	Development Office	Gould	Nicholas	Todd	nicholas.gould@gallaudet.edu	1
Staff	Development Office	Johnson	Lauren	\N	lauren.johnson@gallaudet.edu	1
Staff	Development Office	Jones Rease	Danielle	\N	danielle.jones.rease@gallaudet.edu	1
Staff	Development Office	Moriarty Harrelson	Erin	Elizabeth	erin.moriarty.harrelson@gallaudet.edu	1
Staff	Development Office	Murray	Lynne	\N	lynne.murray@gallaudet.edu	1
Staff	Development Office	Pluviose	Nathalie	\N	nathalie.pluviose@gallaudet.edu	1
Staff	Development Office	Polk	Allison	Michael	allison.polk@gallaudet.edu	1
Staff	Development Office	Reekers	David	Rubin	david.reekers@gallaudet.edu	1
Staff	Development Office	Sickon	Matthew	Nash	matthew.sickon@gallaudet.edu	1
Staff	Development Office	Stone	Elizabeth	D	elizabeth.stone@gallaudet.edu	1
Staff	Development Office	Sturm	Deborah	\N	deborah.sturm@gallaudet.edu	1
Staff	Development Office	Yearout	Danielle	Puzio	danielle.yearout@gallaudet.edu	1
Staff	Discovery Scholarship	Acuff-Passi	Tracy	Lynn	tracy.acuff-passi@gallaudet.edu	1
Staff	Discovery Scholarship	Arazi	Dana	Joseph	dana.arazi@gallaudet.edu	1
Staff	Discovery Scholarship	Atwood	Michael	John	michael.atwood@gallaudet.edu	1
Staff	Discovery Scholarship	DeMare	Anthony	Rosario	anthony.demare@gallaudet.edu	1
Staff	Discovery Scholarship	Drefs	Megan	\N	megan.drefs@gallaudet.edu	1
Staff	Discovery Scholarship	Khouri	Paulus	Ghalib	paulus.khouri@gallaudet.edu	1
Staff	Diversity &amp; Equity forStudents	Guillermo	Elvia	\N	elvia.guillermo@gallaudet.edu	1
Staff	Diversity &amp; Equity forStudents	Levinson	Chaim	Malachy	chaim.levinson@gallaudet.edu	1
Staff	Diversity &amp; Equity forStudents	Molock	Eloise	\N	eloise.molock@gallaudet.edu	1
Staff	Diversity &amp; Equity forStudents	Palmer	Edgar	B	edgar.palmer@gallaudet.edu	1
Staff	ED / GURIEC	Hollrah	Beverly	J	beverly.hollrah@gallaudet.edu	1
Faculty	ED/Combine Person Develop 7/13	Frank	Audrey	K	audrey.frank@gallaudet.edu	1
Staff	ED/RERC Hearing Enh 9/2013	Baxter	Jodi	H	jodi.baxter@gallaudet.edu	1
Staff	ED/RERC Hearing Enh 9/2013	Bernstein	Claire	M	claire.bernstein@gallaudet.edu	1
Staff	ED/RERC Hearing Enh 9/2013	Perry	Trevor	\N	trevor.perry@gallaudet.edu	1
Faculty	Education	Appanah	Thangi	Melanie	thangi.appanah@gallaudet.edu	1
Faculty	Education	Batamula	Christi	Lee	christi.batamula@gallaudet.edu	1
Faculty	Education	Garate	Maribel	\N	maribel.garate@gallaudet.edu	1
Staff	Education	Henson	Tramell	J	tramell.henson@gallaudet.edu	1
Faculty	Education	Hile	Amy	Elizabeth	amy.hile@gallaudet.edu	1
Faculty	Education	Keith	Cara	Lee	cara.keith@gallaudet.edu	1
Staff	Education	Kinsler	Tammy	V	tammy.kinsler@gallaudet.edu	1
Faculty	Education	Kite	Bobbie	Jo	bobbie.kite@gallaudet.edu	1
Faculty	Education	Kruse-McConville	Brenda	Lynn	brenda.kruse-mcconville@gallaudet.edu	1
Faculty	Education	Kuntze	Marlon	Henry	marlon.kuntze@gallaudet.edu	1
Faculty	Education	Mangrubang	Fred	R	fred.mangrubang@gallaudet.edu	1
Faculty	Education	Mitchiner	Julie	Cantrell	julie.mitchiner@gallaudet.edu	1
Faculty	Education	Neese Bailes	Cynthia	\N	cynthia.neese.bailes@gallaudet.edu	1
Faculty	Education	Sass-Lehrer	Marilyn	A	marilyn.sass-lehrer@gallaudet.edu	1
Faculty	Education	Simms	Laurene	E	laurene.simms@gallaudet.edu	1
Faculty	Education	Theoharis	Nena	Raschelle	nena.theoharis@gallaudet.edu	1
Faculty	Education	Thumann	Helen	R	helen.thumann@gallaudet.edu	1
Faculty	Education	Vold	Florence	C	florence.vold@gallaudet.edu	1
Faculty	Education	Yuknis	Christina	Marie	christina.yuknis@gallaudet.edu	1
Faculty	Education Foundation/Research	Clark	Mary	Diane	mary.clark@gallaudet.edu	1
Faculty	Education Foundation/Research	Gerner De Garcia	Barbara	\N	barbara.gerner.de.garcia@gallaudet.edu	1
Faculty	Education Foundation/Research	Mertens	Donna	M	donna.mertens@gallaudet.edu	1
Faculty	Education Foundation/Research	Wilson	Amy	Terra	amy.wilson@gallaudet.edu	1
Faculty	English	Bradbury	Jill	Marie	jill.bradbury@gallaudet.edu	1
Faculty	English	Edwards	Cynthia	A	cynthia.edwards@gallaudet.edu	1
Faculty	English	Franklin	Paige	Elizabeth	paige.franklin@gallaudet.edu	1
Faculty	English	Harmon	Kristen	Carol	kristen.harmon@gallaudet.edu	1
Faculty	English	Heuer	Christopher	\N	christopher.heuer@gallaudet.edu	1
Faculty	English	Nelson	Jennifer	L	jennifer.nelson@gallaudet.edu	1
Faculty	English	Nickerson	Jane	F	jane.nickerson@gallaudet.edu	1
Faculty	English	O'Connor	Diane	D	diane.oconnor@gallaudet.edu	1
Faculty	English	Pajka	Sharon	Louise	sharon.pajka@gallaudet.edu	1
Faculty	English	Palmer	Carie	L	carie.palmer@gallaudet.edu	1
Faculty	English	Pancost	David	\N	david.pancost@gallaudet.edu	1
Faculty	English	Pettie	Cynthia	\N	cynthia.pettie@gallaudet.edu	1
Faculty	English	Ricasa	Rosalinda	\N	rosalinda.ricasa@gallaudet.edu	1
Faculty	English	Stremlau	Tonya	\N	tonya.stremlau@gallaudet.edu	1
Faculty	English	Taavila	Pia	Seija	pia.taavila@gallaudet.edu	1
Faculty	English	Torres	Franklin	Cristian	franklin.torres@gallaudet.edu	1
Faculty	English	Wood	Kathleen	M	kathleen.wood@gallaudet.edu	1
Staff	English Language Institute	Anderson	Timothy	J	timothy.anderson@gallaudet.edu	1
Staff	English Language Institute	Dakim	Mary	Pam	mary.dakim@gallaudet.edu	1
Staff	English Language Institute	Gabor	Maria	Gemma	maria.gabor@gallaudet.edu	1
Staff	English Language Institute	Gore	Jimmy	Challis	jimmy.gore@gallaudet.edu	1
Staff	English Language Institute	Peiris	Tissa	Vijayananda	tissa.peiris@gallaudet.edu	1
Staff	English Language Institute	Quaynor	Alexander	Q	alexander.quaynor@gallaudet.edu	1
Staff	English Language Institute	Sanjabi	Ali	\N	ali.sanjabi@gallaudet.edu	1
Faculty	English Language Institute	Smith	Julianna	Rose	julianna.smith@gallaudet.edu	1
Staff	Enrollment Management Services	Finklea	Sara	Caroline	sara.finklea@gallaudet.edu	1
Staff	Enrollment Management Services	McKenzie	Alexis	\N	alexis.mckenzie@gallaudet.edu	1
Staff	Enrollment Management Services	Saunders	Jesse	Benjamin	jesse.saunders@gallaudet.edu	1
Staff	Enrollment Management Services	Trudo	Jason	\N	jason.trudo@gallaudet.edu	1
Staff	Extension Centers	Bixler	Emma	Jeanette	emma.bixler@gallaudet.edu	1
Staff	Facilities	Banks	William	\N	william.banks@gallaudet.edu	1
Staff	Facilities	Batten-Mickens	Meloyde	\N	meloyde.batten-mickens@gallaudet.edu	1
Staff	Facilities	Bogans	William	\N	william.bogans@gallaudet.edu	1
Staff	Facilities	Cavanaugh	Gwendolyn	\N	gwendolyn.cavanaugh@gallaudet.edu	1
Staff	Facilities	Johnson	Della	Reese	della.johnson@gallaudet.edu	1
Staff	Facilities	Kebede	Getahun	\N	getahun.kebede@gallaudet.edu	1
Staff	Facilities	Kerins	Sean	\N	sean.kerins@gallaudet.edu	1
Staff	Facilities	Lasko	Justin	Thomas	justin.lasko@gallaudet.edu	1
Staff	Facilities	Newkirk	Antonio	D	antonio.newkirk@gallaudet.edu	1
Staff	Facilities	Nichols	Sharon	\N	sharon.nichols@gallaudet.edu	1
Staff	Facilities	Parker	Patricia	\N	patricia.parker@gallaudet.edu	1
Staff	Finance Office	Aciek	Makur	\N	makur.aciek@gallaudet.edu	1
Staff	Finance Office	Bertrand	Bernadine	\N	bernadine.bertrand@gallaudet.edu	1
Staff	Finance Office	Cibuzar	Jean	M	jean.cibuzar@gallaudet.edu	1
Staff	Finance Office	Clark	Stephanie	\N	stephanie.clark@gallaudet.edu	1
Staff	Finance Office	DaSilva	Stephen	William	stephen.dasilva@gallaudet.edu	1
Staff	Finance Office	DeSiervi	Joseph	A	joseph.desiervi@gallaudet.edu	1
Staff	Finance Office	Djaouga	Contina	A	contina.djaouga@gallaudet.edu	1
Staff	Finance Office	Gorham	Lynn	R	lynn.gorham@gallaudet.edu	1
Staff	Finance Office	Leach	Jeffrey	V	jeffrey.leach@gallaudet.edu	1
Staff	Finance Office	Leung	Wing	Yin	wing.leung@gallaudet.edu	1
Staff	Finance Office	McNally	Barbara	\N	barbara.mcnally@gallaudet.edu	1
Staff	Finance Office	Reinbold	Juan	\N	juan.reinbold@gallaudet.edu	1
Staff	Finance Office	Sealock	Victoria	A	victoria.sealock@gallaudet.edu	1
Staff	Finance Office	Shaw	Deborah	F.	deborah.shaw@gallaudet.edu	1
Staff	Finance Office	Spillane	Katie	\N	katie.spillane@gallaudet.edu	1
Staff	Finance Office	Tsai	Yun-Han	R	yun-han.tsai@gallaudet.edu	1
Staff	Finance Office	Wyatt	Mary	\N	mary.wyatt@gallaudet.edu	1
Staff	Financial Aid	Aybar-Morales	Miriam	\N	miriam.aybar-morales@gallaudet.edu	1
Staff	Financial Aid	Benjamin	Shirley	\N	shirley.benjamin@gallaudet.edu	1
Staff	Financial Aid	Dickson Mitchell	Shondra	Camille	shondra.dickson.mitchell@gallaudet.edu	1
Staff	Financial Aid	DiMeo	Sabatino	\N	sabatino.dimeo@gallaudet.edu	1
Staff	Financial Aid	Grossinger	Janel	L	janel.grossinger@gallaudet.edu	1
Staff	Financial Aid	Shaw	Justin	\N	justin.shaw@gallaudet.edu	1
Faculty	Foreign Lang, Lit &amp; Cultures	Berdichevsky	Cristina	\N	cristina.berdichevsky@gallaudet.edu	1
Faculty	Foreign Lang, Lit &amp; Cultures	Herrera	Roberto	\N	roberto.herrera@gallaudet.edu	1
Faculty	Foreign Lang, Lit &amp; Cultures	Holzrichter	Amanda	\N	amanda.holzrichter@gallaudet.edu	1
Faculty	Foreign Lang, Lit &amp; Cultures	Mitchell	Janice	\N	janice.mitchell@gallaudet.edu	1
Faculty	Foreign Lang, Lit &amp; Cultures	Pinar	Pilar	\N	pilar.pinar@gallaudet.edu	1
Faculty	Foreign Lang, Lit &amp; Cultures	Thompson	Linda	\N	linda.thompson@gallaudet.edu	1
Faculty	Foreign Lang, Lit &amp; Cultures	Weinberg	Mark	\N	mark.weinberg@gallaudet.edu	1
Staff	Foreign Lang, Lit &amp; Cultures	West	Denise	A	denise.west@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Alley	Erica	Lauren	erica.alley@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Bahl	Kari	Elizabeth	kari.bahl@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Barnes	Candas	Irene	candas.barnes@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Bartley	Adam	T	adam.bartley@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Chino	Yoshiko	Anna	yoshiko.chino@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Christie	Paul	Charles	paul.christie@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Collins	Pamela	Franciene	pamela.collins@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Craig	Kim	Marie	kim.craig@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	De Witt	Nancy	Marie	nancy.de.witt@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Deja	Stephanie	S.	stephanie.deja@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Doremus	Marianne	\N	marianne.doremus@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Dutton	Loriel	\N	loriel.dutton@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Eger	Amanda	Leigh	amanda.eger@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Ferracuti	Riccardo	\N	riccardo.ferracuti@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Fields	Christi	Antoria	christi.fields@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Ford	Folami	M	folami.ford@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Fournier	Kelly	Lynn	kelly.fournier@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Gouby	Gino	Scott	gino.gouby@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Gregorich	Elizabeth	Graham	elizabeth.gregorich@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Grigg	Kirsi	Mari	kirsi.grigg@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Grundy	Shannon	Marie	shannon.grundy@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Hardison	Jeffrey	A	jeffrey.hardison@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Holmes	Marc	\N	marc.holmes@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Hunt	Danielle	Ivy Joan	danielle.hunt@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Kimble	Stephen	Michael	stephen.kimble@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Lanasa	Amy	K	amy.lanasa@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Malone	Kamille	\N	kamille.malone@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Markel	Diana	\N	diana.markel@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Marquis-Grauze	Lisa	Anne	lisa.marquis-grauze@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Mueller	Amanda	H.F.	amanda.mueller@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Obermiller	Laureen	Mae	laureen.obermiller@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Perry	Steven	G.	steven.perry@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Plaster	Rayni	Elaine	rayni.plaster@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Ressler	Carolyn	I	carolyn.ressler@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Rogers	Phyllis	C	phyllis.rogers@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Schein	Ellen	\N	ellen.schein@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Sedgwick	Mark	A.	mark.sedgwick@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Smith	Caitlin	H	caitlin.smith@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Terry	Matthew	A	matthew.terry@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Trzebny	Jason	\N	jason.trzebny@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Walker	Steven	G	steven.walker@gallaudet.edu	1
Staff	Gallaudet Interpreting Service	Yost	Jamie	Ann	jamie.yost@gallaudet.edu	1
Staff	Gallaudet Leadership Institute	Davis	Azalea	L.	azalea.davis@gallaudet.edu	1
Staff	Gallaudet Leadership Institute	Guteng	Simon	I	simon.guteng@gallaudet.edu	1
Staff	Gallaudet Press Administrative	Clark	Frances	W	frances.clark@gallaudet.edu	1
Staff	Gallaudet Press Administrative	Mullervy	Deirdre	\N	deirdre.mullervy@gallaudet.edu	1
Staff	Gallaudet Press Administrative	Simmons	Valencia	\N	valencia.simmons@gallaudet.edu	1
Staff	Gallaudet Press Administrative	Wallace	Daniel	C	daniel.wallace@gallaudet.edu	1
Staff	Gallaudet Press Administrative	Wallace	Ivey	P	ivey.wallace@gallaudet.edu	1
Staff	Gallaudet Research Institute	Barac-Cikoja	Dragana	\N	dragana.barac-cikoja@gallaudet.edu	1
Staff	Gallaudet Research Institute	Benaissa	Senda	\N	senda.benaissa@gallaudet.edu	1
Staff	Gallaudet Research Institute	Cole	Kevin	J	kevin.cole@gallaudet.edu	1
Staff	Gallaudet Research Institute	Hack-McCafferty	Shirley	D	shirley.hack-mccafferty@gallaudet.edu	1
Staff	Gallaudet Research Institute	Lam	Kay	H	kay.lam@gallaudet.edu	1
Staff	Gallaudet Research Institute	Qi	Sen	\N	sen.qi@gallaudet.edu	1
Staff	Gallaudet Research Institute	Reilly	Charles	\N	charles.reilly@gallaudet.edu	1
Staff	Gallaudet Research Institute	Thumann-Prezioso	Carlene	\N	carlene.thumann-prezioso@gallaudet.edu	1
Staff	Gallaudet Research Institute	Winiarczyk	Rowena	Elizabeth	rowena.winiarczyk@gallaudet.edu	1
Staff	Gallaudet Research Institute	Woo	John	K	john.woo@gallaudet.edu	1
Staff	Gallaudet Technology Services	Adams	James	D	james.adams@gallaudet.edu	1
Staff	Gallaudet Technology Services	Alexis	Dorothy	\N	dorothy.alexis@gallaudet.edu	1
Staff	Gallaudet Technology Services	Amrozowicz	Keith	\N	keith.amrozowicz@gallaudet.edu	1
Staff	Gallaudet Technology Services	Arce	Cedric	C.	cedric.arce@gallaudet.edu	1
Staff	Gallaudet Technology Services	Augustine	Shannon	Lesley Kennedy	shannon.augustine@gallaudet.edu	1
Staff	Gallaudet Technology Services	Babson	Norvan	Kent	norvan.babson@gallaudet.edu	1
Staff	Gallaudet Technology Services	Baker	Richard	R	richard.r.baker@gallaudet.edu	1
Staff	Gallaudet Technology Services	Barretto	Harold	W	harold.barretto@gallaudet.edu	1
Staff	Gallaudet Technology Services	Bennett	Rosemary	\N	rosemary.bennett@gallaudet.edu	1
Staff	Gallaudet Technology Services	Bowie	Charles	E	charles.bowie@gallaudet.edu	1
Staff	Gallaudet Technology Services	Brooks	Gary	W	gary.brooks@gallaudet.edu	1
Staff	Gallaudet Technology Services	Burke	Christian	B	christian.burke@gallaudet.edu	1
Staff	Gallaudet Technology Services	Dellon	James	R	james.dellon@gallaudet.edu	1
Staff	Gallaudet Technology Services	Drawdy	Charles	E	charles.drawdy@gallaudet.edu	1
Staff	Gallaudet Technology Services	Ferguson	Thad	Lee	thad.ferguson@gallaudet.edu	1
Staff	Gallaudet Technology Services	Fernandez	Rafael	\N	rafael.fernandez@gallaudet.edu	1
Staff	Gallaudet Technology Services	Fletcher	Clarence	D	clarence.fletcher@gallaudet.edu	1
Staff	Gallaudet Technology Services	Galanter	David	\N	david.galanter@gallaudet.edu	1
Staff	Gallaudet Technology Services	Galich	Stephany	K	stephany.galich@gallaudet.edu	1
Staff	Gallaudet Technology Services	Gorsuch	Patrick	N	patrick.gorsuch@gallaudet.edu	1
Staff	Gallaudet Technology Services	Green	Yvonne	J	yvonne.green@gallaudet.edu	1
Staff	Gallaudet Technology Services	Grossinger	Harvey	I	harvey.grossinger@gallaudet.edu	1
Staff	Gallaudet Technology Services	Harris	Patrick	J	patrick.harris@gallaudet.edu	1
Staff	Gallaudet Technology Services	Hauptman	Sharon	L	sharon.hauptman@gallaudet.edu	1
Staff	Gallaudet Technology Services	Hourihan	Sean	\N	sean.hourihan@gallaudet.edu	1
Staff	Gallaudet Technology Services	Huseby	Kimberly	\N	kimberly.huseby@gallaudet.edu	1
Staff	Gallaudet Technology Services	Jones	Richard	E	richard.jones@gallaudet.edu	1
Staff	Gallaudet Technology Services	Joyner	Wayland	J	wayland.joyner@gallaudet.edu	1
Staff	Gallaudet Technology Services	Kim	Cheol	\N	cheol.kim@gallaudet.edu	1
Staff	Gallaudet Technology Services	Kim	Seung	Hyun	seung.kim@gallaudet.edu	1
Staff	Gallaudet Technology Services	King	James	S	james.king@gallaudet.edu	1
Staff	Gallaudet Technology Services	Klusza	Matthew	George	matthew.klusza@gallaudet.edu	1
Staff	Gallaudet Technology Services	Kowalczyk	Jay	Lyman	jay.kowalczyk@gallaudet.edu	1
Staff	Gallaudet Technology Services	Krigsman	Joel	P	joel.krigsman@gallaudet.edu	1
Staff	Gallaudet Technology Services	Lemons	Kafi	S.	kafi.lemons@gallaudet.edu	1
Staff	Gallaudet Technology Services	Longson	Julie	Theresa	julie.longson@gallaudet.edu	1
Staff	Gallaudet Technology Services	Matthews	Francis	Allen	francis.matthews@gallaudet.edu	1
Staff	Gallaudet Technology Services	McGlone	Todd	Michael	todd.mcglone@gallaudet.edu	1
Staff	Gallaudet Technology Services	McKenzie	Suzy	Kay	suzy.mckenzie@gallaudet.edu	1
Staff	Gallaudet Technology Services	Murray	Jeffrey	D	jeffrey.murray@gallaudet.edu	1
Staff	Gallaudet Technology Services	Parks	Earl	Clayton	earl.parks@gallaudet.edu	1
Staff	Gallaudet Technology Services	Petrova-Margason	Maria	\N	maria.petrova-margason@gallaudet.edu	1
Staff	Gallaudet Technology Services	Scheuermann	Suzanne	\N	suzanne.scheuermann@gallaudet.edu	1
Staff	Gallaudet Technology Services	Scotton	John	William	john.scotton@gallaudet.edu	1
Staff	Gallaudet Technology Services	Shelton	Stephanie	\N	stephanie.shelton@gallaudet.edu	1
Staff	Gallaudet Technology Services	Silvestri	Toselli	Ted	toselli.silvestri@gallaudet.edu	1
Staff	Gallaudet Technology Services	Sorkin	Elizabeth	Dena	elizabeth.sorkin@gallaudet.edu	1
Staff	Gallaudet Technology Services	Swann	Leonard	B	leonard.swann@gallaudet.edu	1
Staff	Gallaudet Technology Services	Syriani	Borhan	I	borhan.syriani@gallaudet.edu	1
Staff	Gallaudet Technology Services	Tan	Wilson	M.	wilson.tan@gallaudet.edu	1
Staff	Gallaudet Technology Services	Taylor	Shay	\N	shay.taylor@gallaudet.edu	1
Staff	Gallaudet Technology Services	Traxler	Susan	M	susan.traxler@gallaudet.edu	1
Staff	Gallaudet Technology Services	Un	Peter	K	peter.un@gallaudet.edu	1
Staff	Gallaudet Technology Services	Vali	Hatim	A	hatim.vali@gallaudet.edu	1
Staff	Gallaudet Technology Services	Washington	Laura	A.	laura.washington@gallaudet.edu	1
Staff	Gallaudet Technology Services	Wenger	Michael	Eric	michael.wenger@gallaudet.edu	1
Staff	Gallaudet Technology Services	Whitaker	Jeffrey	\N	jeffrey.whitaker@gallaudet.edu	1
Staff	Gallaudet Technology Services	White	Barry	N	barry.white@gallaudet.edu	1
Staff	Gallaudet Technology Services	Youens-Un	Sheri	Alana	sheri.youens-un@gallaudet.edu	1
Staff	Gallaudet Technology Services	Young	Kindsey	L	kindsey.young@gallaudet.edu	1
Faculty	General Studies	Baldridge	Kathryn	A	kathryn.baldridge@gallaudet.edu	1
Faculty	General Studies	Buchko	Lindsay	A	lindsay.buchko@gallaudet.edu	1
Faculty	General Studies	Dillehay	Jane	R	jane.dillehay@gallaudet.edu	1
Faculty	General Studies	Dzougoutov	Marina	\N	marina.dzougoutov@gallaudet.edu	1
Faculty	General Studies	Malzkuhn	Matthew	Louis	matthew.malzkuhn@gallaudet.edu	1
Faculty	General Studies	Marc-Charles	Sylvie	\N	sylvie.marc-charles@gallaudet.edu	1
Staff	General Studies	Parker	Lisa	\N	lisa.parker@gallaudet.edu	1
Faculty	General Studies	Rach	Leslie	A	leslie.rach@gallaudet.edu	1
Faculty	General Studies	Stevens	Amy	\N	amy.stevens@gallaudet.edu	1
Staff	GIS-Community Interpreting Svc	Withrow	Dale	\N	dale.withrow@gallaudet.edu	1
Staff	Graduate School	King	Susan	J	susan.king@gallaudet.edu	1
Staff	Graduate School	Luria-Appell	Deborah	Ann	deborah.luria-appell@gallaudet.edu	1
Staff	Graduate School	Madeo	Margarita	B	margarita.madeo@gallaudet.edu	1
Staff	Graduate School	Young	Hank	\N	hank.young@gallaudet.edu	1
Staff	Grounds Services	Alfonso	Elio	\N	elio.alfonso@gallaudet.edu	1
Staff	Grounds Services	Borges	Anthony	J	anthony.borges@gallaudet.edu	1
Staff	Grounds Services	Cheek	Ronald	E	ronald.cheek@gallaudet.edu	1
Staff	Grounds Services	Gates	James	A.	james.gates@gallaudet.edu	1
Staff	Grounds Services	Haselhuhn	Trudy	L	trudy.haselhuhn@gallaudet.edu	1
Staff	Grounds Services	Montesdeoca	Juan	W.	juan.montesdeoca@gallaudet.edu	1
Staff	Grounds Services	Morant	Leverne	\N	leverne.morant@gallaudet.edu	1
Staff	Grounds Services	Navas	Carlos	\N	carlos.navas@gallaudet.edu	1
Staff	Grounds Services	Smith	John	W.	john.smith@gallaudet.edu	1
Staff	Grounds Services	Wood	Darryl	P.	darryl.wood@gallaudet.edu	1
Staff	Grounds Services	Woods	Chris	\N	chris.woods@gallaudet.edu	1
Staff	GTS Enterprise Info Systems	Canning	Elwyn	\N	elwyn.canning@gallaudet.edu	1
Staff	Hearing and Speech Center	Allen	Khera	B	khera.allen@gallaudet.edu	1
Staff	Hearing and Speech Center	Jefferson	April	Camille	april.jefferson@gallaudet.edu	1
Staff	Hearing and Speech Center	Saleem	Naheed	\N	naheed.saleem@gallaudet.edu	1
Staff	High School Academic Bowl	McKnight	Thomas	Michael	thomas.mcknight@gallaudet.edu	1
Faculty	History	Bergen	Barry	H	barry.bergen@gallaudet.edu	1
Faculty	History	Brune	Jeffrey	\N	jeffrey.brune@gallaudet.edu	1
Faculty	History	Ennis	William	T	william.ennis@gallaudet.edu	1
Faculty	History	Greenwald	Brian	H.	brian.greenwald@gallaudet.edu	1
Faculty	History	Kinner	Joseph	G	joseph.kinner@gallaudet.edu	1
Faculty	History	Marquez	Frances	\N	frances.marquez@gallaudet.edu	1
Faculty	History	Penna	David	R	david.penna@gallaudet.edu	1
Faculty	History	Sanchez	Robert	\N	robert.sanchez@gallaudet.edu	1
Faculty	History	Vrbetic	Marta	\N	marta.vrbetic@gallaudet.edu	1
Staff	HMCT Payroll	Herrera	Jose	A	jose.herrera@gallaudet.edu	1
Staff	HMCT Payroll	Humm	William	J	william.humm@gallaudet.edu	1
Staff	Honors Program	McIntosh	Gladys	O	gladys.mcintosh@gallaudet.edu	1
Staff	Honors Program	Peruzzi	Meredith	Michelle	meredith.peruzzi@gallaudet.edu	1
Faculty	Honors Program	Shultz Myers	Shirley	\N	shirley.shultz.myers@gallaudet.edu	1
Staff	Honors Program	Whitebread	Geoffrey	Alan	geoffrey.whitebread@gallaudet.edu	1
Staff	Human Resources Services	Boswell	Marylynn	\N	marylynn.boswell@gallaudet.edu	1
Staff	Human Resources Services	Cohen	Erin	C	erin.cohen@gallaudet.edu	1
Staff	Human Resources Services	Daniels	Shana	Ann	shana.daniels@gallaudet.edu	1
Staff	Human Resources Services	Jones	Joanne	B	joanne.jones@gallaudet.edu	1
Staff	Human Resources Services	Kempton	Ryan	\N	ryan.kempton@gallaudet.edu	1
Staff	Human Resources Services	Klassen	Brent	R	brent.klassen@gallaudet.edu	1
Staff	Human Resources Services	Locks	Barbara	R	barbara.locks@gallaudet.edu	1
Staff	Human Resources Services	Mitton	Evelyne	\N	evelyne.mitton@gallaudet.edu	1
Staff	Human Resources Services	Muse	Agnes	M	agnes.muse@gallaudet.edu	1
Staff	Human Resources Services	Shen-Austin	Christina	\N	christina.shen-austin@gallaudet.edu	1
Staff	Human Resources Services	Vance	Janet	Elaine	janet.vance@gallaudet.edu	1
Staff	Human Resources Services	White	Chani	B.	chani.white@gallaudet.edu	1
Staff	Human Resources Services	Whitfield	Linda	Theresa	linda.whitfield@gallaudet.edu	1
Staff	Human Resources Services	Wilson	Cassie	B	cassie.wilson@gallaudet.edu	1
Staff	International Relations Office	Lovik	Julianna	Marie	julianna.lovik@gallaudet.edu	1
Staff	International Relations Office	Mason	Asiah	\N	asiah.mason@gallaudet.edu	1
Staff	International Relations Office	Torres	Danilo	E.	danilo.torres@gallaudet.edu	1
Staff	Judical Affairs	Goldberg	Hillel	A	hillel.goldberg@gallaudet.edu	1
Staff	KDES Library	Sadoski	Cynthia	J	cynthia.sadoski@gallaudet.edu	1
Staff	Keeping the Promise	Aina	Olugbenga	\N	olugbenga.aina@gallaudet.edu	1
Staff	Keeping the Promise	Lozano-Martinez	Delia	Raquel	delia.lozano-martinez@gallaudet.edu	1
Staff	Language Plan CAEBER	Nover	Stephen	Michael	stephen.nover@gallaudet.edu	1
Staff	Library	Bills	David	\N	david.bills@gallaudet.edu	1
Staff	Library	Fox	Sandra	L	sandra.fox@gallaudet.edu	1
Staff	Library	Hahn	Seung	\N	seung.hahn@gallaudet.edu	1
Staff	Library	Hamrick	Sarah	E	sarah.hamrick@gallaudet.edu	1
Staff	Library	Hedberg	Karl	Ulf Wilhelm	karl.hedberg@gallaudet.edu	1
Staff	Library	Henry	Elizabeth	\N	elizabeth.henry@gallaudet.edu	1
Staff	Library	Jacobi	Laura	A	laura.jacobi@gallaudet.edu	1
Staff	Library	McCarthy	James	K	james.mccarthy@gallaudet.edu	1
Staff	Library	Moore	Diana	Lyn	diana.moore@gallaudet.edu	1
Staff	Library	Oberholtzer	Patrick	B	patrick.oberholtzer@gallaudet.edu	1
Staff	Library	Olson	Michael	J	michael.olson@gallaudet.edu	1
Staff	Library	Peterson	Jeffrey	Kraig	jeffrey.peterson@gallaudet.edu	1
Staff	Library	Rivas	Valerie	C	valerie.rivas@gallaudet.edu	1
Staff	Library	Smith	Jamie	Nichole	jamie.smith@gallaudet.edu	1
Staff	Maintenance Services	Alsobrooks	James	R	james.alsobrooks@gallaudet.edu	1
Staff	Maintenance Services	Banks	Stanley	D	stanley.banks@gallaudet.edu	1
Staff	Maintenance Services	Barker	Douglas	W.	douglas.barker@gallaudet.edu	1
Staff	Maintenance Services	Beaton	Robert	M.	robert.beaton@gallaudet.edu	1
Staff	Maintenance Services	Beaver	Mark	Alan	mark.beaver@gallaudet.edu	1
Staff	Maintenance Services	Brown	Amon	R	amon.brown@gallaudet.edu	1
Staff	Maintenance Services	Bunting	John	D.	john.bunting@gallaudet.edu	1
Staff	Maintenance Services	Casey	John	E	john.casey@gallaudet.edu	1
Staff	Maintenance Services	Delauder	Michael	B	michael.delauder@gallaudet.edu	1
Staff	Maintenance Services	Dickerson	Robert	V	robert.dickerson@gallaudet.edu	1
Staff	Maintenance Services	Dudley	Mark	A.	mark.dudley@gallaudet.edu	1
Staff	Maintenance Services	Dzougoutov	Konstantin	Z.	konstantin.dzougoutov@gallaudet.edu	1
Staff	Maintenance Services	Easterly	Michael	P	michael.easterly@gallaudet.edu	1
Staff	Maintenance Services	Edelen	David	L	david.edelen@gallaudet.edu	1
Staff	Maintenance Services	Fortiz	Sigfrido	\N	sigfrido.fortiz@gallaudet.edu	1
Staff	Maintenance Services	Hill	Thomas	A.	thomas.hill@gallaudet.edu	1
Staff	Maintenance Services	Horton	John	G	john.horton@gallaudet.edu	1
Staff	Maintenance Services	Ingram	Norman	T	norman.ingram@gallaudet.edu	1
Staff	Maintenance Services	Janschek	John	J	john.janschek@gallaudet.edu	1
Staff	Maintenance Services	McCoy	Laurence	D	laurence.mccoy@gallaudet.edu	1
Staff	Maintenance Services	Min	Pyong	Yol	pyong.min@gallaudet.edu	1
Staff	Maintenance Services	Nega	Nebiyu	\N	nebiyu.nega@gallaudet.edu	1
Staff	Maintenance Services	Segreti	Paul	G	paul.segreti@gallaudet.edu	1
Staff	Maintenance Services	Spurr	Christopher	Wayne	christopher.spurr@gallaudet.edu	1
Staff	Maintenance Services	Stevenson	Troy	L	troy.stevenson@gallaudet.edu	1
Staff	Maintenance Services	Tabron	James	\N	james.tabron@gallaudet.edu	1
Staff	Maintenance Services	Thomas	Brian	K	brian.thomas@gallaudet.edu	1
Staff	Maintenance Services	Wilkins	DeAndre	S.	deandre.wilkins@gallaudet.edu	1
Staff	Master Plan	Davidson	Liletha	A	liletha.davidson@gallaudet.edu	1
Faculty	Math &amp; Computer Science	Henderson	Susanna	\N	susanna.henderson@gallaudet.edu	1
Faculty	Math &amp; Computer Science	Lam	Fat	C	fat.lam@gallaudet.edu	1
Faculty	Math &amp; Computer Science	Nickerson	James	A	james.nickerson@gallaudet.edu	1
Faculty	Math &amp; Computer Science	Nuzzo	Regina	L.	regina.nuzzo@gallaudet.edu	1
Faculty	Math &amp; Computer Science	Obiedat	Mohammad	\N	mohammad.obiedat@gallaudet.edu	1
Faculty	Math &amp; Computer Science	Phipps	Mark	Allen	mark.phipps@gallaudet.edu	1
Faculty	Math &amp; Computer Science	Shank	Vicki	J	vicki.shank@gallaudet.edu	1
Staff	Math &amp; Computer Science	Southwell	Leslie	J	leslie.southwell@gallaudet.edu	1
Faculty	Math &amp; Computer Science	Tseng	Tsuihsia	I	tsuihsia.tseng@gallaudet.edu	1
Faculty	Matric Grad Students - Summer	Beam	Micheline	\N	micheline.beam@gallaudet.edu	1
Faculty	Matric Grad Students - Summer	Brand	Jeanne	Marie	jeanne.brand@gallaudet.edu	1
Faculty	Matric Grad Students - Summer	Brown-Kurz	Kim	L	kim.brown-kurz@gallaudet.edu	1
Faculty	Matric Grad Students - Summer	Jenkins	Valata	\N	valata.jenkins@gallaudet.edu	1
Faculty	Matric Grad Students - Summer	Mock	Matthew	\N	matthew.mock@gallaudet.edu	1
Staff	Mellon Deaf Digital Library	Bartoli	Anthony	Ross	anthony.bartoli@gallaudet.edu	1
Staff	Mental Health Center	Kachman	William	P	william.kachman@gallaudet.edu	1
Staff	Mental Health Center	Patterson	Kasi	Gabriel	kasi.patterson@gallaudet.edu	1
Staff	Mental Health Center	Pugh	Lisa	R	lisa.pugh@gallaudet.edu	1
Staff	Mental Health Center	Rush	Lauri	L	lauri.rush@gallaudet.edu	1
Staff	Mental Health Center	Schilling	Ruth	Marie	ruth.schilling@gallaudet.edu	1
Staff	Mental Health Center	Shird	Carla	\N	carla.shird@gallaudet.edu	1
Staff	Mental Health Center	Vandegrift	Randi	\N	randi.vandegrift@gallaudet.edu	1
Staff	Mental Health Center	Zelaya	Doris	Maybelline	doris.zelaya@gallaudet.edu	1
Staff	Molecular Genetics Laboratory	Craft	Elizabeth	A	elizabeth.craft@gallaudet.edu	1
Staff	MSSD Departments	Acton	Bobby	Joe	bobby.acton@gallaudet.edu	1
Staff	MSSD Departments	Adamca-Balzer	Rosemary	R	rosemary.adamca-balzer@gallaudet.edu	1
Staff	MSSD Departments	Alston	Dwight	\N	dwight.alston@gallaudet.edu	1
Staff	MSSD Departments	Baldi	Ronald	C	ronald.baldi@gallaudet.edu	1
Staff	MSSD Departments	Beam	Fred	Michael	fred.beam@gallaudet.edu	1
Staff	MSSD Departments	Bogdan	Philip	S	philip.bogdan@gallaudet.edu	1
Staff	MSSD Departments	Brecheen	Michael	\N	michael.brecheen@gallaudet.edu	1
Staff	MSSD Departments	Catt	Yvonne	Michelle	yvonne.catt@gallaudet.edu	1
Staff	MSSD Departments	Cetrano	Jonathan	Warren	jonathan.cetrano@gallaudet.edu	1
Staff	MSSD Departments	Edwards	Mary	\N	mary.edwards@gallaudet.edu	1
Staff	MSSD Departments	Gerald	Ida	B	ida.gerald@gallaudet.edu	1
Staff	MSSD Departments	Herbert	Lisa	Brenda	lisa.herbert@gallaudet.edu	1
Staff	MSSD Departments	Hollywood	Michael	J	michael.hollywood@gallaudet.edu	1
Staff	MSSD Departments	Hunt	Cynthia	Dawn	cynthia.hunt@gallaudet.edu	1
Staff	MSSD Departments	Lothridge	Joshua	Daniel	joshua.lothridge@gallaudet.edu	1
Staff	MSSD Departments	Malone	Amy	Leigh	amy.malone@gallaudet.edu	1
Staff	MSSD Departments	Matthews	Sharon	Louise	sharon.matthews@gallaudet.edu	1
Staff	MSSD Departments	Monahan Sewell	Kitty	Ann	kitty.monahan.sewell@gallaudet.edu	1
Staff	MSSD Departments	Montalvo	Lisa	Marie	lisa.montalvo@gallaudet.edu	1
Staff	MSSD Departments	Naeem	Taiyabah	Ahmed	taiyabah.naeem@gallaudet.edu	1
Staff	MSSD Departments	Nasukiewicz	Jennifer	\N	jennifer.nasukiewicz@gallaudet.edu	1
Staff	MSSD Departments	Naumann	Jill	Denise	jill.naumann@gallaudet.edu	1
Staff	MSSD Departments	Neumann	Sarah	\N	sarah.neumann@gallaudet.edu	1
Staff	MSSD Departments	Opauski	Marsha	M	marsha.opauski@gallaudet.edu	1
Staff	MSSD Departments	Palka	Dennis	Thomas	dennis.palka@gallaudet.edu	1
Staff	MSSD Departments	Parkinson	Robert	Fitzgerald	robert.parkinson@gallaudet.edu	1
Staff	MSSD Departments	Perry	James	M	james.perry@gallaudet.edu	1
Staff	MSSD Departments	Peterson	Michael	\N	michael.peterson@gallaudet.edu	1
Staff	MSSD Departments	Phipps	Maria	Rose	maria.phipps@gallaudet.edu	1
Staff	MSSD Departments	Pickering	Melissa	Dawn	melissa.pickering@gallaudet.edu	1
Staff	MSSD Departments	Ramroop	Abiodun	Olawale	abiodun.ramroop@gallaudet.edu	1
Staff	MSSD Departments	Sandle	Jessica	A	jessica.sandle@gallaudet.edu	1
Staff	MSSD Departments	Schreiner	Emily	\N	emily.schreiner@gallaudet.edu	1
Staff	MSSD Departments	Seremeth	Mary	Ann	mary.seremeth@gallaudet.edu	1
Staff	MSSD Departments	Spinosi	Luciana	Melinda	luciana.spinosi@gallaudet.edu	1
Staff	MSSD Departments	Stallard	Sara	Rachel	sara.stallard@gallaudet.edu	1
Staff	MSSD Departments	Talbert	Allen	G	allen.talbert@gallaudet.edu	1
Staff	MSSD Departments	Tao	Mark	C	mark.tao@gallaudet.edu	1
Staff	MSSD Departments	Walla	Nancy	\N	nancy.walla@gallaudet.edu	1
Staff	MSSD Departments	Ward	Charlene	Ann	charlene.ward@gallaudet.edu	1
Staff	MSSD Departments	Warn	David	Justin	david.warn@gallaudet.edu	1
Staff	MSSD Departments	Weaver	Georgia	Edwards	georgia.weaver@gallaudet.edu	1
Staff	MSSD Departments	Whitaker	Robert	A	robert.whitaker@gallaudet.edu	1
Staff	MSSD Departments	Wright	Tyese	Dornie	tyese.wright@gallaudet.edu	1
Staff	MSSD Departments	Wynne	Dorothy	H	dorothy.wynne@gallaudet.edu	1
Staff	MSSD Departments	Zornes-Foster	Heidi	\N	heidi.zornes-foster@gallaudet.edu	1
Staff	MSSD Library	Lopez	Ricardo	\N	ricardo.lopez@gallaudet.edu	1
Staff	Multicultural Student Services	Elkassem	Yekaterina	\N	yekaterina.elkassem@gallaudet.edu	1
Staff	Multicultural Student Services	Gonzalez-Rodriguez	Ivelisse	\N	ivelisse.gonzalez-rodriguez@gallaudet.edu	1
Staff	Multicultural Student Services	Guo	Lin	\N	lin.guo@gallaudet.edu	1
Staff	Multicultural Student Services	Guzman Aguilar	Laura	\N	laura.guzman.aguilar@gallaudet.edu	1
Staff	Multicultural Student Services	Palchik	Guillermo	\N	guillermo.palchik@gallaudet.edu	1
Staff	Multicultural Student Services	Rodriguez-Ortiz	Annette	Marie	annette.rodriguez-ortiz@gallaudet.edu	1
Staff	Multicultural Student Services	Sazo Canales	Timoteo	A	timoteo.sazo.canales@gallaudet.edu	1
Staff	Multicultural Student Services	Szuchman	Sandra	Silvana	sandra.szuchman@gallaudet.edu	1
Staff	NSF-Science Learning Cnt 08/12	Hanumantha	Shilpa	\N	shilpa.hanumantha@gallaudet.edu	1
Staff	NSF/Paid/Pay It Forward 09/13	Pyles	Saida	G	saida.pyles@gallaudet.edu	1
Staff	Ofc of CEMO	Reedy Hines	Charity	\N	charity.reedy.hines@gallaudet.edu	1
Staff	Ofc of Exec Director,Clerc Ctr	Barbin	Cary	Gus	cary.barbin@gallaudet.edu	1
Staff	Ofc of Exec Director,Clerc Ctr	Fallstone	Hollie	Michele	hollie.fallstone@gallaudet.edu	1
Staff	Ofc of Exec Director,Clerc Ctr	Frelich	Timothy	G	timothy.frelich@gallaudet.edu	1
Staff	Ofc of Exec Director,Clerc Ctr	Hotto	Gary	J	gary.hotto@gallaudet.edu	1
Staff	Ofc of Exec Director,Clerc Ctr	Jefferson	Tyrei	D	tyrei.jefferson@gallaudet.edu	1
Staff	Ofc of Exec Director,Clerc Ctr	Pitts	Marteal	Helena	marteal.pitts@gallaudet.edu	1
Staff	Ofc of Exec Director,Clerc Ctr	Prickett	Rosalyn	Gail	rosalyn.prickett@gallaudet.edu	1
Staff	Ofc of Exec Director,Clerc Ctr	Simmons	Karin	Louise	karin.simmons@gallaudet.edu	1
Staff	Ofc of KDES Principal	Ballenger	Phyllis	J	phyllis.ballenger@gallaudet.edu	1
Staff	Ofc of KDES Principal	Bell	Michelle	C.	michelle.bell@gallaudet.edu	1
Staff	Ofc of KDES Principal	Benson	Steven	M	steven.benson@gallaudet.edu	1
Staff	Ofc of KDES Principal	Blachly	Emily	Royce	emily.blachly@gallaudet.edu	1
Staff	Ofc of KDES Principal	Burns	Heidi	\N	heidi.burns@gallaudet.edu	1
Staff	Ofc of KDES Principal	Cingel	Maureen	Ann	maureen.cingel@gallaudet.edu	1
Staff	Ofc of KDES Principal	Cushner	Debra	\N	debra.cushner@gallaudet.edu	1
Staff	Ofc of KDES Principal	DaCosta	Kristin	Beth	kristin.dacosta@gallaudet.edu	1
Staff	Ofc of KDES Principal	Downing	Tara	Joan	tara.downing@gallaudet.edu	1
Staff	Ofc of KDES Principal	Ellis Sandoval	Sharon	Denita	sharon.ellis.sandoval@gallaudet.edu	1
Staff	Ofc of KDES Principal	English	Akilah	Michelle	akilah.english@gallaudet.edu	1
Staff	Ofc of KDES Principal	Erting	Lynne	C	lynne.erting@gallaudet.edu	1
Staff	Ofc of KDES Principal	Furlano	Jennifer	Jeane	jennifer.furlano@gallaudet.edu	1
Staff	Ofc of KDES Principal	Goehring	Senoa	\N	senoa.goehring@gallaudet.edu	1
Staff	Ofc of KDES Principal	Golightly	Michael	Dennis	michael.golightly@gallaudet.edu	1
Staff	Ofc of KDES Principal	Gonzalez	Changer	Miorit	changer.gonzalez@gallaudet.edu	1
Staff	Ofc of KDES Principal	Gordon	Cara	Ann	cara.gordon@gallaudet.edu	1
Staff	Ofc of KDES Principal	Gough	Michelle	Sue	michelle.gough@gallaudet.edu	1
Staff	Ofc of KDES Principal	Hall	Elizabeth	M	elizabeth.hall@gallaudet.edu	1
Staff	Ofc of KDES Principal	Hed-Edington	Carina	G	carina.hed-edington@gallaudet.edu	1
Staff	Ofc of KDES Principal	Jackson	Bonita	Ann	bonita.jackson@gallaudet.edu	1
Staff	Ofc of KDES Principal	Jackson	Charity	Rose	charity.jackson@gallaudet.edu	1
Staff	Ofc of KDES Principal	Jackson	Cookie	Loretta	cookie.jackson@gallaudet.edu	1
Staff	Ofc of KDES Principal	King	Lynn Olden	\N	lynn.olden.king@gallaudet.edu	1
Staff	Ofc of KDES Principal	Lasko	Eugenia	Page	eugenia.lasko@gallaudet.edu	1
Staff	Ofc of KDES Principal	Matthews	Marlene	Theresa	marlene.matthews@gallaudet.edu	1
Staff	Ofc of KDES Principal	McIntyre	Patricia	\N	patricia.mcintyre@gallaudet.edu	1
Staff	Ofc of KDES Principal	Mosholder	Caitlin	Grace	caitlin.mosholder@gallaudet.edu	1
Staff	Ofc of KDES Principal	Munoz-Torres	Sibila	Andrea	sibila.munoz-torres@gallaudet.edu	1
Staff	Ofc of KDES Principal	Nawrocki	Roman	Russell	roman.nawrocki@gallaudet.edu	1
Staff	Ofc of KDES Principal	Nolan	Kristi	Ann	kristi.nolan@gallaudet.edu	1
Staff	Ofc of KDES Principal	O'Connor	Barbara	Ann	barbara.oconnor@gallaudet.edu	1
Staff	Ofc of KDES Principal	Offreda	Liza	Eleonora	liza.offreda@gallaudet.edu	1
Staff	Ofc of KDES Principal	Owens	Jenna	Lee	jenna.owens@gallaudet.edu	1
Staff	Ofc of KDES Principal	Perrodin	Brenda	\N	brenda.perrodin@gallaudet.edu	1
Staff	Ofc of KDES Principal	Pongor	Kathy	N	kathy.pongor@gallaudet.edu	1
Staff	Ofc of KDES Principal	Pouncy	Zenobia	\N	zenobia.pouncy@gallaudet.edu	1
Staff	Ofc of KDES Principal	Rangel	Francisca	\N	francisca.rangel@gallaudet.edu	1
Staff	Ofc of KDES Principal	Reed	Ruth	E	ruth.reed@gallaudet.edu	1
Staff	Ofc of KDES Principal	Reilly	Nipapon	W	nipapon.reilly@gallaudet.edu	1
Staff	Ofc of KDES Principal	Rice	Anna	R.	anna.rice@gallaudet.edu	1
Staff	Ofc of KDES Principal	Richardson	Tiffani	Joy	tiffani.richardson@gallaudet.edu	1
Staff	Ofc of KDES Principal	Richmond	Joseph	\N	joseph.richmond@gallaudet.edu	1
Staff	Ofc of KDES Principal	Romero	Richard	Anthony	richard.romero@gallaudet.edu	1
Staff	Ofc of KDES Principal	Rossoshansky	Tanya	\N	tanya.rossoshansky@gallaudet.edu	1
Staff	Ofc of KDES Principal	Schatz	Susan	Jane	susan.schatz@gallaudet.edu	1
Staff	Ofc of KDES Principal	Shen	Weimin	\N	weimin.shen@gallaudet.edu	1
Staff	Ofc of KDES Principal	Shumate	Melissa	Elaine	melissa.shumate@gallaudet.edu	1
Staff	Ofc of KDES Principal	Sipek	Dana	Lee	dana.sipek@gallaudet.edu	1
Staff	Ofc of KDES Principal	Sussewell	Gwendolyn	V	gwendolyn.sussewell@gallaudet.edu	1
Staff	Ofc of KDES Principal	Turk	Frank	W	frank.turk@gallaudet.edu	1
Staff	Ofc of KDES Principal	Virnig	Nanette	Sara	nanette.virnig@gallaudet.edu	1
Staff	Ofc of KDES Principal	Washington	Darla	\N	darla.washington@gallaudet.edu	1
Staff	Ofc of KDES Principal	Watson	Andrea	Nicara	andrea.watson@gallaudet.edu	1
Staff	Ofc of Vice Pres, Clerc Center	Berrigan	Nancy	Lee	nancy.berrigan@gallaudet.edu	1
Staff	Ofc of Vice Pres, Clerc Center	Bosso	Edward	H	edward.bosso@gallaudet.edu	1
Staff	Ofc of Vice Pres, Clerc Center	Failing	Melinda	L	melinda.failing@gallaudet.edu	1
Staff	Ofc of Vice Pres, Clerc Center	Goedecke	Matthew	J	matthew.goedecke@gallaudet.edu	1
Staff	Ofc of Vice Pres, Clerc Center	Hoshina	Ben	Mineo	ben.hoshina@gallaudet.edu	1
Staff	Ofc of Vice Pres, Clerc Center	Jacoby	Susan	\N	susan.jacoby@gallaudet.edu	1
Staff	Ofc of Vice Pres, Clerc Center	Sutliffe	Nicole	A.	nicole.sutliffe@gallaudet.edu	1
Staff	Office of Academic Quality	Hanumantha	Smitha	\N	smitha.hanumantha@gallaudet.edu	1
Staff	Office of Academic Quality	Hulsebosch	Patricia	L	patricia.hulsebosch@gallaudet.edu	1
Staff	Office of Academic Quality	Lancaster	Linda	J	linda.lancaster@gallaudet.edu	1
Staff	Office of Academic Quality	Schlette	Emiko	L	emiko.schlette@gallaudet.edu	1
Staff	Office of Assessment	Dorminy	Jerri	Lyn	jerri.dorminy@gallaudet.edu	1
Staff	Office of Assessment	Moran	Norma	Y.	norma.moran@gallaudet.edu	1
Staff	Office of Diversity	Gilbert	Gizelle	Leonie	gizelle.gilbert@gallaudet.edu	1
Staff	Office of Diversity	Johnson	Janice	M	janice.johnson@gallaudet.edu	1
Staff	Office of Diversity	McCaskill	Angela	Patrice	angela.mccaskill@gallaudet.edu	1
Staff	Office of Diversity	Myers	Candace	Shavonne	candace.myers@gallaudet.edu	1
Staff	Office of Diversity	Zhou	Jiayi	\N	jiayi.zhou@gallaudet.edu	1
Staff	Office of Inst Research	Bangura	Rosanne	Elizabeth	rosanne.bangura@gallaudet.edu	1
Staff	Office of Inst Research	Frelich	Daryl	\N	daryl.frelich@gallaudet.edu	1
Staff	Office of Sponsored Programs	Foster	Audrey	Lane Wineglass	audrey.foster@gallaudet.edu	1
Staff	Office of Sponsored Programs	Houston	Ashuantay	Careese	ashuantay.houston@gallaudet.edu	1
Staff	Office of Sponsored Programs	Katsapis	Christine	C	christine.katsapis@gallaudet.edu	1
Staff	Office of the Ombuds	Singleton	Suzanne	Rosen	suzanne.singleton@gallaudet.edu	1
Staff	Office Students w/Disabilities	Akridge	James	C	james.akridge@gallaudet.edu	1
Staff	Office Students w/Disabilities	Byrne	Janet	L	janet.byrne@gallaudet.edu	1
Staff	Office Students w/Disabilities	Lydon	Charmaine	Marie	charmaine.lydon@gallaudet.edu	1
Staff	Office Students w/Disabilities	Pennington	Heba	Toulan	heba.pennington@gallaudet.edu	1
Staff	Office Students w/Disabilities	Razavi	Marva	\N	marva.razavi@gallaudet.edu	1
Staff	Office Students w/Disabilities	Tesar	Patricia	M	patricia.tesar@gallaudet.edu	1
Faculty	Philosophy and Religion	Burke	Teresa	Blankmeyer	teresa.burke@gallaudet.edu	1
Faculty	Philosophy and Religion	Stock	Barbara	\N	barbara.stock@gallaudet.edu	1
Faculty	Philosophy and Religion	VanGilder	Kirk	\N	kirk.vangilder@gallaudet.edu	1
Staff	Physical Educ. &amp; Recreation	Baylor	Benjamin	\N	benjamin.baylor@gallaudet.edu	1
Faculty	Physical Educ. &amp; Recreation	Brinks	Andrew	T	andrew.brinks@gallaudet.edu	1
Faculty	Physical Educ. &amp; Recreation	Carmichael	Robbie	J	robbie.carmichael@gallaudet.edu	1
Faculty	Physical Educ. &amp; Recreation	Doleac	Sarah	\N	sarah.doleac@gallaudet.edu	1
Faculty	Physical Educ. &amp; Recreation	Francavillo	Gwendolyn	Suzanne Roberts	gwendolyn.francavillo@gallaudet.edu	1
Staff	Physical Educ. &amp; Recreation	Gill-Doleac	Susan	\N	susan.gill-doleac@gallaudet.edu	1
Staff	Physical Educ. &amp; Recreation	Keeler	Virginia	L.	virginia.keeler@gallaudet.edu	1
Faculty	Physical Educ. &amp; Recreation	Marchitelli	Anita	M	anita.marchitelli@gallaudet.edu	1
Faculty	Physical Educ. &amp; Recreation	McLennon	Ruth	S	ruth.mclennon@gallaudet.edu	1
Faculty	Physical Educ. &amp; Recreation	Pomeroy	Barbara	N	barbara.pomeroy@gallaudet.edu	1
Faculty	Physical Educ. &amp; Recreation	Riddick	Carol	C	carol.riddick@gallaudet.edu	1
Faculty	Physical Educ. &amp; Recreation	Smith	Christen	G	christen.smith@gallaudet.edu	1
Staff	Planning, Dev &amp; Dissemination	Jefferson	Michelle	E	michelle.jefferson@gallaudet.edu	1
Staff	Planning, Dev &amp; Dissemination	Kinsella-Meier	Mary Ann	\N	mary.ann.kinsella-meier@gallaudet.edu	1
Staff	Planning, Dev &amp; Dissemination	Meynardie	Elizabeth	\N	elizabeth.meynardie@gallaudet.edu	1
Staff	Planning, Dev &amp; Dissemination	Page	Leslie	A	leslie.page@gallaudet.edu	1
Staff	Planning, Dev &amp; Dissemination	Szymanski	Christen	Ann	christen.szymanski@gallaudet.edu	1
Staff	Postal Services	Dwyer	Amy	J.	amy.dwyer@gallaudet.edu	1
Staff	Postal Services	Sexton	Kyle	Jeffrey	kyle.sexton@gallaudet.edu	1
Faculty	Professional Studies &amp;Training	Kraft	Roger	Carl	roger.kraft@gallaudet.edu	1
Staff	Program Development	Bauman	Hansel	\N	hansel.bauman@gallaudet.edu	1
Staff	Program Development	Fields	Michael	A	michael.fields@gallaudet.edu	1
Staff	Program Development	Morton	Kati	\N	kati.morton@gallaudet.edu	1
Staff	Program Development	Nehrir	Barbara	Ann	barbara.nehrir@gallaudet.edu	1
Staff	Program Development	Weiner	Fred	S	fred.weiner@gallaudet.edu	1
Staff	Program Monitoring&amp;Evaluation	Atuonah	Patrick	O	patrick.atuonah@gallaudet.edu	1
Staff	Program Monitoring&amp;Evaluation	Jackson	LaWanda	\N	lawanda.jackson@gallaudet.edu	1
Staff	Program Monitoring&amp;Evaluation	Lutz	Lori	\N	lori.lutz@gallaudet.edu	1
Staff	Provost	Barnes	Tiffanee	Basse	tiffanee.barnes@gallaudet.edu	1
Staff	Provost	Bergey	Jean	L	jean.bergey@gallaudet.edu	1
Staff	Provost	Clemons	William	\N	william.clemons@gallaudet.edu	1
Staff	Provost	Kelley	Barbara	A	barbara.kelley@gallaudet.edu	1
Faculty	Provost	Moore	Michael	\N	michael.moore@gallaudet.edu	1
Staff	Provost	VanBrakle	Linda	D	linda.vanbrakle@gallaudet.edu	1
Staff	Provost	Ward	Alice	Elizabeth	alice.ward@gallaudet.edu	1
Staff	Provost	Weiner	Stephen	F	stephen.weiner@gallaudet.edu	1
Staff	Provost	Weinstock	Robert	B	robert.weinstock@gallaudet.edu	1
Faculty	Psychology	Blennerhassett	Lynne	\N	lynne.blennerhassett@gallaudet.edu	1
Faculty	Psychology	Brice	Patrick	J	patrick.brice@gallaudet.edu	1
Faculty	Psychology	Corbett	Carolyn	A	carolyn.corbett@gallaudet.edu	1
Faculty	Psychology	Day	Lori	Ann	lori.day@gallaudet.edu	1
Faculty	Psychology	Galvan	Dennis	\N	dennis.galvan@gallaudet.edu	1
Faculty	Psychology	Gibbons	Elizabeth	M	elizabeth.gibbons@gallaudet.edu	1
Staff	Psychology	Johnson	Patricia	R	patricia.johnson@gallaudet.edu	1
Faculty	Psychology	Koo	Daniel	S	daniel.koo@gallaudet.edu	1
Faculty	Psychology	McCaw	Deborah	M.	deborah.mccaw@gallaudet.edu	1
Faculty	Psychology	Miller	Bryan	D	bryan.miller@gallaudet.edu	1
Faculty	Psychology	Morere	Donna	A	donna.morere@gallaudet.edu	1
Faculty	Psychology	Paludneviciene	Raylene	Marie	raylene.paludneviciene@gallaudet.edu	1
Faculty	Psychology	Pezzarossi	Caroline	Kobek	caroline.pezzarossi@gallaudet.edu	1
Faculty	Psychology	Pick	Lawrence	\N	lawrence.pick@gallaudet.edu	1
Faculty	Psychology	Schooler	Deborah	\N	deborah.schooler@gallaudet.edu	1
Faculty	Psychology	Thomas-Presswood	Tania	\N	tania.thomas-presswood@gallaudet.edu	1
Staff	Public Relations	Byrd	Todd	T	todd.byrd@gallaudet.edu	1
Staff	Public Relations	Chinoy	Bilal	Inayat	bilal.chinoy@gallaudet.edu	1
Staff	Public Relations	Ezzell	Teresa	M	teresa.ezzell@gallaudet.edu	1
Staff	Public Relations	Fakunle	Oluyinka	\N	oluyinka.fakunle@gallaudet.edu	1
Staff	Public Relations	Jeong	Hoon	\N	hoon.jeong@gallaudet.edu	1
Staff	Public Relations	Lawson	Debra	S	debra.lawson@gallaudet.edu	1
Staff	Public Relations	Luna	Kaitlin	McCarthy	kaitlin.luna@gallaudet.edu	1
Staff	Public Relations	Murphy	Catherine	M	catherine.murphy@gallaudet.edu	1
Staff	Public Relations	Prickett	Darlene	A	darlene.prickett@gallaudet.edu	1
Staff	Public Relations	Smith	Storm	Sunnie	storm.smith@gallaudet.edu	1
Staff	Public Relations	Wolff	Debra	Sue	debra.wolff@gallaudet.edu	1
Staff	Public Relations	Zhou	Fang	\N	fang.zhou@gallaudet.edu	1
Staff	Public Relations &amp; Marketing	Flanigan	Susan	M	susan.flanigan@gallaudet.edu	1
Staff	Public Relations &amp; Marketing	Gage	Roberta	\N	roberta.gage@gallaudet.edu	1
Staff	Public Relations &amp; Marketing	Lockhart	Glenn	\N	glenn.lockhart@gallaudet.edu	1
Staff	Regional NIO	Jacobs	Lisa	Carol	lisa.jacobs@gallaudet.edu	1
Staff	Regional NIO	Sheffer	Karen	Lydia	karen.sheffer@gallaudet.edu	1
Staff	Registrar	Buenaventura	Minnie	Cruz	minnie.buenaventura@gallaudet.edu	1
Staff	Registrar	Cooper	Erlinda	O	erlinda.cooper@gallaudet.edu	1
Staff	Registrar	Gagnon	Kara	Lynn	kara.gagnon@gallaudet.edu	1
Staff	Registrar	Miskovsky	Laurie	Rose	laurie.miskovsky@gallaudet.edu	1
Staff	Registrar	Patterson	Elice	N	elice.patterson@gallaudet.edu	1
Staff	Registrar	Pickering	Gerald	Michael	gerald.pickering@gallaudet.edu	1
Staff	Registrar	Pigott	Rachel	\N	rachel.pigott@gallaudet.edu	1
Staff	Registrar	Prezioso	Randy	A	randy.prezioso@gallaudet.edu	1
Staff	Registrar	Proctor	Barbara	F	barbara.proctor@gallaudet.edu	1
Staff	Registrar	Willey-Saunders	Laura	\N	laura.willey-saunders@gallaudet.edu	1
Staff	Reprographic Services	Thomas	Donna	L	donna.thomas@gallaudet.edu	1
Staff	Residence Life	Akinlotan	Tajudeen	O.	tajudeen.akinlotan@gallaudet.edu	1
Staff	Residence Life	Akinola	Taye	\N	taye.akinola@gallaudet.edu	1
Staff	Residence Life	Brown	Ericka	N	ericka.brown@gallaudet.edu	1
Staff	Residence Life	Campbell	Adrienne	R	adrienne.campbell@gallaudet.edu	1
Staff	Residence Life	Crockett	Autureo	L	autureo.crockett@gallaudet.edu	1
Staff	Residence Life	DeBerry	Usherla	Riyondia	usherla.deberry@gallaudet.edu	1
Staff	Residence Life	Ewan	Karl	A	karl.ewan@gallaudet.edu	1
Staff	Residence Life	Farrell	Kevin	Joseph	kevin.farrell@gallaudet.edu	1
Staff	Residence Life	Gagnon	Steven	D	steven.gagnon@gallaudet.edu	1
Staff	Residence Life	Hanrahan	Susan	R	susan.hanrahan@gallaudet.edu	1
Staff	Residence Life	Jones	Merry	\N	merry.jones@gallaudet.edu	1
Staff	Residence Life	Little	Jennifer	Tori	jennifer.little@gallaudet.edu	1
Staff	Residence Life	Lopez	Laura	\N	laura.lopez@gallaudet.edu	1
Staff	Residence Life	Mendonsa	Charmaine	\N	charmaine.mendonsa@gallaudet.edu	1
Staff	Residence Life	Morgan	Adrienne	\N	adrienne.morgan@gallaudet.edu	1
Staff	Residence Life	Nguyen	Thuan	\N	thuan.nguyen@gallaudet.edu	1
Staff	Residence Life	Pratt	Florence	Marie-Laure	florence.pratt@gallaudet.edu	1
Staff	Residence Life	Surber	Nikki	Elvene	nikki.surber@gallaudet.edu	1
Staff	Residence Life	White	Christene	Marie	christene.white@gallaudet.edu	1
Staff	Risk Management &amp; Insurance	Rypkema	Pamela	Jo	pamela.rypkema@gallaudet.edu	1
Faculty	Social Work	Barclay	David	Alexander	david.barclay@gallaudet.edu	1
Faculty	Social Work	Brown	Marquessa	V	marquessa.brown@gallaudet.edu	1
Faculty	Social Work	Cohen	Carol	B	carol.cohen@gallaudet.edu	1
Staff	Social Work	Farr	Tommy	Wade	tommy.farr@gallaudet.edu	1
Faculty	Social Work	Mason	Teresa	C	teresa.mason@gallaudet.edu	1
Faculty	Social Work	Moore	Elizabeth	Ann	elizabeth.moore@gallaudet.edu	1
Faculty	Social Work	Sheridan	Martha	A	martha.sheridan@gallaudet.edu	1
Faculty	Social Work	White	Barbara	J	barbara.white@gallaudet.edu	1
Faculty	Social Work Specialization	Betman	Beth	G	beth.betman@gallaudet.edu	1
Faculty	Sociology	Barnartt	Sharon	N	sharon.barnartt@gallaudet.edu	1
Faculty	Sociology	Easterling	Beth	Allen	beth.easterling@gallaudet.edu	1
Faculty	Sociology	Fennell	Julie	Lynn	julie.fennell@gallaudet.edu	1
Staff	Sociology	Ganz	Susan	A	susan.ganz@gallaudet.edu	1
Faculty	Sociology	Horejes	Thomas	\N	thomas.horejes@gallaudet.edu	1
Staff	Sociology	Williams	Jody	Dawn	jody.williams@gallaudet.edu	1
Staff	Student Affairs	Benedict	Albert	D	albert.benedict@gallaudet.edu	1
Staff	Student Affairs	Evans	Karen	E	karen.evans@gallaudet.edu	1
Staff	Student Affairs	Imel	Travis	L	travis.imel@gallaudet.edu	1
Staff	Student Center	Pramuk	Carl	A	carl.pramuk@gallaudet.edu	1
Staff	Student Center	Williams	Yolanda	L.	yolanda.williams@gallaudet.edu	1
Staff	Student Health Services	Balan	Marybel	C.	marybel.balan@gallaudet.edu	1
Staff	Student Health Services	Brooks-Nelson	Carletta	\N	carletta.brooks-nelson@gallaudet.edu	1
Staff	Student Health Services	Caesar	Brandon	T	brandon.caesar@gallaudet.edu	1
Staff	Student Health Services	Chittasingh	Chantrawan	\N	chantrawan.chittasingh@gallaudet.edu	1
Staff	Student Health Services	Lee-Wilkins	Kim	\N	kim.lee-wilkins@gallaudet.edu	1
Staff	Student Health Services	Pegues	Vernetta	D.	vernetta.pegues@gallaudet.edu	1
Staff	Student Life	Bortoletto	Douglas	Fredrick	douglas.bortoletto@gallaudet.edu	1
Staff	Student Life	Bullard	Holly	Paige	holly.bullard@gallaudet.edu	1
Staff	Student Life	Carter	Billy	Alex	billy.carter@gallaudet.edu	1
Staff	Student Life	Castrese	John	David	john.castrese@gallaudet.edu	1
Staff	Student Life	Church	Clinton	R	clinton.church@gallaudet.edu	1
Staff	Student Life	Cooper	Jenny	Lynn	jenny.cooper@gallaudet.edu	1
Staff	Student Life	Davis	Antines	M	antines.davis@gallaudet.edu	1
Staff	Student Life	Day	David	P	david.day@gallaudet.edu	1
Staff	Student Life	Day	Joan	Marie	joan.day@gallaudet.edu	1
Staff	Student Life	Douglas	Garreth	L	garreth.douglas@gallaudet.edu	1
Staff	Student Life	Dvir	Noa	\N	noa.dvir@gallaudet.edu	1
Staff	Student Life	Ford	Dale	K	dale.ford@gallaudet.edu	1
Staff	Student Life	Gill	Sean	P.	sean.gill@gallaudet.edu	1
Staff	Student Life	Ginn	Erin	Danielle	erin.ginn@gallaudet.edu	1
Staff	Student Life	Hampton	Shirley	Ann	shirley.hampton@gallaudet.edu	1
Staff	Student Life	Harrison	Ena	Verona	ena.harrison@gallaudet.edu	1
Staff	Student Life	Helms-Salit	Lauralynn	\N	lauralynn.helms-salit@gallaudet.edu	1
Staff	Student Life	Hower	Lori	Patricia	lori.hower@gallaudet.edu	1
Staff	Student Life	Huseby	Lauren	Marie	lauren.huseby@gallaudet.edu	1
Staff	Student Life	Hynes	James	H	james.hynes@gallaudet.edu	1
Staff	Student Life	Johnson	Baranda	Ann	baranda.johnson@gallaudet.edu	1
Staff	Student Life	Killam	Tonya	L.	tonya.killam@gallaudet.edu	1
Staff	Student Life	Land	Erin	Michelle	erin.land@gallaudet.edu	1
Staff	Student Life	Lawson	Clayton	\N	clayton.lawson@gallaudet.edu	1
Staff	Student Life	Li	Sheng	\N	sheng.li@gallaudet.edu	1
Staff	Student Life	Macfadden	Travis	Cazel	travis.macfadden@gallaudet.edu	1
Staff	Student Life	McKenzie	Patricia	Ann	patricia.mckenzie@gallaudet.edu	1
Staff	Student Life	McLeod	Kevin	K	kevin.mcleod@gallaudet.edu	1
Staff	Student Life	Montes	Maria	Masi Luisa	maria.montes@gallaudet.edu	1
Staff	Student Life	Ogork	Ayuk	Ebot	ayuk.ogork@gallaudet.edu	1
Staff	Student Life	Orsi	Kelly	Ann	kelly.orsi@gallaudet.edu	1
Staff	Student Life	Parker	Rachel	\N	rachel.parker@gallaudet.edu	1
Staff	Student Life	Patterson	Ameena	\N	ameena.patterson@gallaudet.edu	1
Staff	Student Life	Peralta	Jose	Luis	jose.peralta@gallaudet.edu	1
Staff	Student Life	Pfaff	Darrell	Newland	darrell.pfaff@gallaudet.edu	1
Staff	Student Life	Rozynek	Yolanta	W	yolanta.rozynek@gallaudet.edu	1
Staff	Student Life	Rubisch	Charles	Joseph	charles.rubisch@gallaudet.edu	1
Staff	Student Life	Sewell	Christopher	Irwin	christopher.sewell@gallaudet.edu	1
Staff	Student Life	Shepard	Dale	Chancellor	dale.shepard@gallaudet.edu	1
Staff	Student Life	Sivak	Jennie	V	jennie.sivak@gallaudet.edu	1
Staff	Student Life	Solomon	Haley	Carol	haley.solomon@gallaudet.edu	1
Staff	Student Life	Spinosi	John	Craig	john.spinosi@gallaudet.edu	1
Staff	Student Life	St. Romain	Bambi	Ann	bambi.st.romain@gallaudet.edu	1
Staff	Student Life	Stoeckel	Kirk	Allan	kirk.stoeckel@gallaudet.edu	1
Staff	Student Life	Stout	Judy	C	judy.stout@gallaudet.edu	1
Staff	Student Life	Torres	Luis	Uriel	luis.torres@gallaudet.edu	1
Staff	Student Services	Chisholm	Genie	L	genie.chisholm@gallaudet.edu	1
Staff	Student Services	Connelly	Amanda	Dove	amanda.connelly@gallaudet.edu	1
Staff	Student Services	Cronin	Gretchen	Aileen	gretchen.cronin@gallaudet.edu	1
Staff	Student Services	Doyle	Jane	S	jane.doyle@gallaudet.edu	1
Staff	Student Services	Dzime-Assison	Venita	Jean	venita.dzime-assison@gallaudet.edu	1
Staff	Student Services	Golocovsky	Silvia	N	silvia.golocovsky@gallaudet.edu	1
Staff	Student Services	Howell	Rebecca	Ann	rebecca.howell@gallaudet.edu	1
Staff	Student Services	Marshall	Stephanie	S	stephanie.marshall@gallaudet.edu	1
Staff	Student Services	Rolnick	Lori	A	lori.rolnick@gallaudet.edu	1
Staff	Student Services	Schaefer	Christine	Michelle	christine.schaefer@gallaudet.edu	1
Staff	Student Services	Schroer	Katy	Lynn	katy.schroer@gallaudet.edu	1
Faculty	Student Success	Aristy	Katrina	Sasha	katrina.aristy@gallaudet.edu	1
Staff	Student Success	Burwell	Darian	Jeneen	darian.burwell@gallaudet.edu	1
Staff	Student Success	Chandani	Alim	S.	alim.chandani@gallaudet.edu	1
Staff	Student Success	Tao	Andy	Kenji	andy.tao@gallaudet.edu	1
Staff	Summer Programs	Blazek	Jeffrey	\N	jeffrey.blazek@gallaudet.edu	1
Faculty	Summer Programs	Fitzpatrick	Paul	Francis	paul.fitzpatrick@gallaudet.edu	1
Faculty	Summer Programs	Masi	Carla	E	carla.masi@gallaudet.edu	1
Staff	Summer Recreation Program	Burrhus	Robin	L	robin.burrhus@gallaudet.edu	1
Staff	Summer Recreation Program	Carr	Maya	Nicole	maya.carr@gallaudet.edu	1
Staff	Summer Recreation Program	Fellows	Nefitiri	Torrance	nefitiri.fellows@gallaudet.edu	1
Staff	Summer Recreation Program	Hall	Kevin	\N	kevin.hall@gallaudet.edu	1
Staff	Summer Recreation Program	Hayes	Nukeitra	Shaniece	nukeitra.hayes@gallaudet.edu	1
Staff	Summer Recreation Program	McMurry	Christina	Lea	christina.mcmurry@gallaudet.edu	1
Staff	Summer Recreation Program	Spiegel	Molly	Octavia	molly.spiegel@gallaudet.edu	1
Staff	Summer Recreation Program	Whitaker	Jamel	Quaran	jamel.whitaker@gallaudet.edu	1
Staff	Systems and Operations	Cain	Kenneth	D	kenneth.cain@gallaudet.edu	1
Staff	Systems and Operations	Gateau	George	M	george.gateau@gallaudet.edu	1
Staff	Systems and Operations	Palacios	Nathaniel	\N	nathaniel.palacios@gallaudet.edu	1
Staff	Systems and Operations	Romero	Harold	J	harold.romero@gallaudet.edu	1
Staff	Telecom Access RERC	Kozma-Spytek	Linda	K	linda.kozma-spytek@gallaudet.edu	1
Staff	Telecom Access RERC	Williams	Norman	Scott	norman.williams@gallaudet.edu	1
Staff	The Gallaudet SoTL Initiative	Klein	Bridget	Ann	bridget.klein@gallaudet.edu	1
Staff	The President's Office	Baldwin	Carolyn	\N	carolyn.baldwin@gallaudet.edu	1
Staff	The President's Office	Banks	Daun	L.	daun.banks@gallaudet.edu	1
Staff	The President's Office	Beil	Donald	\N	donald.beil@gallaudet.edu	1
Staff	The President's Office	Blakely	Paul	R	paul.blakely@gallaudet.edu	1
Staff	The President's Office	Garvin	Harold	F	harold.garvin@gallaudet.edu	1
Staff	The President's Office	Hurwitz	T Alan	\N	alan.hurwitz@gallaudet.edu	1
Staff	The President's Office	Lipkey	Debra	S.	debra.lipkey@gallaudet.edu	1
Staff	Theatre Arts	Cebe	Juanita	\N	juanita.cebe@gallaudet.edu	1
Faculty	Theatre Arts	Conley	Willy	\N	willy.conley@gallaudet.edu	1
Staff	Theatre Arts	Davis	Kristopher	Charles	kristopher.davis@gallaudet.edu	1
Staff	Theatre Arts	Fisher	Jacob	Ernesto	jacob.fisher@gallaudet.edu	1
Faculty	Theatre Arts	Lundquist	Cheryl	Lee	cheryl.lundquist@gallaudet.edu	1
Faculty	Theatre Arts	Sinnott	Ethan	M	ethan.sinnott@gallaudet.edu	1
Staff	Training Professional Dev	Dabney	Patricia	L	patricia.dabney@gallaudet.edu	1
Staff	Training Professional Dev	Gilbert	Ella	A	ella.gilbert@gallaudet.edu	1
Staff	Training Professional Dev	Gilbert	Matthew	\N	matthew.gilbert@gallaudet.edu	1
Staff	Training Professional Dev	Hanyzewski	Carla	E	carla.hanyzewski@gallaudet.edu	1
Staff	Training Professional Dev	Jeffries	Richard	Lee	richard.jeffries@gallaudet.edu	1
Staff	Training Professional Dev	Kabusk	Christopher	Andrew	christopher.kabusk@gallaudet.edu	1
Staff	Training Professional Dev	Lightfoot	Mary	H	mary.lightfoot@gallaudet.edu	1
Staff	Training Professional Dev	Valcourt-Pearce	Catherine	\N	catherine.valcourt-pearce@gallaudet.edu	1
Staff	Transportation	Allumaga	Hilary	\N	hilary.allumaga@gallaudet.edu	1
Staff	Transportation	Armstrong	Wendy	L	wendy.armstrong@gallaudet.edu	1
Staff	Transportation	Bowman	Jimmie	L	jimmie.bowman@gallaudet.edu	1
Staff	Transportation	Brown	Ronnell	\N	ronnell.brown@gallaudet.edu	1
Staff	Transportation	Bryant	Charles	\N	charles.bryant@gallaudet.edu	1
Staff	Transportation	Campbell	Jacqueline	J	jacqueline.campbell@gallaudet.edu	1
Staff	Transportation	Canady	Stevie	\N	stevie.canady@gallaudet.edu	1
Staff	Transportation	Crosby	Charles	A.	charles.crosby@gallaudet.edu	1
Staff	Transportation	Curtis	Lawrence	\N	lawrence.curtis@gallaudet.edu	1
Staff	Transportation	Cutlip	Richard	C	richard.cutlip@gallaudet.edu	1
Staff	Transportation	Dawes	William	L	william.dawes@gallaudet.edu	1
Staff	Transportation	Duren	Thelma	\N	thelma.duren@gallaudet.edu	1
Staff	Transportation	Emerson	DeRon	Jason	deron.emerson@gallaudet.edu	1
Staff	Transportation	Fleming	Diane	M.	diane.fleming@gallaudet.edu	1
Staff	Transportation	Fleming	Phyllis	M	phyllis.fleming@gallaudet.edu	1
Staff	Transportation	Gamble	Tonya	S	tonya.gamble@gallaudet.edu	1
Staff	Transportation	Hardy	Georgette	\N	georgette.hardy@gallaudet.edu	1
Staff	Transportation	Hayes	Paul	\N	paul.hayes@gallaudet.edu	1
Staff	Transportation	Ji	Haibin	\N	haibin.ji@gallaudet.edu	1
Staff	Transportation	Jones	Dorothea	\N	dorothea.jones@gallaudet.edu	1
Staff	Transportation	Kelly	Michael	D	michael.kelly@gallaudet.edu	1
Staff	Transportation	Kinsler	Karen	\N	karen.kinsler@gallaudet.edu	1
Staff	Transportation	Laster	Saharita	\N	saharita.laster@gallaudet.edu	1
Staff	Transportation	Mack	Linda	\N	linda.mack@gallaudet.edu	1
Staff	Transportation	Price	Kenyona	Lashawn	kenyona.price@gallaudet.edu	1
Staff	Transportation	Raye	Linda	A	linda.raye@gallaudet.edu	1
Staff	Transportation	Riley	Jimmy	C	jimmy.riley@gallaudet.edu	1
Staff	Transportation	Sampson	Tyra	Denise	tyra.sampson@gallaudet.edu	1
Staff	Transportation	Smith	Olivia	L	olivia.smith@gallaudet.edu	1
Staff	Transportation	Thompson	Danielle	Nicole	danielle.n.thompson@gallaudet.edu	1
Staff	Transportation	Williams	Harvey	T	harvey.williams@gallaudet.edu	1
Staff	Transportation	Williams	Richard	K	richard.williams@gallaudet.edu	1
Staff	Transportation	Wright	Terry	\N	terry.wright@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Allara	Le Toudjida	\N	le.toudjida.allara@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Bryce	Alex	\N	alex.bryce@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Hansen	Scott	\N	scott.hansen@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	LaRue	Sanremi	Irene	sanremi.larue@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Madden	Rhonda	W	rhonda.madden@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Myrick	Ana Paula	R	ana.paula.myrick@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Plummer	LaToya	LaTika	latoya.plummer@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Powell	Amee	Charlotte	amee.powell@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Quintero	Sharon	Ann	sharon.quintero@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Strickler	Robert	L.	robert.strickler@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Washington-Shepard	Niesha	La'Chea	niesha.washington-shepard@gallaudet.edu	1
Staff	Tutorial and Instructional Pgm	Williams	Linda	A	linda.williams@gallaudet.edu	1
Staff	Utilities Services	Ham	David	D.	david.ham@gallaudet.edu	1
Staff	Utilities Services	Lewis	Lawrence	\N	lawrence.lewis@gallaudet.edu	1
Staff	Utilities Services	Morgan	Robert	\N	robert.morgan@gallaudet.edu	1
Staff	Utilities Services	Morgan	Sanford	\N	sanford.morgan@gallaudet.edu	1
Staff	Utilities Services	Ploussiou	Antonios	\N	antonios.ploussiou@gallaudet.edu	1
Staff	Utilities Services	Sathya	Samuel	P	samuel.sathya@gallaudet.edu	1
Staff	Utilities Services	Wells	Michael	A.	michael.wells@gallaudet.edu	1
Staff	Utilities Services	Williams	Wayne	A.	wayne.williams@gallaudet.edu	1
Staff	Vice Pres. Admin. &amp; Finance	Allen	Connie	S	connie.allen@gallaudet.edu	1
Staff	Vice Pres. Admin. &amp; Finance	Hughes	William	\N	william.hughes@gallaudet.edu	1
Staff	Vice Pres. Admin. &amp; Finance	Kelly	Paul	\N	paul.kelly@gallaudet.edu	1
Staff	Vice Pres. Admin. &amp; Finance	McCaskill	Sharrell	V	sharrell.mccaskill@gallaudet.edu	1
Staff	Visitors Center	Norris	Rian	Andrea	rian.norris@gallaudet.edu	1
Staff	VisualLang&amp;VisualLrngRschCtr	Barrett	Ryan	Matthew	ryan.barrett@gallaudet.edu	1
Staff	VisualLang&amp;VisualLrngRschCtr	Choi	Song Hoa	\N	song.hoa.choi@gallaudet.edu	1
Staff	VisualLang&amp;VisualLrngRschCtr	Gauna	Kristine	M.	kristine.gauna@gallaudet.edu	1
Staff	VisualLang&amp;VisualLrngRschCtr	Herzig	Melissa	Pia	melissa.herzig@gallaudet.edu	1
Staff	VisualLang&amp;VisualLrngRschCtr	Malzkuhn	Melissa	Anne	melissa.malzkuhn@gallaudet.edu	1
Staff	VisualLang&amp;VisualLrngRschCtr	Price	Shanon	M	shanon.price@gallaudet.edu	1
Staff	Wellness Programs	Gannon	Christine	L	christine.gannon@gallaudet.edu	1
\.
