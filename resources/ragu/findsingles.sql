-- Written by Kevin Cole <kjcole@gallaudet.edu> 2012.08.28
--
-- Find projects with only one investigator Results used in
-- makeleader.sql
--

select ragu_investigator.project_id
from ragu_investigator
inner join (select ragu_investigator.project_id, count(ragu_investigator.project_id) as lones
            from ragu_investigator
            group by ragu_investigator.project_id
            having (count(ragu_investigator.project_id) = 1))
as singles
on ragu_investigator.project_id = singles.project_id
order by ragu_investigator.project_id;
