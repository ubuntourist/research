# Last modified by Kevin Cole <kjcole@gri.gallaudet.edu> 2012.03.30
#
# 2012.03.30 KJC - Switching to generic views for static pages
# 2013.07.16 KJC - Switching to class-based generic views from function-based
#

from django.conf.urls.defaults import *
from resources.ragu.views      import *
from django.views.generic.base import TemplateView

urlpatterns = patterns("",
    (r"^$",              TemplateView.as_view(template_name="ragu/index.html")),
    (r"^copyright/$",    TemplateView.as_view(template_name="ragu/copyright.html")),
    (r"^preface/$",      TemplateView.as_view(template_name="ragu/preface.html")),
    (r"^colophon/$",     TemplateView.as_view(template_name="ragu/colophon.html")),
    (r"^autofill/$",     TemplateView.as_view(template_name="ragu/autofill.html")),
    (r"^review/$",       TemplateView.as_view(template_name="ragu/review.html")),
    (r"^sesame/$",       TemplateView.as_view(template_name="ragu/maintenance.html")),
    (r"^prioritize/$",                             prioritize),
    (r"^search/$",                                 search),
    (r"^abstract/(\d{1,5})/$",                     abstract),
    (r"^report/$",                                 report),
    (r"^priorities/$",                             priorities),
    (r"^screen_investigator/$",                    screen_investigator),
    (r"^investigator/(\d{1,5})/$",                 by_investigator),
    (r"^screen_dates/$",                           screen_dates),
    (r"^chronology/$",                             chronology),
    (r"^screen_funder/$",                          screen_funder),
    (r"^funder/(\d{1,5})/$",                       by_funder),
    (r"^screen_priorities/$",                      screen_priorities),
    (r"^priority/(\d{1,2})/$",                     by_priority),
    (r"^screen_unit/$",                            screen_unit),
    (r"^unit/(\d{1,5})/$",                         by_unit),
    (r"^funding/$",                                funding),
    (r"^citations/(\d{4})/$",                      citations),
    (r"^students/(\d{4})/$",                       students),
    (r"^small_grants/(\d{4})/$",                   small_grants),
    (r"^book/(\d{4})/$",                           book),
    (r"^add_project/$",                            add_project),
    (r"^update_project/(\d{1,5})/$",               update_project),
    (r"^add_product/$",                            add_product),
    (r"^update_product/(\d{1,5})/$",               update_product),
    (r"^queue/pro(je|du)ct/(\d{1,5})/(\d{1,2})/$", queue),
    (r"^pending/pro(je|du)cts/(\d{1,2})/$",        pending),
    (r"^logout/$",                                 logout),
    (r"^api/autoname/",                            autoname),
    (r"^leadership/$",                             leadership),
    (r"^units/intros/$",                           units_intros),
    (r"^splitsville/$",                            splitsville),
    (r"^playbill/$",                               playbill),
    (r"^rerank/$",                                 rerank),
#   (r"^sesame/$",                                 sesame),
    (r"^internal_use/$",                           sesame),
    (r"^reminders/$",                              reminders),
    (r"^firstperson/$",                            firstperson),
    (r"^unresolved/$",                             unresolved),
)
