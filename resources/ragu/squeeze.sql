------------------------------------------------------------------------------
-- Remove non-printable characters
UPDATE ragu_project
  SET description = regexp_replace(description, E'[\\r]+', ' ', 'g'),
      title = regexp_replace(title, E'[\\r]+', ' ', 'g');
UPDATE ragu_project
  SET description = regexp_replace(description, E'[\\n]+', ' ', 'g'),
      title = regexp_replace(title, E'[\\n]+', ' ', 'g');
UPDATE ragu_project
  SET description = regexp_replace(description, E'[\\t]+', ' ', 'g'),
      title = regexp_replace(title, E'[\\t]+', ' ', 'g');

-- Change multiple spaces to a single space
UPDATE ragu_project
  SET description = replace(description, '&nbsp;',' '),
      title = replace(title, '&nbsp;',' ');
UPDATE ragu_project
  SET description = regexp_replace(description, E' +', ' ', 'g'),
      title = regexp_replace(title, E' +', ' ', 'g');

-- Correct common mis-grouping of <strong> and <em>
UPDATE ragu_project
  SET description = replace(description, '<b>','<strong>');
UPDATE ragu_project
  SET description = replace(description, '</b>','</strong>');
UPDATE ragu_project
  SET description = replace(description, '<i>','<em>');
UPDATE ragu_project
  SET description = replace(description, '</i>','</em>');
UPDATE ragu_project
  SET description = replace(description, '<strong> ',' <strong>');
UPDATE ragu_project
  SET description = replace(description, ' </strong>','</strong> ');
UPDATE ragu_project
  SET description = replace(description, '<em> ',' <em>');
UPDATE ragu_project
  SET description = replace(description, ' </em>','</em> ');

-- Trim extraneous spaces from beginning and ending of string
UPDATE ragu_project
  SET description = trim(both ' ' from description),
      title = trim(both ' ' from title);

-- Add <p> to the starts of descriptions that are missing it
UPDATE ragu_project
  SET description = '<p>' || description
  WHERE description  !~* E'^[\<]p[\>]';

-- Add </p> to the ends of descriptions that are missing it
UPDATE ragu_project
  SET description = description || '</p>'
  WHERE description  !~* E'[\<]/p[\>]$';

-- Remove that </p> if the description ends with a list
UPDATE ragu_project
  SET description = regexp_replace(description, E'[\<]/ol[\>][\<]/p[\>]$',E'</ol>','g');
UPDATE ragu_project
  SET description = regexp_replace(description, E'[\<]/ul[\>][\<]/p[\>]$',E'</ul>','g');

-- Remove space after opening a paragraph and before closing one
UPDATE ragu_project
  SET description = replace(description, '<p> ', '<p>');
UPDATE ragu_project
  SET description = replace(description, ' </p>', '</p>');
UPDATE ragu_project
  SET description = replace(description, '<li> ', '<li>');
UPDATE ragu_project
  SET description = replace(description, ' </li>', '</li>');
UPDATE ragu_project
  SET description = replace(description, ' <br />', '<br />');
UPDATE ragu_project
  SET description = replace(description, '<br /> ', '<br />');
UPDATE ragu_project
  SET description = replace(description, '<br /><br />', '</p><p>');

-- Handle quote abuse
UPDATE ragu_project
  SET description = replace(description, '&quot;','"');
UPDATE ragu_project
  SET description = replace(description, '&#39;','''');
UPDATE ragu_project
  SET description = replace(description, '&ldquo;','"');
UPDATE ragu_project
  SET description = replace(description, '&rdquo;','"');
UPDATE ragu_project
  SET description = replace(description, '&lsquo;','''');
UPDATE ragu_project
  SET description = replace(description, '&rsquo;','''');

------------------------------------------------------------------------------
-- Remove non-printable characters
UPDATE ragu_department
  SET description = regexp_replace(description, E'[\\r]+', ' ', 'g');
UPDATE ragu_department
  SET description = regexp_replace(description, E'[\\n]+', ' ', 'g');
UPDATE ragu_department
  SET description = regexp_replace(description, E'[\\t]+', ' ', 'g');

-- Change multiple spaces to a single space
UPDATE ragu_department
  SET description = replace(description, '&nbsp;',' ');
UPDATE ragu_department
  SET description = regexp_replace(description, E' +', ' ', 'g');

-- Correct common mis-grouping of <strong> and <em>
UPDATE ragu_department
  SET description = replace(description, '<b>','<strong>');
UPDATE ragu_department
  SET description = replace(description, '</b>','</strong>');
UPDATE ragu_department
  SET description = replace(description, '<i>','<em>');
UPDATE ragu_department
  SET description = replace(description, '</i>','</em>');
UPDATE ragu_department
  SET description = replace(description, '<strong> ',' <strong>');
UPDATE ragu_department
  SET description = replace(description, ' </strong>','</strong> ');
UPDATE ragu_department
  SET description = replace(description, '<em> ',' <em>');
UPDATE ragu_department
  SET description = replace(description, ' </em>','</em> ');

-- Trim extraneous spaces from beginning and ending of string
UPDATE ragu_department
  SET description = trim(both ' ' from description);

-- Add <p> to the starts of descriptions that are missing it
UPDATE ragu_department
  SET description = '<p>' || description
  WHERE description  !~* E'^[\<]p[\>]';

-- Add </p> to the ends of descriptions that are missing it
UPDATE ragu_department
  SET description = description || '</p>'
  WHERE description  !~* E'[\<]/p[\>]$';

-- Remove that </p> if the description ends with a list
UPDATE ragu_department
  SET description = regexp_replace(description, E'[\<]/ol[\>][\<]/p[\>]$',E'</ol>','g');
UPDATE ragu_department
  SET description = regexp_replace(description, E'[\<]/ul[\>][\<]/p[\>]$',E'</ul>','g');

-- Remove space after opening a paragraph and before closing one
UPDATE ragu_department
  SET description = replace(description, '<p> ', '<p>');
UPDATE ragu_department
  SET description = replace(description, ' </p>', '</p>');
UPDATE ragu_department
  SET description = replace(description, '<li> ', '<li>');
UPDATE ragu_department
  SET description = replace(description, ' </li>', '</li>');
UPDATE ragu_department
  SET description = replace(description, ' <br />', '<br />');
UPDATE ragu_department
  SET description = replace(description, '<br /> ', '<br />');
UPDATE ragu_department
  SET description = replace(description, '<br /><br />', '</p><p>');

-- Handle quote abuse
UPDATE ragu_department
  SET description = replace(description, '&quot;','"');
UPDATE ragu_department
  SET description = replace(description, '&#39;','''');
UPDATE ragu_department
  SET description = replace(description, '&ldquo;','"');
UPDATE ragu_department
  SET description = replace(description, '&rdquo;','"');
UPDATE ragu_department
  SET description = replace(description, '&lsquo;','''');
UPDATE ragu_department
  SET description = replace(description, '&rsquo;','''');

------------------------------------------------------------------------------

-- Remove non-printable characters
UPDATE ragu_product
  SET description = regexp_replace(description, E'[\\r]+', ' ', 'g');
UPDATE ragu_product
  SET description = regexp_replace(description, E'[\\n]+', ' ', 'g');
UPDATE ragu_product
  SET description = regexp_replace(description, E'[\\t]+', ' ', 'g');

-- Change multiple spaces to a single space
UPDATE ragu_product
  SET description = replace(description, '&nbsp;',' ');
UPDATE ragu_product
  SET description = regexp_replace(description, E' +', ' ', 'g');

-- Correct common mis-grouping of <strong> and <em>
UPDATE ragu_product
  SET description = replace(description, '<b>','<strong>');
UPDATE ragu_product
  SET description = replace(description, '</b>','</strong>');
UPDATE ragu_product
  SET description = replace(description, '<i>','<em>');
UPDATE ragu_product
  SET description = replace(description, '</i>','</em>');
UPDATE ragu_product
  SET description = replace(description, '<strong> ',' <strong>');
UPDATE ragu_product
  SET description = replace(description, ' </strong>','</strong> ');
UPDATE ragu_product
  SET description = replace(description, '<em> ',' <em>');
UPDATE ragu_product
  SET description = replace(description, ' </em>','</em> ');

-- Trim extraneous spaces from beginning and ending of string
UPDATE ragu_product
  SET description = trim(both ' ' from description);

-- Add <p> to the starts of descriptions that are missing it
UPDATE ragu_product
  SET description = '<p>' || description
  WHERE description  !~* E'^[\<]p[\>]';

-- Add </p> to the ends of descriptions that are missing it
UPDATE ragu_product
  SET description = description || '</p>'
  WHERE description  !~* E'[\<]/p[\>]$';

-- Remove that </p> if the description ends with a list
UPDATE ragu_product
  SET description = regexp_replace(description, E'[\<]/ol[\>][\<]/p[\>]$',E'</ol>','g');

UPDATE ragu_product
  SET description = regexp_replace(description, E'[\<]/ul[\>][\<]/p[\>]$',E'</ul>','g');

-- Remove space after opening a paragraph and before closing one
UPDATE ragu_product
  SET description = replace(description, '<p> ', '<p>');
UPDATE ragu_product
  SET description = replace(description, ' </p>', '</p>');
UPDATE ragu_product
  SET description = replace(description, '<li> ', '<li>');
UPDATE ragu_product
  SET description = replace(description, ' </li>', '</li>');
UPDATE ragu_product
  SET description = replace(description, ' <br />', '<br />');
UPDATE ragu_product
  SET description = replace(description, '<br /> ', '<br />');
UPDATE ragu_product
  SET description = replace(description, '<br /><br />', '</p><p>');

-- Handle quote abuse
UPDATE ragu_product
  SET description = replace(description, '&quot;','"');
UPDATE ragu_product
  SET description = replace(description, '&#39;','''');
UPDATE ragu_product
  SET description = replace(description, '&ldquo;','"');
UPDATE ragu_product
  SET description = replace(description, '&rdquo;','"');
UPDATE ragu_product
  SET description = replace(description, '&lsquo;','''');
UPDATE ragu_product
  SET description = replace(description, '&rsquo;','''');
------------------------------------------------------------------------------

-- Click the product_only button when appropriate.

UPDATE ragu_project
  SET product_only = 't'
  WHERE description = '<p>(product only)</p>';

-- Put periods after initials

UPDATE ragu_employee
  SET middle_name = middle_name || '.'
  WHERE middle_name ~ '^.$';
------------------------------------------------------------------------------
-- Eliminate empty strings

UPDATE ragu_agency
  SET homepage = NULL
  WHERE LENGTH(homepage) = 0;

UPDATE ragu_agency
  SET homepage = NULL
  WHERE LENGTH(homepage) = 0;

UPDATE ragu_department
  SET homepage = NULL
  WHERE LENGTH(homepage) = 0;

UPDATE ragu_department
  SET description = NULL
  WHERE LENGTH(description) = 0;

UPDATE ragu_employee
  SET middle_name = NULL
  WHERE LENGTH(middle_name) = 0;

UPDATE ragu_funder
  SET acronym = NULL
  WHERE LENGTH(acronym) = 0;

UPDATE ragu_funder
  SET homepage = NULL
  WHERE LENGTH(homepage) = 0;

UPDATE ragu_person
  SET first_name = NULL
  WHERE LENGTH(first_name) = 0;

UPDATE ragu_person
  SET email = NULL
  WHERE LENGTH(email) = 0;

UPDATE ragu_program
  SET homepage = NULL
  WHERE LENGTH(homepage) = 0;

UPDATE ragu_staff
  SET homepage = NULL
  WHERE LENGTH(homepage) = 0;
------------------------------------------------------------------------------
-- Prime for resorting
UPDATE ragu_product
  SET sort_order = NULL;

UPDATE ragu_project
  SET sort_order = NULL;
