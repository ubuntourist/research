Last modified by Kevin Cole <kjcole@gallaudet.edu> 2012.03.26
______________________________________________________________________________

2012.03.21 KJC

  GPRA = Government Performance and Results Act, used for reporting to
  the U.S. Department of Education. The project table used to include
  a gpra_code field but it appears to be no longer used, and was null
  in 153 out of 155 cases.  The remaining two were 0.

  Project 760 "Perceived Difficulty with Face-to-Face Communication: A
  Comparison of Adult Hearing Aid Users and Adults with Normal
  Hearing" from Cynthia Compton and Erica Pane (Student) in HSLS was
  entered incorrectly and never made it into the final report. Given
  that it messes up my nice system, it's gone.  It ran from Summer 2009
  to January 2010. Other info needed to recreate the record:

    product_only       = f
    is_student_project = t
    is_clerc_project   = f
    status             = 2
    order_nbr          = 0
______________________________________________________________________________

2012.03.26 KJC

  backup.py is a working models.py.  models.py being modified to see if we
  can do better with ManyToMany fields...
______________________________________________________________________________

2012.04.05 KJC

  Not really understanding the right way to work with "ManyToMany ... through".
  But here are some examinations of the objects created, produced via the
  manager.py shell.

    >>> dir(Project)

    ['DoesNotExist', 'MultipleObjectsReturned', '__class__', '__delattr__',
    '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__',
    '__hash__', '__init__', '__metaclass__', '__module__', '__ne__',
    '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__',
    '__sizeof__', '__str__', '__subclasshook__', '__unicode__', '__weakref__',
    '_base_manager', '_default_manager', '_deferred', '_get_FIELD_display',
    '_get_next_or_previous_by_FIELD', '_get_next_or_previous_in_order',
    '_get_pk_val', '_get_unique_checks', '_meta', '_perform_date_checks',
    '_perform_unique_checks', '_set_pk_val', 'agencies', 'clean',
    'clean_fields', 'date_error_message', 'delete', 'departments', 'focus_set',
    'full_clean', 'funders', 'funding_set', 'get_next_by_changed',
    'get_previous_by_changed', 'investigator_set', 'investigators', 'objects',
    'pk', 'prepare_database_save', 'priorities', 'product_set', 'professions',
    'programs', 'save', 'save_base', 'serializable_value',
    'unique_error_message', 'validate_unique']

    >>> dir(Project.objects)
    ['__class__', '__delattr__', '__dict__', '__doc__', '__format__',
    '__getattribute__', '__hash__', '__init__', '__module__', '__new__',
    '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__',
    '__str__', '__subclasshook__', '__weakref__', '_copy_to_model', '_db',
    '_inherited', '_insert', '_set_creation_counter', '_update', 'aggregate',
    'all', 'annotate', 'complex_filter', 'contribute_to_class', 'count',
    'create', 'creation_counter', 'dates', 'db', 'db_manager', 'defer',
    'distinct', 'exclude', 'exists', 'extra', 'filter', 'get',
    'get_empty_query_set', 'get_or_create', 'get_query_set', 'in_bulk',
    'iterator', 'latest', 'model', 'none', 'only', 'order_by', 'raw',
    'reverse', 'select_related', 'update', 'using', 'values', 'values_list']

    >>> dir(Priority)
    ['DoesNotExist', 'MultipleObjectsReturned', '__class__', '__delattr__',
    '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__',
    '__hash__', '__init__', '__metaclass__', '__module__', '__ne__',
    '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__',
    '__sizeof__', '__str__', '__subclasshook__', '__unicode__', '__weakref__',
    '_base_manager', '_default_manager', '_deferred', '_get_FIELD_display',
    '_get_next_or_previous_by_FIELD', '_get_next_or_previous_in_order',
    '_get_pk_val', '_get_unique_checks', '_meta', '_perform_date_checks',
    '_perform_unique_checks', '_set_pk_val', 'clean', 'clean_fields',
    'date_error_message', 'delete', 'focus_set', 'full_clean',
    'get_next_by_changed', 'get_previous_by_changed', 'objects', 'pk',
    'prepare_database_save', 'project_set', 'save', 'save_base',
    'serializable_value', 'unique_error_message', 'validate_unique']

  Reducing them a bit to the methods we have a clue about:

    >>> dir(Project)

    ['agencies', 'departments', 'focus_set', 'funders', 'funding_set',
    'investigator_set', 'investigators', 'priorities', 'product_set',
    'professions', 'programs']

    >>> dir(Priority)
    ['focus_set', 'project_set']

______________________________________________________________________________

2012.05.30 KJC

  Which markup (and Django markup plugin) to use?

  Thus far, the simplest to implement, and hence, implemented, is:

    http://pypi.python.org/pypi/django-wysiwyg

  which uses (IIRC) Yahoo's YUI.  However, the following looks
  promising as well, albeit somewhat more complex.

    http://pypi.python.org/pypi/django-markitup/1.0.0

  In fact, there are way too many choices that look promising. I wish
  there was concensus on a best practice for this!  (And AJAX.) The
  waffling, wishy-washy answers are unhelpful.
______________________________________________________________________________

2012.08.01 KJC

  Today's stupid pet trick: Removing trailing periods from fields with SQL:

    -- Show records with trailing periods
    SELECT  REGEXP_REPLACE(title,'(.*)\.$',E'\\1','g')
      AS    new_title
      FROM  ragu_project
      WHERE title ~ '\. *$';

    -- Remove trailing periods
    UPDATE ragu_project
      SET title = REGEXP_REPLACE(title,'(.*)\. *$',E'\\1','g');

    -- Remove trailing spaces
    UPDATE ragu_project
      SET title = REGEXP_REPLACE(title,'(.*) +$',E'\\1','g');

  Sigh.  Apparently, differences between 8.4 and 9.1 mean that the
  older PostgreSQL requires a lot more escaping. I need to figure out
  the rules again, but the remove trailing periods example devolved
  into:

    UPDATE ragu_project
       SET title = REGEXP_REPLACE(title,E'(.*)\\. *$',E'\\1','g')
       WHERE title ~ E'\\. *$';
______________________________________________________________________________

2012.08.23 KJC

  Today's stupid pet trick:

    # DEBUG: Randomly select a project from the set of all project IDs.

    import random

    projects = Project.objects.all()
    pks      = projects.values_list("id", flat=True)
    winner   = pks[random.randrange(0,len(pks))]
    project  = get_object_or_404(Project, pk=winner)
______________________________________________________________________________

