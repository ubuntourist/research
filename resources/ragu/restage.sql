-- List as book-ready any projects listed as reported in
-- years prior to 2012, and NOT product_only.

update ragu_project set stage = 3 where id in (select id
  from ragu_project
  where     is_live
  and   not product_only
  and   id in (select  project_id
                 from  ragu_report
                 where reported < 2012));

-- List as FORMERLY book-ready any projects listed as reported in
-- years prior to 2012, and NOT product_only and having an ending
-- date in FY 2012, which means it's still active and perhaps in
-- need of massaging.

update ragu_project set stage = 4 where id in (select id
  from ragu_project
  where     is_live
  and   not product_only
  and   id in (select  project_id
                 from  ragu_report
                 where reported < 2012)
  and end_date > '2011-09-30');

-- For both of the above stanzas... Why NOT product_only?  And should
-- we explicitly do something with is_live = FALSE records?

