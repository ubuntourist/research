SELECT * FROM ragu_agency
  WHERE LENGTH(name) = 0
  OR name = '<p> </p>'
  OR name = '<p></p>';

SELECT * FROM ragu_agency
  WHERE LENGTH(homepage) = 0
  OR homepage = '<p> </p>'
  OR homepage = '<p></p>';

SELECT * FROM ragu_classification
  WHERE LENGTH(name) = 0
  OR name = '<p> </p>'
  OR name = '<p></p>';

SELECT * FROM ragu_department
  WHERE LENGTH(name) = 0
  OR name = '<p> </p>'
  OR name = '<p></p>';

SELECT * FROM ragu_department
  WHERE LENGTH(homepage) = 0
  OR homepage = '<p> </p>'
  OR homepage = '<p></p>';

SELECT * FROM ragu_department
  WHERE LENGTH(description) = 0
  OR description = '<p> </p>'
  OR description = '<p></p>';

SELECT * FROM ragu_editor
  WHERE LENGTH(username) = 0
  OR username = '<p> </p>'
  OR username = '<p></p>';

SELECT * FROM ragu_employee
  WHERE LENGTH(caste) = 0
  OR caste = '<p> </p>'
  OR caste = '<p></p>';

SELECT * FROM ragu_employee
  WHERE LENGTH(department) = 0
  OR department = '<p> </p>'
  OR department = '<p></p>';

SELECT * FROM ragu_employee
  WHERE LENGTH(last_name) = 0
  OR last_name = '<p> </p>'
  OR last_name = '<p></p>';

SELECT * FROM ragu_employee
  WHERE LENGTH(first_name) = 0
  OR first_name = '<p> </p>'
  OR first_name = '<p></p>';

SELECT * FROM ragu_employee
  WHERE LENGTH(middle_name) = 0
  OR middle_name = '<p> </p>'
  OR middle_name = '<p></p>';

SELECT * FROM ragu_employee
  WHERE LENGTH(email) = 0
  OR email = '<p> </p>'
  OR email = '<p></p>';

SELECT * FROM ragu_funder
  WHERE LENGTH(name) = 0
  OR name = '<p> </p>'
  OR name = '<p></p>';

SELECT * FROM ragu_funder
  WHERE LENGTH(acronym) = 0
  OR acronym = '<p> </p>'
  OR acronym = '<p></p>';

SELECT * FROM ragu_funder
  WHERE LENGTH(homepage) = 0
  OR homepage = '<p> </p>'
  OR homepage = '<p></p>';

SELECT * FROM ragu_garbage
  WHERE LENGTH(name) = 0
  OR name = '<p> </p>'
  OR name = '<p></p>';

SELECT * FROM ragu_person
  WHERE LENGTH(last_name) = 0
  OR last_name = '<p> </p>'
  OR last_name = '<p></p>';

SELECT * FROM ragu_person
  WHERE LENGTH(first_name) = 0
  OR first_name = '<p> </p>'
  OR first_name = '<p></p>';

SELECT * FROM ragu_person
  WHERE LENGTH(email) = 0
  OR email = '<p> </p>'
  OR email = '<p></p>';

SELECT * FROM ragu_priority
  WHERE LENGTH(summary) = 0
  OR summary = '<p> </p>'
  OR summary = '<p></p>';

SELECT * FROM ragu_priority
  WHERE LENGTH(description) = 0
  OR description = '<p> </p>'
  OR description = '<p></p>';

SELECT * FROM ragu_product
  WHERE LENGTH(description) = 0
  OR description = '<p> </p>'
  OR description = '<p></p>';

SELECT * FROM ragu_profession
  WHERE LENGTH(name) = 0
  OR name = '<p> </p>'
  OR name = '<p></p>';

SELECT * FROM ragu_program
  WHERE LENGTH(name) = 0
  OR name = '<p> </p>'
  OR name = '<p></p>';

SELECT * FROM ragu_program
  WHERE LENGTH(homepage) = 0
  OR homepage = '<p> </p>'
  OR homepage = '<p></p>';

SELECT * FROM ragu_project
  WHERE LENGTH(title) = 0
  OR title = '<p> </p>'
  OR title = '<p></p>';

SELECT * FROM ragu_project
  WHERE LENGTH(description) = 0
  OR description = '<p> </p>'
  OR description = '<p></p>';

SELECT * FROM ragu_staff
  WHERE LENGTH(homepage) = 0
  OR homepage = '<p> </p>'
  OR homepage = '<p></p>';
