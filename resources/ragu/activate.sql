-- Written by Kevin Cole <kjcole@gallaudet.edu> 2013.10.27
--

\set ongoing 1
\set completed 2

\set unsubmitted 0
\set unproven 1
\set webready 2
\set bookready 3

\set accepted 't'
\set rejected 'f'
\set undecided null

\set fy 2013

-- Surpress column headings, row count footer, and column separators.
\pset tuples_only
\pset border 0

\o report.sql

-- Find live projects that were active in FY 2013.
--
-- "Active" is determined to be meeting one of:
--             * starting before the end of the fiscal year in question,
--             * having an unreported starting date,
-- AND one of:
--             * starting after the end of the previous fiscal year,
--             * ending   after the end of the previous fiscal year,
--             * having an unreported end date AND marked ONGOING
--

SELECT E'INSERT INTO ragu_report (changed,project_id,reported,editor_id)\n' ||
       '  VALUES (CURRENT_TIMESTAMP,' || id || ',' || :fy::varchar || ',1);'
  AS   insert_query
  FROM ragu_project
  WHERE is_live
  AND   is_worthy
  AND   stage = :bookready
  AND ((begin_date IS NULL) OR (begin_date < (:fy::varchar || '-10-01')::date))
  AND ((begin_date > ((:fy-1)::varchar || '-09-30')::date)
  OR   (end_date   > ((:fy-1)::varchar || '-09-30')::date)
  OR   (end_date   IS NULL  AND status = :ongoing))
