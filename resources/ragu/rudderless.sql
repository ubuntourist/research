-- Projects with NO investigator!

select id, substring(title for 99) as title
  from ragu_project
  where id not in
     (select project_id
        from ragu_investigator)
  and is_live
  order by title;

-- Projects having no PRINCIPAL investigator

select id, substring(title for 99) as title
  from ragu_project
  where id not in
     (select project_id
        from ragu_investigator
        where is_principal)
  and is_live
  order by title;

-- Projects having no LEAD investigator

select id, substring(title for 99) as title
  from ragu_project
  where id not in
     (select project_id
        from ragu_investigator
        where is_leader)
  and is_live
  order by title;
