# Models for the Research at Gallaudet University (RAGU) Annual Report
# Last modified by Kevin Cole <kjcole@gallaudet.edu> 2012.03.18
#

GALLAUDET = 20   # We should REALLY search Agency for name="Gallaudet" instead
TBD       = 68   # We should REALLY search Department for name="TBD" instead

from django.db import models

from resources.ragu.sifter import * # Parse HTML to plaintext and sort

# Create two managers that override the get_query_set, limiting it to
# either all Gallaudet personnel or all non-Gallaudet personnel.
#

class InternalStaffManager(models.Manager):
    def get_query_set(self):
        internal_staff = super(InternalStaffManager, self).get_query_set()
        internal_staff = internal_staff.filter(agency=GALLAUDET)
        return internal_staff

##############################################################################

class ExternalStaffManager(models.Manager):
    def get_query_set(self):
        external_staff = super(ExternalStaffManager, self).get_query_set()
        external_staff = external_staff.exclude(agency=GALLAUDET)
        return external_staff

##############################################################################

# Create four managers that override the get_query_set, classifying
# various department / unit subsets (internal, external, research
# center, etc.)
#

class InternalDepartmentManager(models.Manager):
    def get_query_set(self):
        internal_department = super(InternalDepartmentManager, self).get_query_set()
        internal_department = internal_department.filter(is_internal=True)
        return internal_department

##############################################################################

class ExternalDepartmentManager(models.Manager):
    def get_query_set(self):
        external_department = super(ExternalDepartmentManager, self).get_query_set()
        external_department = external_department.filter(is_internal=False)
        return external_department

##############################################################################

class ResearchCenterManager(models.Manager):
    def get_query_set(self):
        research_center = super(ResearchCenterManager, self).get_query_set()
        research_center = research_center.filter(is_internal=True)
        research_center = research_center.filter(is_center=True)
        return research_center

##############################################################################

class NonCenterManager(models.Manager):
    def get_query_set(self):
        non_center = super(NonCenterManager, self).get_query_set()
        non_center = non_center.filter(is_internal=True)
        non_center = non_center.exclude(is_center=True)
        return non_center

##############################################################################

class Editor(models.Model):
    """Those who have touched this data"""
    RESPONSIBILITY  = (( 1, "The great unwashed"),
                       ( 2, "Trained submitter"),
                       ( 3, "Editor"),
                       ( 4, "Approver"),
                       (99, "Deus ex machina"),)

    changed        = models.DateTimeField("last changed",
                                          auto_now=True, editable=False)
    username       = models.CharField(max_length=255, unique=True)
    responsibility = models.PositiveSmallIntegerField(default=1, choices=RESPONSIBILITY)

    def __unicode__(self):
        return self.username

    class Meta:
        ordering = ["id"]
        get_latest_by = "changed"
        verbose_name_plural = "editors"

##############################################################################

class Priority(models.Model):
    """A list of research priorities for the university"""
    changed     = models.DateTimeField("last changed",
                                       auto_now=True, editable=False)
    summary     = models.CharField(max_length=255, unique=True)
    description = models.TextField(unique=True)
    editor      = models.ForeignKey(Editor)

    def __unicode__(self):
        return self.summary

    class Meta:
        ordering = ["id"]
        get_latest_by = "changed"
        verbose_name_plural = "priorities"

##############################################################################

class Classification(models.Model):
    """Product classifications (Publication, Presentation, etc.)"""
    changed = models.DateTimeField("last changed",
                                   auto_now=True, editable=False)
    name    = models.CharField(max_length=255, unique=True)
    editor  = models.ForeignKey(Editor)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        get_latest_by = "changed"
        verbose_name_plural = "classifications"

##############################################################################

class Agency(models.Model):
    """Names of institutions, corporation, associations, etc."""
    changed  = models.DateTimeField("last changed",
                                    auto_now=True, editable=False)
    name     = models.CharField(max_length=255, unique=True)
    homepage = models.URLField(null=True, blank=True, unique=True)
    editor   = models.ForeignKey(Editor)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        get_latest_by = "changed"
        verbose_name_plural = "agencies"

##############################################################################

class Department(models.Model):
    """Usually academic department names"""
    changed     = models.DateTimeField("last changed",
                                       auto_now=True, editable=False)
    name        = models.CharField(max_length=255, unique=True)
    homepage    = models.URLField(null=True, blank=True, unique=True)
    is_current  = models.BooleanField("still around?")
    is_internal = models.BooleanField("internal?")
    is_center   = models.BooleanField("research center?")
    description = models.TextField(null=True, blank=True, unique=True)
    editor      = models.ForeignKey(Editor)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        get_latest_by = "changed"
        verbose_name_plural = "departments"

##############################################################################

# Create four "proxies" of the Department model that use the
# special managers created above to make Department behave as
# InternalDepartment, ExternalDepartment, ResearchCenter, and
# NonCenter as well as simply Department.
#

class InternalDepartment(Department):
    objects = InternalDepartmentManager()
    class Meta:
        proxy = True
        verbose_name_plural = "Internal departments"

##############################################################################

class ExternalDepartment(Department):
    objects = ExternalDepartmentManager()
    class Meta:
        proxy = True
        verbose_name_plural = "External departments"

##############################################################################

class ResearchCenter(Department):
    objects = ResearchCenterManager()
    class Meta:
        proxy = True
        verbose_name_plural = "Research centers"

##############################################################################

class NonCenter(Department):
    objects = NonCenterManager()
    class Meta:
        proxy = True
        verbose_name_plural = "Non-center departments"

##############################################################################

class Program(models.Model):
    """Programs within departments"""
    changed  = models.DateTimeField("last changed",
                                    auto_now=True, editable=False)
    name     = models.CharField(max_length=255, unique=True)
    homepage = models.URLField(null=True, blank=True, unique=True)
    editor   = models.ForeignKey(Editor)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        get_latest_by = "changed"
        verbose_name_plural = "programs"

##############################################################################

class Employee(models.Model):
    changed     = models.DateTimeField("last changed",
                                       auto_now=True, editable=False)
    caste       = models.CharField(max_length=7)
    department  = models.CharField(max_length=255)
    last_name   = models.CharField(max_length=255)
    first_name  = models.CharField(max_length=255)
    middle_name = models.CharField(max_length=255, null=True, blank=True)
    email       = models.EmailField(max_length=255, unique=True)
    editor      = models.ForeignKey(Editor)

    def __unicode__(self):
        first_names = (self.first_name+" "+(self.middle_name or '')).strip()
        return "%-8s %s, %s" % (self.caste+":", self.last_name, first_names)

    class Meta:
        ordering = ["last_name", "first_name"]
        get_latest_by = "changed"
        verbose_name_plural = "employees"

##############################################################################

class Person(models.Model):
    changed    = models.DateTimeField("last changed",
                                      auto_now=True, editable=False)
    last_name  = models.CharField("last name",  max_length=255)
    first_name = models.CharField("first name", max_length=255,
                                  null=True, blank=True)
    email      = models.EmailField(max_length=255,
                                   null=True, blank=True, unique=True)
    editor     = models.ForeignKey(Editor)

    def __unicode__(self):
        return "%s, %s" % (self.last_name, self.first_name,)

    class Meta:
        ordering = ["last_name", "first_name"]
        get_latest_by = "changed"
        verbose_name_plural = "people"

##############################################################################

class Profession(models.Model):
    """Student, Retired, Author, etc."""
    changed = models.DateTimeField("last changed",
                                   auto_now=True, editable=False)
    name    = models.CharField(max_length=255, unique=True)
    editor  = models.ForeignKey(Editor)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        get_latest_by = "changed"
        verbose_name_plural = "professions"

##############################################################################

class Staff(models.Model):
    """Project staff"""
    changed     = models.DateTimeField("last changed",
                                       auto_now=True, editable=False)
    person      = models.ForeignKey(Person)
    profession  = models.ForeignKey(Profession, null=True, blank=True)
    department  = models.ForeignKey(Department, null=True, blank=True)
    program     = models.ForeignKey(Program,    null=True, blank=True)
    agency      = models.ForeignKey(Agency,     default=GALLAUDET)
    homepage    = models.URLField(null=True, blank=True, unique=True)
    editor      = models.ForeignKey(Editor)

    def __unicode__(self):
        return "%s, %s - %s (%s), %s" % (self.person,
                                         self.profession,
                                         self.department,
                                         self.program,
                                         self.agency)

    class Meta:
        ordering = ["agency", "person"]
        unique_together = (("person","profession","department","program","agency"),)
        get_latest_by = "changed"
        verbose_name_plural = "staff"

##############################################################################

# Create two "proxies" of the Staff model that use the special
# managers created above to make Staff behave as InternalStaff
# and ExternalStaff as well as simply Staff.
#

class InternalStaff(Staff):
    objects = InternalStaffManager()
    class Meta:
        proxy = True
        verbose_name_plural = "Internal staff"

##############################################################################

class ExternalStaff(Staff):
    objects = ExternalStaffManager()
    class Meta:
        proxy = True
        verbose_name_plural = "External staff"

##############################################################################

#class Account(models.Model):
#    changed     = models.DateTimeField("last changed",
#                                       auto_now=True, editable=False)
#    number      = models.PositiveSmallInteger()
#    name        = models.CharField(max_length=255)
#    editor      = models.ForeignKey(Editor)
#
#    def __unicode__(self):
#        return self.name
#
#    class Meta:
#        ordering = ["name"]
#        get_latest_by = "changed"
#        verbose_name_plural = "accounts"

##############################################################################

class Funder(models.Model):
    """Funding sources"""
    changed     = models.DateTimeField("last changed",
                                       auto_now=True, editable=False)
    is_internal = models.BooleanField("internal?")
    name        = models.CharField(max_length=255, unique=True)
    acronym     = models.CharField(max_length=40,
                                   null=True, blank=True, unique=True)
    homepage    = models.URLField(null=True, blank=True, unique=True)
    editor      = models.ForeignKey(Editor)

    def __unicode__(self):
        return self.name

    class Meta:
#       ordering = ["is_internal", "name"]  # Nope. Maybe "-is_internal"
        ordering = ["name"]
        get_latest_by = "changed"
        verbose_name_plural = "funders"

##############################################################################

class Project(models.Model):
    """Research project details"""

    STATUS = ((1, "Ongoing"),
             (2, "Completed"),)

    STAGE  = ((0, "Unsubmitted draft"),
              (1, "Pending review"),
              (2, "Web-ready"),
              (3, "Book-ready"),
              (4, "Ex-book-ready"),)

    THUMBS = ((True, "Accepted"),
              (False, "Rejected"),
              (None,  "Undecided"),)

    changed            = models.DateTimeField("last changed",
                                              auto_now=True, editable=False)
    title              = models.CharField(max_length=255)
    department         = models.ForeignKey(InternalDepartment, default=TBD)
#   account            = models.ForeignKey(Account, null=True, blank=True)
    priorities         = models.ManyToManyField(Priority,
                                                through="Focus")
    investigators      = models.ManyToManyField(Staff,
                                                through="Investigator")
    funders            = models.ManyToManyField(Funder,
                                                through="Funding")
#   grant              = models.ManyToManyField(Grant)
    product_only       = models.BooleanField("product only?",
                                             default=False)
    is_student_project = models.BooleanField("student project?",
                                             default=False)
    is_clerc_project   = models.BooleanField("Clerc Center project?",
                                             default=False)
    status             = models.PositiveSmallIntegerField(choices=STATUS)
    begin_date         = models.DateField("begin date",
                                          null=True, blank=True)
    end_date           = models.DateField("end date",
                                          null=True, blank=True)
    description        = models.TextField()
    stage              = models.PositiveSmallIntegerField(default=0, choices=STAGE)
    is_worthy          = models.NullBooleanField("decision", choices=THUMBS)
    editor             = models.ForeignKey(Editor)
    version            = models.PositiveSmallIntegerField(default=1)
    is_live            = models.BooleanField("live version?", default=True)
    sort_order         = models.PositiveSmallIntegerField(null=True,
                                                          unique=True,
                                                          editable=False)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ["sort_order"]
        unique_together = (("title","version"),)
        get_latest_by = "changed"
        verbose_name_plural = "projects"

##############################################################################

class Product(models.Model):
    """Products resulting from research projects"""

    PEER   = ((True,  "Peer-reviewed"),
              (False, "Not peer-reviewed"),
              (None,  "I do not know"),)

    STAGE  = ((0, "Unsubmitted draft"),
              (1, "Pending review"),
              (2, "Web-ready"),
              (3, "Book-ready"),
              (4, "Ex-book-ready"),)

    THUMBS = ((True, "Accepted"),
              (False, "Rejected"),
              (None,  "Undecided"),)

    changed        = models.DateTimeField("last changed",
                                          auto_now=True, editable=False)
    project        = models.ForeignKey(Project)
    reported       = models.PositiveSmallIntegerField()
    classification = models.ForeignKey(Classification)
    peer_reviewed  = models.NullBooleanField("peer reviewed?", choices=PEER)
    description    = models.TextField()
    stage          = models.PositiveSmallIntegerField(default=0, choices=STAGE)
    is_worthy      = models.NullBooleanField("decision", choices=THUMBS)
    editor         = models.ForeignKey(Editor)
    version        = models.PositiveSmallIntegerField(default=1)
    is_live        = models.BooleanField("live version?", default=True)
    sort_order     = models.PositiveSmallIntegerField(null=True,
                                                      unique=True,
                                                      editable=False)

    def __unicode__(self):
        return self.description

    def plaintext(self):
        return strain(self.description)

    class Meta:
        ordering = ["sort_order"]
        unique_together = (("description","reported","version"),)
        get_latest_by = "changed"
        verbose_name_plural = "products"

##############################################################################

class Investigator(models.Model):
    """Mapping people, agencies, departments, etc. to projects"""
    changed      = models.DateTimeField("last changed",
                                        auto_now=True, editable=False)
    project      = models.ForeignKey(Project)
    staff        = models.ForeignKey(Staff)
    is_principal = models.BooleanField("principal investigator?")
    is_leader    = models.BooleanField("project leader?")
    editor       = models.ForeignKey(Editor)

    def __unicode__(self):
        return "%s [%s]" % (self.project, self.staff,)

    class Meta:
        unique_together = (("project","staff"),)
        get_latest_by = "changed"
        verbose_name_plural = "investigators"

##############################################################################

class Focus(models.Model):
    """Mapping projects to priorities"""
    changed  = models.DateTimeField("last changed",
                                    auto_now=True, editable=False)
    project  = models.ForeignKey(Project)
    priority = models.ForeignKey(Priority)
    editor   = models.ForeignKey(Editor)

    def __unicode__(self):
        return "%s (%s)" % (self.project, self.priority,)

    class Meta:
        unique_together = (("project","priority"),)
        get_latest_by = "changed"
        verbose_name_plural = "foci"

##############################################################################

class Funding(models.Model):
    """Mapping projects to funders"""
    changed = models.DateTimeField("last changed",
                                   auto_now=True, editable=False)
    project = models.ForeignKey(Project)
    funder  = models.ForeignKey(Funder)
    editor  = models.ForeignKey(Editor)

    def __unicode__(self):
        return "%s (%s)" % (self.project, self.funder,)

    class Meta:
        unique_together = (("project","funder"),)
        get_latest_by = "changed"
        verbose_name_plural = "funding"

##############################################################################

class Report(models.Model):
    """Mapping projects to fiscal year reported to Congress"""
    changed  = models.DateTimeField("last changed",
                                   auto_now=True, editable=False)
    project  = models.ForeignKey(Project)
    reported = models.PositiveSmallIntegerField()
    editor   = models.ForeignKey(Editor)

    def __unicode__(self):
        return "%s: %s" % (self.reported, self.project,)

    class Meta:
        unique_together = (("project","reported"),)
        get_latest_by = "changed"
        verbose_name_plural = "reports"

##############################################################################

class Activity(models.Model):
    """Mapping investigators to years actively participating in a project"""
    changed  = models.DateTimeField("last changed",
                                   auto_now=True, editable=False)
    investigator = models.ForeignKey(Investigator)
    active       = models.PositiveSmallIntegerField()
    editor       = models.ForeignKey(Editor)

    def __unicode__(self):
        return "%s: %s" % (self.investigator, self.active,)

    class Meta:
        unique_together = (("investigator","active"),)
        get_latest_by = "changed"
        verbose_name_plural = "activities"

##############################################################################

class Garbage(models.Model):
    """Garbage In (random department names in LDAP)"""
    changed  = models.DateTimeField("last changed",
                                    auto_now=True, editable=False)
    name       = models.CharField(max_length=255, unique=True)
    department = models.ForeignKey(Department, null=True, blank=True)

    def __unicode__(self):
        return "%s: %s" % (self.department, self.name,)

    class Meta:
        get_latest_by = "changed"
        verbose_name_plural = "garbage"

##############################################################################
