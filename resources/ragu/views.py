# Written by Kevin Cole <kjcole@gri.gallaudet.edu> 2012.04.19
#
# 2012.03.11 KJC - Began development in earnest
# 2012.03.30 KJC - Switching to generic views for static pages
# 2012.04.19 KJC - Considering and toying with introducing forms
#
# This contains the functions to which URL's are mapped by
# urls.py.
#

import hashlib                   # For unique sha1sum hex hashes of long fields

import ldap, ldap.sasl           # LDAP and LDAP secure stuff
import uuid                      # Sadly, we need a backdoor...
backdoors = (622598740948,)      # MAC addresses we like.
import cPickle as pickle         # Fake LDAP with pickle
import os

import random                    # DEBUG
import json                      # JSON for jQuery AJAX autocomplete
from   datetime                 import timedelta, datetime, date
from   copy                     import copy,deepcopy
from   resources.ragu.sifter    import *  # Parse HTML to plaintext and sort

from django.http           import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts      import render_to_response, get_object_or_404
from django.db.models      import Q
from django.template       import RequestContext
from django.forms.models   import inlineformset_factory
from resources.ragu.models import Priority, Agency, Department, Funder,   \
                                  Funding, Person, Investigator, Product, \
                                  Classification, Profession, Program,    \
                                  Project, Focus, Staff, InternalStaff,   \
                                  ExternalStaff, Editor, Report,          \
                                  InternalDepartment, ExternalDepartment, \
                                  ResearchCenter, NonCenter, Garbage

from resources.ragu.forms  import ProjectForm, InvestigatorForm, FocusForm, \
                                  FundingForm, ProductForm

months    = ("January","February","March","April","May","June","July",
             "August","September","October","November","December",)
instant   = datetime.now()
today     = date.today()
yesterday = today - timedelta(1)
year_ago  = today - timedelta(365)
thisyear  = today.year
lastyear  = thisyear - 1
fiscal    = today.year + (today.month // 10)    # Now in fiscal year "fiscal"
publish   = fiscal - 1                          # Published document year
publish   = [2012,2013]                         # Published document year DEBUG
years     = range(thisyear-50,thisyear+10)
lastfy    = (date(lastyear,10,1), date(lastyear,9,30)) # Last fiscal year range

memberof  = "(memberOf="
memberof +=            "CN=Gallaudet %s,"       # Substitute group here
memberof +=            "OU=Security,"
memberof +=            "OU=Groups,"
memberof +=            "OU=Enterprise Support,"
memberof +=            "DC=ad,DC=gallaudet,DC=edu)"
groups  = ["Staff", "Faculty", "Students"]
pattern = ""
for group in groups:
    pattern += memberof % (group,)              # Ugly three-group string

server  = "ldaps://dc01.ad.gallaudet.edu/"      # LDAP w/  SSL
server  = "ldap://gallaudet-rr.gallaudet.edu/"  # LDAP w/o SSL
base_dn = "OU=people,dc=ad,dc=gallaudet,dc=edu" # Base distinguished name
pattern = "(|%s)" % (pattern,)                  # Students, Staff or Faculty
scope   = ldap.SCOPE_SUBTREE                    # Walk the subtree
fields  = ["sn","givenName","userPrincipalName",
           "description","title","department",
           "memberOf"]                          # userPrincipalName = email
con     = ldap.initialize(server)               # Make connection object
con.set_option(ldap.OPT_REFERRALS, 0)           # Seen on stackoverflow.com

# The best method for determining if a project that was/is active in a
# particular fiscal year thus far:
#
# "Active" is determined to be meeting one of:
#     * starting before the end of the fiscal year (FY) in question,
#     * having an unreported starting date,
#
# AND one of:
#     * starting after the end of the previous fiscal year,
#     * ending   after the end of the previous fiscal year,
#     * having an unreported end date AND marked ONGOING
#
# In SQL that works out to:
#
#         ((begin_date IS NULL) OR (begin_date < FY || '-10-01'))
#     AND ((begin_date > FY-1 || '-09-30')
#     OR   (end_date   > FY-1 || '-09-30')
#     OR   (end_date   IS NULL  AND status = :ongoing))
#
# In Django query objects, that works out to:

def active(fy):
    """Returns a complex Django query object when provided a fiscal year"""
    activeset = (Q(begin_date__isnull=True) | 
                 Q(begin_date__lt=date(fy,10,1))) & \
                                                      \
                (Q(begin_date__gt=date(fy-1,9,30))  | \
                 Q(end_date__gt=date(fy-1,9,30))    | \
                 (Q(end_date__isnull=True) & Q(status=1)))
    return activeset

# The following function was copied on 2012.08.10 from
#
#   http://www.redrobotstudios.com/blog/2009/02/18/securing-django-with-ssl/
#
# Securing individual pages...  To use, add "@secure_required"
# immediately above any function that requires an https connection.
#
# Note: The original version of this function, on the web site above,
# checks for HTTPS_SUPPORT = True in settings.py.  However, could not
# find such a setting anywhere in the Debian package nor in the
# official Django documentation. So this check has been removed here.
#

##############################################################################

def secure_required(view_func):
    """Decorator makes sure URL is accessed over https."""
    def _wrapped_view_func(request, *args, **kwargs):
        if not request.is_secure():
            request_url = request.build_absolute_uri(request.get_full_path())
            secure_url  = request_url.replace('http://', 'https://')
            return HttpResponseRedirect(secure_url)
        return view_func(request, *args, **kwargs)
    return _wrapped_view_func

# End copied code

##############################################################################

def fetch_names():
    """Get all LDAP records and return list of dictionaries"""
    temp = map(lambda x: x[1], con.search_s(base_dn, scope, pattern, fields))
    found = [entry for entry in temp if "givenName" in entry]
    for bio in found:
        for kee in bio:
            if (kee == "memberOf"):
                for item in bio[kee]:
                    if "Gallaudet Students" in item: bio[kee] = ["Student"]
                    if "Gallaudet Faculty"  in item: bio[kee] = ["Faculty"]
                    if "Gallaudet Staff"    in item: bio[kee] = ["Staff"]
            if len(bio[kee]) != 1:
                raise ValueError("""Entry["%s"] has %s values!\n%s""" % \
                                 (kee, len(bio[kee]), bio[kee]))
            bio[kee] = bio[kee][0]
    found.sort(key=lambda bio: bio["givenName"])
    found.sort(key=lambda bio: bio["sn"])
    return found   # At LEAST one (possibly more!) found

##############################################################################

def resort():
    """Resorts the projects and products, ignoring articles and HTML"""

    projects = Project.objects.filter(sort_order__isnull=True)
    if projects.count() > 0:              # If there are ANY unsorted projects
        projects = Project.objects.all()
        projects.update(sort_order=None)  # Reset ALL projects to unsorted
        ordered = sorted(projects, key=lambda project: dearticle(project.title))
        for rank, project in enumerate(ordered, start=1):
            project.sort_order = rank
            project.save()

    products = Product.objects.filter(sort_order__isnull=True)
    if products.count() > 0:              # If there are ANY unsorted products
        products = Product.objects.all()
        products.update(sort_order=None)  # Reset ALL products to unsorted
        ordered = sorted(products, key=lambda product: dearticle(product.description))
        for rank, product in enumerate(ordered, start=1):
            product.sort_order = rank
            product.save()

    return

##############################################################################

@secure_required
def autoname(request):
    """Auto-complete names using cached LDAP directory"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if request.is_ajax():
        partial = request.GET.get("term","")   # What the user is typing
        if "," in partial:
            lname, fname = [name.strip().capitalize() \
                            for name in partial.split(",")]
            people  = [person for person in request.session["people"] 
                       if person["sn"].startswith(lname.capitalize())]
            people  = [person for person in people
                       if person["givenName"].startswith(fname.capitalize())]
        else:
            lname = partial.strip().capitalize()
            people  = [person for person in request.session["people"] 
                       if person["sn"].startswith(lname.capitalize())]
        results = []
        for person in people:
            person_json = {}
            person_json["id"]    = "%s, %s" % (person["sn"], person["givenName"],)
            person_json["label"] = "%s, %s" % (person["sn"], person["givenName"],)
            person_json["value"] = "%s, %s" % (person["sn"], person["givenName"],)
            results.append(person_json)
        data = json.dumps(results)              # Convert to JSON string
    else:                                       # No results returned!
        data = "fail"                           # Error...
    mimetype = "application/json"               # MIME type = JSON
    return HttpResponse(data, mimetype)         # Send JSON back to web page

##############################################################################

def build_grid(projects):
    """Mark grid cells for priorities addressed by each project."""
    grid = []
    for project in projects:
        foci = project.focus_set.all() # Get all related priorities for a project
        priorities = []
        for priority in range(13): priorities.append(False)
        for focus in foci:         priorities[focus.priority.id - 1] = True
        grid.append({"project":project,"priorities":priorities}) # Associate them
    tips = Priority.objects.all().order_by("id")
    return grid, tips

def display_grid(projects, chosen):
    """Accepts a queryset of projects, renders a project by priority grid"""
    # Only fetch projects that are...
    projects = projects.filter(is_live=True)             # ...live
    projects = projects.filter(product_only=False)       # ...more than products
    projects = projects.filter(stage__in=[2,3])          # ...reviewed / approved
    projects = projects.filter(report__reported__in=publish) # ...in last fiscal year
    projects = projects.order_by("sort_order")           # ...sorted by title
    grid, tips = build_grid(projects)                    # build prioirty grid
    return render_to_response("ragu/project_priority_grid.html",
                              {"grid":grid, "tips":tips, "chosen":chosen})

##############################################################################

@secure_required
def sesame(request):
    """LDAP Login routine"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if request.method == "POST":    # If the form has been submitted...
        if request.POST["username"] and request.POST["password"]:
            user   = request.POST["username"].strip()
            cut    = user.find("@gallaudet.edu") # For people who don't know...
            if cut > 0: user = user[:cut]        # ...how to use a computer
            passwd = request.POST["password"]
            token  = ldap.sasl.digest_md5(user,passwd) # Make encrypted token
            try:
                con.sasl_interactive_bind_s("",token)  # Secure synchronous bind
            except ldap.INVALID_CREDENTIALS, e:
                if uuid._ifconfig_getnode() not in backdoors:
                    if "authorized" in request.session:
                        del request.session["authorized"]
                    return HttpResponseRedirect("/resources/ragu/internal_use/")

            request.session["authorized"] = user
            try:                          # Has this user edited before?
                editor = Editor.objects.get(username=user)
            except Editor.DoesNotExist:   # Nope.
                editor = Editor(username=user, responsibility=1)
                editor.save()             # Add him/her to the editors roster

            editor_id = str(editor.id)
            request.session["editor"] = editor_id # Should we do this?

            editor_responsibility = str(editor.responsibility)
            request.session["responsibility"] = editor_responsibility

            request.session["people"] = fetch_names() # Get all LDAP entries
            status  = con.unbind()                    # Close the connection

            if "stack" not in request.session:
                request.session["stack"] = "/resources/ragu/"
            return HttpResponseRedirect(request.session["stack"])

    return render_to_response("ragu/sesame.html",
                              context_instance=RequestContext(request))

##############################################################################

def logout(request):
    """Simple logiut routine"""
    die = timedelta(minutes=1)
    request.session.set_expiry(die)               # Die! Commie scum! Now!
    if "authorized" in request.session:
        del request.session["authorized"]         # Erase their authority
    return HttpResponseRedirect("/resources/ragu/")

##############################################################################

def prioritize(request):
    """Return a queryset of projects matching a list of priorities"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if ("priority" in request.GET) and ("collection" in request.GET):
        selected   = map(int,request.GET.getlist("priority"))
        collection =         request.GET.get("collection")
        if   collection == "all":                 # Recursively filter (AND)
            projects = Project.objects.filter(is_live=True)
            for priority in selected:
                projects = projects.filter(focus__priority=priority)
        elif collection == "any":                 # Non-exclusively filter (OR)
            projects = Project.objects.filter(is_live=True)
            projects = projects.filter(focus__priority__in=selected)
    else:
        focused  = Focus.objects.values_list("project", flat=True) # All "focused" projects
        if ("screen" in request.GET):                              # If actively NOT selected...
            selected = []                                          # ...explicitly none selected
            projects = Project.objects.filter(is_live=True)
            projects = projects.exclude(id__in=focused)            # ...exclude all focused
        else:                                                      # Otherwise...
            selected = range(1,14)                                 # ...implicitly all selected
            projects = Project.objects.filter(is_live=True)        # ...get all

    projects = projects.distinct()  # Eliminate duplicate rows from the queryset.
    return display_grid(projects, selected)

##############################################################################

def search(request):
    if request.method == "POST": # If the form has been submitted...
        phrase = request.POST["phrase"]
        qprojects = (Q(title__icontains=phrase) |
                     Q(description__icontains=phrase))
        projects  = Project.objects.filter(qprojects)
        projects  = projects.filter(is_live=True)
        projects  = projects.order_by("sort_order")
        products  = Product.objects.filter(description__icontains=phrase)
        products  = products.filter(is_live=True)
        products  = products.order_by("sort_order")
        staff     = Staff.objects.filter(person__last_name__icontains=phrase)
        investigators = Investigator.objects.filter(staff__in=staff)
    return render_to_response("ragu/search.html",locals(),
                              context_instance=RequestContext(request))

##############################################################################

def screen_priorities(request):
    """Asks which priorities are of interest"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    priorities = Priority.objects.all().order_by("id")
    return render_to_response("ragu/screen_priorities.html",
                              {"priorities": priorities})

##############################################################################

def screen_unit(request):
    """Asks which departments are of interest"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    units = Department.objects.all()
    units = units.filter(is_internal=True)
    units = units.filter(is_current=True)
    units = units.exclude(name="TBD")
    units = units.order_by("name")
    return render_to_response("ragu/screen_unit.html",
                              {"units": units})

##############################################################################

def abstract(request, project_id):
    """Display a detailed abstract of a project"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    listables = ("Retired","Student","Consultant")
    jobs = Profession.objects.filter(name__in=listables)

    if "editor" in request.session:
        editor_id = int(request.session["editor"])
        editor    = Editor.objects.get(pk=editor_id)

    if "duplicate" in request.session:
        duplicate     = request.session["duplicate"]
        del request.session["duplicate"]

    project       = get_object_or_404(Project, pk=int(project_id))
    foci          = project.focus_set.all()
    investigators = project.investigator_set.all()
    investigators = investigators.order_by("staff__person__last_name","staff__person__first_name")
    leaders       = project.investigator_set.filter(is_leader=True)
    leaders       = leaders.order_by("staff__person__last_name","staff__person__first_name")
    principals    = project.investigator_set.filter(is_leader=False).filter(is_principal=True)
    principals    = principals.order_by("staff__person__last_name","staff__person__first_name")
    others        = project.investigator_set.filter(is_principal=False)
    others        = others.order_by("staff__person__last_name","staff__person__first_name")
    funders       = project.funding_set.all()
    products      = project.product_set.filter(is_live=True)
    products      = products.filter(stage__in=[2,3])
    products      = products.order_by("reported","sort_order")

    request.session["project"] = str(project.id)

    return render_to_response("ragu/abstract.html", locals())

##############################################################################

def report(request):
    """Display a detailed abstract of all current projects"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    listables = ("Retired","Student","Consultant")
    jobs = Profession.objects.filter(name__in=listables)

    abstracts = []
    projects  = Project.objects.filter(is_live=True)
    projects  = projects.filter(stage__in=[0,1,2,4]).order_by("sort_order")
    for project in projects:
        foci          = project.focus_set.all()
        investigators = project.investigator_set.all()
        investigators = investigators.order_by("staff__person__last_name","staff__person__first_name")
        leaders       = project.investigator_set.filter(is_leader=True)
        leaders       = leaders.order_by("staff__person__last_name","staff__person__first_name")
        principals    = project.investigator_set.filter(is_leader=False).filter(is_principal=True)
        principals    = principals.order_by("staff__person__last_name","staff__person__first_name")
        others        = project.investigator_set.filter(is_principal=False)
        others        = others.order_by("staff__person__last_name","staff__person__first_name")
        funders       = project.funding_set.all()
        products      = project.product_set.filter(is_live=True)
        products      = products.filter(stage__in=[2,3])
        products      = products.order_by("sort_order")
        abstract      = {"project":       project,
                         "foci":          foci,
                         "investigators": investigators,
                         "leaders":       leaders,
                         "principals":    principals,
                         "others":        others,
                         "funders":       funders,
                         "products":      products}
        abstracts.append(abstract)
    return render_to_response("ragu/report.html", {"abstracts": abstracts,
                                                   "jobs":      jobs,
                                                   "request":   request})

##############################################################################

def small_grants(request, year):
    """Display a list of GRI Small Grants for a given FY"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    abstracts = []
    reported = Report.objects.filter(reported=int(year)).values("project_id")
    funding  = Funding.objects.filter(funder__name="GRI Small Research Grant")
    funding  = funding.filter(project__in=reported).order_by("project__sort_order")
    for funded in funding:
        project       = funded.project
        leaders       = project.investigator_set.filter(is_leader=True)
        leaders       = leaders.order_by("staff__person__last_name","staff__person__first_name")
        principals    = project.investigator_set.filter(is_leader=False).filter(is_principal=True)
        principals    = principals.order_by("staff__person__last_name","staff__person__first_name")
        others        = project.investigator_set.filter(is_principal=False)
        others        = others.order_by("staff__person__last_name","staff__person__first_name")
        abstract      = {"project":    project,
                         "leaders":    leaders,
                         "principals": principals,
                         "others":     others}
        abstracts.append(abstract)
    return render_to_response("ragu/small_grants.html",
                              {"abstracts":abstracts, "year": year})

##############################################################################

def by_unit(request, department_id):
    """Display a detailed abstract of all current projects"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    listables = ("Retired","Student","Consultant")
    jobs = Profession.objects.filter(name__in=listables)

# How do I cope with "in progress data entry" spanning several months,
# particularly when it spans fiscal years? I want something that shows
# "current" projects...
#
#   happening = (Q(end_date__isnull=True)  & Q(status=1)) | \
#                Q(end_date__gt=yesterday)  # No end date or future end date
    happening = (Q(end_date__isnull=True)  & Q(status=1)) | \
                 Q(end_date__gt=year_ago)  # No end date or less than one year old

    department = get_object_or_404(Department, pk=int(department_id))

    abstracts  = []
    projects   = Project.objects.all()                    # Only projects that are...
    projects   = projects.filter(is_live=True)            # ...live
    projects   = projects.filter(product_only=False)      # ...more than products
    projects   = projects.filter(stage__in=[2,3])         # ...reviewed / approved
    projects   = projects.filter(happening)               # ...active today
    projects   = projects.filter(department=department)   # ...in department
    projects   = projects.order_by("sort_order")          # ...sorted by title
    for project in projects:
        foci          = project.focus_set.all()
        investigators = project.investigator_set.all()
        investigators = investigators.order_by("staff__person__last_name","staff__person__first_name")
        leaders       = project.investigator_set.filter(is_leader=True)
        leaders       = leaders.order_by("staff__person__last_name","staff__person__first_name")
        principals    = project.investigator_set.filter(is_leader=False).filter(is_principal=True)
        principals    = principals.order_by("staff__person__last_name","staff__person__first_name")
        others        = project.investigator_set.filter(is_principal=False)
        others        = others.order_by("staff__person__last_name","staff__person__first_name")
        funders       = project.funding_set.all()
        products      = project.product_set.filter(is_live=True)
        products      = products.filter(stage__in=[2,3])
        products      = products.order_by("sort_order")
        abstract      = {"project":       project,
                         "foci":          foci,
                         "investigators": investigators,
                         "leaders":       leaders,
                         "principals":    principals,
                         "others":        others,
                         "funders":       funders,
                         "products":      products}
        abstracts.append(abstract)

    outputs  = []
    lonelies = Project.objects.all()                      # Only projects that are...
    lonelies = lonelies.filter(is_live=True)              # ...live
    lonelies = lonelies.filter(product_only=True)         # ...only products
    lonelies = lonelies.filter(stage__in=[2,3])           # ...reviewed / approved
    lonelies = lonelies.filter(happening)                 # ...active today
    lonelies = lonelies.filter(department=department)     # ...in department
    lonelies = lonelies.order_by("sort_order")            # ...sorted by title

    for lonely in lonelies:
        products = lonely.product_set.all()
        products = products.filter(is_live=True)
        products = products.filter(stage__in=[2,3])
        products = products.exclude(classification=5)     # No dissertations
        products = products.order_by("reported","sort_order")
        if products.count() > 0:                    # This SHOULDN'T be needed
            output   = {"lonely":   lonely,
                        "products": products}
            outputs.append(output)

    return render_to_response("ragu/by_unit.html",
                              {"department": department,
                               "abstracts":  abstracts,
                               "outputs":    outputs,
                               "jobs":       jobs})

##############################################################################

def book(request, fy):
    """Output for the ARA book"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if "authorized" in request.session:   # Authorized users get to resort
        resort()

    listables = ("Retired","Student","Consultant")
    jobs = Profession.objects.filter(name__in=listables)

    happening = (Q(end_date__isnull=True)  & Q(status=1)) | \
                 Q(end_date__gt=lastfy[1])  # No end date or less than one year old

    reported = Report.objects.filter(reported=int(fy)).values("project_id")

    # Only fetch projects that are...
    projects = Project.objects.filter(is_live=True)     # ...live
    projects = projects.filter(pk__in=reported)         # ...reported in FY
#   projects = projects.filter(happening)               # ...active today
    projects = projects.filter(stage=3)                 # ...fully reviewed
    projects = projects.filter(is_worthy=True)          # ...book worthy
    projects = projects.filter(product_only=False)      # ...more than products
    projects = projects.order_by("sort_order")          # ...sorted by title

    involved  = 0                           # Students involved in projects
    involving = 0                           # Projects involving   students
    included = []
    for project in projects:
        students = project.investigator_set.all()
        students = students.filter(staff__profession__name="Student")
        students = students.filter(staff__agency__name="Gallaudet University")
        if students.count():
            involving += 1
        for student in students:
            if student.staff not in included:
                involved += 1
                included.append(student.staff)

    small_grants = 0
    funding  = Funding.objects.filter(project__in=reported)
    funding  = funding.filter(funder__name="GRI Small Research Grant")
    for funded in funding:
        project  = funded.project
        students = project.investigator_set.all()
        students = students.filter(staff__profession__name="Student")
        students = students.filter(staff__agency__name="Gallaudet University")
        if students.count():
            small_grants += 1

    grid, tips = build_grid(projects)                    # build prioirty grid

    priorities = Priority.objects.all().order_by("id")
    for priority in priorities:
        projects = priority.project_set.all()           # Associated products...
        projects = projects.filter(is_live=True)        # ...live
        projects = projects.filter(pk__in=reported)     # ...reported in FY
        projects = projects.filter(stage=3)             # ...fully reviewed
        projects = projects.filter(is_worthy=True)      # ...book worthy
        projects = projects.filter(product_only=False)  # ...more than products
        priority.sum = projects.count()                 # add a fake field

    dissertations = Product.objects.filter(is_live=True)
    dissertations = dissertations.filter(classification=5)    # Dissertations
    dissertations = dissertations.filter(reported=int(fy))
    dissertations = dissertations.filter(stage=3)             # Fully reviewed
    dissertations = dissertations.filter(is_worthy=True)      # Book worthy
    dissertations = dissertations.order_by("sort_order")

    centers = []
    departments = ResearchCenter.objects.all().order_by("name")
    for department in departments:
        abstracts = []
        staff = InternalStaff.objects.all()
        staff = staff.filter(department=department)
        investigators = Investigator.objects.all()
        investigators = investigators.filter(staff__in=staff)
        investigators = investigators.filter(is_principal=True)
        investigating = investigators.values("project_id")
        projects1 = Project.objects.filter(id__in=investigating)
        projects2 = Project.objects.filter(department=department)
        projects = projects1 | projects2
        projects = projects.filter(is_live=True)
        projects = projects.filter(pk__in=reported)
        projects = projects.filter(stage=3)
        projects = projects.filter(is_worthy=True)
        projects = projects.filter(product_only=False)
        projects = projects.order_by("sort_order")

        for project in projects:
            if project.department.id == department.id:
                foci          = project.focus_set.all()
                investigators = project.investigator_set.all()
                investigators = investigators.order_by("staff__person__last_name","staff__person__first_name")
                leaders       = project.investigator_set.filter(is_leader=True)
                leaders       = leaders.order_by("staff__person__last_name","staff__person__first_name")
                principals    = project.investigator_set.filter(is_leader=False).filter(is_principal=True)
                principals    = principals.order_by("staff__person__last_name","staff__person__first_name")
                others        = project.investigator_set.filter(is_principal=False)
                others        = others.order_by("staff__person__last_name","staff__person__first_name")
                funders       = project.funding_set.all()
                products      = project.product_set.filter(reported=int(fy))
                products      = products.filter(is_live=True)
                products      = products.filter(stage=3)
                products      = products.filter(is_worthy=True)
                products      = products.exclude(classification=5) # No dissertations
                products      = products.order_by("sort_order")
                abstract      = {"project":       project,
                                 "phantom":       False,
                                 "foci":          foci,
                                 "investigators": investigators,
                                 "leaders":       leaders,
                                 "principals":    principals,
                                 "others":        others,
                                 "funders":       funders,
                                 "products":      products}
            else:
                abstract      = {"project":       project,
                                 "phantom":       True}
            abstracts.append(abstract)

        outputs  = []
        lonelies = Project.objects.filter(is_live=True)
        lonelies = lonelies.filter(pk__in=reported)
        lonelies = lonelies.filter(stage=3)
        lonelies = lonelies.filter(is_worthy=True)
        lonelies = lonelies.filter(product_only=True)
        lonelies = lonelies.filter(department=department)
        lonelies = lonelies.order_by("sort_order")

        for lonely in lonelies:
            products = lonely.product_set.filter(reported=int(fy))
            products = products.filter(is_live=True)
            products = products.filter(stage=3)
            products = products.filter(is_worthy=True)
            products = products.exclude(classification=5) # No dissertations
            products = products.order_by("sort_order")
            if products.count() > 0:
                output   = {"lonely":   lonely,
                            "products": products}
                outputs.append(output)

        center = {"department": department,
                  "abstracts":  abstracts,
                  "outputs":    outputs}
        centers.append(center)

    units = []
    departments = NonCenter.objects.all().order_by("name")
    for department in departments:
        abstracts = []
        staff = InternalStaff.objects.all()
        staff = staff.filter(department=department)
        investigators = Investigator.objects.all()
        investigators = investigators.filter(staff__in=staff)
        investigators = investigators.filter(is_principal=True)
        investigating = investigators.values("project_id")
        projects1 = Project.objects.filter(id__in=investigating)
        projects2 = Project.objects.filter(department=department)
        projects = projects1 | projects2
        projects = projects.filter(is_live=True)
        projects = projects.filter(pk__in=reported)
        projects = projects.filter(stage=3)
        projects = projects.filter(is_worthy=True)
        projects = projects.filter(product_only=False)
        projects = projects.order_by("sort_order")

        for project in projects:
            if project.department.id == department.id:
                foci          = project.focus_set.all()
                investigators = project.investigator_set.all()
                investigators = investigators.order_by("staff__person__last_name","staff__person__first_name")
                leaders       = project.investigator_set.filter(is_leader=True)
                leaders       = leaders.order_by("staff__person__last_name","staff__person__first_name")
                principals    = project.investigator_set.filter(is_leader=False).filter(is_principal=True)
                principals    = principals.order_by("staff__person__last_name","staff__person__first_name")
                others        = project.investigator_set.filter(is_principal=False)
                others        = others.order_by("staff__person__last_name","staff__person__first_name")
                funders       = project.funding_set.all()
                products      = project.product_set.filter(reported=int(fy))
                products      = products.filter(is_live=True)
                products      = products.filter(stage=3)
                products      = products.filter(is_worthy=True)
                products      = products.exclude(classification=5) # No dissertations
                products      = products.order_by("sort_order")
                abstract      = {"project":       project,
                                 "phantom":       False,
                                 "foci":          foci,
                                 "investigators": investigators,
                                 "leaders":       leaders,
                                 "principals":    principals,
                                 "others":        others,
                                 "funders":       funders,
                                 "products":      products}
            else:
                abstract      = {"project":       project,
                                 "phantom":       True}
            abstracts.append(abstract)

        outputs  = []
        lonelies = Project.objects.filter(is_live=True)
        lonelies = lonelies.filter(pk__in=reported)
        lonelies = lonelies.filter(stage=3)
        lonelies = lonelies.filter(is_worthy=True)
        lonelies = lonelies.filter(product_only=True)
        lonelies = lonelies.filter(department=department)
        lonelies = lonelies.order_by("sort_order")

        for lonely in lonelies:
            products = lonely.product_set.filter(reported=int(fy))
            products = products.filter(is_live=True)
            products = products.filter(stage=3)
            products = products.filter(is_worthy=True)
            products = products.exclude(classification=5) # No dissertations
            products = products.order_by("sort_order")
            if products.count() > 0:
                output   = {"lonely":   lonely,
                            "products": products}
                outputs.append(output)

        unit = {"department": department,
                "abstracts":  abstracts,
                "outputs":    outputs}
        units.append(unit)

    return render_to_response("ragu/book.html",
                              {"fy":            fy,
                               "priorities":    priorities,
                               "grid":          grid,
                               "tips":          tips,
                               "jobs":          jobs,
                               "involved":      involved,
                               "involving":     involving,
                               "small_grants":  small_grants,
                               "dissertations": dissertations,
                               "centers":       centers,
                               "units":         units})

##############################################################################

def priorities(request):
    """Display a detailed list of priorities"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    priorities = Priority.objects.all().order_by("id")
    return render_to_response("ragu/priorities.html", {"priorities":priorities})

##############################################################################

def by_priority(request, priority_id):
    """Display a specific priority details & links to projects meeting it"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    priority = get_object_or_404(Priority, pk=int(priority_id))
    projects = priority.project_set.all()
    projects = projects.filter(is_live=True)             # ...live
    projects = projects.filter(product_only=False)       # ...more than products
    projects = projects.filter(stage__in=[2,3])          # ...reviewed / approved
    projects = projects.filter(report__reported__in=publish) # ...in last fiscal year
    projects = projects.order_by("sort_order")           # ...sorted by title
    return render_to_response("ragu/priority.html", locals())

##############################################################################

def funding(request):
    """Display two lists of funders - internal and external"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    internal, external, unknown = set(), set(), set()
    projects = Project.objects.all()
    projects = projects.filter(is_live=True)            # ...live
    projects = projects.filter(product_only=False)      # ...more than products
    projects = projects.filter(stage__in=[2,3])         # ...reviewed / approved
    projects = projects.filter(report__reported__in=publish) # ...in last fiscal year
    projects = projects.order_by("sort_order")          # ...sorted by title
    funders  = Funder.objects.all().order_by("is_internal","name")
    for project in projects:
        funds = project.funding_set.all()
        if funds.exists():
            for fund in funds:
                if fund.funder.is_internal == True:
                    internal.add(project.pk)
                elif fund.funder.is_internal == False:
                    external.add(project.pk)
        else:
            unknown.add(project.pk)

    internals = projects.filter(pk__in=internal).order_by("sort_order")
    externals = projects.filter(pk__in=external).order_by("sort_order")
    unknowns  = projects.filter(pk__in=unknown).order_by("sort_order")
    return render_to_response("ragu/funding.html",{"internal":internals,
                                                   "external":externals,
                                                   "unknown":unknowns})

##############################################################################

def screen_funder(request):
    """List funders"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    internal = Funder.objects.filter(is_internal=True).order_by("name")
    external = Funder.objects.filter(is_internal=False).order_by("name")
    return render_to_response("ragu/screen_funder.html",locals())

##############################################################################

def by_funder(request, funder_id):
    """List details for a specific funding source"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    funder   = get_object_or_404(Funder,pk=int(funder_id))
    projects = funder.project_set.all()             # Projecs funded by funder
    projects = projects.filter(is_live=True)            # ...live
    projects = projects.filter(product_only=False)      # ...more than products
    projects = projects.filter(stage__in=[2,3])         # ...reviewed / approved
    projects = projects.filter(report__reported__in=publish) # ...in last fiscal year
    projects = projects.order_by("sort_order")          # ...sorted by title
    return render_to_response("ragu/funder.html",locals())

##############################################################################
# DEBUG: Here be dragons.  Below this point, who knows what dangers lurk?    #
##############################################################################

def update_project(request, project_id):
    """Update existing project"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if "authorized" not in request.session:    # Prevent unauthorized access
        request.session["stack"] = "/resources/ragu/update_project/%s/" % (project_id,)
        return HttpResponseRedirect("/resources/ragu/internal_use/")

    if "stack" in request.session:
        del request.session["stack"]     # Pop the return pointer stack

    editor_id = int(request.session["editor"])
    editor    = Editor.objects.get(pk=editor_id)

    # Gather resources for pull-down menus

    internals   = InternalStaff.objects.all()
    externals   = ExternalStaff.objects.all()
    departments = Department.objects.all()
    internald   = InternalDepartment.objects.all()
    programs    = Program.objects.all()
    funders     = Funder.objects.all()
    priorities  = Priority.objects.all().order_by("id")
    professions = [{"id":16, "name":"Staff"},
                   {"id":7,  "name":"Professor Emeritus"},
                   {"id":8,  "name":"Retired"},
                   {"id":9,  "name":"Student"},
                   {"id":13, "name":"Faculty"},
                   {"id":15, "name":"Consultant"}]
#   unemployed  = Investigator.objects.all().order_by("agency")

    # Grab details for a project

    project       = get_object_or_404(Project, pk=int(project_id))
    foci          = project.focus_set.all()
    investigators = project.investigator_set.all().order_by("-is_principal")
    investigators = investigators.order_by("staff__person__last_name","staff__person__first_name")
    leaders       = project.investigator_set.filter(is_leader=True)
    leaders       = leaders.order_by("staff__person__last_name","staff__person__first_name")
    principals    = project.investigator_set.filter(is_leader=False).filter(is_principal=True)
    principals    = principals.order_by("staff__person__last_name","staff__person__first_name")
    others        = project.investigator_set.filter(is_principal=False)
    others        = others.order_by("staff__person__last_name","staff__person__first_name")
    funds         = project.funding_set.all()
    products      = project.product_set.filter(is_live=True)
    products      = products.order_by("sort_order")

    if project.begin_date:
        begin_month = project.begin_date.month
        begin_year  = project.begin_date.year
    else:
        begin_month = 0
        begin_year  = 0

    if project.end_date:
        end_month = project.end_date.month
        end_year  = project.end_date.year
    else:
        end_month = 0
        end_year  = 0

    request.session["project"] = str(project.id)

# Now we have all of the info to display the existing record... I think.

    if request.method == "POST": # If the form has been submitted...
        backup = deepcopy(project)  # Backup the Project object
        version = backup.version
        backup.version = 0
        backup.pk = None            # Prevent duplicate primary key
        backup.sort_order = None    # Prevent duplicate sort order
        backup.is_live = False      # Archive it
        backup.save()               # Save to database w/ new primary key
        request.session["backup"] = str(backup.id)

        project_form = ProjectForm(request.POST, instance=project)
        focus_form   = FocusForm(request.POST, instance=project)
        if  project_form.is_valid() \
        and focus_form.is_valid():
            project = project_form.save(commit=False)
            project.editor_id = editor_id
            project.version  += 1
            if request.POST["beginyear"]:  by = int(request.POST["beginyear"])
            if request.POST["beginmonth"]: bm = int(request.POST["beginmonth"])
            if request.POST["endyear"]:    ey = int(request.POST["endyear"])
            if request.POST["endmonth"]:   em = int(request.POST["endmonth"])
            if (by > 0) and (bm > 0):      project.begin_date = date(by,bm,1)
            if (ey > 0) and (em > 0):      project.end_date = date(ey,em,1)
            project.sort_order = None
            project.save()

            unfocused = {}
            for unfocus in range(1,14): # "Nothing really matters... to me."
                unfocused[unfocus] = unfocus

            for project_priority in focus_form.cleaned_data["priorities"]:
                del unfocused[project_priority.id]  # Well, THAT matters.
                try:  # Is this priority a focus of the project?
                    focus = Focus.objects.get(project=project,
                                              priority=project_priority)
                except Focus.DoesNotExist:   # Nope.
                    focus = Focus(project=project,
                                  priority=project_priority,
                                  editor=editor)
                    focus.save()

            # Delete priorities that are no longer priorities.
            for project_priority in unfocused:
                try:  # Is this priority a focus of the project?
                    focus = Focus.objects.get(project=project,
                                              priority=project_priority)
                except Focus.DoesNotExist: pass
                else:                      focus.delete()

            # Delete investigators that are no longer investigators.
            if request.POST["drop_investigators"]:
                drops    = request.POST["drop_investigators"]
                droplist = map(int,drops[:-1].split(",")) # split into ints
                Investigator.objects.filter(id__in=droplist).delete()

            # Add new investigators (known. internal and external.)
            for lot in ("internal_","external_",):
                bodies = int(request.POST[lot+"bodies"])
                for body in range(1,bodies+1):
                    staff_id  = int(request.POST[lot+"person."+str(body)])
                    if staff_id:
                        personnel = Staff.objects.get(pk=staff_id)
                        lead      = lot+"lead."+str(body)
                        principal = lot+"principal."+str(body)
                        if lead in request.POST:      lead      = True
                        else:                         lead      = False
                        if principal in request.POST: principal = True
                        else:                         principal = False
                        if lead:                      principal = True
                        try:  # Is this person already on the project?
                            investigator = Investigator.objects.get(project=project,
                                                                    staff=personnel,
                                                                    is_principal=principal,
                                                                    is_leader=lead)
                        except Investigator.DoesNotExist:   # Nope.
                            investigator = Investigator(project=project,
                                                        staff=personnel,
                                                        is_principal=principal,
                                                        is_leader=lead,
                                                        editor=editor)
                            investigator.save()

            # Add new investigators (unknown. internal.)
            lot = "new_internal_"
            bodies = int(request.POST[lot+"bodies"])
            for body in range(1,bodies+1):
                person = request.POST[lot+"person."+str(body)]
                if "," in person:
                    lname, fname = [name.strip() for name in person.split(",")]
                    try:              # Is this person already in the system?
                        person = Person.objects.get(last_name=lname,
                                                    first_name=fname)
                    except Person.DoesNotExist:   # Nope.
                        person = Person(last_name=lname, first_name=fname,
                                        editor=editor)
                        person.save()
                    agency_id = int(request.POST[lot+"agency."+str(body)])
                    agency    = Agency.objects.get(pk=agency_id)
                    staff = Staff(person=person, agency=agency)

                    if lot+"department."+str(body) in request.POST:
                        dept_id = int(request.POST[lot+"department."+str(body)])
                        if dept_id:
                            department = Department.objects.get(pk=dept_id)
                            staff.department=department
                    if lot+"program."+str(body) in request.POST:
                        prog_id = int(request.POST[lot+"program."+str(body)])
                        if prog_id:
                            program = Program.objects.get(pk=prog_id)
                            staff.program=program
                    if lot+"profession."+str(body) in request.POST:
                        job_id = int(request.POST[lot+"profession."+str(body)])
                        if job_id:
                            profession = Profession.objects.get(pk=job_id)
                            staff.profession=profession
                    staff.editor=editor
                    staff.save()

                    staff_id  = staff.id
                    personnel = Staff.objects.get(pk=staff_id)
                    lead      = lot+"lead."+str(body)
                    principal = lot+"principal."+str(body)
                    if lead in request.POST:      lead      = True
                    else:                         lead      = False
                    if principal in request.POST: principal = True
                    else:                         principal = False
                    if lead:                      principal = True
                    try:  # Is this person already on the project?
                        investigator = Investigator.objects.get(project=project,
                                                                staff=personnel,
                                                                is_principal=principal,
                                                                is_leader=lead)
                    except Investigator.DoesNotExist:   # Nope.
                        investigator = Investigator(project=project,
                                                    staff=personnel,
                                                    is_principal=principal,
                                                    is_leader=lead,
                                                    editor=editor)
                        investigator.save()
#               else:
#                   Handle when "lastname, firstname" format not followed

            # Add new investigators (unknown. external.)
            lot = "new_external_"
            bodies = int(request.POST[lot+"bodies"])
            for body in range(1,bodies+1):
                person = request.POST[lot+"person."+str(body)]
                if "," in person:
                    lname, fname = [name.strip() for name in person.split(",")]
                    try:              # Is this person already in the system?
                        person = Person.objects.get(last_name=lname,
                                                    first_name=fname)
                    except Person.DoesNotExist:   # Nope.
                        person = Person(last_name=lname, first_name=fname,
                                        editor=editor)
                        person.save()
                    agency_name = request.POST[lot+"agency."+str(body)]
                    try:              # Is this agency already in the system?
                        agency = Agency.objects.get(name=agency_name)
                    except Agency.DoesNotExist:   # Nope.
                        agency = Agency(name=agency_name, editor=editor)
                        agency.save()
                    staff = Staff(person=person, agency=agency)

                    if lot+"department."+str(body) in request.POST:
                        dept = request.POST[lot+"department."+str(body)]
                        if dept:
                            try:              # Is this department already in the system?
                                department = Department.objects.get(name=dept)
                            except Department.DoesNotExist:   # Nope.
                                department = Department(name=dept, editor=editor)
                                department.save()
                            staff.department=department
                    staff.editor=editor
                    staff.save()

                    staff_id  = staff.id
                    personnel = Staff.objects.get(pk=staff_id)
                    lead      = lot+"lead."+str(body)
                    principal = lot+"principal."+str(body)
                    if lead in request.POST:      lead      = True
                    else:                         lead      = False
                    if principal in request.POST: principal = True
                    else:                         principal = False
                    if lead:                      principal = True
                    try:  # Is this person already on the project?
                        investigator = Investigator.objects.get(project=project,
                                                                staff=personnel,
                                                                is_principal=principal,
                                                                is_leader=lead)
                    except Investigator.DoesNotExist:   # Nope.
                        investigator = Investigator(project=project,
                                                    staff=personnel,
                                                    is_principal=principal,
                                                    is_leader=lead,
                                                    editor=editor)
                        investigator.save()
#               else:
#                   Handle when "lastname, firstname" format not followed

            lot = ""
            bodies = int(request.POST["funding_bodies"])
            for body in range(1,bodies+1):
                funder_id = int(request.POST[lot+"funder."+str(body)])
                if funder_id:
                    funder = Funder.objects.get(pk=funder_id)
                    try:  # Is this funding source already on the project?
                        funding = Funding.objects.get(project=project,
                                                      funder=funder)
                    except Funding.DoesNotExist:   # Nope.
                        funding = Funding(project=project,
                                          funder=funder,
                                          editor=editor)
                        funding.save()

            lot = "new_funding_"
            if lot+"bodies" in request.POST:
                bodies = int(request.POST[lot+"bodies"])
                for body in range(1,bodies+1):
                    source = request.POST[lot+"funder."+str(body)]
                    if source:
                        try: # Is funder already in the funder pool?
                            funder = Funder.objects.get(name=source)
                        except Funder.DoesNotExist:  # Nope.
                            funder = Funder(name=source, is_internal=False,
                                            editor=editor)
                            funder.save()
                        try:  # Is this funding source already on the project?
                            funding = Funding.objects.get(project=project,
                                                          funder=funder)
                        except Funding.DoesNotExist:   # Nope.
                            funding = Funding(project=project,
                                              funder=funder,
                                              editor=editor)
                            funding.save()

            backup.version = version
            backup.sort_order = None    # Prevent duplicate sort order
            backup.save()
            del backup

            return HttpResponseRedirect("/resources/ragu/add_product/")
#       else:
#           backup = int(request.session["backup"])
#           deep-delete...

    else:                                 # If form NOT submitted...
        project_form  = ProjectForm(instance=project)
        focus_form    = FocusForm(instance=project)

    return render_to_response("ragu/update_project.html",
                              {"project_form":  project_form,
                               "focus_form":    focus_form,
                               "project":       project,
                               "begin_month":   begin_month,
                               "begin_year":    begin_year,
                               "end_month":     end_month,
                               "end_year":      end_year,
                               "priorities":    priorities,
                               "investigators": investigators,
                               "principals":    principals,
                               "others":        others,

                               "foci":        foci,
                               "funders":     funders,
                               "funds":       funds,
                               "internals":   internals,
                               "externals":   externals,
                               "departments": departments,
                               "internald":   internald,
                               "programs":    programs,
                               "professions": professions,

                               "months":        months,
                               "years":         years,
                               "editor":        editor,
                               "request":       request},
                              context_instance=RequestContext(request))

##############################################################################

def add_project(request):
    """Add a project to the database"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if "authorized" not in request.session:    # Prevent unauthorized access
        request.session["stack"] = "/resources/ragu/add_project/"
        return HttpResponseRedirect("/resources/ragu/internal_use/")

    if "stack" in request.session:
        del request.session["stack"]     # Pop the return pointer stack

    editor_id = int(request.session["editor"])
    editor    = Editor.objects.get(pk=editor_id)

    # Gather resources for pull-down menus
    internals   = InternalStaff.objects.all()
    externals   = ExternalStaff.objects.all()
    departments = Department.objects.all()
    internald   = InternalDepartment.objects.all()
    programs    = Program.objects.all()
    funders     = Funder.objects.all()
    professions = [{"id":16, "name":"Staff"},
                   {"id":7,  "name":"Professor Emeritus"},
                   {"id":8,  "name":"Retired"},
                   {"id":9,  "name":"Student"},
                   {"id":13, "name":"Faculty"},
                   {"id":15, "name":"Consultant"}]

    if request.method == "POST": # If the form has been submitted...
        project_form = ProjectForm(request.POST)
        focus_form   = FocusForm(request.POST)
        if  project_form.is_valid() \
        and focus_form.is_valid():
            title = project_form.cleaned_data["title"]
            project = Project.objects.filter(is_live=True)

            try:
                project = project.get(title=title)
            except Project.DoesNotExist:
                request.session["duplicate"] = ""
                project = project_form.save(commit=False)
                project.editor_id = editor_id
                if request.POST["beginyear"]:  by = int(request.POST["beginyear"])
                if request.POST["beginmonth"]: bm = int(request.POST["beginmonth"])
                if request.POST["endyear"]:    ey = int(request.POST["endyear"])
                if request.POST["endmonth"]:   em = int(request.POST["endmonth"])
                if (by > 0) and (bm > 0):      project.begin_date = date(by,bm,1)
                if (ey > 0) and (em > 0):      project.end_date = date(ey,em,1)
                project.sort_order = None
                project.save()
                request.session["project"] = str(project.id)

                for project_priority in focus_form.cleaned_data["priorities"]:
                    focus = Focus(project=project,
                                  priority=project_priority,
                                  editor=editor)
                    focus.save()

                for lot in ("internal_","external_",):
                    bodies = int(request.POST[lot+"bodies"])
                    for body in range(1,bodies+1):
                        staff_id  = int(request.POST[lot+"person."+str(body)])
                        if staff_id:
                            personnel = Staff.objects.get(pk=staff_id)
                            lead      = lot+"lead."+str(body)
                            principal = lot+"principal."+str(body)
                            if lead in request.POST:      lead      = True
                            else:                         lead      = False
                            if principal in request.POST: principal = True
                            else:                         principal = False
                            if lead:                      principal = True
                            investigator = Investigator(project=project,
                                                        staff=personnel,
                                                        is_principal=principal,
                                                        is_leader=lead,
                                                        editor=editor)
                            investigator.save()

                lot = "new_internal_"
                bodies = int(request.POST[lot+"bodies"])
                for body in range(1,bodies+1):
                    person = request.POST[lot+"person."+str(body)]
                    if "," in person:
                        lname, fname = [name.strip() for name in person.split(",")]
                        try:              # Is this person already in the system?
                            person = Person.objects.get(last_name=lname,
                                                        first_name=fname)
                        except Person.DoesNotExist:   # Nope.
                            person = Person(last_name=lname, first_name=fname,
                                            editor=editor)
                            person.save()
                        agency_id = int(request.POST[lot+"agency."+str(body)])
                        agency    = Agency.objects.get(pk=agency_id)
                        staff = Staff(person=person, agency=agency)

                        if lot+"department."+str(body) in request.POST:
                            dept_id = int(request.POST[lot+"department."+str(body)])
                            if dept_id:
                                department = Department.objects.get(pk=dept_id)
                                staff.department=department
                        if lot+"program."+str(body) in request.POST:
                            prog_id = int(request.POST[lot+"program."+str(body)])
                            if prog_id:
                                program = Program.objects.get(pk=prog_id)
                                staff.program=program
                        if lot+"profession."+str(body) in request.POST:
                            job_id = int(request.POST[lot+"profession."+str(body)])
                            if job_id:
                                profession = Profession.objects.get(pk=job_id)
                                staff.profession=profession
                        staff.editor=editor
                        staff.save()

                        staff_id  = staff.id
                        personnel = Staff.objects.get(pk=staff_id)
                        lead      = lot+"lead."+str(body)
                        principal = lot+"principal."+str(body)
                        if lead in request.POST:      lead = True
                        else:                         lead = False
                        if principal in request.POST: principal = True
                        else:                         principal = False
                        if lead:                      principal = True
                        investigator = Investigator(project=project,
                                                    staff=personnel,
                                                    is_principal=principal,
                                                    is_leader=lead,
                                                    editor=editor)
                        investigator.save()
#                   else:
#                       Handle when "lastname, firstname" format not followed

                lot = "new_external_"
                bodies = int(request.POST[lot+"bodies"])
                for body in range(1,bodies+1):
                    person = request.POST[lot+"person."+str(body)]
                    if "," in person:
                        lname, fname = [name.strip() for name in person.split(",")]
                        try:              # Is this person already in the system?
                            person = Person.objects.get(last_name=lname,
                                                        first_name=fname)
                        except Person.DoesNotExist:   # Nope.
                            person = Person(last_name=lname, first_name=fname,
                                            editor=editor)
                            person.save()
                        agency_name = request.POST[lot+"agency."+str(body)]
                        try:              # Is this agency already in the system?
                            agency = Agency.objects.get(name=agency_name)
                        except Agency.DoesNotExist:   # Nope.
                            agency = Agency(name=agency_name, editor=editor)
                            agency.save()
                        staff = Staff(person=person, agency=agency)

                        if lot+"department."+str(body) in request.POST:
                            dept = request.POST[lot+"department."+str(body)]
                            if dept:
                                try:              # Is this department already in the system?
                                    department = Department.objects.get(name=dept)
                                except Department.DoesNotExist:   # Nope.
                                    department = Department(name=dept, editor=editor)
                                    department.save()
                                staff.department=department
                        staff.editor=editor
                        staff.save()

                        staff_id  = staff.id
                        personnel = Staff.objects.get(pk=staff_id)
                        lead      = lot+"lead."+str(body)
                        principal = lot+"principal."+str(body)
                        if lead in request.POST:      lead      = True
                        else:                         lead      = False
                        if principal in request.POST: principal = True
                        else:                         principal = False
                        if lead:                      principal = True
                        investigator = Investigator(project=project,
                                                    staff=personnel,
                                                    is_principal=principal,
                                                    is_leader=lead,
                                                    editor=editor)
                        investigator.save()
#                   else:
#                       Handle when "lastname, firstname" format not followed

                lot = ""
                bodies = int(request.POST["funding_bodies"])
                for body in range(1,bodies+1):
                    funder_id = int(request.POST[lot+"funder."+str(body)])
                    if funder_id:
                        funder = Funder.objects.get(pk=funder_id)
                        funding = Funding(project=project,
                                          funder=funder,
                                          editor=editor)
                        funding.save()

                lot = "new_funding_"
                if lot+"bodies" in request.POST:
                    bodies = int(request.POST[lot+"bodies"])
                    for body in range(1,bodies+1):
                        source = request.POST[lot+"funder."+str(body)]
                        if source:
                            funder = Funder(name=source, is_internal=False,
                                            editor=editor)
                            funder.save()
                            funding = Funding(project=project,
                                              funder=funder,
                                              editor=editor)
                            funding.save()

                return HttpResponseRedirect("/resources/ragu/add_product/")

            request.session["duplicate"] = project_form.cleaned_data["description"]
            duplicate = "/resources/ragu/abstract/%s" % (project.id,)
            return HttpResponseRedirect(duplicate)
    else:                                 # If form NOT submitted...
        department   = Department.objects.get(name="TBD") # ...instantiate ALMOST empty form
        project_form = ProjectForm(initial={"department":department.pk})
        focus_form   = FocusForm()

    return render_to_response("ragu/add_project.html",
                              {"project":     project_form,
                               "foci":        focus_form,
                               "funders":     funders,
                               "internals":   internals,
                               "externals":   externals,
                               "departments": departments,
                               "internald":   internald,
                               "programs":    programs,
                               "professions": professions,
                               "months":      months,
                               "years":       years,
                               "editor":      editor,
                               "request":     request},
                              context_instance=RequestContext(request))

#  internal = Funder.objects.filter(is_internal=True).order_by("name")
#  funding = Funding.objects.filter(funder__in=internal)
#  for fund in funding:                                # DEBUG
#      debug += "<li>%s<br />" % (fund.project.title,) # DEBUG
#      debug += "%s</li>"      % (fund.funder.name,)   # DEBUG

#  external = Funder.objects.filter(is_internal=False).order_by("name")
#  funding = Funding.objects.filter(funder__in=external)
#  for fund in funding:                                # DEBUG
#      debug += "<li>%s<br />" % (fund.project.title,) # DEBUG
#      debug += "%s</li>"      % (fund.funder.name,)   # DEBUG

#  unknown = Funder.objects.filter(is_internal=None).order_by("name")
#  funding = Funding.objects.filter(funder__in=unknown)
#  for fund in funding:                                # DEBUG
#      debug += "<li>%s<br />" % (fund.project.title,) # DEBUG
#      debug += "%s</li>"      % (fund.funder.name,)   # DEBUG

##############################################################################

def add_product(request):
    """Add a product to the database"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if "authorized" not in request.session:    # Prevent unauthorized access
        request.session["stack"] = "/resources/ragu/add_product/"
        return HttpResponseRedirect("/resources/ragu/internal_use/")

    if "stack" in request.session:
        del request.session["stack"]     # Pop the return pointer stack

    editor_id  = int(request.session["editor"])
    project_id = int(request.session["project"])
#   project_id = 873  # DEBUG - remove this and uncomment above
    project    = Project.objects.get(pk=project_id)
    products   = project.product_set.filter(is_live=True)  # Get previously entered products
    products   = products.order_by("sort_order")

    if request.method == "POST":     # If the form has been submitted...
        form = ProductForm(request.POST)
        if form.is_valid():          # ...instantiate w/ posted data & check for valid
            description = form.cleaned_data["description"]

            try:
                product = Product.objects.get(description=description)
            except Product.DoesNotExist:
                request.session["duplicate"] = ""
                new_product = form.save(commit=False)
                new_product.project_id = project_id
                new_product.editor_id  = editor_id
                new_product.sort_order = None
                new_product.save()
                return HttpResponseRedirect("")

            request.session["duplicate"] = description
    else:                            # If form NOT submitted...
        form = ProductForm()         # ...instantiate empty form

    return render_to_response("ragu/add_product.html",
                              {"project":  project,
                               "products": products,
                               "form":     form,
                               "request":  request},
                              context_instance=RequestContext(request))

##############################################################################

def update_product(request, product_id):
    """Update a product in the database"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if "authorized" not in request.session:    # Prevent unauthorized access
        request.session["stack"] = "/resources/ragu/update_product/%s/" % (product_id,)
        return HttpResponseRedirect("/resources/ragu/internal_use/")

    if "stack" in request.session:
        del request.session["stack"]     # Pop the return pointer stack

    product  = get_object_or_404(Product, pk=int(product_id))
    project  = product.project
    products = project.product_set.filter(is_live=True)
    products = products.order_by("sort_order")

    if request.method == "POST":     # If the form has been submitted...
        editor_id = int(request.session["editor"])
        editor    = Editor.objects.get(pk=editor_id)

        backup = deepcopy(product)  # Backup the Product object
        version = backup.version
        backup.version = 0
        backup.pk = None            # Prevent duplicate primary key
        backup.is_live = False      # Archive it
        backup.sort_order = None
        backup.save()
        request.session["backup"] = str(backup.id)

        form = ProductForm(request.POST, instance=product)
        if form.is_valid():          # ...instantiate w/ posted data & check for valid
            request.session["duplicate"] = ""
            product = form.save(commit=False)
            product.editor_id  = editor_id
            product.version   += 1
            product.sort_order = None
            product.save()

            backup.version = version
            backup.sort_order = None
            backup.save()
            del backup

            return HttpResponseRedirect("/resources/ragu/abstract/%s" % (product.project_id,))

    else:                                    # If form NOT submitted...
        form = ProductForm(instance=product) # ...instantiate pre-filled form

    return render_to_response("ragu/add_product.html",
                              {"project":  project,
                               "products": products,
                               "form":     form,
                               "request":  request},
                              context_instance=RequestContext(request))

##############################################################################

def queue(request, item, item_id, stage):
    """Queue for next stage"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if "editor" in request.session:
        editor_id = int(request.session["editor"])
        editor    = Editor.objects.get(pk=editor_id)

    if item == "je":   # ProJEct
        project       = get_object_or_404(Project, pk=int(item_id))
        project.stage = (int(stage) % 10)  # Get more milage...
        if (int(stage) // 10) == 1:
            project.is_worthy = True       # ...out of the stage field
            try:             # Has project already been marked worth this year?
                report = Report.objects.get(project=project,
                                            reported=publish[1])
            except Report.DoesNotExist:   # Nope.
                report = Report(project=project,
                                reported=publish[1],
                                editor=editor)
                report.save()
        if (int(stage) // 10) == 2:
            project.is_worthy = False      # ...for the book
        project.editor = editor
        project.sort_order = None
        project.save()
        return HttpResponseRedirect("/resources/ragu/abstract/%s" % (item_id,))
    if item == "du":   # ProDUct
        product       = get_object_or_404(Product, pk=int(item_id))
        penders       = product.stage
        product.stage = (int(stage) % 10)                      # Get more milage...
        if (int(stage) // 10) == 1: product.is_worthy = True   # ...out of stage
        if (int(stage) // 10) == 2: product.is_worthy = False  # ...for book
        product.editor = editor
        product.sort_order = None
        product.save()
        return HttpResponseRedirect("/resources/ragu/pending/products/%s/" % (penders,))

##############################################################################

def pending(request, items, stage):
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if "editor" in request.session:
        editor_id = int(request.session["editor"])
        editor    = Editor.objects.get(pk=editor_id)
    else:
        editor    = None

    folder = []
    if   items == "je":   # ProJEcts
        projects = Project.objects.filter(is_live=True)
        projects = projects.filter(stage=int(stage)).order_by("sort_order")
        for project in projects:
            leaders  = project.investigator_set.filter(is_leader=True)
            leaders  = leaders.order_by("staff__person__last_name","staff__person__first_name")
            abstract = {"project": project,
                        "leaders": leaders}
            folder.append(abstract)
    elif items == "du":   # ProDUcts
        products  = Product.objects.filter(is_live=True)
#       if today.month < 10:
#           products = products.filter(reported=fiscal)
#       else:
#           products = products.filter(reported=publish[1])
        products = products.filter(stage=int(stage)).order_by("sort_order")
        for product in products:
            abstract = {"product": product}
            folder.append(abstract)

    return render_to_response("ragu/pending.html", {"items":   items,
                                                    "stage":   stage,
                                                    "folder":  folder,
                                                    "editor":  editor,
                                                    "request": request})

##############################################################################

def screen_investigator(request):
    """List all investigators?"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    listables = ("Retired","Student","Consultant")
    jobs = Profession.objects.filter(name__in=listables)

    investigators = InternalStaff.objects.all()

    d_list = []     # Department id list
    ds = investigators.values("department").distinct()
    for d in ds: d_list.append(d["department"])
#       investigators = investigators.filter(department=d["department"])
#       ps = investigators.values("person").distinct()
#       for p in ps:
#           p_list[d["department"]]
    departments = Department.objects.filter(id__in=d_list).order_by("name")

    p_list = []     # Person id list
    ps = investigators.values("person").distinct()
    for p in ps: p_list.append(p["person"])
    people = Person.objects.filter(id__in=p_list).order_by("last_name","first_name")
    return render_to_response("ragu/investigators.html", locals())

##############################################################################

def by_investigator(request, person_id):
    """List details for a specific investigator?"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    person   = get_object_or_404(Person, pk=int(person_id))
    staff    = Staff.objects.filter(person=int(person_id))
    roles    = Investigator.objects.filter(staff__in=staff)

    projects = Project.objects.all()
    projects = projects.filter(is_live=True)            # ...live
    projects = projects.filter(product_only=False)      # ...more than products
    projects = projects.filter(investigator__in=roles)  # ...investigated by
    draftpjs = projects.filter(stage__in=[0,1])         # ...unsubmitted / draft
    projects = projects.filter(stage__in=[2,3])         # ...reviewed / approved
#   projects = projects.filter(report__reported__in=publish) # ...in last fiscal year
    projects = projects.order_by("sort_order")          # ...sorted by title

    lonelies = Project.objects.all()
    lonelies = lonelies.filter(is_live=True)
    lonelies = lonelies.filter(product_only=True)
    lonelies = lonelies.filter(investigator__in=roles)
    lonelies = lonelies.filter(stage__in=[2,3])         # ...reviewed / approved
#   lonelies = lonelies.filter(report__reported__in=publish)
    lonelies = lonelies.order_by("sort_order")

    outputs = []
    for lonely in lonelies:
        products = lonely.product_set.all()
        products = products.filter(is_live=True)
        products = products.filter(description__contains=person.last_name)
        draftpds = products.filter(stage__in=[0,1])     # ...unsubmitted / draft
        products = products.filter(stage__in=[2,3])     # ...reviewed / approved
        products = products.order_by("reported","sort_order")
        output   = {"lonely":   lonely,
                    "products": products}
        outputs.append(output)

    return render_to_response("ragu/investigator.html", locals())

##############################################################################

def screen_dates(request):
    return render_to_response("ragu/screen_dates.html",
                               {"months": months,
                               "years":  years},
                               context_instance=RequestContext(request))

##############################################################################

def chronology(request):
    """List projects with end date after month/year"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    projects = Project.objects.all()
    projects = projects.filter(is_live=True)            # ...live
    projects = projects.filter(product_only=False)      # ...more than products
    projects = projects.filter(stage__in=[2,3])         # ...reviewed / approved
#   projects = projects.filter(report__reported__in=publish) # ...in last fiscal year
    projects = projects.order_by("sort_order")          # ...sorted by title

    timespan = request.POST["timespan"]

    year1    = int(request.POST["year1"])
    month1   = int(request.POST["month1"])
    day1     = 1

    year2    = int(request.POST["year2"])
    month2   = int(request.POST["month2"])
    day2     = 1

    if not year1 or not month1:           # If either evaluate to 0, do over.
        return render_to_response("ragu/screen_dates.html",
                                  {"months": months,
                                   "years":  years},
                                  context_instance=RequestContext(request))

    if   timespan == "beginning before":
        date1    = date(year1,month1,day1)
        projects = projects.filter(begin_date__lt=date1) # before start of month

    elif timespan == "beginning after":
        year1    = year1 + (month1 // 12)
        month1   = (month1 % 12) + 1
        date1    = date(year1,month1,day1) - timedelta(days=1)
        projects = projects.filter(begin_date__gt=date1) # after end of month

    elif timespan == "ending before":
        date1    = date(year1,month1,day1)
        projects = projects.filter(end_date__lt=date1) # before start of month

    elif timespan == "ending after":
        year1    = year1 + (month1 // 12)
        month1   = (month1 % 12) + 1
        date1    = date(year1,month1,day1) - timedelta(days=1)
        projects = projects.filter(end_date__gt=date1) # after end of month

    elif timespan == "active between":
        if not year2 or not month2:   # If either evaluate to 0, do over.
            return render_to_response("ragu/screen_dates.html",
                                      {"months": months,
                                       "years":  years},
                                      context_instance=RequestContext(request))

        year1    = year1 + (month1 // 12)
        month1   = (month1 % 12) + 1
        date1    = date(year1,month1,day1) - timedelta(days=1)
        date2    = date(year2,month2,day2)
        projects = projects.filter(begin_date__gt=date1) # after end of month1 AND
        projects = projects.filter(end_date__lt=date2)   # before start of month2

    projects = projects.order_by("sort_order")           # ...sorted by title
    return render_to_response("ragu/chronology.html", locals())

##############################################################################

def students(request, fy):
    """Student research (currently dissertations only)"""

    dissertations = Product.objects.filter(reported=int(fy))
    dissertations = dissertations.filter(is_live=True)
    dissertations = dissertations.filter(stage__in=[2,3])
    dissertations = dissertations.filter(classification=5)
    dissertations = dissertations.order_by("sort_order")

    return render_to_response("ragu/students.html", locals())

def citations(request, year):
    """Publications and Presentations - DEBUG ONLY"""
    categories = []
    classifications = Classification.objects.all().order_by("name")
    for classification in classifications:
      products = classification.product_set.filter(reported=int(year))
      products = products.filter(is_live=True)
      products = products.order_by("sort_order")
      if products.exists():
          categories.append({"name":classification.name,"products":products})
    return render_to_response("ragu/citations.html",locals())

##############################################################################

def leadership(request):
    """Manually make leaders of men and women"""
    projects = Project.objects.filter(is_live=True)

    investigators = Investigator.objects.all()
    investigators = investigators.order_by("staff__person__last_name","staff__person__first_name")
    principals    = Investigator.objects.filter(is_principal=True)
    principals    = principals.order_by("staff__person__last_name","staff__person__first_name")
    leaders       = Investigator.objects.filter(is_leader=True)
    leaders       = leaders.order_by("staff__person__last_name","staff__person__first_name")
    wtfs          = leaders.filter(is_principal=False)
    wtfs           = wtfs.order_by("staff__person__last_name","staff__person__first_name")

    investigators = investigators.values_list('project_id', flat=True)
    principals    = principals.values_list('project_id', flat=True)
    leaders       = leaders.values_list('project_id', flat=True)
    wtfs          = wtfs.values_list('project_id', flat=True)

    rudderless    = projects.exclude(id__in=investigators).order_by("sort_order")
    unprincipaled = projects.exclude(id__in=principals).order_by("sort_order")
    leaderless    = projects.exclude(id__in=leaders).order_by("sort_order")
    wtfers        = projects.filter(id__in=wtfs).order_by("sort_order")
    return render_to_response("ragu/leadership.html",
                              {"rudderless":    rudderless,
                               "unprincipaled": unprincipaled,
                               "leaderless":    leaderless,
                               "wtfers":        wtfers,
                               "request":       request})

##############################################################################

def units_intros(request):
    """Display unit names and descriptions"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    reported = Report.objects.filter(reported=thisyear).values("project_id")

    unfixed = []
    departments = NonCenter.objects.filter(description__isnull=True)
    departments = departments.order_by("name")
    for department in departments:
        projects   = Project.objects.filter(is_live=True)
        projects   = projects.filter(pk__in=reported)
        projects   = projects.filter(stage=3)
        projects   = projects.filter(is_worthy=True)
        projects   = projects.filter(product_only=False)
        projects   = projects.filter(department=department)

        activities = Project.objects.filter(is_live=True)
        activities = activities.filter(pk__in=reported)
        activities = activities.filter(stage=3)
        activities = activities.filter(is_worthy=True)
        activities = activities.filter(product_only=True)
        activities = activities.filter(department=department)

        unfixed.append({"department": department,
                        "projects":   projects,
                        "activities": activities})

    fixed = []
    departments = NonCenter.objects.filter(description__isnull=False)
    departments = departments.order_by("name")
    for department in departments:
        projects   = Project.objects.filter(is_live=True)
        projects   = projects.filter(pk__in=reported)
        projects   = projects.filter(stage=3)
        projects   = projects.filter(is_worthy=True)
        projects   = projects.filter(product_only=False)
        projects   = projects.filter(department=department)

        activities = Project.objects.filter(is_live=True)
        activities = activities.filter(pk__in=reported)
        activities = activities.filter(stage=3)
        activities = activities.filter(is_worthy=True)
        activities = activities.filter(product_only=True)
        activities = activities.filter(department=department)

        fixed.append({"department": department,
                      "projects":   projects,
                      "activities": activities})

    return render_to_response("ragu/units_intros.html",
                              {"unfixed":unfixed, "fixed":fixed})

##############################################################################

def splitsville(request):
    """List all projects with two masters (PI's split across departments)"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    internals = set()
    staff = InternalStaff.objects.all()
    for person in staff:
        internals.add(person.pk)

    reported = Report.objects.filter(reported=thisyear).values("project_id")

    splits = []

    # Only fetch projects that are...
    projects = Project.objects.filter(is_live=True) # ...live
    projects = projects.filter(pk__in=reported)     # ...reported in year
    projects = projects.filter(stage=3)             # ...fully reviewed
    projects = projects.filter(is_worthy=True)      # ...book worthy
    projects = projects.filter(product_only=False)  # ...more than products
    projects = projects.order_by("sort_order")      # ...sorted by title

    for project in projects:
        principals = project.investigator_set.filter(is_principal=True)
        principals = principals.filter(staff__in=internals)
        principals = principals.values("staff__department").distinct()
        if principals.count() > 1:
            units = []
            for principal in principals:
                unit = Department.objects.get(pk=principal["staff__department"])
                units.append(unit)
            splits.append({"project":project, "units":units}) # Associate them

    return render_to_response("ragu/splitsville.html", {"splits":splits})

##############################################################################

def playbill(request):
    """List all staff by project"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    listables = ("Retired","Student","Consultant")
    jobs = Profession.objects.filter(name__in=listables)

    happening = (Q(end_date__isnull=True)  & Q(status=1)) | \
                 Q(end_date__gt=date(2011,9,30))  # No end date or less than one year old

    projects  = Project.objects.filter(is_live=True)
    projects  = projects.filter(happening)               # ...active today
    projects  = projects.order_by("sort_order")

    performances = []
    for project in projects:
        investigators = project.investigator_set.all()
        performances.append({"play":project, "cast":investigators})

    return render_to_response("ragu/playbill.html",
                              {"performances": performances,
                               "jobs":         jobs})

##############################################################################

def rerank(request):
    """Sets the sort_order field for the Project and Product models"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    if "authorized" not in request.session: # Prevent unauthorized access
        request.session["stack"] = "/resources/ragu/rerank/"
        return HttpResponseRedirect("/resources/ragu/internal_use/")

    if "stack" in request.session:
        del request.session["stack"]        # Pop the return pointer stack

    resort()

    return HttpResponseRedirect("/resources/ragu/")

##############################################################################

def reminders(request):
    """Generate a listing to inform / remind departments"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    units = Department.objects.all()
    units = units.filter(is_internal=True)
    units = units.filter(is_current=True)
    units = units.exclude(name="TBD")
    units = units.order_by("name")
#-----------------------------------------------------------------------------
    listables = ("Retired","Student","Consultant")
    jobs = Profession.objects.filter(name__in=listables)

# How do I cope with "in progress data entry" spanning several months,
# particularly when it spans fiscal years? I want something that shows
# "current" projects...
#
    happening = (Q(end_date__isnull=True)  & Q(status=1)) | \
                 Q(end_date__gt=lastfy[1])  # No end date or less than one year old

    departments = []
    total_abstracts = 0
    total_outputs   = 0
    for unit in units:
        abstracts = 0
        outputs   = 0
        remindees = []
        projects  = Project.objects.all()             # Only projects that are...
        projects  = projects.filter(department=unit)  # ...in department
        projects  = projects.filter(is_live=True)     # ...live
        projects  = projects.filter(happening)        # ...active today
        for project in projects:
            leaders = project.investigator_set.filter(is_leader=True)
            for leader in leaders:
                staffer = leader.staff
                if staffer not in remindees:
                    remindees.append(staffer)
            products = project.product_set.all()
            products = products.filter(is_live=True)
            products = products.filter(reported=2013)      # When?
            products = products.exclude(classification=5) # No dissertations
            outputs += products.count()
            total_outputs += products.count()
        projects   = projects.filter(product_only=False) # ...more than products
        abstracts += projects.count()
        total_abstracts += projects.count()
        department = {"unit":           unit,
                      "abstracts":      abstracts,
                      "outputs":        outputs,
                      "remindees":      remindees}
        departments.append(department)
    return render_to_response("ragu/reminders.html",
                              {"departments": departments,
                               "totalabstracts": total_abstracts,
                               "totaloutputs":   total_outputs})

##############################################################################

def firstperson(request):
    """Find project descriptions written in the first person"""

    happening = (Q(end_date__isnull=True)  & Q(status=1)) | \
                 Q(end_date__gt=lastfy[1])  # No end date or less than one year old

    projects = Project.objects.all()
    projects = projects.filter(is_live=True)      # ...live
    projects  = projects.filter(happening)        # ...active today
    projects = projects.filter(description__iregex=r"[\> ](I|we|my|our|me)[\<\. ]")
    projects = projects.order_by("title")
    return render_to_response("ragu/firstperson.html",
                              {"projects": projects})

##############################################################################

def unresolved(request):
    """Display a detailed abstract of all current projects"""
    request.session.set_expiry(0)   # Log-out when browser closes
    die = timedelta(hours=1)        # The above didn't work...
    request.session.set_expiry(die) # ...So die in an hour from hitting this

    listables = ("Retired","Student","Consultant")
    jobs = Profession.objects.filter(name__in=listables)

    abstracts  = []
    projects   = Project.objects.all()            # Only projects that are...
    projects   = projects.filter(is_live=True)    # ...live
    projects   = projects.filter(stage=2)         # ...reviewed / approved
    projects   = projects.filter(active(2013))    # ...active in FY 2013
    projects   = projects.order_by("sort_order")  # ...sorted by title
    for project in projects:
        foci          = project.focus_set.all()
        investigators = project.investigator_set.all()
        investigators = investigators.order_by("staff__person__last_name","staff__person__first_name")
        leaders       = project.investigator_set.filter(is_leader=True)
        leaders       = leaders.order_by("staff__person__last_name","staff__person__first_name")
        principals    = project.investigator_set.filter(is_leader=False).filter(is_principal=True)
        principals    = principals.order_by("staff__person__last_name","staff__person__first_name")
        others        = project.investigator_set.filter(is_principal=False)
        others        = others.order_by("staff__person__last_name","staff__person__first_name")
        funders       = project.funding_set.all()
        products      = project.product_set.filter(is_live=True)
        products      = products.filter(stage__in=[2,3])
        products      = products.order_by("sort_order")
        abstract      = {"project":       project,
                         "foci":          foci,
                         "investigators": investigators,
                         "leaders":       leaders,
                         "principals":    principals,
                         "others":        others,
                         "funders":       funders,
                         "products":      products}
        abstracts.append(abstract)

    outputs  = []

    return render_to_response("ragu/unresolved.html",
                              {"abstracts":  abstracts,
                               "outputs":    outputs,
                               "jobs":       jobs})

##############################################################################
