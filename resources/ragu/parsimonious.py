#!/usr/bin/env python
# Written by Kevin Cole <kjcole@gallaudet.edu> 2012.01.03
#
# This is an attempt to write an HTML interpreter that strips
# tags out and converts entities to ... Unicode?  This will give
# us something consistently sortable. I hope.
#

import re
from   operator      import itemgetter  # For sort. Gets item from an iterable
from   BeautifulSoup import BeautifulSoup, BeautifulStoneSoup

def strain(html):
    """Expects an HTML string. Returns a plaintext UTF-8 encoded string."""
    soup = BeautifulSoup(html,
                         convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
    plain = "".join(soup.findAll(text=re.compile(".*")))
    return plain.encode("utf-8")

def stir(qset,key):
    """Expects QuerySet & key. Returns sorted [(ID,stirred text),...]"""
    whatever = []
    for row in qset:
        whatever.append((row.id,row.key))
        print row.id, row.key
    return whatever
#______________________________________________________________________________
#
##!/usr/bin/env python
## Written by Kevin Cole <kjcole@gallaudet.edu> 2012.01.03
##
## A general outline of what we're trying to do here:
## 1. Get all of the HTML product descriptions into a list.
## 2. Make a copy and strip away the HTML, converting entities to Unicode.
## 3. Put tuples in a new list [(HTML,plaintext), (HTML,plaintext)...].
## 4. Sort the list keyed on the second element of each tuple.
## 5. For each tuple in the sorted new list, update the table, setting
##    sort_by = (new list index + 1) * 10.  The * 10 might not be
##    necessary, if we resort nightly, as long as the default value for
##    new inserts is ridiculous... maybe a negative number...
##
#
# e.g ordered = sorted(Foo.objects.all(), key=lambda n: (n[0], int(n[1:])))
#
#descriptions = []
#
#rsca    = pgdb.connect("localhost:rsca:kjcole:In4$in4#.")
#product = rsca.cursor()
#
#query  = """select id, description         """
#query += """  from "product"                  """
#query += """  where description is not null;"""
#
#product.execute(query)
#print "%s records found." % (product.rowcount,)
#for row in product.fetchall():
#     id        = row[0]                   # Unique PostgreSQL internal ID
#     html      = row[1]                   # Product description, HTML encoded
#     plaintext = strain(html.lower())     # Lower case, Unicode, no HTML
#     descriptions.append([id,plaintext])  # Store pairs to be sorted
#
#descriptions.sort(key=itemgetter(1))      # Use 2nd of pair as sort key
#
#update  = """update product     """
#update += """  set sort_by = %s """
#update += """  where id = %s;  """
#
#sort_by = 0
#for row in descriptions:
#    sort_by += 1
#    id, description = row
##   product.execute(update % (sort_by,id))
#    print sort_by, id, description[:70]     # DEBUG: Verify sort order
##rsca.commit()
#
## for row in results:
##    descriptions.append(row[0])
#
## """update "product"
##      set sort_by = %s
##      where description = '%s';""" % ((key * 10), product_des)
#______________________________________________________________________________

if __name__ == "__main__":
    html  = "<p>I guess this handles <em>tags</em><br /> <br />"
    html += "on the fly. What about &aacute; and A&#773;?</p>"
    print strain(html)

#   print soup
#   print soup.renderContents()
#   print soup.prettify()
#   print "source   = %s" % (soup.contents,)
#         [<p>I guess this handles <em>tags</em> on the fly &aacute;?</p>]
#   print "contents = %s" % (soup.contents[0].contents,)
#         [u'I guess this handles ', <em>tags</em>, u' on the fly &aacute;?']
#   print soup.findAll(text=re.compile(".*"))
#         [u'I guess this handles ', u'tags', u' on the fly &aacute;?']
