from django.conf.urls.defaults import *
from resources.screening.views import *
from django.views.generic.base import TemplateView

urlpatterns = patterns("",
    (r"^$",         TemplateView.as_view(template_name="marquee/index.html")),
    (r"^sesame/$",  "django.contrib.auth.views.login", 
                                    {"template_name": "marquee/sesame.html"}),
)
