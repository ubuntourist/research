--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.product DROP CONSTRAINT product_project_fkey;
ALTER TABLE ONLY public.product DROP CONSTRAINT product_classification_fkey;
ALTER TABLE ONLY public.investigator DROP CONSTRAINT investigator_project_fkey;
ALTER TABLE ONLY public.investigator DROP CONSTRAINT investigator_program_fkey;
ALTER TABLE ONLY public.investigator DROP CONSTRAINT investigator_profession_fkey;
ALTER TABLE ONLY public.investigator DROP CONSTRAINT investigator_person_fkey;
ALTER TABLE ONLY public.investigator DROP CONSTRAINT investigator_department_fkey;
ALTER TABLE ONLY public.investigator DROP CONSTRAINT investigator_agency_fkey;
ALTER TABLE ONLY public.funding DROP CONSTRAINT funding_project_fkey;
ALTER TABLE ONLY public.funding DROP CONSTRAINT funding_funder_fkey;
ALTER TABLE ONLY public.focus DROP CONSTRAINT focus_project_fkey;
ALTER TABLE ONLY public.focus DROP CONSTRAINT focus_priority_fkey;
ALTER TABLE ONLY public.project DROP CONSTRAINT project_pkey;
ALTER TABLE ONLY public.program DROP CONSTRAINT program_pkey;
ALTER TABLE ONLY public.profession DROP CONSTRAINT profession_pkey;
ALTER TABLE ONLY public.product DROP CONSTRAINT product_pkey;
ALTER TABLE ONLY public.priority DROP CONSTRAINT priority_pkey;
ALTER TABLE ONLY public.person DROP CONSTRAINT person_pkey;
ALTER TABLE ONLY public.investigator DROP CONSTRAINT investigator_pkey;
ALTER TABLE ONLY public.funding DROP CONSTRAINT funding_pkey;
ALTER TABLE ONLY public.funder DROP CONSTRAINT funder_pkey;
ALTER TABLE ONLY public.focus DROP CONSTRAINT focus_pkey;
ALTER TABLE ONLY public.department DROP CONSTRAINT department_pkey;
ALTER TABLE ONLY public.classification DROP CONSTRAINT classification_pkey;
ALTER TABLE ONLY public.agency DROP CONSTRAINT agency_pkey;
ALTER TABLE public.project ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.program ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.profession ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.product ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.priority ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.person ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.investigator ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.funding ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.funder ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.focus ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.department ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.classification ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.agency ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.project_id_seq;
DROP TABLE public.project;
DROP SEQUENCE public.program_id_seq;
DROP TABLE public.program;
DROP SEQUENCE public.profession_id_seq;
DROP TABLE public.profession;
DROP SEQUENCE public.product_id_seq;
DROP TABLE public.product;
DROP SEQUENCE public.priority_id_seq;
DROP TABLE public.priority;
DROP SEQUENCE public.person_id_seq;
DROP TABLE public.person;
DROP SEQUENCE public.investigator_id_seq;
DROP TABLE public.investigator;
DROP SEQUENCE public.funding_id_seq;
DROP TABLE public.funding;
DROP SEQUENCE public.funder_id_seq;
DROP TABLE public.funder;
DROP SEQUENCE public.focus_id_seq;
DROP TABLE public.focus;
DROP SEQUENCE public.department_id_seq;
DROP TABLE public.department;
DROP SEQUENCE public.classification_id_seq;
DROP TABLE public.classification;
DROP SEQUENCE public.agency_id_seq;
DROP TABLE public.agency;
DROP FUNCTION public.xpath_nodeset(text, text, text);
DROP FUNCTION public.xpath_nodeset(text, text);
DROP FUNCTION public.xpath_list(text, text);
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET search_path = public, pg_catalog;

--
-- Name: xpath_list(text, text); Type: FUNCTION; Schema: public; Owner: kjcole
--

CREATE FUNCTION xpath_list(text, text) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$SELECT xpath_list($1,$2,',')$_$;


ALTER FUNCTION public.xpath_list(text, text) OWNER TO kjcole;

--
-- Name: xpath_nodeset(text, text); Type: FUNCTION; Schema: public; Owner: kjcole
--

CREATE FUNCTION xpath_nodeset(text, text) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$SELECT xpath_nodeset($1,$2,'','')$_$;


ALTER FUNCTION public.xpath_nodeset(text, text) OWNER TO kjcole;

--
-- Name: xpath_nodeset(text, text, text); Type: FUNCTION; Schema: public; Owner: kjcole
--

CREATE FUNCTION xpath_nodeset(text, text, text) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$SELECT xpath_nodeset($1,$2,'',$3)$_$;


ALTER FUNCTION public.xpath_nodeset(text, text, text) OWNER TO kjcole;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agency; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE agency (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.agency OWNER TO kjcole;

--
-- Name: agency_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE agency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.agency_id_seq OWNER TO kjcole;

--
-- Name: agency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE agency_id_seq OWNED BY agency.id;


--
-- Name: agency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('agency_id_seq', 78, true);


--
-- Name: classification; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE classification (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.classification OWNER TO kjcole;

--
-- Name: classification_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE classification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.classification_id_seq OWNER TO kjcole;

--
-- Name: classification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE classification_id_seq OWNED BY classification.id;


--
-- Name: classification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('classification_id_seq', 39, true);


--
-- Name: department; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE department (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.department OWNER TO kjcole;

--
-- Name: department_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE department_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.department_id_seq OWNER TO kjcole;

--
-- Name: department_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE department_id_seq OWNED BY department.id;


--
-- Name: department_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('department_id_seq', 64, true);


--
-- Name: focus; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE focus (
    id integer NOT NULL,
    project integer NOT NULL,
    priority integer NOT NULL
);


ALTER TABLE public.focus OWNER TO kjcole;

--
-- Name: focus_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE focus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.focus_id_seq OWNER TO kjcole;

--
-- Name: focus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE focus_id_seq OWNED BY focus.id;


--
-- Name: focus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('focus_id_seq', 232, true);


--
-- Name: funder; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE funder (
    id integer NOT NULL,
    is_internal boolean NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.funder OWNER TO kjcole;

--
-- Name: funder_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE funder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.funder_id_seq OWNER TO kjcole;

--
-- Name: funder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE funder_id_seq OWNED BY funder.id;


--
-- Name: funder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('funder_id_seq', 31, true);


--
-- Name: funding; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE funding (
    id integer NOT NULL,
    project integer NOT NULL,
    funder integer NOT NULL
);


ALTER TABLE public.funding OWNER TO kjcole;

--
-- Name: funding_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE funding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.funding_id_seq OWNER TO kjcole;

--
-- Name: funding_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE funding_id_seq OWNED BY funding.id;


--
-- Name: funding_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('funding_id_seq', 55, true);


--
-- Name: investigator; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE investigator (
    id integer NOT NULL,
    project integer NOT NULL,
    person integer NOT NULL,
    profession integer,
    department integer,
    program integer,
    agency integer NOT NULL,
    is_principal boolean NOT NULL,
    is_leader boolean NOT NULL
);


ALTER TABLE public.investigator OWNER TO kjcole;

--
-- Name: investigator_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE investigator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.investigator_id_seq OWNER TO kjcole;

--
-- Name: investigator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE investigator_id_seq OWNED BY investigator.id;


--
-- Name: investigator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('investigator_id_seq', 388, true);


--
-- Name: person; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE person (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255),
    last_name character varying(255) NOT NULL,
    first_name character varying(255)
);


ALTER TABLE public.person OWNER TO kjcole;

--
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.person_id_seq OWNER TO kjcole;

--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE person_id_seq OWNED BY person.id;


--
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('person_id_seq', 273, true);


--
-- Name: priority; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE priority (
    id integer NOT NULL,
    summary character varying(255) NOT NULL,
    description text NOT NULL
);


ALTER TABLE public.priority OWNER TO kjcole;

--
-- Name: priority_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE priority_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.priority_id_seq OWNER TO kjcole;

--
-- Name: priority_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE priority_id_seq OWNED BY priority.id;


--
-- Name: priority_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('priority_id_seq', 1, false);


--
-- Name: product; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE product (
    id integer NOT NULL,
    project integer NOT NULL,
    classification integer NOT NULL,
    description text NOT NULL
);


ALTER TABLE public.product OWNER TO kjcole;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO kjcole;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE product_id_seq OWNED BY product.id;


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('product_id_seq', 253, true);


--
-- Name: profession; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE profession (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.profession OWNER TO kjcole;

--
-- Name: profession_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE profession_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.profession_id_seq OWNER TO kjcole;

--
-- Name: profession_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE profession_id_seq OWNED BY profession.id;


--
-- Name: profession_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('profession_id_seq', 11, true);


--
-- Name: program; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE program (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.program OWNER TO kjcole;

--
-- Name: program_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE program_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.program_id_seq OWNER TO kjcole;

--
-- Name: program_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE program_id_seq OWNED BY program.id;


--
-- Name: program_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('program_id_seq', 9, true);


--
-- Name: project; Type: TABLE; Schema: public; Owner: kjcole; Tablespace: 
--

CREATE TABLE project (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    product_only boolean NOT NULL,
    is_student_project boolean NOT NULL,
    is_clerc_project boolean NOT NULL,
    status smallint,
    begin_date character varying(50),
    end_date character varying(50),
    description text NOT NULL,
    this_fy boolean NOT NULL,
    last_fy boolean NOT NULL
);


ALTER TABLE public.project OWNER TO kjcole;

--
-- Name: project_id_seq; Type: SEQUENCE; Schema: public; Owner: kjcole
--

CREATE SEQUENCE project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.project_id_seq OWNER TO kjcole;

--
-- Name: project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kjcole
--

ALTER SEQUENCE project_id_seq OWNED BY project.id;


--
-- Name: project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kjcole
--

SELECT pg_catalog.setval('project_id_seq', 870, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE agency ALTER COLUMN id SET DEFAULT nextval('agency_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE classification ALTER COLUMN id SET DEFAULT nextval('classification_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE department ALTER COLUMN id SET DEFAULT nextval('department_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE focus ALTER COLUMN id SET DEFAULT nextval('focus_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE funder ALTER COLUMN id SET DEFAULT nextval('funder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE funding ALTER COLUMN id SET DEFAULT nextval('funding_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE investigator ALTER COLUMN id SET DEFAULT nextval('investigator_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE person ALTER COLUMN id SET DEFAULT nextval('person_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE priority ALTER COLUMN id SET DEFAULT nextval('priority_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE product ALTER COLUMN id SET DEFAULT nextval('product_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE profession ALTER COLUMN id SET DEFAULT nextval('profession_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE program ALTER COLUMN id SET DEFAULT nextval('program_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kjcole
--

ALTER TABLE project ALTER COLUMN id SET DEFAULT nextval('project_id_seq'::regclass);


--
-- Data for Name: agency; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY agency (id, name) FROM stdin;
1	American Embassy, Bamako
2	American University
3	Atlanta Area School for the Deaf
4	Boston, MA
5	Boston University
6	Buckinghamshire New University, England
7	Buea School for the Deaf, Cameroun
8	Capella University
9	Carnegie-Mellon University
10	Centers for Disease Control and Prevention (CDC)
11	Chilean Deaf Association
12	Colombian Deaf Association
13	Costa Rica Deaf Association
14	Department of Youth and Community Development, New York City
15	duPont Hospital for Children, Wilmington, DE
16	Eastern Kentucky University
17	Fairfax County Public Schools
18	Florentine Films/Hott Productions
19	Gabonese Deaf Association, Libreville
20	Gallaudet University
21	Georgetown University
22	George Washington University
23	Georgia Institute of Technology
24	Georgia School for the Deaf
25	Goddard Space Flight Center
26	Guadalajara Deaf Association
27	James Madison University
29	Macquarie University
30	Mendoza Deaf Association, Argentina
31	Mexican Deaf Federation, Mexico City
34	National Cancer Institute
35	New Mexico Department of Health
36	New Zealand
37	Northeastern University
38	Pennsylvania State University
39	Purdue University
41	Rutgers University
42	San Diego State University
43	St. Catherine University
44	Teaching Interpreting Educators and Mentors (TIEM) Center
48	Tweedie and Associates
49	Universidad de M&aacute;laga, Spain
50	Universidade Federal de Santa Catarina, Brazil
51	University of Alberta
52	University of Barcelona
53	University of California, Davis
54	University of California-San Diego
55	University of California, San Diego
56	University of Connecticut
57	University of Connecticut, Storrs, CT
58	University of Georgia
59	University of Haifa, Israel
60	University of Iowa
61	University of Manitoba
62	University of Miami
63	University of New Mexico
64	University of Northern Florida
65	University of Rochester
66	University of Tennessee
67	University of Texas, Arlington
68	University of Texas, Austin
69	University of Utah
70	University of Zagreb, Croatia
71	Vaal University of Technology, South Africa
72	Vienna, Austria
73	Virginia Commonwealth University
74	Western Oregon University
75	WETA TV
76	Yale University
28	King Consultants
45	University of Wisconsin, Madison
40	Rochester Institute of Technology (RIT)
33	National Aeronautics and Space Administration (NASA)
77	Self-employed
78	Friends of Deaf Haiti
32	Montfort Institute
\.


--
-- Data for Name: classification; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY classification (id, name) FROM stdin;
1	Article
2	Book chapter
3	Book
4	Chapter
5	Dissertation
6	DVD
7	Electronic booklet
8	Encyclopedia entry
9	Essay
10	Final report
11	Journal submission
12	Keynote address
13	Legal document
14	Panel Presentation
15	Paper
16	Peer-reviewed article
17	Plenary address
18	Poster presentation
19	Presentation
20	Report
21	Survey
22	Technical/research report
23	Thesis
24	TV taping
25	Unpublished manuscript
26	Video presentation
27	Workshop
28	Website
29	Other
30	Public Programs
31	Documentary
32	Newspaper article
33	Journal article
34	Documentary film
35	FCC Filings
36	Other - Software Applications
37	Publication
38	Proceedings
\.


--
-- Data for Name: department; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY department (id, name) FROM stdin;
1	Academic Technology
2	Administration and Supervision
3	Applied Literacy
4	Art, Television, Photography, and Digital Media
5	ASL and Deaf Studies
6	ASL/Interpreting
7	ATLAS Learning Services
8	Audiology-Linguistics
9	Biology
10	Business
11	Center for American Sign Language Literacy
12	Center for ASL/English Bilingual Education and Research (CAEBER)
13	Chemistry and Physics
14	Clerc Center
15	College of Liberal Arts, Sciences, and Technologies (CLAST)
16	College of Professional Studies and Outreach (CPSO)
17	Communications Disorders
18	Communication Studies
19	Computer Science
20	Counseling
21	Early Childhood Education
22	Education
23	Educational Foundations and Research
24	English
25	English Language Institute
26	Family &amp; Child Studies
27	First Year Experience
28	Foreign Languages, Literatures, and Cultures
29	Gallaudet Research Institute (GRI)
30	Government and History
31	Graduate School and Professional Programs (GSPP)
32	Health and Wellness Programs
33	Hearing, Speech, and Language Sciences
34	Honors Program
35	Industrial Engineering
36	International Development
37	Interpretation
38	Interpreting Training Program
39	Language Planning Institute (LPI)
40	Library
41	Linguistics
42	Mathematics and Computer Science
43	M&eacute;todo Oral Complementado (MOC)
44	Medical Center Development
45	Mental Health Center
46	Miller School of Medicine
47	Multicultural Student Programs
48	National Technical Institute for the Deaf (NTID)
49	Office of Academic Quality
50	Office of Bilingual Teaching and Learning
51	Office of the Provost
52	Other
53	Outreach
54	Pediatrics
55	Physical Education and Recreation
56	Psychology
57	Richard B. &amp; Lynne V. Cheney Cardiovascular Institute
58	Science of Learning Center on Visual Language &amp; Visual Learning (VL&sup2;)
59	Social Work
60	Sociology
61	Speech and Hearing Sciences
62	Speech Pathology &amp; Audiology
63	Speech Research Lab
64	College of Engineering
\.


--
-- Data for Name: focus; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY focus (id, project, priority) FROM stdin;
1	845	1
2	689	1
3	766	1
4	800	1
5	248	1
6	273	1
7	826	1
8	844	1
9	836	1
10	810	1
11	51	1
12	112	1
13	859	1
14	447	1
15	837	1
16	858	1
17	629	1
18	869	1
19	832	1
20	833	1
21	610	1
22	845	2
23	689	2
24	766	2
25	822	2
26	621	2
27	273	2
28	826	2
29	844	2
30	570	2
31	810	2
32	51	2
33	112	2
34	777	2
35	869	2
36	833	2
37	610	2
38	820	2
39	572	3
40	845	3
41	689	3
42	827	3
43	830	3
44	746	3
45	300	3
46	736	3
47	824	3
48	858	3
49	819	3
50	823	3
51	828	3
52	742	3
53	2	4
54	144	4
55	845	4
56	689	4
57	766	4
58	822	4
59	273	4
60	827	4
61	844	4
62	3	4
63	830	4
64	783	4
65	843	4
66	306	4
67	810	4
68	6	4
69	841	4
70	51	4
71	444	4
72	112	4
73	859	4
74	578	4
75	858	4
76	777	4
77	834	4
78	629	4
79	747	4
80	869	4
81	833	4
82	829	4
83	610	4
84	572	5
85	2	5
86	845	5
87	689	5
88	766	5
89	273	5
90	827	5
91	830	5
92	810	5
93	6	5
94	841	5
95	444	5
96	112	5
97	300	5
98	578	5
99	742	5
100	777	5
101	812	5
102	747	5
103	869	5
104	610	5
105	144	6
106	420	6
107	845	6
108	838	6
109	273	6
110	51	6
111	444	6
112	777	6
113	34	7
114	50	7
115	70	7
116	217	7
117	218	7
118	831	7
119	689	7
120	835	7
121	785	7
122	841	7
123	444	7
124	112	7
125	300	7
126	615	7
127	859	7
128	765	7
129	858	7
130	747	7
131	784	7
132	845	8
133	800	8
134	840	8
135	273	8
136	844	8
137	841	8
138	839	8
139	112	8
140	842	8
141	859	8
142	447	8
143	837	8
144	858	8
145	629	8
146	832	8
147	845	9
148	821	9
149	846	9
150	844	9
151	843	9
152	841	9
153	839	9
154	842	9
155	858	9
156	834	9
157	629	9
158	2	10
159	50	10
160	218	10
161	845	10
162	766	10
163	800	10
164	821	10
165	273	10
166	3	10
167	843	10
168	306	10
169	810	10
170	6	10
171	841	10
172	300	10
173	615	10
174	542	10
175	858	10
176	812	10
177	629	10
178	747	10
179	809	10
180	70	11
181	831	11
182	845	11
183	689	11
184	835	11
185	821	11
186	273	11
187	844	11
188	810	11
189	247	11
190	859	11
191	542	11
192	578	11
193	858	11
194	834	11
195	832	11
196	809	11
197	833	11
198	572	12
199	2	12
200	420	12
201	821	12
202	273	12
203	826	12
204	836	12
205	746	12
206	306	12
207	810	12
208	842	12
209	837	12
210	658	12
211	629	12
212	572	13
213	2	13
214	70	13
215	689	13
216	800	13
217	822	13
218	838	13
219	273	13
220	830	13
221	841	13
222	112	13
223	615	13
224	859	13
225	542	13
226	765	13
227	578	13
228	858	13
229	812	13
230	834	13
231	629	13
232	747	13
\.


--
-- Data for Name: funder; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY funder (id, is_internal, name) FROM stdin;
1	t	GRI Priority Grant
2	t	GRI Small Grant
4	t	GSPP Dean's office
5	t	Molecular Genetics Laboratory
8	t	Spencer Foundation
9	t	VL&sup2;
10	t	VL&sup2; Student Research Grant
11	f	Booth Ferris Foundation
13	f	Centers for Disease Control and Prevention (CDC)
14	f	Delta Zeta Alumnae Scholarship Fund
16	f	Mellon Foundation
17	f	Ministry of Education (Trinidad &amp; Tobago)
18	f	NASA/DC Space Grant Consortium
19	f	National Institute on Disability and Rehabilitation Research (NIDRR)
20	f	National Institutes of Health (NIH)
21	f	National Science Foundation (NSF)
22	f	Rehabilitation Engineering Research Center on Hearing Enhancement (RERC-HE)
23	f	RSA Department of Education Grant funding
25	f	Teagle Foundation
26	f	University of Texas at Austin
27	f	U.S. Department of Education
28	t	Department of History funds
3	t	Faculty Development Grant
29	t	Office for Diversity and Inclusion
6	t	Provost Office
15	f	Japan Social Development Fund - World Bank (via World Concern)
12	f	Camp Confidence (camper fees)
7	t	Sorenson Legacy Foundation
30	f	Computer and Network Systems (CNS) - National Science Foundation (NSF)
31	f	Rehabilitation Services Administration (RSA) - U.S. Department of Education
\.


--
-- Data for Name: funding; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY funding (id, project, funder) FROM stdin;
1	783	1
2	859	1
3	837	1
4	629	1
5	621	1
6	845	2
7	447	2
8	578	2
9	736	2
10	825	2
11	824	2
12	858	2
13	819	2
14	823	2
15	828	2
16	742	2
17	777	2
18	812	2
19	658	2
20	784	3
21	836	4
22	833	4
23	704	5
24	784	28
25	838	6
26	838	29
27	831	7
28	747	8
29	837	9
30	820	9
31	742	10
32	621	9
33	833	11
34	763	12
35	746	13
36	742	14
37	869	15
38	765	7
39	765	16
40	765	18
41	839	17
42	809	19
43	809	27
44	830	27
45	800	20
46	273	21
47	810	21
48	829	21
49	832	30
50	247	22
51	247	19
52	247	27
53	841	31
54	834	26
55	866	25
\.


--
-- Data for Name: investigator; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY investigator (id, project, person, profession, department, program, agency, is_principal, is_leader) FROM stdin;
50	70	89	2	\N	\N	77	t	t
112	838	156	6	\N	\N	77	t	t
4	50	248	5	\N	\N	77	t	t
371	834	241	11	\N	\N	77	f	f
267	834	49	11	\N	\N	77	f	f
288	70	90	4	\N	\N	77	f	f
388	204	273	\N	\N	\N	78	f	f
76	681	133	\N	56	\N	20	t	f
8	34	11	\N	9	\N	20	t	t
183	771	153	9	56	\N	20	t	f
196	812	159	9	2	\N	20	t	f
136	679	183	9	56	\N	20	t	f
185	819	189	9	56	\N	20	t	f
186	824	189	9	56	\N	20	t	f
138	679	216	9	56	\N	20	t	f
106	802	230	9	56	\N	20	t	f
171	714	252	9	33	\N	20	t	t
137	679	255	9	56	\N	20	t	f
141	823	271	9	56	\N	20	t	f
48	746	8	9	56	\N	20	t	f
70	680	269	9	55	\N	20	t	f
43	351	247	\N	35	8	45	t	f
78	680	4	9	56	\N	20	t	f
154	764	138	\N	13	\N	20	t	t
15	112	16	\N	22	\N	20	t	t
97	621	72	\N	\N	\N	38	t	t
13	112	79	\N	22	\N	20	t	t
205	273	102	\N	31	\N	20	t	f
3	34	167	\N	\N	\N	73	t	t
168	838	130	\N	29	\N	20	t	f
47	744	272	9	56	\N	20	t	f
71	625	8	9	56	\N	20	t	t
74	679	25	9	56	\N	20	t	f
75	679	94	9	56	\N	20	t	f
77	681	158	9	56	\N	20	t	f
130	745	178	9	41	\N	20	t	f
126	685	270	9	22	\N	20	t	f
187	827	60	9	56	\N	20	t	f
139	679	127	9	56	\N	20	t	f
134	742	4	9	56	\N	20	t	f
118	836	95	\N	5	1	20	t	f
49	572	154	8	56	\N	20	t	t
46	554	160	\N	43	\N	49	t	f
36	143	164	\N	59	\N	20	t	f
44	832	170	\N	41	\N	5	t	f
32	544	172	\N	42	\N	20	t	t
45	683	177	\N	3	\N	20	t	t
10	144	227	\N	13	\N	20	t	t
41	747	246	\N	22	\N	38	t	f
42	305	258	\N	8	\N	39	t	t
122	832	15	\N	5	\N	20	t	t
84	360	20	\N	60	\N	20	t	t
85	361	20	\N	60	\N	20	t	t
90	826	23	\N	50	\N	20	t	f
92	831	23	\N	50	\N	20	t	f
65	305	52	\N	41	\N	20	t	t
66	677	52	\N	41	\N	20	t	f
83	1	54	\N	60	\N	20	t	f
67	677	63	\N	41	\N	20	t	t
52	770	65	\N	56	\N	20	t	f
64	633	67	\N	37	\N	20	t	t
69	837	71	\N	41	\N	20	t	f
72	615	133	\N	56	\N	20	t	t
82	1	133	\N	56	\N	20	t	t
124	704	138	\N	13	\N	20	t	t
101	831	140	\N	58	\N	20	t	f
131	836	144	\N	41	\N	20	t	f
68	629	145	\N	41	\N	20	t	t
81	1	146	\N	56	\N	20	t	f
125	2	150	\N	23	\N	20	t	t
94	832	151	\N	19	\N	41	t	f
73	837	179	\N	56	\N	20	t	f
127	621	187	\N	28	\N	20	t	f
96	834	194	\N	41	\N	68	t	f
99	781	195	\N	10	\N	20	t	t
93	832	217	\N	19	\N	5	t	f
129	684	219	\N	37	\N	20	t	f
98	842	233	\N	6	\N	43	t	f
80	572	236	\N	56	\N	20	t	f
100	835	246	\N	21	\N	38	t	f
103	783	254	\N	10	\N	20	t	t
140	742	44	\N	56	\N	20	t	f
149	810	55	\N	58	\N	20	t	f
160	825	55	\N	23	\N	20	t	f
150	866	68	\N	27	\N	20	t	f
152	766	74	\N	5	\N	20	t	t
195	801	81	\N	60	\N	20	t	t
159	405	92	\N	23	\N	20	t	t
188	821	93	\N	56	\N	20	t	f
151	766	103	\N	5	\N	20	t	f
143	747	110	\N	60	\N	20	t	f
182	769	124	\N	56	\N	20	t	t
5	70	24	\N	\N	\N	60	t	t
102	781	5	\N	15	\N	20	t	f
109	809	247	\N	35	8	45	t	t
156	832	250	\N	18	5	20	t	f
142	689	133	\N	56	\N	20	t	f
221	810	161	\N	56	\N	20	t	f
157	800	134	\N	41	\N	56	t	f
161	829	141	\N	22	\N	20	t	f
190	761	143	\N	59	\N	20	t	f
194	711	143	\N	59	\N	20	t	t
176	772	172	\N	42	\N	20	t	t
135	689	179	\N	56	\N	20	t	f
163	820	187	\N	28	\N	20	t	f
177	780	199	\N	55	\N	20	t	t
178	779	199	\N	55	\N	20	t	t
158	777	209	\N	13	\N	20	t	t
189	758	214	\N	59	\N	20	t	f
193	830	220	\N	59	\N	20	t	f
172	714	234	\N	33	\N	20	t	f
153	773	253	\N	5	\N	20	t	f
60	658	3	\N	33	\N	20	t	f
224	810	6	\N	58	\N	20	t	t
220	866	7	\N	51	\N	20	t	f
201	217	11	\N	9	\N	20	t	t
95	832	13	\N	19	\N	67	t	f
198	831	15	\N	5	\N	20	t	f
63	576	17	\N	33	\N	20	t	t
175	705	17	\N	33	\N	20	t	t
120	835	23	\N	5	\N	20	t	f
132	836	23	\N	50	\N	20	t	f
133	833	23	\N	50	\N	20	t	f
184	823	44	\N	56	\N	20	t	f
223	843	46	\N	60	\N	20	t	f
212	743	67	\N	37	\N	20	t	f
217	800	52	\N	41	\N	20	t	f
162	766	91	\N	22	\N	20	t	f
233	785	96	\N	30	\N	20	t	t
234	786	96	\N	30	\N	20	t	t
235	787	96	\N	30	\N	20	t	f
236	784	96	\N	30	\N	20	t	t
181	736	133	\N	56	\N	20	t	f
155	763	138	\N	13	\N	20	t	f
79	671	169	\N	56	\N	20	t	t
179	778	199	\N	55	\N	20	t	f
180	776	199	\N	55	\N	20	t	t
121	836	205	\N	5	\N	20	t	f
11	51	227	\N	13	\N	20	t	t
197	836	12	\N	5	1	20	t	f
119	836	32	\N	5	1	20	t	f
51	765	41	\N	9	4	20	t	f
229	809	250	\N	18	5	20	t	t
230	615	54	8	60	\N	20	t	t
169	858	26	\N	29	\N	20	t	f
111	837	106	\N	48	\N	40	t	f
191	830	33	\N	59	\N	20	t	f
192	828	33	\N	59	\N	20	t	f
28	50	182	\N	30	\N	20	t	t
57	766	197	\N	29	\N	20	t	f
7	218	180	\N	\N	\N	73	t	t
38	684	200	\N	\N	\N	64	t	f
6	70	31	\N	16	\N	20	t	t
148	273	106	\N	48	\N	40	t	f
9	218	11	\N	9	\N	20	t	t
14	444	16	\N	22	\N	20	t	t
34	560	20	\N	59	\N	20	t	t
55	578	27	\N	22	\N	20	t	t
17	204	29	\N	28	\N	20	t	t
18	430	29	\N	28	\N	20	t	t
19	167	29	\N	28	\N	20	t	t
33	420	36	\N	56	\N	20	t	f
59	658	42	\N	33	\N	20	t	f
58	609	45	\N	30	\N	20	t	t
30	447	52	\N	41	\N	20	t	f
31	448	52	\N	41	\N	20	t	t
54	679	55	\N	23	\N	20	t	t
12	542	92	\N	23	\N	20	t	f
61	658	100	\N	33	\N	20	t	f
53	764	70	\N	\N	\N	27	t	f
1	405	82	\N	\N	\N	52	t	t
2	444	142	\N	\N	\N	4	t	f
21	247	19	\N	29	\N	20	t	t
22	3	112	\N	29	\N	20	t	t
23	6	112	\N	29	\N	20	t	t
20	570	120	\N	29	\N	20	t	t
24	6	130	\N	29	\N	20	t	f
16	444	114	\N	22	\N	20	t	f
62	658	119	\N	33	\N	20	t	t
29	554	132	\N	33	\N	20	t	t
56	306	141	\N	22	\N	20	t	t
35	143	143	\N	59	\N	20	t	t
39	684	207	\N	\N	\N	51	t	f
25	6	266	\N	29	\N	20	t	f
128	833	79	\N	31	\N	20	t	f
91	826	59	\N	49	9	20	t	f
115	841	9	\N	\N	\N	74	t	f
114	839	64	\N	\N	\N	66	t	f
108	800	66	\N	\N	\N	50	t	f
116	841	69	\N	\N	\N	37	t	f
88	778	115	\N	\N	\N	6	t	f
87	774	135	\N	\N	\N	33	t	f
105	820	162	\N	\N	\N	63	t	f
110	813	168	\N	\N	\N	29	t	f
104	792	202	\N	\N	\N	20	t	t
86	774	245	\N	\N	\N	48	t	f
89	820	259	\N	\N	\N	61	t	f
117	842	264	\N	\N	\N	44	t	f
113	839	265	\N	\N	\N	66	t	f
146	273	58	\N	\N	\N	53	t	f
144	273	75	\N	\N	\N	21	t	f
40	747	238	\N	21	\N	58	t	f
165	858	197	\N	29	\N	20	t	f
166	869	197	\N	29	\N	20	t	f
26	248	19	\N	29	\N	20	t	t
27	570	19	\N	29	\N	20	t	t
170	859	26	\N	29	\N	20	t	f
207	273	6	\N	31	\N	20	t	f
147	273	162	\N	\N	\N	63	t	f
145	273	224	\N	\N	\N	23	t	f
123	835	15	\N	5	\N	20	t	f
226	820	128	\N	\N	\N	38	t	f
167	859	197	\N	29	\N	20	t	f
206	273	185	\N	31	\N	20	t	f
37	610	164	\N	59	\N	20	t	t
228	351	101	\N	18	5	20	t	t
232	300	173	8	55	\N	20	t	t
231	50	174	8	30	\N	20	t	t
107	782	239	\N	\N	\N	8	t	t
208	803	76	\N	37	\N	20	t	f
202	866	85	\N	24	\N	20	t	f
203	788	104	\N	28	\N	20	t	t
204	791	104	\N	28	\N	20	t	t
214	841	109	\N	37	\N	20	t	f
218	840	125	\N	41	\N	20	t	f
227	705	139	\N	61	\N	22	t	f
222	815	143	\N	59	\N	20	t	f
216	76	152	\N	37	\N	20	t	f
199	784	166	\N	5	\N	20	t	f
225	633	186	\N	38	\N	16	t	f
210	813	206	\N	37	\N	20	t	f
215	76	206	\N	37	\N	20	t	t
213	840	211	\N	37	\N	20	t	f
219	840	215	\N	41	\N	20	t	f
209	803	218	\N	37	\N	20	t	f
200	783	228	\N	9	\N	20	t	f
164	822	187	\N	28	\N	20	t	f
211	743	213	9	37	\N	20	t	f
244	204	273	\N	\N	\N	32	f	f
300	572	106	\N	48	\N	40	f	f
301	248	107	\N	41	\N	20	f	f
302	305	108	\N	5	\N	20	f	f
303	273	112	\N	29	\N	20	f	f
304	810	112	\N	29	\N	20	f	f
305	838	112	\N	29	\N	20	f	f
306	681	113	\N	45	\N	20	f	f
307	838	114	\N	49	\N	20	f	f
308	842	116	\N	37	\N	20	f	f
309	430	117	9	28	\N	20	f	f
310	788	118	\N	57	\N	22	f	f
311	681	121	\N	\N	\N	76	f	f
312	70	122	\N	\N	\N	75	f	f
313	570	123	9	56	\N	20	f	f
314	351	126	\N	18	5	20	f	f
315	809	126	\N	18	5	20	f	f
316	705	126	\N	18	5	20	f	f
317	273	129	\N	\N	\N	5	f	f
318	112	129	\N	\N	\N	5	f	f
319	70	131	8	48	\N	40	f	f
320	448	134	\N	\N	\N	57	f	f
321	204	136	\N	\N	\N	13	f	f
322	572	137	\N	\N	\N	40	f	f
323	273	147	\N	\N	\N	54	f	f
324	204	148	\N	\N	\N	12	f	f
325	704	149	\N	9	\N	20	f	f
326	273	155	\N	\N	\N	59	f	f
327	2	157	\N	\N	\N	71	f	f
328	273	161	\N	56	\N	20	f	f
329	822	162	\N	\N	\N	63	f	f
330	821	163	\N	\N	\N	24	f	f
331	833	165	\N	41	3	20	f	f
332	218	167	\N	\N	\N	73	f	f
333	217	167	\N	\N	\N	73	f	f
334	765	167	\N	\N	\N	73	f	f
335	218	171	\N	9	\N	20	f	f
336	273	175	\N	\N	\N	55	f	f
337	833	176	\N	24	3	20	f	f
338	217	180	\N	\N	\N	73	f	f
339	765	180	\N	\N	\N	73	f	f
340	34	180	\N	\N	\N	73	f	f
341	204	184	\N	\N	\N	1	f	f
342	273	187	\N	28	\N	20	f	f
343	273	188	\N	\N	\N	9	f	f
344	305	190	\N	\N	\N	70	f	f
345	610	191	\N	58	\N	20	f	f
346	838	192	\N	29	\N	20	f	f
347	448	193	\N	\N	\N	50	f	f
348	273	194	\N	\N	\N	68	f	f
349	273	196	\N	\N	\N	55	f	f
350	838	197	\N	29	\N	20	f	f
351	112	197	\N	29	\N	20	f	f
352	112	198	\N	3	\N	20	f	f
353	430	201	1	\N	\N	20	f	f
354	788	203	3	44	\N	22	f	f
355	360	204	\N	\N	\N	59	f	f
356	792	208	\N	30	7	20	f	f
357	204	210	\N	\N	\N	31	f	f
358	305	212	\N	\N	\N	72	f	f
359	792	221	\N	34	\N	20	f	f
360	204	222	\N	\N	\N	19	f	f
361	833	223	\N	22	\N	20	f	f
362	2	225	\N	23	\N	20	f	f
363	144	226	\N	\N	\N	25	f	f
364	840	229	\N	\N	\N	20	f	f
365	447	231	\N	41	\N	20	f	f
366	736	232	\N	56	\N	20	f	f
367	765	235	\N	46	\N	62	f	f
368	833	237	\N	29	\N	20	f	f
369	112	237	\N	29	\N	20	f	f
370	273	240	\N	\N	\N	53	f	f
372	829	242	10	\N	\N	17	f	f
373	144	243	\N	\N	\N	25	f	f
374	351	244	\N	18	5	20	f	f
375	809	244	\N	18	5	20	f	f
376	833	249	\N	41	\N	20	f	f
377	305	251	\N	\N	\N	70	f	f
378	869	256	\N	26	\N	20	f	f
379	792	257	\N	1	\N	20	f	f
380	843	260	\N	37	\N	20	f	f
381	351	261	\N	18	5	20	f	f
382	809	261	\N	18	5	20	f	f
383	2	262	\N	23	\N	20	f	f
384	869	263	9	36	\N	20	f	f
385	838	266	\N	29	\N	20	f	f
386	833	267	\N	24	3	20	f	f
387	705	268	\N	62	\N	60	f	f
238	825	8	\N	56	\N	20	f	f
239	736	8	\N	56	\N	20	f	f
240	765	11	\N	9	\N	20	f	f
241	833	110	\N	60	\N	20	f	f
242	769	132	\N	33	\N	20	f	f
243	736	60	\N	56	\N	20	f	f
245	704	41	\N	9	\N	20	f	f
246	765	10	8	\N	\N	20	f	f
247	204	14	\N	\N	\N	11	f	f
248	838	18	\N	49	\N	20	f	f
249	705	19	\N	29	\N	20	f	f
250	1	21	\N	\N	\N	14	f	f
251	204	22	\N	\N	\N	30	f	f
252	826	26	\N	29	\N	20	f	f
253	705	28	\N	62	\N	60	f	f
254	51	30	\N	\N	2	2	f	f
255	204	34	\N	\N	\N	7	f	f
256	218	35	\N	\N	\N	62	f	f
257	217	35	\N	\N	\N	62	f	f
258	704	37	\N	\N	\N	34	f	f
259	578	38	8	\N	\N	20	f	f
260	273	39	\N	\N	\N	55	f	f
261	430	40	\N	28	\N	20	f	f
262	705	43	\N	61	\N	22	f	f
263	824	44	\N	56	\N	20	f	f
264	819	44	\N	56	\N	20	f	f
265	705	47	\N	63	\N	15	f	f
266	204	48	\N	\N	\N	26	f	f
268	542	50	\N	\N	\N	35	f	f
269	576	51	\N	17	\N	69	f	f
270	821	53	\N	\N	\N	3	f	f
271	273	55	\N	23	\N	20	f	f
272	570	56	\N	29	\N	20	f	f
273	247	56	\N	29	\N	20	f	f
274	2	61	\N	\N	\N	36	f	f
275	736	62	\N	56	\N	20	f	f
276	273	71	\N	41	\N	20	f	f
277	822	72	\N	\N	\N	38	f	f
278	273	73	\N	\N	\N	65	f	f
279	769	75	\N	54	\N	21	f	f
280	273	77	\N	\N	\N	42	f	f
281	273	78	\N	\N	\N	61	f	f
282	112	80	\N	14	\N	20	f	f
283	839	83	\N	37	\N	20	f	f
284	830	84	\N	59	\N	20	f	f
285	838	86	\N	49	\N	20	f	f
286	542	87	\N	\N	\N	12	f	f
287	542	88	\N	\N	\N	10	f	f
289	70	111	\N	\N	\N	18	f	f
290	681	92	\N	23	\N	20	f	f
291	825	94	\N	56	\N	20	f	f
292	204	97	\N	47	\N	20	f	f
293	430	97	\N	47	\N	20	f	f
294	681	98	7	56	\N	20	f	f
295	859	99	\N	29	\N	20	f	f
296	858	99	\N	29	\N	20	f	f
297	610	102	\N	58	\N	20	f	f
298	112	103	\N	22	\N	20	f	f
299	576	105	\N	33	\N	20	f	f
\.


--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY person (id, name, email, last_name, first_name) FROM stdin;
3	Ackley, R. Steven	robert.ackley@gallaudet.edu	Ackley	R. Steven
4	Adams, Elizabeth B.	elizabeth.adams@gallaudet.edu	Adams	Elizabeth B.
5	Agboola, Isaac	isaac.agboola@gallaudet.edu	Agboola	Isaac
6	Allen, Thomas	thomas.allen@gallaudet.edu	Allen	Thomas
7	Andersen, Catherine	\N	Andersen	Catherine
8	Anderson, Melissa L.	melissa.anderson@gallaudet.edu	Anderson	Melissa L.
9	Annarino, Pauline	\N	Annarino	Pauline
10	Armstrong, David F.	\N	Armstrong	David F.
11	Arnos, Kathleen S.	kathleen.arnos@gallaudet.edu	Arnos	Kathleen S.
12	Arrellano, Leticia	\N	Arrellano	Leticia
13	Athitsos, Vassilis	\N	Athitsos	Vassilis
14	Ayala, Fernando	\N	Ayala	Fernando
15	Bahan, Ben	\N	Bahan	Ben
16	Bailes, Cynthia Neese	cynthia.bailes@gallaudet.edu	Bailes	Cynthia Neese
17	Bakke, Matthew H.	matthew.bakke@gallaudet.edu	Bakke	Matthew H.
18	Bangura, Rosanne	\N	Bangura	Rosanne
19	Barac-Cikoja, Dragana	dragana.barac-cikoja@gallaudet.edu	Barac-Cikoja	Dragana
20	Barnartt, Sharon	sharon.barnartt@gallaudet.edu	Barnartt	Sharon
21	Bat-Chava, Yael	\N	Bat-Chava	Yael
22	Battistelli, Luis	\N	Battistelli	Luis
23	Bauman, Dirksen	\N	Bauman	Dirksen
24	Baynton, Douglas	\N	Baynton	Douglas
25	Begue, Jason	jason.begue@gallaudet.edu	Begue	Jason
26	Benaissa, Senda	\N	Benaissa	Senda
27	Benedict, Beth	beth.benedict@gallaudet.edu	Benedict	Beth
28	Bentler, Ruth	\N	Bentler	Ruth
29	Berdichevsky, Cristina	cristina.berdichevsky@gallaudet.edu	Berdichevsky	Cristina
30	Berendzen, Richard	\N	Berendzen	Richard
31	Bergey, Jean	jean.bergey@gallaudet.edu	Bergey	Jean
32	Berrigan, Dennis	\N	Berrigan	Dennis
33	Betman, Beth G.	\N	Betman	Beth G.
34	Bibum, Aloy	\N	Bibum	Aloy
35	Blanton, Susan H.	\N	Blanton	Susan H.
36	Blennerhassett, Lynne	lynne.blennerhassett@gallaudet.edu	Blennerhassett	Lynne
37	Blumbeirg, Peter	\N	Blumbeirg	Peter
38	Bodner-Johnson, Barbara	\N	Bodner-Johnson	Barbara
39	Bosworth, Rain	\N	Bosworth	Rain
40	Bradford, Stacey (Tashi)	\N	Bradford	Stacey (Tashi)
41	Braun, Derek C.	derek.braun@gallaudet.edu	Braun	Derek C.
42	Brewer, Carmen	carmen.brewer@gallaudet.edu	Brewer	Carmen
43	Brewer, Diane	\N	Brewer	Diane
44	Brice, Patrick	patrick.brice@gallaudet.edu	Brice	Patrick
45	Brune, Jeffrey A.	jeffrey.brune@gallaudet.edu	Brune	Jeffrey A.
46	Brunson, Jeremy L.	\N	Brunson	Jeremy L.
47	Bunnell, Timothy H.	\N	Bunnell	Timothy H.
48	Camarena, Silvia	\N	Camarena	Silvia
49	Casanova de Canales, Kristie	\N	Casanova de Canales	Kristie
50	Chacon, Susan	\N	Chacon	Susan
51	Chatterjee, Monita	\N	Chatterjee	Monita
52	Chen Pichler, Deborah	deborah.pichler@Gallaudet.edu	Chen Pichler	Deborah
53	Chilvers, Amanda	\N	Chilvers	Amanda
54	Christiansen, John	john.christiansen@gallaudet.edu	Christiansen	John
55	Clark, Diane	diane.clark@gallaudet.edu	Clark	Diane
56	Cole, Kevin	kevin.cole@gallaudet.edu	Cole	Kevin
57	Compton-Conley, Cynthia	cynthia.conley@gallaudet.edu	Compton-Conley	Cynthia
58	Corina, David	\N	Corina	David
59	Coye, Terry	\N	Coye	Terry
60	Craig, Kelly S. Wolf	\N	Craig	Kelly S. Wolf
61	Cram, Fiona	\N	Cram	Fiona
62	Crisologo, Anna	\N	Crisologo	Anna
63	Cull, Amber	amber.goeke@gallaudet.edu	Cull	Amber
64	Dadvis, Jeffrey	\N	Dadvis	Jeffrey
65	Daggett, Dorri	dorri.daggett@gallaudet.edu	Daggett	Dorri
66	de Quadros, Ronice	\N	de Quadros	Ronice
67	Dively, Valerie	valerie.dively@gallaudet.edu	Dively	Valerie
68	Dorminy, Jerri Lyn	\N	Dorminy	Jerri Lyn
69	Doucette, Diana	\N	Doucette	Diana
70	Downey, Daniel	\N	Downey	Daniel
71	Dudis, Paul	\N	Dudis	Paul
72	Dussias, Paola E.	\N	Dussias	Paola E.
73	Dye, Matt	\N	Dye	Matt
74	Earth, Barbara	barbara.earth@gallaudet.edu	Earth	Barbara
75	Eden, Guinevere	\N	Eden	Guinevere
76	El-Amin, Sequoia	sequoia.el-amin@gallaudet.edu	El-Amin	Sequoia
77	Emmorey, Karen	\N	Emmorey	Karen
78	Enns, Charlotte	\N	Enns	Charlotte
79	Erting, Carol J.	carol.erting@gallaudet.edu	Erting	Carol J.
80	Erting, Lynne	\N	Erting	Lynne
81	Fennell, Julie	julie.fennell@gallaudet.edu	Fennell	Julie
82	Fernandez-Viader, Pilar	\N	Fernandez-Viader	Pilar
83	Ford, Folami M.	\N	Ford	Folami M.
84	Frank, Audrey	\N	Frank	Audrey
85	Franklin, Paige	\N	Franklin	Paige
86	Frelich, Daryl	\N	Frelich	Daryl
87	Gaffney, Claudia	\N	Gaffney	Claudia
88	Gaffney, Marcus	\N	Gaffney	Marcus
89	Gannon, Jack	\N	Gannon	Jack
90	Gannon, Rosalyn	\N	Gannon	Rosalyn
91	Garate, Maribel	maribel.garate@gallaudet.edu	Garate	Maribel
92	Gerner de Garcia, Barbara	barbara.gerner.de.garcia@gallaudet.edu	Gerner de Garcia	Barbara
93	Gibbons, Elizabeth	\N	Gibbons	Elizabeth
94	Gilbert, Gizelle	gizelle.gilbert@gallaudet.edu	Gilbert	Gizelle
95	Gordon, Jean	\N	Gordon	Jean
96	Greenwald, Brian H.	brian.greenwald@gallaudet.edu	Greenwald	Brian H.
97	Guillermo, Elvia	\N	Guillermo	Elvia
98	Gutman, Virginia	\N	Gutman	Virginia
99	Hack-McCafferty, Shirley	\N	Hack-McCafferty	Shirley
100	Hanks, Wendy	wendy.hanks@gallaudet.edu	Hanks	Wendy
101	Harkins, Judy	judith.harkins@gallaudet.edu	Harkins	Judy
102	Harmon, Kristen	\N	Harmon	Kristen
103	Harris, Raychelle	raychelle.harris@gallaudet.edu	Harris	Raychelle
210	Sanabria, Gaspar	\N	Sanabria	Gaspar
104	Hartig, Rachel M.	rachel.hartig@gallaudet.edu	Hartig	Rachel M.
105	Harvey, Tiffany	\N	Harvey	Tiffany
106	Hauser, Peter	\N	Hauser	Peter
107	Hill, Joseph	\N	Hill	Joseph
108	Hochgesang, Julie	\N	Hochgesang	Julie
109	Hollrah, Beverly	\N	Hollrah	Beverly
110	Horejes, Thomas	thomas.horejes@gallaudet.edu	Horejes	Thomas
111	Hott, Larry	\N	Hott	Larry
112	Hotto, Sue	sue.hotto@gallaudet.edu	Hotto	Sue
113	Hufnell, Mary	\N	Hufnell	Mary
114	Hulsebosch, Patricia	patricia.hulsebosch@gallaudet.edu	Hulsebosch	Patricia
115	Humberstone, Barbara	\N	Humberstone	Barbara
116	Hunt, Danielle	\N	Hunt	Danielle
117	Jimenez, Alvaro	\N	Jimenez	Alvaro
118	Johnson, Jessica	\N	Johnson	Jessica
119	Karch, Stephanie	stephanie.karch@gallaudet.edu	Karch	Stephanie
120	Kelly, Leonard	leonard.kelly@gallaudet.edu	Kelly	Leonard
121	Kendall, Carolin	\N	Kendall	Carolin
122	Kenton, Karen	\N	Kenton	Karen
123	Klein, Leslie	\N	Klein	Leslie
124	Koo, Daniel	daniel.koo@gallaudet.edu	Koo	Daniel
125	Kozak, L.Viola	\N	Kozak	L.Viola
126	Kozma-Spytek, Linda	\N	Kozma-Spytek	Linda
127	Krieger, Amanda	amanda.krieger@gallaudet.edu	Krieger	Amanda
128	Kroll, Judith	\N	Kroll	Judith
129	Kuntze, Marlon	\N	Kuntze	Marlon
130	Lam, Kay	kay.lam@gallaudet.edu	Lam	Kay
131	Lang, Harry	\N	Lang	Harry
132	LaSasso, Carol J.	carol.lasasso@gallaudet.edu	LaSasso	Carol J.
134	Lillo-Martin, Diane	diane.lillo-martin@gallaudet.edu	Lillo-Martin	Diane
135	Lindstrom, Eric	\N	Lindstrom	Eric
136	L&oacute;pez, Leonel	\N	L&oacute;pez	Leonel
137	Lukomski, Jennifer	\N	Lukomski	Jennifer
138	Lundberg, Daniel	daniel.lundberg@gallaudet.edu	Lundberg	Daniel
139	Mahshie, James	\N	Mahshie	James
140	Malzkuhn, Melissa	\N	Malzkuhn	Melissa
141	Mangrubang, Fred R.	fred.mangrubang@gallaudet.edu	Mangrubang	Fred R.
142	Martin, David	\N	Martin	David
143	Mason, Teresa C.	teresa.mason@gallaudet.edu	Mason	Teresa C.
144	Mather, Susan	\N	Mather	Susan
145	Mathur, Gaurav	gaurav.mathur@gallaudet.edu	Mathur	Gaurav
146	Maxwell-McCaw, Deborah	deborah.mccaw@gallaudet.edu	Maxwell-McCaw	Deborah
147	Mayberry, Rachel	\N	Mayberry	Rachel
148	Mej&iacute;a, Henri	\N	Mej&iacute;a	Henri
149	Merritt, Raymond	\N	Merritt	Raymond
150	Mertens, Donna	Donna.Mertens@gallaudet.edu	Mertens	Donna
151	Metaxas, Dimitris	\N	Metaxas	Dimitris
152	Metzger, Melanie	Melanie.Metzger@gallaudet.edu	Metzger	Melanie
153	Miller, Cara	cara.miller@gallaudet.edu	Miller	Cara
154	Miller, Margery	margery.miller@gallaudet.edu	Miller	Margery
155	Miller, Paul	\N	Miller	Paul
156	Mitchell, Ross E.	\N	Mitchell	Ross E.
157	Moloi, Connie	\N	Moloi	Connie
158	Mompremier, LaNi&ntilde;a	lanina.mompremier@gallaudet.edu	Mompremier	LaNi&ntilde;a
159	Moore, Elizabeth A.	elizabeth.moore@gallaudet.edu	Moore	Elizabeth A.
160	Moreno-Torres, Ignacio	\N	Moreno-Torres	Ignacio
161	Morere, Donna	\N	Morere	Donna
162	Morford, Jill	\N	Morford	Jill
163	Morris, Connie	\N	Morris	Connie
164	Mounty, Judith L.	judith.mounty@gallaudet.edu	Mounty	Judith L.
165	Mulrooney, Kristin	\N	Mulrooney	Kristin
166	Murray, Joseph J.	joseph.murray@gallaudet.edu	Murray	Joseph J.
167	Nance, Walter E.	\N	Nance	Walter E.
168	Napier, Jemina	\N	Napier	Jemina
169	Nead, Daniel	daniel.nead@gallaudet.edu	Nead	Daniel
170	Neidle, Carol	\N	Neidle	Carol
171	Norris, Virginia	\N	Norris	Virginia
172	Obiedat, Mohammad	mohammad.obiedat@gallaudet.edu	Obiedat	Mohammad
173	Oliva, Gina A.	gina.oliva@gallaudet.edu	Oliva	Gina A.
174	Olson, Russell	Russell.Olson@gallaudet.edu	Olson	Russell
175	Padden, Carol	\N	Padden	Carol
176	Pajka, Sharon	\N	Pajka	Sharon
177	Pajka-West, S.L.	sharon.pajka@gallaudet.edu	Pajka-West	S.L.
178	Palmer, Jeffrey L.	jeffrey.palmer@gallaudet.edu	Palmer	Jeffrey L.
179	Paludneviciene, Raylene	raylene.paludneviciene@gallaudet.edu	Paludneviciene	Raylene
180	Pandya, Arti	\N	Pandya	Arti
181	Pane, Erica	erica.pane@gallaudet.edu	Pane	Erica
182	Penna, David	David.Penna@gallaudet.edu	Penna	David
183	Penny, Jonathan	jonathan.penny@gallaudet.edu	Penny	Jonathan
184	Peoples, Kathleen	\N	Peoples	Kathleen
185	Petitto, Laura Ann	\N	Petitto	Laura Ann
186	Petronio, Karen	\N	Petronio	Karen
187	Pi&ntilde;ar, Pilar	pilar.pinar@gallaudet.edu	Pi&ntilde;ar	Pilar
188	Plaut, David	\N	Plaut	David
189	Plotkin, Rachael	\N	Plotkin	Rachael
190	Pribanic, Ljubica	\N	Pribanic	Ljubica
191	Pucci, Concetta	\N	Pucci	Concetta
192	Qi, Sen	\N	Qi	Sen
193	Quadros, Ronice	\N	Quadros	Ronice
194	Quinto-Pozos, David	\N	Quinto-Pozos	David
195	Rashid, Khadijat	khadijat.rashid@gallaudet.edu	Rashid	Khadijat
196	Rayner, Keith	\N	Rayner	Keith
197	Reilly, Charles	charles.reilly@gallaudet.edu	Reilly	Charles
198	Ricasa, Rosalinda	\N	Ricasa	Rosalinda
199	Riddick, Carol C.	carol.riddick@gallaudet.edu	Riddick	Carol C.
200	Roberson, Len	\N	Roberson	Len
201	Rogers, Buck	\N	Rogers	Buck
202	Rose, Rachel M.	rachel.rose@gallaudet.edu	Rose	Rachel M.
203	Rosental, Lynn	\N	Rosental	Lynn
204	Rotman, Rachel	\N	Rotman	Rachel
205	Roult, Loretta	\N	Roult	Loretta
206	Roy, Cynthia	cynthia.roy@gallaudet.edu	Roy	Cynthia
207	Russell, Debra	\N	Russell	Debra
208	Ryan, Donna F.	\N	Ryan	Donna F.
209	Sabila, Paul S.	paul.sabila@gallaudet.edu	Sabila	Paul S.
211	Santiago, Roberto	\N	Santiago	Roberto
212	Schalber, Katharina	\N	Schalber	Katharina
213	Schenker, Emily	emily.schenker@gallaudet.edu	Schenker	Emily
214	Schiller, James	\N	Schiller	James
215	Schneider, Erin	\N	Schneider	Erin
216	Schroeder, Vivienne	vivienne.schroeder@gallaudet.edu	Schroeder	Vivienne
217	Sclaroff, Stan	\N	Sclaroff	Stan
218	Sharp, Jerrod	jerrod.sharp@gallaudet.edu	Sharp	Jerrod
219	Shaw, Risa	risa.shaw@gallaudet.edu	Shaw	Risa
220	Sheridan, Martha A.	\N	Sheridan	Martha A.
221	Shutlz, Shirley	\N	Shutlz	Shirley
222	Siety, Blandine	\N	Siety	Blandine
223	Simms, Laurene	\N	Simms	Laurene
224	Singleton, Jenny	\N	Singleton	Jenny
225	Singuita, Inga	\N	Singuita	Inga
226	Slayback, Daniel	\N	Slayback	Daniel
227	Snyder, Henry David	henry.snyder@gallaudet.edu	Snyder	Henry David
228	Solomon, Caroline	caroline.solomon@gallaudet.edu	Solomon	Caroline
229	Stephen, Anika	\N	Stephen	Anika
230	Stone, Martin	martin.stone@gallaudet.edu	Stone	Martin
231	Students of LIN 812 class	\N	Students of LIN 812 class	\N
232	Sutton, Nadine	\N	Sutton	Nadine
233	Swabey, Laurie	\N	Swabey	Laurie
234	Tamaki, Chizuko	chizuko.tamaki@gallaudet.edu	Tamaki	Chizuko
235	Tekin, Mustafa	\N	Tekin	Mustafa
236	Thomas-Presswood, Tania	tania.thomas-presswood@gallaudet.edu	Thomas-Presswood	Tania
237	Thumann-Prezioso, Carlene	\N	Thumann-Prezioso	Carlene
238	Tobin, Joseph	\N	Tobin	Joseph
239	Torres, Franklin C.	franklin.torres@gallaudet.edu	Torres	Franklin C.
240	Traxler, Matthew	\N	Traxler	Matthew
241	Trevino, Rafael	\N	Trevino	Rafael
242	Trullender, Mallory Carrico	\N	Trullender	Mallory Carrico
243	Tucker, Compton J.	\N	Tucker	Compton J.
244	Tucker, Paula	\N	Tucker	Paula
245	Tweedie, M.S.	\N	Tweedie	M.S.
246	Valente, Joseph	\N	Valente	Joseph
247	Vanderheiden, Gregg	\N	Vanderheiden	Gregg
248	Veith, Mairin	mairin.veith@gallaudet.edu	Veith	Mairin
249	Villanueva, Miako	\N	Villanueva	Miako
250	Vogler, Christian	\N	Vogler	Christian
251	Vulje, Martina	\N	Vulje	Martina
252	Wagoner, Julie	julie.wagoner@gallaudet.edu	Wagoner	Julie
253	Wai Yan Rebecca, Siu	\N	Wai Yan Rebecca	Siu
254	Wang, Qi	qi.wang@gallaudet.edu	Wang	Qi
255	Weber, Brianne	brianne.weber@gallaudet.edu	Weber	Brianne
256	Weber, Samuel	\N	Weber	Samuel
257	White, Sandra	\N	White	Sandra
258	Wilbur, Ronnie	\N	Wilbur	Ronnie
259	Wilkinson, Erin	\N	Wilkinson	Erin
260	Williams, Leandra	\N	Williams	Leandra
261	Williams, Norman	\N	Williams	Norman
262	Wilson, Amy	\N	Wilson	Amy
263	Winiarczyk, Rowena	\N	Winiarczyk	Rowena
264	Winston, Betsy	\N	Winston	Betsy
265	Wolbers, Kimberly	\N	Wolbers	Kimberly
266	Woo, John	john.woo@gallaudet.edu	Woo	John
267	Wood, Kathy	\N	Wood	Kathy
268	Wu, Yu-Hsiang	\N	Wu	Yu-Hsiang
269	Yates, Michael	michael.yates@gallaudet.edu	Yates	Michael
270	Yuknis, Christina	christina.yuknis@gallaudet.edu	Yuknis	Christina
271	Ziskind, Brittney	\N	Ziskind	Brittney
272	Zodda, Jason J.	jason.zodda@gallaudet.edu	Zodda	Jason J.
133	Leigh, Irene W.	irene.leigh@gallaudet.edu	Leigh	Irene W.
273	Staff	\N	Staff	\N
\.


--
-- Data for Name: priority; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY priority (id, summary, description) FROM stdin;
1	Development of Signed Language Fluency	Research aimed at understanding the sensory, cognitive, affective, linguistic, pedagogical, and socio-cultural processes by which individuals acquire American Sign Language or other signed languages. This priority applies both to individuals acquiring signed language in childhood and to those who acquire or learn signed languages later in life.
2	Development of English Literacy	Research aimed at increasing understanding of the sensory, cognitive, linguistic, and socio-cultural processes by which deaf and hard of hearing individuals learn to read and write, plus the relationship between literacy learning and the signed, printed, and spoken languages used in the individual's home, school, community, and cultural environments.
3	Psycho-Social Development and Mental Health Needs	Research focusing on biological, neurological, psychological, and sociological aspects of deaf and hard of hearing people's psychosocial development and mental health throughout their life spans.
4	Teaching, Learning and the Communication Environment	Research on how pedagogical practices and accessibility of information affect learning for deaf and hard of hearing students.
5	School, Home, and Community Relationships	Research aimed at understanding home, school, and community relationships, school readiness, family and community involvement, and dynamics in homes and schools with deaf or hard of hearing members.
6	Transition through School and into Postsecondary Education and Work	Research aimed at understanding and identifying the transition processes of deaf and hard of hearing students through school and beyond into post-secondary education, work, and independent living.
7	History and Culture of Deaf People	Studies of Deaf peoples' history, cultures, creative productions, and signed languages, including research into and preservation of the contributions of visual and tactile ways of knowing and experiencing the world. This priority highlights studies of the origins and development of literature, the visual arts, and other creative, political, and social contributions of deaf people around the world.
8	Linguistics of Signed Languages	Linguistic studies of signed languages, including phonological, morphological, and syntactic phenomena as well as meaning construction, discourse, and variation. This priority supports cross-linguistic comparison among signed languages as well as research on language contact and historical change.
9	Interpretation and Translation	Research examining processes, practices, and pedagogy involved in interpreting for hearing, hard of hearing, deaf, and deaf-blind individuals in a broad range of settings. This priority relates to situations involving Deaf and hearing interpreters working with signed and spoken languages or other visual or tactile communication systems. In addition, this priority concerns literary and other translations involving signed languages.
10	Studies that Inform Public Policies and Programs	Research essential for the development, administration, and evaluation of public policies and programs affecting education, mental health, communication access, medicine, employment, and other services used by deaf and hard of hearing people throughout their lives.
11	Technologies that Affect Deaf and Hard of Hearing People	Studies of technology's impact on the lives of deaf and hard of hearing people, including research on and development of technologies and media aimed at enhancing communication.
12	Assessment	Research related to the development, translation, validation and practical application of appropriate tools, techniques, and models for assessing a wide range of characteristics, skills and abilities of deaf and hard of hearing people.
13	Diverse Deaf and Hard of Hearing Populations	Research that examines multicultural awareness, knowledge and/or skills as well as methods of social advocacy related to diverse deaf and hard of hearing children, youth, adults, their families and their communities. Diversity includes, but is not limited to differences of race, ethnicity, gender, age, creed, disability, socioeconomic status, sexual orientation, school experience, linguistic background, and immigration experience.
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY product (id, project, classification, description) FROM stdin;
98	847	19	Leigh, I. W. (2011, May). <i>Psychosocial aspects of cochlear implantation</i>. PowerPoint presentation at the University of Wellington, New Zealand.
1	742	37	Adams, E. (2011). <i>Hearing mothers' resolution of the identification of child hearing loss: An exploration of the reaction to diagnosis interview</i>. Unpublished dissertation, Gallaudet University, Washington, DC.
2	746	37	Anderson, M. L., Leigh, I. W., &amp; Samar, V. J. (2011). Intimate partner violence against deaf women: A review. Invited manuscriptfor <i>Aggression and Violent Behavior: A Review Journal, 16</i>(3), 200-206. DOI: 10.1016/j.avb.2011.02.006
3	863	19	Conley, W. (2011, April). <i>My journey as a deaf playwright</i>. Presentation to Deaf Performance Artistry course, Georgetown University, Washington, DC.
4	809	19	Williams, N. (2011, June). <i>National 911 SMS Relay Center update</i>. Presented at the NENA conference, Minneapolis, MN.
5	867	37	Adam, R., Collins, S., Metzger, M., &amp; Stone, C. (forthcoming). Deaf interpreters: International perspectives. <i>Studies in interpretation, vol. 10</i>. Washington, D.C.: Gallaudet University Press.
6	273	19	Allen, T. (2011, May). <i>Knowledge of English syntax and fluency in American Sign Language: Joint predictors of reading comprehension skill among deaf adult readers</i>. Poster session presented at the 2011 Association for Psychological Science Annual Convention, Washington, DC.
7	273	37	Allen, T., &amp; Anderson, M. (in preparation). <i>ASL and English syntax skills as independent predictors of reading comprehension among deaf college students</i>.
8	862	19	Andersen, C. F. (2011, February). <i>Revisiting the foundations of excellence</i>. Presentation at the Winter Meeting of the Gardner Institute for Excellence in Undergraduate Education, Atlanta, GA
9	862	19	Andersen, C. F. (2011, May). <i>Using the foundations of excellence to support accreditation</i>. Presentation to the Universidad Metropolitana, San Juan, Puerto Rico.
10	862	19	Andersen, C. F., &amp; Kluwin, T. N. (2011, February). <i>Understanding a path to graduation</i>. Presentation at the 30th Annual Conference on The First Year Experience and Students in Transition, Atlanta GA.
11	862	19	Andersen, C. F., &amp; Kluwin, T. N. (2011, November). <i>Understanding a path to graduation</i>. Presentation at the 6th Annual National Symposium on Student Retention, Mobile, AL.
12	862	19	Andersen, C. F., &amp; Kluwin, T. N.. (2010, December). <i>Keeping students on the path to graduation through organized assessment</i>. Presentation at the Middle States Association Annual Conference, Philadelphia, PA.
112	868	37	Mathur, G. &amp; Napoli, D. (Eds.) (2010). <i>Deaf around the world: Impact of language.</i> Oxford: Oxford University Press.
13	847	37	Anderson, M. L., &amp; Leigh, I. W. (2011). Intimate partner violence against deaf female college students. <i>Violence Against Women 17</i>(7), 822-834. DOI: 10.1177/1077801211412544.
14	746	37	Anderson, M. L., Leigh, I. W., &amp; Samar, V. J. (in preparation). <i>Prevalence and predictors of intimate partner violence against deaf women</i>.
15	847	37	Anderson, M., Leigh, I.W., &amp; Samar, V: Intimate partner violence against Deaf women: A review. <i>Aggression and violent behavior</i>, DOI 10.1016/j.avb.2011.02.006
16	273	19	Baker, S., &amp; Simms, L. (2011, April). <i>Deaf and hard of hearing children's visual language and developmental checklist</i>. Symposium paper presented at the Early Childhood Educators' Summit II, Gallaudet University, Washington, DC.
17	247	19	Barac-Cikoja, D., &amp; Karch, S. (2011, March). <i>Delay detection thresholds for speech feedback</i>. Invited presentation at the American Auditory Society Scientific and Technical Meeting, Scottsdale, AZ.
18	247	37	Barac-Cikoja, D., Karch, S., Kokx, M., &amp; Lancaster, L. (2011). Relative loudness of speech feedback during speech production. <i>The Journal of the Acoustical Society of America, 129</i>, 2588.
20	830	19	Betman, B. Sandtray Therapy: Seeing a Deaf Child's World, ADARA Conference, April 14, 2011
22	851	37	Brice, P. J. &amp; Adams, E. B. (2011) Developing concepts of self and other: Factors of risk and resilience. In D. Zand (Ed.), <i>Risk and resilience: Adaptation in the context of being deaf</i>. Springer.
23	273	19	Brown, B. T., Mainard, H., Byrnes, S., Clark, M. D., Agyen, S., Musyoka, M., Baker-Ward, L., &amp; Morris, G. (2011, May). <i>Gender differences in the earliest memories of deaf college students</i>. Presented at the annual meeting of the Association for Psychological Science, Washington, DC.
24	867	37	Brunson, J. (2011). Video relay service interpreters: Intricacies of sign language access. <i>Studies in interpretation vol. 8</i>. Washington, D.C.: Gallaudet University Press.
25	800	19	Chen Pichler, D. (2010).<i> Interesting questions from bimodal bilingual acquisition</i>. National Chung Cheng University, Chiayi, Taiwan.
26	447	37	Chen Pichler, D. (2011) Sources of handshape error in first-time signers of ASL. In D. J. Napoli &amp; G. Mathur (Eds.) <i>Deaf around the world</i>. Oxford University Press, pp. 96-121
27	447	37	Chen Pichler, D. (2011). <i>Aquisi&ccedil;&atilde;o de segunda modalidade de l&iacute;ngua: Aquisi&ccedil;&atilde;o de L2 e de LM</i>. X Ciclo de Palestras-Temas em Teoria Gerativa VI Workshop em L&iacute;nguas de Sinais e Bilinguismo dos Surdos, Brasilia, Brazil.
28	800	19	Chen Pichler, D., &amp; Lillo-Martin, D. (2010). <i>The development of bimodal bilingualism</i>. Keynote presentation, Michigan Linguistics Society, Flint, MI.
29	800	19	Chen Pichler, D., Lillo-Martin, D., &amp; de Quadros, R. (2010, September/October). <i>Sign language acquisition</i> (discussant).10th Congress on Theoretical Issues in Sign Language Research (TISLR), West Lafayette, IN.
30	615	37	Christiansen, J. B., &amp; Leigh, I. W. (2011). Cochlear implants and the Deaf community. In R. Paludneviciene &amp; I.W. Leigh (Eds.). <i>Cochlear Implants and the Deaf Community: Evolving Perspectives</i> (pp. 39-55). Washington, DC: Gallaudet University Press.
31	273	37	Clark, M. D., Gilbert, G., &amp; Anderson, M. (2011). Morphological knowledge and decoding skills of deaf readers. <i>Psychology, 2</I>, 109-116.
32	825	37	Gilbert, G. L., Clark, M. D., &amp; Anderson, M. L. (in press). Do Deaf individuals' dating scripts follow the traditional sexual script? <i>Sexuality and Culture</i>.
33	273	19	Clark, M. D., Musyoka, M., Erasmus, M., Korn, W., Brown, B. T., Baker-Ward, L., &amp; Morris, G. (2011, February. <i>Impact of language and gender on earliest memories</i>. Poster session presented at the annual meeting of the Association for College Educators, Deaf and Hard of Hearing, Fort Worth, TX.
135	849	19	Mertens, D. M. (2011, April). <i>What is the difference between traditional and transformative research?</i> Research colloquium at Bridgewater State University, Bridgewater, MA.
35	863	37	Conley, W. (2011). Five original black-and-white photographs: Sandstorm, Boathouse, San Juan Palms, Little House on the Suburban Prairie, House in Savage. <i>Folio&mdash;A Literary Journal at American University</i> 78-83.
36	863	19	Conley, W. (2011). <i>In Search of the deaf poet within</i>. Workshop/presentation at the International and European Deaf Theatre Festival, Equalizent, Vienna, Austria.
37	863	19	Conley, W. (2011). <i>The Universal Drum</i>. An original production performed by Arbos at the International and European Theatre Festival, Tanz Atelier Theatre, Vienna, Austria.
38	863	19	Conley, W. (2011, February/March). <i>Deaf theatre: A little history, some methodology, issues &amp; trends, and my journey as a Deaf playwright</i>. Video conference presentation at Japan ASL Signers Society (JASS)/Center for International Programs and Services, Gallaudet University. Participants included Tokyo and Kyoto, Japan.
39	863	19	Conley, W. (2011, July). <i>WORLDEAF Cinema Festival Welcome Video</i> (original script and direction). DVD presentation at the WORLDEAF Cinema Festival Awards Evening, Elstad Auditorium, Gallaudet University, Washington, DC.
41	837	19	Daggett, D., Paludneviciene, R., Hauser, P C., Dudis, P. &amp; Riddle, W. (2011, April). <i>Assessing the use of depiction in American Sign Language</i>. Poster presented at the inter-Science of Learning Center conference, Gallaudet University, Washington, DC.
42	800	19	de Quadros, R., Lillo-Martin, D., &amp; Chen Pichler, D. (2010, September/October). <i>Two languages but one computation: Code-blending in bimodal bilingual development</i>. 10th Congress on Theoretical Issues in Sign Language Research (TISLR), West Lafayette, IN.
44	217	37	Dodson, K. M., Blanton, S. H., Welch, K. O., Norris, V. W., Nuzzo, R. L., Wegelin, J. A., Marin, R. S., Nance, W. E., Pandya, A., &amp; Arnos, K. S. (2011). Vestibular dysfunction in deafness. <i>American Journal of Medical Genetics Part A, 155</i>, 993-1000.
45	854	37	Dudis, P. G. (in press). The body in scene depictions. In C. Roy (Ed.), <i>Discourse in signed languages</i>. Washington, DC: Gallaudet University Press
46	273	19	Dudis, P. G., Langdon, C. D., Nelson Schmitt, S. S., &amp; Young, L. M. (2010, October). <i>Coding depiction</i>. Presentation at the Theoretical Issues in Sign Language Research Conference, Purdue University, West Lafayette, IN.
47	766	19	Earth, B. (2011, July). <i>Bilingual deaf education: Techniques to teach reading and writing</i>. Presentation at the WFD World Congress in Durban, South Africa.
48	273	19	Feldman, J., &amp; Clark, M. D. (2011, May). <i>Psychometric development of two tests of phonological awareness for deaf individuals</i>. Poster session presented at the Annual Meeting of the Association for Psychological Science, Washington, DC.
49	861	37	Fennell, J. (2011). Men bring condoms, women take pills: Men's and women's roles in contraceptive decision-making. <i>Garden &amp; Society 25</i> (4): 496-521.
50	273	37	Freel, B. L., Clark, M. D., Anderson, M. L., Gilbert, G., Musyoka, M. M., &amp; Hauser, P. C. (2011). Deaf individuals' bilingual abilities: American Sign Language proficiency, reading skills, and family characteristics. <i>Psychology, 2</i>, 18-23.
51	6	37	Gallaudet Research Institute. (2011). <i>Regional and national summary report of data from the 2009-10 Annual Survey of Deaf and Hard of Hearing Children and Youth</i>, Washington, DC: GRI, Gallaudet University.
52	6	37	Gallaudet Research Institute. (2011). <i>State summary report of data from the 2009-10 Annual Survey of Deaf and Hard of Hearing Children and Youth</i>, Washington, DC: GRI, Gallaudet University.
53	842	19	<i>Garbage Out (GIGO)-Source Text Selection for Interpreting Assessment</i> presented at the European Forum of Sign Language Interpreters (EFSLI): September 2010.
54	842	19	<i>Garbage Out (GIGO)-Source Text Selection for Interpreting Assessment</i> presented to the American Sign Language Teachers Association (ASLTA): June 2011.
55	842	19	<i>Garbage Out (GIGO)-Source Text Selection for Interpreting Assessment</i> presented to the Registry of Interpreters for the Deaf (RID): July 2011.
56	857	19	Gerner de Garcia, B. A. (2010, October). <i>Educaci&oacute;n Biling&uuml;e para Sordos en los EEUU : La Actualidad (Bilingual Education of the Deaf in the U.S: The reality)</i>. X Congreso Latinoamericano y V Congreso Nacional de Educaci&oacute;n Biling&uuml;e para Sordos. Santiago de Chile.
57	857	19	Gerner de Garcia, B. A. (2011, April). <i>Immigrants among us: Building understanding and empathy in ourselves and our students</i>. Refereed presentation at the Maryland Multicultural Coalition conference, Fulton, MD.
58	857	19	Gerner de Garcia, B. A. (2011, May). <i> Bilingual education of the Deaf in the United States: The current situation</i>. Invited presentation at Universitat de Barcelona, Spain.
59	857	37	Gerner de Garcia, B. A. (in press). Creating language in a vacuum: Deaf children as creative communicators. In A. S. Yeung, C. F. K. Lee, &amp; E. L. Brown (Eds.), <i>Communication and language, vol. 7. International advances in education: Global initiatives for equity and social justice</i>. Charlotte, NC: Information Age Publishing.
60	542	37	Gerner de Garcia, B.A., Gaffney, C., Chacon, S., &amp; Gaffney, M. (2011). An overview of newborn hearing screening activities in Latin America. <i>Revista Panamericana de Salud P&uacute;blica/Pan American Journal of Public Health, 29</i>(3), 145-152.
61	850	19	Gibbons, E. (2011, February). <i>The role of the school psychologist in educational placement decisions</i>. Poster session presented at the meeting of the National Association of School Psychologists, San Francisco, CA.
62	849	37	Mertens, D. M. &amp; Wilson, A. T. (in press). <i>Program evaluation theory and practice: A comprehensive guide</i>. NY: Guilford.
63	850	19	Gibbons, E. (2011, June). <i>Parental influence on educational placement decisions</i>. Paper presented at the meeting of the American Society for Deaf Children, Frederick, MD.
64	784	19	Greenwald, B. H., &amp; Murray, J. J. (2011, April). <i>How the past informs the present: Intersections of Deaf history with Deaf studies</i>. Presentation at the Deaf Studies Today conference, Utah Valley University, Orem, UT.
65	273	19	Hall, W. C., Murphy, L., Hanumantha, S., &amp; Morere, D. (2011, March). <i>Testing English reading abilities of deaf college students: Justification for inclusion of English reading measures in the VL&sup2; Toolkit project</i>. Poster session presented at the 4th Annual inter-Science of Learning Center Student and Post-Doc Conference, Washington, DC.
66	273	19	Halper, E., Hall, W. C. &amp; Morere, D. (2011, June). <i>Short form of the Mental Rotation Test with Deaf Participants</i>. Poster session presented at the 9th annual American Academy of Clinical Neuropsychology Conference, Washington, DC.
67	273	19	Halper, E., Musyoka, M.M., Schatz, S. J. Hwang, Y., Witkin, G. &amp; VL&sup2; EELS Team. (2011, February). <i>A snapshot of parents' belief on deaf education: Preliminary findings from VL&sup2; EELS</i>. Paper presented at the Association of College Educators Deaf and Hard of Hearing (ACE-DHH) student conference, Ft. Worth, TX.
68	809	37	Harkins, J. E., &amp; Bakke, M. (2011). Communication technologies: Status and trends. In M. Marschark , P. E. Nathan, &amp; P. Spencer (Eds.) <i>The Oxford handbook of deaf studies, language, and education</i> (Vol. 1, pp. 425-438), New York, Oxford University Press.
69	273	37	Harmon, K. (2010). Addressing deafness: From hearing loss to deaf gain. <i>The Profession</i>, 124-130. New York, NY: The Modern Language Association Press.
70	273	37	Harmon, K., &amp; Nelson, J. (in press). <i>Deaf papers: Contemporary deaf American prose</i>. Washington, D.C.: Gallaudet University Press.
71	112	19	Harris R. (2011, July). <i>Academic use of ASL: Advancing linguistic and cognitive skills in deaf children using extended discourse</i>. Paper presented at the National American Sign Language Teachers Association National Bilingual Conference, Seattle, WA.
72	112	19	Harris, R. (2011, April). <i>Advancing linguistic and cognitive skills in deaf children using extended discourse</i>. Paper presented at the National American Sign Language and English Bilingual Early Childhood Education Summit, Washington, DC.
73	112	19	Harris, R. (2010, October). <i>A case study of extended discourse in an ASL/English preschool classroom</i>. Paper presented at Visual Language, Visual Learning Lecture Series, Gallaudet University, Washington, DC.
74	112	37	Harris, R. (2011). <i>A case study of extended discourse in an ASL/English bilingual preschool classroom</i>. Unpublished doctoral dissertation, Gallaudet University, Washington, DC.
75	112	19	Harris, R. (2011, February). <i>Academic discourse in English and ASL</i>. Paper presented at the ASL in Academics Lecture Series, Gallaudet University, Washington, DC.
76	112	19	Harris. R. (2010, October). <i>Extended discourse in an ASL/English bilingual early childhood classroom</i>. Paper presented at Kendall Demonstration Elementary School, Washington, DC.
77	273	37	Hauser, P., Clark, D., et al. (in preparation). <i> Prelingually deaf readers' basic processing skills for visual and printed information: Evidence from different orthographies</i> (title not final).
78	853	19	Horejes, T. (2011). <i>Kafkaesque social justice: A tribute to Paul K. Longmore</i>. Presentation to the Juvenile and Other Specialty Courts session at the Society for the Study of Social Problems, Las Vegas, NV.
79	853	19	Horejes, T. (2011). <i>(Re)defining disability policy frameworks: Connecting theory to praxis</i>. Presentation to the Disability and Society/The Americans with Disability Act: Twenty Years Later session of the American Sociological Association. Las Vegas, NV.
80	853	19	Horejes, T. (2011). <i>The language battleground within deaf education: Paradigm clashes toward pedagogies of language</i>. Presentation to the Disability and Social Life session at the American Sociological Association, Las Vegas, NV.
81	853	37	Horejes, T. (in press). <i> Languacultures: Constructions of deafness and deaf education</i>. Gallaudet University: Gallaudet University Press.
82	853	19	Horejes, T., Tobin, J., &amp; Valente, J. (2011). <i>Kindergartens as sites of acculturation for the deaf: A cross-comparative ethnographic study of two kindercultures</i>. Roundtable discussion at the Early Education and Child Development session at the American Educational Research Association annual conference. New Orleans, LA.
83	3	37	Hotto, S. (2011). Schools and programs in Canada: Canada directory listing and Canada program and services chart. <i>American Annals of the Deaf, 156,</i>(2), 152-157.
84	3	37	Hotto, S. (2011). Schools and programs in the United States: U.S. directory listing and U.S. program and services chart. <i>American Annals of the Deaf, 156,</i>(2), 88-151.
85	868	19	Hwang, S., Langdon, C., Mathur, G., &amp; Idsardi, W. (2010, November). <i>Windows into ASL perception: Cognitive restoration of reversed signs</i>. Poster session presented at the Neurobiology of Language Conference (NLC), San Diego, CA.
86	273	19	Hwang, S.-O, Langdon, C., Pucci, C., Mathur, G., &amp; Idsardi, W. (2011, April). <i>Windows for the visual processing of language: Evidence from compressed and locally-reversed ASL</i>. Poster session presented at the Cognitive Neuroscience Society meeting, San Francisco, CA.
87	273	19	Hwang, S.-O., Langdon, C., Mathur, G., &amp; Idsardi, W. J. (2010, November). <i>Windows into ASL perception: Cognitive restoration of reversed signs</i>. Poster presented at Neurobiology of Language Conference, San Diego, CA, USA.
88	273	37	Hwang, S.-O., Langdon, C., Pucci, C., Figueroa, V., Poeppel, D., Mathur, G., Idsardi, W. J. (in preparation). <i>Windows for the visual processing of language: Locally-time-reversed ASL implicates 300 ms-duration perceptual primitives</i>.
89	273	19	Hwang, S.-O., Langdon, C., Pucci, C., Mathur, G., &amp; Idsardi, W. J. (2011, March). <i>Temporal integration windows in the visual processing of language</i>. Symposium paper presented at the inter-Science of Learning Centers (iSLC) conference, Washington, DC, USA.
90	849	19	Mertens, D. M. &amp; Solomon, C. (2010, October). <i>Transformative evaluation mixed methods</i>. Presented at The Evaluation Conclave, New Delhi, India.
122	849	19	Mertens, D. M. (2010, December). <i>The evaluator's professional growth path</i>. Presentation at the Washington Evaluator's meeting, Washington, DC.
91	658	19	Karch, S., Hanks, W., Ackley, R .S., &amp; Brewer, C. (2010, March). <i> Effect of stimulus onset asynchrony in a (c)APD at-risk group</i>. Poster session presented at the American Auditory Society Annual Conference, Scottsdale, AZ.
93	809	19	Kozma-Spytek, L., Anderson, C., Tibbs, D., Galster, J., Beck, D., &amp; Taylor, B. (2011, June). <i>Telecoil Panel</i>. Presentation at the 2nd International Hearing Loop Conference, Crystal City, VA. (video recording found at: http://www.hearingloops.org/).
94	689	37	Leigh, I. W. &amp; Maxwell-McCaw, D. (2011). Cochlear implants: Implications for deaf identities. In Paludneviciene, R. &amp; Leigh, I.W. (Eds.) <i>Cochlear implants and the Deaf community: Evolving perspectives</i> (pp. 95-110). Washington, DC: Gallaudet University Press.
95	847	19	Leigh, I. W. (2011, August). <i>Pediatric cochlear implants: Implications</i>. PowerPoint presentation at the Great Start/Low Incidence conference, University of Pennsylvania, State College, PA.
96	847	19	Leigh, I. W. (2011, June). <i>Disability, deaf, and cochlear implants</i>. PowerPoint presentation at the Renwick Centre for Research &amp; Professional Education, Perth, Australia.
136	849	19	Mertens, D. M. (2011, July). <i>UNIFEM/IPEN transformative mixed methods evaluation</i>. Workshop presented in Almaty, Kazakhstan for UN Women.
97	847	19	Leigh, I. W. (2011, June). <i>Deaf identities and psychosocial implications</i>. PowerPoint presentation at the Renwick Centre for Research &amp; Professional Education, Sydney, Australia.
99	847	37	Leigh, I. W. (in press). Not just deaf: Multiple intersections. In R. Nettles &amp; R. Balter (Eds.) <i>Multiple minority identities: Why clinical implications matter</i>. New York: Springer.
100	847	19	Leigh, I. W., &amp; Toscano, R. M. (2011, April). <i>Health care careers task force for the Deaf and hard of hearing community: An update</i>. PowerPoint presentation at ADARA, San Diego, CA.
101	847	37	Leigh, I. W., &amp; Pollard, R. (2011). Mental health and deaf adults. In M.Marschark &amp; P. Spencer (Eds.). <i>Oxford handbook of Deaf studies, language, and education, 1</i> (pp. 214-226). New York: Oxford University Press.
102	847	37	Leigh, I. W., Morere, D. A., &amp; Pezzarossi, C. (submitted). Deaf gain: Beyond Deaf culture. In J. Murray &amp; H-D. Bauman (Eds.). <i>Deaf gain and the future of human diversity</i>. University of Minnesota Press.
103	849	19	Mertens, D. M. (2011, April). <i>The challenges surrounding creating a research project</i>. Invited presentation at the Association of College &amp; Research Libraries, Philadelphia, PA.
104	800	37	Lillo-Martin, D., de Quadros, R., Koulidobrova, H., &amp; Chen Pichler, D. (2010). Bimodal bilingual cross-language influence in unexpected domains. In J. Costa, M. Lobo &amp; F. Pratas (Eds.) <i>Language acquisition and development: Proceedings of GALA 2009</i>. Cambridge Scholars Publishing.
105	809	19	Livingston, D., &amp; Kozma-Spytek, L. (2011, April). <i>Cellular phone service provider website usability survey: Information on Hearing Aid Compatibility (HAC)</i>. Poster presentation at the American Academy of Audiology's annual meeting, AudiologyNOW!, Chicago, IL.
106	444	19	Martin, D.S. (2011, July). <i>Deaf pre-service teachers with hearing students: Analysis of reflective journal content</i>. Presented (on behalf of C.Neese Bailes &amp; P. Hulsebosch) at the XVI World Congress of the World Federation of the Deaf, Durban, South Africa.
107	629	19	Mathur, G. (2010, November). <i>Experimental approaches to sign language phonology</i>. Invited colloquium talk, GRI First Wednesday Seminar, Gallaudet University, Washington, DC.
108	868	19	Mathur, G. (2010, November). <i>Signs around the world</i>. Invited presentation at McDaniel College, Westminster, MD.
109	629	19	Mathur, G. (2010, November). <i>What makes sign languages unique? Building signs</i>. Invited lecture to ASL/Deaf Culture Lecture Series, University of Virginia, Charlottesville.
110	629	19	Mathur, G. (2010, October). <i>Experimental approaches to the phonetics and phonology of sign languages</i>. Invited presentation at Formal and Experimental Approaches to Sign Languages, &Eacute;cole Normale Sup&eacute;rieure, Paris.
111	629	19	Mathur, G. (2011, June). <i>Are sign languages processed differently than spoken languages?</i> DEC Colloquium, &Eacute;cole Normale Sup&eacute;rieure, Paris.
113	868	19	Mathur, G. (2011, June). <i>A formal approach to nonconcatenative morphology in signed languages</i>. Keynote presentation at Formal and Experimental Advances in Sign Language Theory (FEAST), University of Venice.
114	868	19	Mathur, G. (2011, June). <i>Argument structure in American Sign Language</i>. Invited presentation at the SIGMA Seminar, &Eacute;cole Normale Sup&eacute;rieure, Paris.
115	868	37	Mathur, G., &amp; Rathmann, C. (in preparation). The structure of sign languages. In V. Ferreira, M. Goldrick, &amp; M. Miozzo (Eds.) <i>Oxford handbook of language production</i>. Oxford: Oxford University Press.
116	868	37	Mathur, G., &amp; Rathmann, C. (2010). Two types of nonconcatenative morphology in signed languages. In D. Napoli &amp; G. Mathur (Eds.) <i>Deaf around the world: Impact of language.</i> Oxford: Oxford University Press.
117	868	37	Mathur, G., &amp; Rathmann, C. (in press). The features of verb agreement in signed languages. In R. Pfau, M. Steinbach, &amp; B. Woll (Eds.) <i>Handbooks of linguistics and communication sciences on sign languages</i>. Berlin: Mouton de Gruyter.
118	849	19	Mertens, D. M. (2010, September). <i>Social transformation and research</i>. Presentation at the NGA&#773; PAE O TE MA&#773;RAMATANGA Seminar at the University of Auckland, New Zealand.
119	849	19	Mertens, D. M. &amp; Bledsoe, K. (2011, June). <i>Transformative mixed methods evaluation</i>. Invited presentation at the American Evaluation Association and Centers for Disease Control annual evaluation conference in Atlanta, GA.
120	849	19	Mertens, D. M. &amp; Mager, V. (2010, October). <i>Gender and transformative evaluation</i>. Presented at the Evaluation Conclave, New Delhi, India.
121	849	37	Mertens, D. M. (2010). Social transformation and evaluation. <i>Evaluation Journal of Australasia, 10</i>(2), pp. 3-10.
123	849	19	Mertens, D. M. (2010, November). <i>Transformative lens applied to gender focused evaluation</i>. Presentation at the annual meeting of the American Evaluation Association, San Antonio, TX.
124	849	19	Mertens, D. M. (2010, October). <i>Context matters in social justice research</i>. Presentation to the Faculty of Public Health, Curtin University, Perth, Australia.
125	849	19	Mertens, D. M. (2010, October). <i> Linking Aboriginal health research with social justice goals</i>. Aboriginal Health Education and Research Unit, Geraldton, Australia.
126	849	19	Mertens, D. M. (2010, October). <i> Linking evaluation and social justice</i>. Presented as part of the Australasian Evaluation Society's Professional Development Programme in Melbourne, Brisbane, and Perth, Australia.
127	849	19	Mertens, D. M. (2010, September). <i> Credibility of research evidence from a transformative stance</i>. Videoconference presentation at the Building Research Capacity Seminar, Palmerston North, New Zealand.
128	849	19	Mertens, D. M. (2010, September). <i>Disability research and social transformation</i>. Presentation at the Donald Beasley Research Institute, Dunedin, New Zealand.
129	849	19	Mertens, D. M. (2010, September). <i>Introduction to transformative research</i>. Lecture at Massey University Graduate School, Palmerston University, New Zealand.
130	849	19	Mertens, D. M. (2010, September). <i>Transformative research with the disability community</i>. Presentation at Victoria University, Pipitea Campus, Wellington New Zealand.
132	849	37	Mertens, D. M. (2011). Publishing mixed methods research. <i>Journal of Mixed Methods Research, 5</i>(1), p. 3-6.
133	849	19	Mertens, D. M. (2011, April). <i>Mixed methods in evaluation: Theories and approaches</i>. Invited presentation at the Evaluation Curriculum group meeting in Hyderabad, India.
134	849	19	Mertens, D. M. (2011, April). <i>Research and social transformation</i>. Research colloquium at Bridgewater State University, Bridgewater, MA.
137	849	19	Mertens, D. M. (2011, June). <i>Current issues in mixed methods research</i>. Plenary presentation at the International Mixed Methods conference, Leeds, UK.
138	849	19	Mertens, D. M. (2011, June). <i>Evaluation of poverty reduction programs in Sri Lanka</i>. Invited presentation at the Centre for Poverty Analysis, Colombo, Sri Lanka.
139	849	19	Mertens, D. M. (2011, June). <i>Methodological guidance in evaluation for social justice</i>. Invited presentation at the Sri Lanka Evaluation Association's annual meeting, Colombo, Sri Lanka.
140	849	19	Mertens, D. M. (2011, June). <i> Mixed methods contribution to social change</i>. Presentation at the International Mixed Methods conference, Leeds, UK.
141	849	19	Mertens, D. M. (2011, May). <i>Publishing mixed methods research</i>. Presentation at the Vaal University of Technology, Vanderbijlpark South Africa.
142	849	19	Mertens, D. M. (2011, May). <i>The ethics of transformational research</i>. Presentation at the Vaal University of Technology, Vanderbijlpark South Africa.
143	849	37	Mertens, D. M. (in press). Integrating pathways: Research and policy making in pursuit of social justice. <i>International Review of Qualitative Research</i>.
144	849	37	Mertens, D. M. (in press). A transformative feminist stance: Inclusion of multiple dimensions of diversity with gender. In D. Seigart, S. Brisolera, S., &amp; S. SenGupta (Eds.) <i>Feminist evaluation and research: Advances in understanding and implementation</i>. NY: Guilford.
145	849	37	Mertens, D. M. (in press). Social transformation and evaluation. In M. Alkin (Ed.), <i>Roots 2nd ed</i>. Thousand Oaks, CA: Sage.
146	849	37	Mertens, D. M. (in press). Special education and gender. In J. A. Banks (Ed.), <i>Encyclopedia of diversity in education</i>. Thousand Oaks, CA: Sage.
147	849	37	Mertens, D. M. (in press). Transformative mixed methods: Addressing inequities. <i>American Behavioral Scientist</i>.
148	849	37	Mertens, D. M. (in press). When human rights is the starting point for evaluation. In M. Segone (Ed.), <i>Evaluation for equity: Fostering human rights and development results</i>. NY: United Nations.
149	849	37	Mertens, D. M., Cram, F., &amp; Chilisa, B. (Eds.) (in press). <i>Being and becoming indigenous social researchers</i>. Walnut Creek, CA: Left Coast Press.
150	849	37	Mertens, D. M., Harris, R., &amp; MacGlaughlin, H. (2010). Bringing a cultural lens to research with disability and deaf communities. <i>The Community Psychologist, 43</i>(4), pp. 11-13.
151	849	37	Mertens, D. M., Sullivan, M., &amp; Stace, H. (2011). Disability communities: Transformative research and social justice. In N. K. Denzin &amp; Y. S. Lincoln (Eds.) <i>Handbook of qualitative research, 4th ed</i>. Thousand Oaks, CA.
152	867	19	Metzger, M. (2010, November). <i>The interaction of frames and schema in ASL-English interpreted discourse</i>. Keynote presentation at II Congresso Nacional de Pesquisa em Tradu&ccedil;&atilde;o e Interpreta&ccedil;&atilde;o de L&iacute;ngua de Sinais (TILS II). UFSC, Florianopolis, Brazil.
210	856	37	Sass-Lehrer, M. (2011). Birth to three: Early intervention. In In M. Marschark &amp; P. Spencer (Eds.) <i>Handbook of deaf studies, language and education</i>. (Second edition). New York: Oxford University Press.
153	867	19	Metzger, M. (2010, November).<i>Discourse analysis and signed language interpretation</i>. Invited presentation at II Congresso Nacional de Pesquisa em Tradu&ccedil;&atilde;o e Interpreta&ccedil;&atilde;o de L&iacute;ngua de Sinais (TILS II). UFSC, Florianopolis, Brazil.
154	867	19	Metzger, M. (2011, February). <i>Discourse analysis and interpretation</i>. Invited presentation at 2011 Annual conference of Letras-Libras Tutors. UFSC. Florianopolis, Brazil.
155	867	19	Metzger, M. (2011, February). <i>Signed language interpretation research: A survey of four decades</i>. Presentation at 2011 Annual conference of Letras-Libras Tutors. UFSC. Florianopolis, Brazil.
156	846	37	Metzger, M. &amp; Roy, C.. (in press). Sociolinguistic studies of sign language interpreting. In R. Bailey, R. Cameron, &amp; C. Lucas (eds.), <i>The Oxford Handbook of Sociolinguistics</i>. New York: Oxford University Press.
157	867	37	Metzger, M., &amp; M&uuml;ller de Quadros, R. (forthcoming). Brazil: Signed language interpretation studies. <i>Studies in interpretation, vol. 9</i>. Washington, D.C.: Gallaudet University Press.
158	867	19	Metzger, M., Roy, C., M&uuml;ller de Quadros, R., &amp; Weininger, M. (2011, September-December). <i>Semin&aacute;rio de L&iacute;ngua de Sinais e Interpreta&ccedil;&atilde;o</i>. Universidade Federale de Santa Catarina and Gallaudet University.
159	855	19	Miller, B. D. (2010, December). <i>Behavior issues related to deaf and hard of hearing students</i>. Presentation at the Illinois Service Resource Center, Chicago, IL.
160	855	19	Miller, B. D. (2011, February). <i> Utility of curriculum-based approaches for students with hearing loss</i>. Presentation at the National Association of School Psychologists Annual Convention, San Francisco, CA.
161	855	19	Miller, B. D. (2011, July). <i>Utility of curriculum-based approaches for students with hearing loss</i>. Presentation at The Learning Center for the Deaf, Framingham, MA.
162	273	37	Miller, P., &amp; Clark, M. D. (in press). Phonology is not necessary to become a skilled deaf reader. <i>Journal of Developmental and Physical Disabilities</i>.
163	860	19	Moore, E. (2011, May). <i>Black deaf persons and their families: Ecological and systems perspectives</i>. Presented at the Hospital for Sick Children, Washington, DC.
164	812	19	Moore, E.A. (2011, April). <i>Black deaf administrators: Leadership issues and perceived challenges to organizational advancement</i>. Presented at Department of Administration and Supervision, Gallaudet University.
165	273	37	Morere, D., Allen, T., Halper, E., Hanumantha, S., Murphy, L., &amp; Hall, W. (pending). <i>Measuring literacy and its neurocognitive predictors among deaf individuals: An assessment toolkit</i>.
166	820	37	Morford, J. P, Kroll, J. F., Pi&ntilde;ar, P., &amp; Wilkinson, E. (under review). Bilingual word recognition in deaf and hearing signers: Effects of proficiency and language dominance on cross-language activation. <i>Second Language Research</i>.
167	621	37	Morford, J. P., Dussias, P. E., &amp; Pi&ntilde;ar, P. (in preparation). L2 sensitivity to verb subcategorization bias in deaf ASL-English bilinguals. To be submitted to <i>Language Learning</i>.
168	273	19	Morford, J. P., Kroll, J., Occhino-Kehoe, C., Pi&ntilde;ar, P., Twitchell, P. A., &amp; Wilkinson, E. (2010, November). <i>Written word recognition in hearing bimodal bilinguals: Does knowledge of a signed language affect spoken language processing?</i> Poster presented at the 9th High Desert Linguistics Society conference, Albuquerque, NM.
169	273	19	Morford, J. P., Kroll, J., Occhino-Kehoe, C., Pi&ntilde;ar, P., Twitchell, P. A., &amp; Wilkinson, E. (2010, November). <i>Written word recognition in hearing bimodal bilinguals: Does knowledge of a signed language affect spoken language processing?</i> Poster session presented at the 9th High Desert Linguistics Society conference, Albuquerque, NM.
170	273	19	Morford, J. P., Wilkinson, E., Pi&ntilde;ar, P., &amp; Kroll, J. F. (2011, March). <i>Effects of L2 proficiency on cross-language activation in deaf bilinguals</i>. Paper presented at the Society for Research in Child Development (SRCD) Preconference on the Development of Deaf and Hard of Hearing Children, Montreal, Canada.
171	820	37	Morford, J. P., Wilkinson, E., Villwock, A., Pi&ntilde;ar, P., &amp; Kroll, J. F. (2011). When deaf signers read English: Do written words activate their sign translations? <i>Cognition 118</i>(2), 286-292.
172	273	37	Morford, J. P., Wilkinson, E., Villwock, A., Pi&ntilde;ar, P., &amp; Kroll, J. F. (2011). When deaf signers read English: Do written words activate their sign translations? <i>Cognition, 118</i>(2), 286-292.
173	849	37	Munger, K., &amp; Mertens, D. M. (in press). Conducting research with the disability community: A rights based approach. In T. S. Rocco (Ed.), <i>Challenging ableism, understanding disability, including adults with disabilities in workplaces and learning spaces</i>. San Francisco, CA: Jossey Bass.
174	273	19	Murphy, L. (2011, June). <i>Preliminary findings from the Early Education Longitudinal Study</i>. Paper presented at American Society for Deaf Children (ASDC), Maryland School for the Deaf, Frederick, MD.
175	273	19	Murphy, L. (2011, June). <i>Use of verbal fluency tests with the deaf college students</i>. Poster session presented at the American Academy of Clinical Neuropsychology (AACN) conference, Washington, DC.
176	273	19	Murphy, L. (2011, May). <i>Investigating the Early Education Longitudinal Study (EELS) with a focus on nonverbal measures and demographics when administered to Deaf school-aged children</i>. Poster session presented at the Association for Psychological Science (APS) conference, Washington, DC.
177	273	19	Murphy, L., Hanumantha, S., &amp; Morere, D. (2011, May). <i>The VL&sup2; Early Education Longitudinal Study (VL&sup2;-EELS): Preliminary analysis of nonverbal measures and demographics administered to deaf preschool-aged children</i>. Poster session presented at the American Psychological Sciences (APS) 23rd Annual Convention, Washington, DC.
205	867	37	Roy, C., &amp; Metzger, M. (in press.) The first three years of a three year grant: When a research plan doesn't go as planned. In L. Swabey &amp; B. Nicodemus (Eds.) <i> Interpreting research: Theory and practice</i>. London: John Benjamins Press.
178	273	19	Musyoka, M. M., Anderson, M., Pucci, C., Clark, M. D., Miller, M., Rathmann, C. Kargin, T. &amp; Guldenoglu, B. (2011, March). <i>Systematic developmental skill-oriented investigation of poor and proficient deaf readers from different countries-US study</i>. Poster presented at the fourth annual inter-Science of Learning Centers (iSLC) conference, Washington, DC.
179	273	19	Musyoka, M., Agyen, S., Clark, M. D., Brown, B., Baker-Ward, L. &amp; Morris, G. (2011, May). <i>What do they remember? Content analysis of the earliest memories of native and non-native Deaf individuals</i>. Poster session presented at the annual meeting of the Association for Psychological Science, Washington, DC.
180	273	19	Myers, C., Clark, M. D., Anderson, M. L., Gilbert, G. L., Musyoka, M. M., &amp; Hauser, P. C. (2010, May). <i>Black deaf individuals' reading skills</i>. Poster session presented at the American Psychological Society, Boston, MA.
181	273	37	Myers, C., Clark, M. D., Gilbert, G., Musyoka, M. M., Anderson, M., Agyen, S., &amp; Hauser, P. C. (2010). The impact of culture, family characteristic, reading experiences, and educational level on Black Deaf individuals' reading skills. <i>American Annals of the Deaf, 155</i>, 449-457.
182	867	19	M&uuml;ller de Quadros, R., &amp; Metzger, M. (2011, February). <i>Cognitive control in intermodal bilingual interpreters</i>. Paper presented at the 33rd Annual Meeting of the German Linguistic Society, University of G&ouml;ttingen, Germany.
183	865	19	Nickerson, J., &amp; Pajka, S. (2011, July). <i>Creating a course with bite! Vampires: Their historical significance in literature, film, and pop culture</i>. Presented at the National Association for Media Literacy Education conference, Philadelphia, PA.
184	865	19	Nickerson, J., Kaika, J., Berent, G. P., &amp; Macko. (2011, March). <i>TEDS 101: Teaching English to deaf students in diverse settings</i>. Presented at Teachers of English to Speakers of Other Languages, New Orleans, LA.
185	865	19	Nickerson, J., Nelson, J., Pajka, S., &amp; Stremlau, T. (2010, November). <i>Challenging and engaging students in integrated courses</i>. Presented at the National Council of Teachers of English conference, Orlando, FL.
186	689	37	Paludneviciene, R. &amp; Leigh, I.W. (Eds.) (2011). <i>Cochlear Implants and the Deaf Community: Evolving Perspectives</i>. Washington, DC: Gallaudet University Press.
187	837	19	Paludneviciene, R. (2010). Sign language assessment. <i>Deaf Studies Digital Journal</i>, Fall 2010 issue.
188	809	19	Pitsikalis, V., Theodorakis, S., Vogler, C., &amp; Maragos, P. (2011, June). <i>Advances in phonetics-based sub-unit modeling for transcription, alignment and sign language recognition</i>. Presented at the Workshop on Gesture Recognition, in conjunction with the IEEE Conference on Computer Vision and Pattern Recognition, Colorado Springs, CO.
189	273	37	Pi&ntilde;ar, P., &amp; Dussias, G. (Under review). Deaf readers as bilinguals: An examination of the variables affecting deaf readers' print comprehension in light of current advances on bilingualism and second language processing. Manuscript submitted for publication in <i>Language and Linguistics Compass</i>.
190	822	37	Pi&ntilde;ar, P., Dussias, P. E., &amp; Morford, J. P. (in press). Deaf readers as bilinguals: An examination of deaf readers' print comprehension in light of current advances on bilingualism and second language processing. <i>Language and Linguistics Compass</i>.
192	621	19	Pi&ntilde;ar, P., Dussias, P. E., Morford, J. P., &amp; Carlson, M. (2011, June). <i>Contextualizing the reading patterns of deaf individuals within studies on bilingual sentence processing</i>. Paper presented at the 8th International Symposium on Bilingualism, University of Oslo, Norway.
193	824	19	Plotkin, R. (2011). <i>A pilot study of parenting experiences with raising deaf children: Understanding behavior problems and discipline practices</i>. Poster session presented at the 119th American Psychological Association Convention, Washington, DC.
228	851	37	Szymanski, C., &amp; Brice, P. (pending). Prevalence of school-administration reported diagnosis of deaf children with autism spectrum disorders in the US, 2007-2008. <i>Journal of Autism and Developmental Disabilities</i>.
194	217	37	Rendtorff, N. D., Lodahl, M., Boulahbel, I. R., Johansen, I. R., Pandya, A., Welch, K. O., Norris, V. W., Arnos, K. S., Bitner-Blindzicz, M., Emery, S. B., Mets, M. B., Fagerheim, T., Eriksson, K., Hansen, L., Bruhn, H., Moller, C., Lindholm, S., Ensgard, S., Lesperance, M. M., &amp; Tranebjaerg, L. (2011). Identification of p.A684V missense mutation in the <i>WFS1</i> gene as a frequent cause of autosomal dominant optic atrophy and hearing impairment. <i>American Journal of Medical Genetics Part A, 155</i>,1298-1313.
195	112	37	Ricasa. R. (2011). <i>Extended discourse in ASL between a deaf child and her teachers in an ASL/English preschool classroom: Implications for literacy development</i>. Unpublished doctoral dissertation, Gallaudet University, Washington, DC.
196	684	19	Roberson, L., Russell, D., &amp; Shaw, R. (2011, July). <i>Current practices in signed language interpreting in legal settings in North America</i>. Paper presented at the World Association of the Sign Language Interpreters in Durban, South Africa.
197	846	19	Roy, C. (2010, October). <i>Teaching the history of sign language interpreting</i>. Presentation at the Conference of Interpreter Trainers bi-annual meeting, San Antonio, TX.
198	846	19	Roy, C. (2011, September). <i>Teaching the history of sight translation</i>. European Forum of Sign Language Interpreters annual general meeting, Vietri Sul Mare, Italy.
199	846	37	Roy, C. (in press). Interpreter education series. In L. Swabey &amp; K. Malcolm (Eds). <i> Educating healthcare interpreters</i>. Washington, DC.: Gallaudet University Press.
200	846	37	Roy, C. &amp; Metzger, M. (in press). The first three years of a three year grant: When a research plan doesn't go as planned. In L. Swabey &amp; B. Nicodemus (Eds.), <i>Interpreting research: Theory and practice</i>. London: John Benjamins Press.
201	846	37	Roy, C. &amp; Napier, J. (in progress). <i>The signed language interpreting studies reader</i>. Proposal accepted by John Benjamins Press. Expected date of publication is 2012.
202	846	37	Roy, C. (Ed.) (2011). Discourse in signed languages. Vol 17 of the <i>Sociolinguistics of Deaf communities Series</i>. Washington, DC: Gallaudet University Press.
230	844	19	Thumann, M. (2011, July). <i> Identifying depiction: Teaching students to identify and use depiction</i>. Presentation at the American Sign Language Teachers Association, Seattle, WA.
203	867	19	Roy, C., &amp; Metzger, M. (2011, October). <i>Gathering a corpus of interpreting data: Challenges and solutions</i>. Invited presentation for Semin&aacute;rio de L&iacute;ngua de Sinais e Interpreta&ccedil;&atilde;o. Universidade Federale de Santa Catarina and Gallaudet University.
204	867	19	Roy, C., &amp; Metzger, M. (in press.) Sociolinguistic studies of sign language interpreting. In R. Bailey, R. Cameron, &amp; C. Lucas (Eds.) <i>The oxford handbook of sociolinguistics</i>. New York: Oxford University Press.
207	856	37	Sass-Lehrer, M. (2010). Early intervention for children birth to three: Families, communities and communication. In <i>The NCHAM e-book: A resource guide for early hearing detection and intervention</i>. National Center for Hearing Assessment and Management, Logan, UT: Utah State University.
208	856	19	Sass-Lehrer, M. (2010, October). <i>It takes a village: The power of family and professional collaboration</i>. Keynote presentation at the Maine Newborn Hearing Program's Early Intervention Conference.
209	856	19	Sass-Lehrer, M. (2010, October). <i>Secrets of adult learning</i>. Plenary speaker, Maine Newborn Hearing Program's Early Intervention Conference.
211	856	19	Sass-Lehrer, M. (2011, April). <i>Evidence-based practice: Making it work for all deaf and hard of hearing infants, toddlers, and their families</i>. Invited presentation, Ohio School for the Deaf Online Professional Development Seminar.
212	856	19	Sass-Lehrer, M. (2011, June). <i>Enhancing the professional development of early intervention specialists</i>. Featured speaker at Deaf Education Administrative Leadership Conference, Irving, TX.
213	856	19	Sass-Lehrer, M. (2011, March). <i>Evidence-based practice: Making it work for all deaf and hard of hearing infants, toddlers, and their families</i>. Invited presentation, Minnesota Regional EHDI Team Conference.
214	856	37	Sass-Lehrer, M., Benedict, B., &amp; Hutchinson, N. (2011). Preparing professionals to work with infants, toddlers, and their families: A hybrid approach to learning. <i>Odyssey: Early Interventon and Outreach, 12</i>, pp. 44-51.
217	844	19	Shaw, R., &amp; Thumann M. (2010, October). <i>Students' ASL academic papers: The how to, the benefits, and the F-U-N.</i> Paper presented at the Conference of Interpreter Trainers conference, San Antonio, TX.
218	830	37	Sheridan, M. A. &amp; Betman, B. (2011). <i>Training of school social workers to meet the educational and emotional needs of diverse deaf and hard of hearing children</i>. Annual performance report to the United States Department of Education.
220	864	37	Sheridan, M. A. (2011). Whose literacy is it, anyway? Strengths based guidelines for transforming the developmental environments of deaf children and adolescents. In D. H. Zand &amp; K. J. Pierce (Eds.) <i>Resilience in deaf children: Adaptation through emerging adulthood</i> (pp. 129-150). New York: NY. Springer.
221	847	37	Shultz, I., Taliaferro, G., Leigh, I. W., Geisinger, K., Kriegsman, K., &amp; Seekins, T. (in press). Guidelines for assessment and treatment of persons with disabilities. <i>American Psychologist</i>.
223	273	19	Singleton, J. L., Jones, G. &amp; Hanumantha, S. (2010, October). <i>Deaf-friendly research? Examining ethical research conduct through focus group methodology</i>. Poster session presented at the Theoretical Issues in Sign Language Research conference, Purdue University, West Lafayette, IN.
224	273	37	Singleton, J. L., Jones, G. &amp; Hanumantha, S. (submitted). Deaf friendly research? Foward ethical practice in research involving deaf participants. Manuscript submitted for publication to the <i>Journal of Empirical Research on Human Research Ethics</i>.
225	273	19	Singleton, J. L., Jones, G., &amp; Hanumantha, S. (2011, March). <i>Deaf-friendly science? Toward ethical practice in research involving Deaf participants</i>. Poster session presented at the fourth annual inter-Science of Learning Center (iSLC) Student Conference. Gallaudet University, Washington, DC.
226	851	19	Szymanski, C., &amp; Brice, P. (2011, August). <i>Children with autism and hearing loss</i>. Poster session presented at the 119th American Psychological Association Convention, Washington, DC.
227	851	19	Szymanski, C., &amp; Brice, P. (2011, February). <i>Utility of the GARS-2, SRS, and SCQ for adolescents with autism and hearing loss</i>. Poster session presented at the 44th annual Gatlinburg Conference on Research and Theory in Intellectual and Developmental Disabilities, San Antonio, TX.
229	844	37	Thumann, M. (2010). Book Review: Hearing mother father deaf: Hearing people in deaf families.<i> Sign Language Studies</i> (11) 4.
231	844	37	Thumann, M. (in press). Identifying depiction: Constructed dialogue and constructed action in ASL presentations. In C. Roy, (Ed.) <i>Discourse in signed languages</i>.
233	835	37	Valente, J., Bahan, B., &amp; Bauman, H-D. L. (2011). Sensory politics and the cochlear implant debates. In Paludneviciene, R &amp; Leigh, I. (Eds.) <i> Cochlear impants: Evolving perspectives</i>. Washington, DC: Gallaudet University Press.
236	809	37	Vanderheiden, G., Williams, N., Vogler, C., &amp; Hellstr&ouml;m, G. (July 1, 2011). <i>Comparison of short-term solutions to text mobile communicator access to 911</i>. Distributed to ATIS INES incubator group, and FCC Emergency Access Advisory Committee. Retrieved September 9, 2011, from: http://tap.gallaudet.edu/911text/
237	809	37	Vanderheiden, G., Williams, N., Vogler, C., &amp; Hellstr&ouml;m, G. (July 1, 2011). <i>Central 911SMS &amp; central all-text for emergency mobile text communication for people who cannot use voice calls</i>. Distributed to ATIS INES incubator group, and FCC Emergency Access Advisory Committee, July 1, 2011. Retrieved September 9, 2011, from: http://tap.gallaudet.edu/911text/
239	844	19	Villanueva, M., &amp; Thumann, M. (2011, June). <i>Blending linguistics into our interpretations: Depiction, constructed action, and constructed dialogue</i>. Workshop presentation for the Potomac Chapter of Registry of Interpreters for the Deaf, Washington, DC.
240	809	19	Vogler, C., Tucker, P. E., Williams, N., &amp; Harkins, J. (2011, June). <i>Accessible telecollaboration and web conferencing in the workplace</i>. Presented at the TDI 19th Biennial International Conference, Austin, TX.
244	783	37	Wang, Q. &amp; Solomon, C. (2010). Exploring blended learning to enhance biology instruction&mdash;Literature review and study design. In J. Sanchez &amp; K. Zhang (Eds.), <i>Proceedings of World Conference on E-Learning in Corporate, Government, Healthcare, and Higher Education 2010</i> (pp. 2268-2277). Chesapeake, VA: AACE.
245	783	19	Wang, Q. &amp; Solomon, C. (2011.) <i>Exploring blended learning to enhance biology instruction&mdash;Instructional design and implementation</i>. Submitted as a full-paper presentation to the World Conference on E-Learning in Corporate, Government, Healthcare, and Higher Education.
246	842	19	Webinar Series: <i>Garbage In = Garbage Out (GIGO)-Source Text Selection for Interpreting Assessment 1, 2, &amp; 3</i>: This series of webinars explored the selection and use of source texts in interpreting education and evaluation. GIGO 1 (June 2010) and 2 (March 2011) focused on the input of content experts in interpreting education and research; GIGO 3 (June 2011) expanded the audience to include a broader group of interpreting educators, agencies, and consumers in order to gather input about current practices of source text selection for education and evaluation of interpreting skills. Each webinar resulted in additional data for analysis about this topic, including both qualitative and quantitative data concerning practices about, uses of, and actual review and ratings of selected source texts.
247	847	37	Weiselberg, E., &amp; Leigh, I. W. (2011). Medical and psychosocial considerations for the Deaf adolescent. In M. Fisher, E. Alderman, R. Kreipe, &amp; W. Rosenfeld (Eds.) <i>The textbook of adolescent health care</i>. Elk Grove, IL: American Academy of Pediatrics.
248	809	19	Williams, N., &amp; Vogler, C. (2011, June). <i>Interoperability across videophones, VRS, and off-the-shelf equipment</i>. Presented at the TDI 19th Biennial International Conference, Austin, TX.
249	736	37	Wolf Craig, K., Crisologo, A., Anderson, M., Sutton, N., &amp; Leigh, I.W. (2011). Reliability and validity of the Adapted Cope Scale with Deaf College Students. <i>Journal of the American Deafness and Rehabilitation Association, 44</i>(3), 116-133.
250	848	19	Yuknis, C. (2011, April). <i>What happens when adolescents who are deaf revise their own texts?</i> Presented at the Council for Exceptional Children Conference, National Harbor, MD.
251	848	19	Yuknis, C. (2011, June). <i> Literacy outcomes for students who are deaf</i>. Presented at the Convention of American Instructors of the Deaf Conference, Ft. Worth, TX.
252	823	37	Ziskind, B. (2010). <i>Crying and helping behaviors: Gender differences with deaf adults</i>. Unpublished dissertation, Gallaudet UniversityWashington, DC.
19	826	29	Bauman, D. (in progress). <i>Senior language assessment report: ASL</i>. Prepared for Faculty Senate, Gallaudet University, Washington, DC.
21	420	29	Blennerhassett, L. (2011). <i>School Psychology Practicum Candidates and Interns: An Analysis of Time in Roles</i>. (NASP Data-Based Program Report). Washington, DC: Gallaudet University.
92	809	29	Kozma-Spytek, L. (2010, October). <i>HLAA Webinar: The role of telecoils in hearing device use</i>. Retrieved September 9, 2011, from: http://www.hearingloss.org/Community/transcripts.asp)
34	836	29	<i>Classroom Discourse Observation Report</i>. (2011, May). Submitted to the Faculty Welfare Committee.
40	826	29	Coye, T. (2011, July). <i>Senior language assessment report: English</i>. Prepared for Faculty Senate, Gallaudet University, Washington, DC..
43	831	29	<i>Deaf Studies Digital Journal</i>. Issue #2, Fall 2010. Accessed at: dsdj.gallaudet.edu
131	849	29	Mertens, D. M. (2011). Mixed methods as tools for social change. <i>Journal of Mixed Methods Research</i>, 1-3.
191	830	29	Sheridan, M. A. (2011). <i>Enhancing the curriculum: Evidence based social work practice and empirically supported treatments</i>. Gallaudet University Faculty Development Grant final report.
206	845	29	Russell, D., Shaw, R., &amp; Malcolm, K. (2010). Effective strategies for teaching consecutive interpreting. <i>International Journal of Interpreter Education</i>. Dissertation Abstract.
215	856	29	Sass-Lehrer, M., Benedict, B., &amp; Interdisciplinary Work Group (2011). <i>Deaf and hard of hearing infants, toddlers and their families: Collaboration and leadership</i>. A graduate certificate program proposal submitted to the Council on Graduate Education, Gallaudet University. Approved: April 2011.
216	845	29	Shaw, R. (2010). Meaning in context: The role of context and language in narratives of disclosure of sibling sexual assault. <i>International Journal of Interpreter Education</i>. Dissertation Abstract.
219	830	29	Sheridan, M. A. (2011). <i>Enhancing the curriculum: Evidence based social work practice and empirically supported treatments</i>. Gallaudet University Faculty Development Grant proposal.
222	800	29	<i>Sign Language Development in Deaf and Hearing Children</i>, Linguistics Society of America Summer Institute course, University of Colorado, Boulder, CO.
232	844	29	Thumann, M. (2010). Identifying depiction in American Sign Language presentations, <i>International Journal of Interpreter Education</i>. Dissertation Abstract.
234	809	29	Vanderheiden, G., &amp; Vogler, C. Reply to Comments filed in the Matter of Implementation of Sections 716 and 717 of the Communications Act of 1934, as enacted by the Twenty-First Century Communications and Video Accessibility Act of 2010, CG Docket 10-213, May 24, 2011. Retrieved from: http://fjallfoss.fcc.gov/ecfs/comment/view?id=6016787631
235	809	29	Vanderheiden, G., &amp; Vogler, C. Comments filed in the Matter of Implementation of Sections 716 and 717 of the Communications Act of 1934, as enacted by the Twenty-First Century Communications and Video Accessibility Act of 2010, CG Docket 10-213, April 26, 2011. Retrieved from: http://fjallfoss.fcc.gov/ecfs/comment/view?id=6016478422
238	852	29	VanGilder, K. (2011). <i>Making Sadza with deaf Zimbabwean women: A missiological reorientation of practical theological method toward self-theologizing agency among subaltern communities</i>. Dissertation, Boston University.
241	809	29	Vogler, C., Vanderheiden, G., &amp; Phillips, A. Comments filed in the Matter of the Review of the Emergency Alert System, EB Docket 04-296, June 21, 2011. Retrieved from: http://fjallfoss.fcc.gov/ecfs/comment/view?id=6016830280
242	809	29	Vogler, C., Williams, N., &amp; Vanderheiden, G. Comments filed in the Matter of the Application of New and Emerging Technologies for Video Relay Service Use, CG Docket 10-51, April 4, 2011. Retrieved from: http://fjallfoss.fcc.gov/ecfs/comment/view?id=6016375091
243	809	29	Vogler, C., Williams, N., &amp; Vanderheiden, G. Reply to Comments filed in the Matter of the Application of New and Emerging Technologies for Video Relay Service Use, CG Docket 10-51, April 18, 2011. Retrieved from: http://fjallfoss.fcc.gov/ecfs/comment/view?id=6016377546
\.


--
-- Data for Name: profession; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY profession (id, name) FROM stdin;
1	Alumnus
2	Community Advisor, Author
3	Director
4	Independent Community Advisor
5	Independent Scholar
6	Private Consultant, California
7	Professor Emeritus
8	Retired
9	Student
10	Teacher
11	Trilingual Interpreter
\.


--
-- Data for Name: program; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY program (id, name) FROM stdin;
1	Diagnostic and Evaluation Services
2	District of Columbia Space Grant Consortium
3	General Studies Requirement (GSR)
4	Molecular Genetics Laboratory
5	Technology Access Program (TAP)
7	Women Studies
8	Trace Research &amp; Development Center
9	Faculty Development
\.


--
-- Data for Name: project; Type: TABLE DATA; Schema: public; Owner: kjcole
--

COPY project (id, title, product_only, is_student_project, is_clerc_project, status, begin_date, end_date, description, this_fy, last_fy) FROM stdin;
1	Adolescents and Cochlear Implants: Psychosocial Issues	f	f	f	2	August 1, 2002	March 2010	This project compares the psychosocial adjustment of a sample of deaf adolescents who have had cochlear implants for at least three years, with non-implanted adolescents. Having the implant, in conjunction with several demographic variables, is hypothesized to correlate with deaf-hearing cultural identity as well as social and academic functioning. Results essentially indicated that adolescents with and without cochlear implants scored similarly on psychosocial measures.This project currently has one publication and has been presented at several conferences.	f	t
572	Assessment of Deaf and Hard of Hearing Children and Adolescents	f	f	f	1	2005	No set date	The researchers are writing a book on cognitive assessment of deaf and hard of hearing children based on current research.	t	t
34	Creation of a DNA Repository to Identify Deafness Genes	f	f	f	1	July 1, 2001	No set date	This project is a collaborative effort between Gallaudet (Department of Biology and the Gallaudet Research Institute) and the Department of Human Genetics at the Medical College of Virginia to establish a large repository of DNA samples from deaf individuals and their families. These DNA samples are screened for common forms of deafness and then made available to other investigators for studies of hereditary deafness.	t	t
2	An Alternative Perspective in Research and Evaluation: Feminists, Minorities, and Persons with Disabilities	f	f	f	1	January 1, 1992	No set date	The researchers are examining the meaning of a transformative perspective in educational research and evaluation. An inclusive perspective is based on a body of scholarly work that is sometimes labeled as transformative and is characterized by the writings of feminists, ethnic/racial minorities, people with disabilities, and others who work on behalf of social justice and human rights. The research explores the theoretical and methodological implications of this perspective for research and evaluation and for teaching research methods classes.	t	t
50	Disability Interest Groups in Europe	f	f	f	1	June 1, 2001	No set date	This project involves a survey of various disability organizations in Europe, including in-depth follow-up interviews wherever possible, in an effort to evaluate the impact of Europeanization on the organizations' funding, resources, professionalization, accountability to membership, and choice of tactics. Researchers are working on a draft book/article manuscript which is now largely complete; awaiting last revisions from co-authors.	t	t
76	Interactive Interpreting	f	f	f	2	June 1, 2005	No set date	The purpose of this project is to investigate face-to-face interpreted encounters in medical, mental health, legal, educational, government, and business settings from a discourse perspective. The researchers propose to video-record 15-30 interpreted encounters and analyze them using discourse analysis methodology from various linguistic perspectives. They aim to account for interpreter-mediated conversation as a mode of communication, interpreters' perceptions of their responsibilities, and what interpreters do and what others expect them to do in face-to-face, institutional encounters.	f	f
143	Deaf and Hard of Hearing Social Workers: Licensing and Employment Equity	f	f	f	2	January 1, 2003	No set date	This project gathered information about deaf and hard of hearing social workers' experiences with: preparation for practice in their chosen profession, licensure, licensing tests, and professional employment. The project entailed the administration of a survey to a national sample of deaf and hard of hearing individuals who had received academic preparation in the field of social work. Anecdotal evidence had indicated that many deaf and hard of hearing social workers around the country were locked into entry-level positions, or were choosing jobs that did not require licensing because they could not pass the requisite tests. The sample, collected via mailing lists, web postings, and snowball strategies included, but was not limited to, graduates of the Gallaudet University Department of Social Work. During FY 2005, data analysis was completed on 34 respondents.<br><br>Update October, 2009: No further action on this project. Update August 24, 2010: The investigators are closing the project at this time. If it is resumed at a future time, the instrument and procedures will be revisited and updated.	f	t
167	Gender Issues in the Writings of Mme De Gouges and Mme De St&auml;el	f	f	f	1	No set date	No set date	This is an ongoing project which will result in the publication of a comparative study of the impact of gender politics in works by two daughters of The Enlightenment from opposite socio-economic backgrounds. The purpose of the study is to shed light on possible intersections between the politics of gender and class and the discourse of equality of The Enlightenment, a topic which is currently being debated among scholars in the fields of Women's Studies and 18th Century Studies. On November 7, 2008, the researcher delivered a paper entitled <i>Gender and politics in Revolutionary France</i> at a regional conference of the American Society for 18th Century Studies (ASECS) at Georgetown University in Washington, DC. This paper will be included in the last chapter of the comparative study which includes three chapters: Autobiographical Writings; Fictional Writings; Non-Fictional Writings. <br><br> The researcher submitted an abstract to present at the regional conference of the American Society for 18th Century Studies in October of 2009. The abstract was approved and the paper entitled<i> Violating sacred intimacy: Reading Marie-Antoinette's and Mme de St&auml;el's Correspondence</i> will be delivered on October 10, 2009.	f	t
305	Possessives and Existentials in ASL	f	f	f	2	September 2005	No set date	Led by Dr. Ulrike Zeshan of the University of Centre Lancashire and the International Centre for Sign Language and Deaf Studies located in Preston, UK, researchers in this project are participating in a large cross-linguistic study on possessives and existentials in 25 different sign languages of the world. Sign language data collected at Gallaudet is being compared with that of other sign languages (specifically, Croatian Sign Language and Austrian Sign Language) and will contribute to a future online video database hosted at the Max Plank Institute (MPI) for Psycholinguistics.	f	t
771	Relationship between Deaf Identity and D/deaf and Hard of Hearing Individuals' Attitudes Toward Hearing Dog Use	f	f	f	1	August 2007	August 2011	This project was completed as a function of the requirements for the doctoral dissertation in Psychology.	f	t
711	Intimate Partner Violence in the Deaf Community: A Quantitative Examination	f	f	f	1	April 2009	\N	This study is a web-based survey of deaf individuals and their relationships to explore intimate partner violence.	f	t
779	Gerontology-Leisure Research: The Past and the Future	f	f	f	\N	\N	\N	Identifies shortcomings of gerontology-leisure research and suggests ways to create and monitor evidence-based recreation interventions that enhance the well-being of elders.	f	t
862	ANDERSEN	t	f	f	\N	\N	\N	(product only)	t	f
851	BRICE	t	f	f	\N	\N	\N	(product only)	t	f
70	Through Deaf Eyes Documentary	f	f	f	1	October 1, 1995	No set date	In FY 2010-2011 the <i> Through Deaf Eyes documentary</i> continued to be used in classrooms and at public events. Known public screenings were held in College of the Desert, Palm Desert, CA in October, 2010 and in Mobile Alabama on August 23rd, by the Alabama Institute for the Deaf and the Blind Mobile Regional Center. Arrangements were made to have a "master" copy, without captions, sent to the National Association of the Deaf in Poland, so that they could layer in Polish captions. (June, 2011)	t	t
144	Using Corona Program Imagery to Study Bolivian Deforestation, Mexican Butterfly Habitat and Himalayan Glacier Changes Since the 1960s	f	f	f	1	May 1, 2002	No set date	Software is being written and calibrated to Landsat imagery that will correctly image Corona Program imagery from spy satellites of the 1960s onto a rectangular map grid. Images will be composed into a mosaic and used to study deforestation and land use change since the 1960s. This year the software was inspected and rewritten to reduce error. Geolocation errors were reduced from 150 meters to 50 meters which is close to the resolution of the calibration data.	t	t
204	Empowering Deaf Communities in Latin America and Africa	f	f	f	1	October 1999	No set date	This project is an ongoing collaboration to pilot a "deafness enhanced" participative leadership model which promotes a shared agenda of self-empowerment and "leadership in action" through deaf peer mentoring. The mentoring occurs via exchanges and service learning opportunities within deaf communities in the U.S., in five Latin American countries, Argentina, Chile, Colombia, Costa Rica, and Mexico, and in Cameroun, Gabon, and Mali, Africa. In 2009, this leadership model&mdash;which is known as IDP (International Deaf Partnerships)&mdash;was expanded to Mali. During the summer five undergraduate students successfully completed their 10-week internships in Latin America. One interned in Chile and four in Costa Rica. For the second time one of the students interned at the Office of Human Rights of the Costa Rica Government and was asked to produce a video to educate deaf and hearing Costa Ricans about the UN Convention of Rights for People with Disabilities. Two students interned in Francophone, Africa, one student interned at a new partner organization, Cameroun Deaf Empowerment Organization and the other student at Bamako Deaf School in Mali. In conjunction with a former Gallaudet employee who is now in the Foreign Service, Kathleen Peoples, this student worked on a proposal to improve deaf education in Mali, which was presented to the Minister of Education. Service learning projects on behalf of a deaf school in Bogot&aacute;, Colombia and a deaf youth group in Guatemala City, Colombia were conducted in the spring of 2010 and fall of 2009. A service project focusing on providing relief to Deaf Haitians after the Jan. 12, 2010 earthquake was conducted in the spring of 2010.<br> <br>In the spirit of solidarity with deaf communities in developing countries, in the summer of 2010 the researcher developed and taught a new course focusing on "What's Next for Deaf Haiti?" and started preliminary research on the topic. In the same spirit, in the fall of 2009 the researcher co-taught a course on Africa and collaborated with a deaf organization in Uganda, Deaf Link Uganda and its fight against HIV/AIDS among deaf people in Uganda.	f	t
217	Genetic Deafness in Alumni of Gallaudet University	f	f	f	1	April 15, 2004	No set date	This project, designed to extend the 1898 study of deaf families by Professor E.A. Fay, then Vice-President of Gallaudet University, is a collaborative effort between Gallaudet's Department of Biology and the Department of Human Genetics at the Medical College of Virginia. The goal is to identify and characterize rare genes that interact to cause deafness. A novel molecular genetic approach to identifying these genes will be used in the deaf offspring of deaf parents.	t	t
218	Potential Societal Impact of Advances in Genetic Deafness	f	f	f	1	September 1, 2003	No set date	This project was designed to assess the impact of testing for genes for deafness on the Deaf community and hearing parents of deaf and hard of hearing children. The first goal was to conduct focus groups and perform a survey of these groups to determine the attitudes and concerns related to genetics technologies and advances in the identification of genes for deafness. A second goal was to assess the impact of genetic testing on deaf couples by measuring its influence on selection of a marriage partner.	t	t
351	Rehabilitation Engineering Research Center on Telecommunications Access	f	f	f	2	October 1, 2004	September 30, 2009 (NCE to 9/30/10)	The primary mission of the Telecommunications Access RERC is to advance accessibility and usability in existing and emerging telecommunications products for people with all types of disabilities. Telecommunications accessibility is addressed along all three of its major dimensions: user interface, transmission (including digitization, compression, etc.), and modality translation services (relay services, gateways, etc.). The RERC looks at advances that have both short- and long-term outcomes related to assistive technologies, interoperability, and universal design of telecommunications. The research and development program of this RERC covers three areas: <br><br>1. Development of tools, techniques, and performance-based measures that can be used to evaluate current and evolving telecommunication technologies. Technologies of interest include video telephones and other video communication products and voice telecommunications products such as wireless and cordless phones. <br>2. Projects in cooperation with industry to improve accessibility of digital cellular and cordless wireline phones as well as other emerging products.<br>3. Improving access to emerging telecommunications, particularly digital and IP based systems. Projects in this area will:<br>a) identify techniques to alert people about possible emergencies and to ensure accessible communication in emergency or crisis situations. (A State of the Science Conference on Accessible Emergency Notification and Communication was held Nov. 2-3, 2005); b) seek solutions for the current incompatibility issues around text communications and ways to build the necessary capabilities into mainstream technologies, to allow them to evolve to new text, speech, and visual communication technologies. (This will enable deaf and hard of hearing people to communicate over the mainstream technologies in the modes that work best for them.); and, c) develop guidelines and reference materials to help mainstream telecommunications manufacturers build their regular products in ways that allow individuals with visual, hearing, physical, and cognitive disabilities to be able to use them.	f	t
360	Disability Protests	f	f	f	1	1995	No set date	The investigator in this project has been examining protests related to disability, using written accounts of protest events.	f	t
361	Deaf People and Employment	f	f	f	2	1982	No set date	This project examines various aspects of deaf people's employment, especially regarding gender differences.	f	t
764	Carlson's Trophic State Index (TSI) of Shenandoah Valley Reservoirs	f	f	f	2	May 24, 2010	July 30, 2010	This project determines the water quality of Shenandoah Valley and George Washington National Forest reservoirs, using the Carlson's Trophic State Index (TSI) classification system and water chemistry analysis in collaboration with James Madison University in Harrisonburg, VA. Two undergraduate students, one from Gallaudet University and the other from the National Technology Institute for the Deaf at RIT, learned how to collect water samples, conduct water chemistry measurements in the field, and use instrumentation in the laboratory to analyze the water samples. There are three variables that detemine the trophic state of the reservoirs: chlorophyll a, total phosphorus, and the Secchi disk method. Results were reported to the Virginia Department of Game and Inland Fisheries (VDGIF) who then created proposals on how to manage the reservoir's water quality.	f	t
714	Normative Data for the Calibration of VNG Equipment	f	f	f	2	May 2009	May 2010	The purpose of this study was to find normative data for the calibration of videonystagmography (VNG) equipment so that patients who are unable to perform the calibration task on this equipment can still undergo other parts of this test battery to evaluate their vestibular and balance functions. Data from 20 adult participants were collected and the mean was calculated for each gender, to be used when an individual unable to perform calibration tasks need VNG testing.	f	t
864	SHERIDAN	t	f	f	\N	\N	\N	(product only)	t	f
405	Implementing Bilingual Education for the Deaf in Catalonia: Beliefs About Critical Knowledge Needed in Bilingual Classrooms with Deaf Children	f	f	f	2	March 1, 2006	No set date	The purpose of this investigation is to study what teachers and support staff believe is the essential knowledge needed for the successful implementation of bilingual education for the deaf in Spain. Their views as practitioners and insiders in bilingual settings for deaf students are invaluable for understanding the crucial elements of bilingual deaf education and improving the support and professional development of current and future teachers of the deaf. In the past 10 years, deaf adults have increasingly been incorporated in these settings as sign language teachers and as teacher assistants. The lack of secondary and post-secondary educational opportunities for deaf adults in Spain and the rest of Europe is an obstacle to increasing the number of teachers who are deaf. However, it is critical to explore the roles of deaf adults and the knowledge they bring to bilingual settings, as well as ideas for increasing the supply of deaf teachers. The University of Barcelona has recently established programs offering a specialization in deaf education and training of sign language teachers (for Deaf adults) and sign language interpreters. This study contributes to comparative educational studies in deaf education by involving American and Spanish co-investigators. This collaboration provides both insider and outsider perspectives. Typically, comparative education studies in deaf education involve either a researcher close to the situation (insider) who knows the context, or a foreigner (outsider) bringing new eyes, but limited contextual knowledge. Two interview protocols were designed: one for individual interviews and one for focus groups. The researchers conducted two focus groups at two schools for the deaf. Additionally, they carried out a number of individual interviews and also distributed the questionnaire used for individual interviews to several schools for written responses.	f	t
430	Investigating the Social, Economic, Political, and Cultural Issues That Affect the Lives of Deaf People in Argentina, Costa Rica, and Mexico	f	f	f	1	April, 2006	No set date	Despite a steady increase in the interest in disability and development, only quantitative research exists, and much of it remains focused on categorizing and defining disabilities in the framework of a medical model. This project examines Deafness as a lived experience by investigating issues that affect the lives of deaf Argentinians, Costa Ricans and Mexicans, and what is being done and could be done to address those issues according to deaf people themselves.<br><br>Participants were recruited with assistance from their local deaf associations. Data was collected through sixty signed interviews, written stories and surveys by a team of qualified deaf Americans, in collaboration with local deaf language consultants. Due to lack of funding the project is on hold as the investigators continue to look for funding to finish transcribing and captioning the interviews.	f	t
420	School Psychology Practicum Candidates and Interns: An Analysis of Time in Roles	f	f	f	2	June 2008	July 2011	The purpose of this study is to investigate the development of roles and function of school psychologists from part-time practicum to full-time internship placements. The study provides a curriculum-based assessment of emergent roles and directions for training from school/classroom environments to professional work environments.	t	t
448	Effects of Bilingualism on Word Order and Information Packaging in ASL	f	f	f	1	June 1, 2006	May 31, 2010	Information packaging refers to the ways in which speakers organize old and new information during discourse with an interlocutor. These serve a discourse/pragmatic function, yet they are encoded in sentence structure or prosody. For this reason, information packaging falls under the category of interface phenomena, spanning the otherwise autonomous domains of discourse/pragmatics, syntax, and phonology. Interface phenomena are typically difficult to acquire, exhibiting protracted periods of error in both child (L1) and adult (L2) learners. They are also prime contexts for inter-language transfer in bilingual and second language acquisition. This proposal focuses on the acquisition by ASL monolinguals and ASL/English bilinguals of two aspects of information packaging, topicalization, and focus. The inquiry necessarily begins with identification of the word order patterns used by ASL monolinguals and ASL/English bilinguals in their earliest multi-unit combinations. Only then is it possible to determine the ways in which children subsequently modify word order to encode discourse functions. In addition, inclusion of both mono- and bilingual signers allows investigation of possible cross-modality transfer effects between English and ASL. Recent influential predictions about the domains in which bilingual cross-linguistic transfer is expected are based solely on observation of spoken language bilinguals. Bilingualism across two modalities presents opportunities for a wider variety of potential transfer effects than traditional monomodal bilingualism, and can thus serve as a crucial test case for refining this aspect of linguistic theory. Data collected and transcribed under this project is currently serving as the basis for two independent study research projects by Linguistics Ph.D. students, with the possibility of leading eventually to dissertation research on topics of bimodal bilingualism.	f	t
544	Equivariant Cross Sections of Quaternionic Stiefel Manifolds	f	f	f	2	May 2007	April, 2010	A solution for the G-equivariant quaternionic vector fields problem on S(M) is given, where G is a finite group with no type-H real irreducibles and M is a quaternionic representation space of G with a non-zero fixed point set.	f	t
554	An Investigation into the Oral and Written Narrative Skills of Implanted Children Who Communicate via Cues, Signs, or Speech	f	f	f	2	June 2007	No set date	The purpose of this study was to examine the English narrative skills of deaf students who have cochlear implants. Participants included deaf students between the ages of 8-13 years, who are from oral, signing, or cueing backgrounds. Participants viewed two wordless picture stories; for one they told the story suggested by the pictures; and, for the other they wrote the story told by the pictures. Responses were analyzed for sentence length and specific linguistic structures. An article is in preparation.	f	t
560	Conceptualizing Disability	f	f	f	1	2001	No set date	In this ongoing project, the researcher is exploring ways that sociological and anthropological concepts and theories can illuminate how the concept of disability is enacted in society.	f	t
671	Comparison of Traumatic Stress Symptoms in Deaf and Hearing College Students	f	f	f	1	November 2007	\N	The research project samples deaf college students' exposure to, or lack of exposure to, traumatic events and any resulting psychological sequelae. This data will be used to help standardize the measures for use with deaf populations, and to better assess deaf peoples' experience with trauma and how to provide appropriate services. The results will also be applied to current theories of the development of traumatic stress symptoms and disorders, as well as helping determine factors which influence the etiology, progression, and treatment of related disorders. Data collection is currently in progress.<br><br>The measures used in the research will have a larger deaf norm due to this project, and will be more useful for clinicians who need to assess deaf people for trauma-related symptoms. The results can be used to better plan disaster and emergency response programs for deaf people, and provide more evidence-based practice for more effective interventions.<br><br>The results will be used to write the researcher's PhD pre-dissertation project, as well as submitted for publication in a peer-reviewed psychological journal.	f	t
776	Formative &amp; Summative Evaluations of a Biggest Loser Contest Piloted for a University Community: Incorporating Meaningful Project for a Research Methods Course	f	f	f	\N	Spring 2010	No set date	Formative and summative evaluations of a Biggest Loser (BL) contest that was piloted in a university community (i.e., Gallaudet).	f	t
778	Practicing Yoga Later in Life: A Mixed-Methods Study of English Women	f	f	f	2	2009	2010	Interviewed English women to learn their stories behind the meanings and lived experiences they got from taking up and practicing yoga later in life.	f	t
609	Blind Like Me: John Howard Griffin, Disability, and the Fluidity of Identity in Modern America	f	f	f	1	June 2008	February 2010	Despite five decades of scrutiny, scholars have yet to realize the potential for using John Howard Griffin and his famous book, <i>Black Like Me</i>, to alter some of our fundamental assumptions about identity in modern America. In 1959, Griffin darkened his skin, posed as a black man, and traveled through the Jim Crow South, documenting the cruel treatment he received from whites. His book was a best-seller and it became a staple in many high school and college courses. However, black activists, Griffin himself, and many scholars today find problems with using the account of a white man to explain the African American experience.<br><br>This critique is valid, but there has been little effort to understand Griffin in the context of his earlier experiences with disability. Before he began work on <i>Black Like Me</i>, Griffin lost his sight,was identified as a blind man for more than a decade, then regained his vision and became a nondisabled man once again. In the early period of his disability, before embracing a blind identity, he learned how to hide the markers that identified him as disabled and pass as sighted. In addition, he was paralyzed for a time, but eventually regained the use of his limbs which led him to reexamine identity issues yet again. Disability also threatened to emasculate Griffin, and he struggled to maintain a gender identity he had previously taken for granted. Well before his famous racial experiment, Griffin had crossed identity boundaries many times, something not uncommon in a country where most people experience some kind of disability at some point in their lives. A reinterpretation of Griffin and his work shows that once we pay attention to the issue of disability&mdash;adding it to the list of race, class, and gender&mdash;we will understand identity in modern America as much more fluid and far less static than we do today.	f	t
761	An Evaluation of Mental Health Services for Deaf and Hard of Hearing People in Nepal&mdash;Part I	f	f	f	1	2009	\N	This study is a survey of mental health services among deaf and hard of hearing people who live in Nepal.	f	t
625	Internal Consistency and Factor Structure of the Revised Conflict Tactics Scales with a Sample of Deaf Female College Students	f	f	f	2	August 2008	August 2009	The Revised Conflict Tactics Scales (CTS2) is currently the most widely used measure for identifying cases of intimate partner violence within the hearing population. The CTS2 has been used successfully with individuals from various countries and cultural backgrounds (Straus, 2004). However, the CTS2 had not yet been used with Deaf individuals. The goal of the present study was to investigate the internal consistency reliability and the factor structure of the CTS2 within a sample of Deaf female college students. Psychometric analyses indicated that subscales measuring Victimization of Negotiation, Psychological Aggression, Physical Assault, and Injury proved both reliable and valid in the current sample. Three subscales did not evidence reliability and the factor structure was not valid for Perpetration items.<br><br>It has been estimated that roughly 25% of all Deaf women in the United States are victims of intimate partner violence (ADWAS, 1997), a figure similar to the annual prevalence rates between 16% and 30% in the hearing community. One goal of the present study was to ascertain the prevalence of intimate partner violence victimization within a sample of Deaf female college students. When comparing the prevalence of Physical Assault, Psychological Aggression, and Sexual Coercion victimization to hearing female undergraduates, the current sample was roughly two times as likely to have experienced victimization in the past year.	f	t
633	What are Indicators of Questions in ASL and Tactile ASL?	f	f	f	1	\N	\N	The project further examines the forms of questions in ASL and Tactile ASL with the use of a large NSF corpus of data in ASL groups and a large NSF funded corpus of Tactile ASL interviews.	f	t
831	Deaf Studies Digital Journal	f	f	f	1	Fall 2008	No set date	The <i>Deaf Studies Digital Journal</i> (dsdj.gallaudet.edu) is the world's first peer-reviewed academic and creative arts journal dedicated to the creative and scholarly output of individuals within the signing communities. Hosted by the Department of American Sign Language and Deaf Studies, Issue #2 was published in the fall of 2010 and included academic articles in ASL and English, commentaries, visual arts, signed literature, and historic, archival texts.	t	f
677	Production of Movement in Users of American Sign Language and its Influence on Being Identified as "Non-Native"	f	f	f	1	April 2008	No set date	This project investigates the differences in the signing of ASL native users and second language users, and how these differences affect outsiders' perception of 'accent'. The project will be focusing on the parameter of movement in native and second language production. Once production participants are filmed, their films will be viewed by native ASL users who will be asked to identify which participants are native and which are non-native. The researcher will use these perceptions as well as her own analysis of specific movement sub-features to determine whether the sub-features in question (speed, size, and joint movement) have any bearing on whether or not a signer is seen as native or non-native.	f	t
679	The Guessing Game: The Effect of Morpho-Graphemic Organization on Word Attack Skills	f	f	f	1	January 2008	January 2010	Many studies have found that phonological awareness is required to become a skilled reader, for both hearing and deaf individuals. Unfortunately, many deaf readers lag behind their hearing peers, with the average reading level pinpointed at the fourth grade level. This deficit has often been attributed to poor phonological skills. In contrast to this deficit view, others suggest that phonological awareness is not necessary in order for deaf people to improve their reading efficiency. This study investigated both phonological awareness skills and morphological skills. To determine if deaf students with higher English Placement levels have better word attack strategies, students responded to a morphological test, where words with and without morphemes were matched to their definition. Additionally the project investigated if English Placement levels were related to more accurate responses on a Phonological Awareness test. Results showed that phonological awareness was not related to English Placement scores. However, students with higher college entry English scores were more likely to match both high and low frequency words to their meanings. This finding suggests that phonological awareness is not the sole route for deaf learners to become skilled readers, but that alternative, visually-accessible strategies are involved.	f	t
680	Driving While Deaf: A Comparative Study of Deaf and Hearing Drivers' Classification of Crash Risk	f	t	f	2	\N	January 30, 2010	The current study compared the performance of 50 deaf individuals to 50 hearing controls on measures of visual processing that correlate with effective driving. Specifically, the deaf participants were administered the Useful Field of View Test (UFOV), along with the Hooper Visual Organization Test (VOT). Performance on both of these measures has previously been correlated with effective driving. Demographic information, as well as self-reports of driving safety, was collected and analyzed. This research informed our understanding of deaf individuals' performance on visual processing measures correlated with driving performance. It is hoped that further empirical research of this nature will inform misperceptions regarding deaf individuals' driving ability.	f	t
845	SHAW	t	f	f	\N	\N	\N	(product only)	t	f
681	A Descriptive Phenomenological Study of Symptoms of Schizophrenia in Deaf Clients	f	t	f	2	September 2008	September 2009	Our clinical knowledge and understanding of the manifestation of schizophrenia in deaf patients is limited. Previous studies address specific symptomatolgy observed in deaf persons with schizophrenia. The purpose of this research proposal was to conduct extensive interviews of clinicians working with deaf patients diagnosed with schizophrenia in order to understand how clinicians characterized each patient's presentation and defined the symptomatolgy observed. Supplemental interview data were gathered from client charts by clinicians. Eight clinicians with extensive experience in deaf mental health and advanced sign language skills were recruited to discuss a total of 13 client cases. A qualitative investigation was employed to identify any themes and patterns present in each clinicians' concepts of symptoms manifested in deaf patients diagnosed with schizophrenia. Symptomatolgy observed by clinicians were consistent with diagnostic criteria established for hearing clients with schizophrenia. However, some symptom modality differences were noted in phenomena such as sign language and lip-reading hallucinations and the language-related symptoms reported. The majority of "auditory hallucinations" in this sample were ambiguous in that clients were unable to describe acoustic features and/or the message content of the "voices." Delusional content mirrored hearing samples. The most common language-related phenomena observed were characterized as loose associations, circumstantial and tangential communication. The theme of organization occurred multiple times throughout the interviews with clinicians. Schizophrenia was characterized by clinicians as a disease that disrupted major cognitive processes and eroded the brain's ability to organize information, impairing an individual's mental and social functioning. Disorganization caused misinterpretations of stimuli or perceptual disturbance and impacted motivation and drive as these relate to negative symptoms. Primary deficits reported were often related to the clients' social functioning declines. A major limitation of this study is that data are solely based on the judgments, accuracy and thoroughness of the observations and interpretations of the mental health professionals serving these clients. The nuances of the presentation of schizophrenia in deaf patients and the richness of this qualitative data may be beneficial to clinicians diagnosing schizophrenia in deaf patients and subsequently developing appropriate treatment and intervention programs designed for deaf individuals with schizophrenia.	f	t
683	Blogging: An Effective Tool for Writing	f	f	f	2	August 2009	December 2009	In Spring 2010, the researcher blogged with students in two sections of GSR/ENG 102. One section kept blogs while the other section used more traditional methods for completing writing assignments. Through the sixteen week semester, she collected anonymous writing samples from the students in the blogging section and in the traditional writing section and then analyzed the papers using the Gallaudet University Writing Rubric. This project including data analysis and concluding report was completed in February 2010.<br><br>The latest research points to numerous benefits of blogging which includes student engagement, authentic voice, and interaction and awareness of diverse perspectives. Researchers conclude that student blogging encourages engagement in learning (Ellison &amp; Wu, 2008; Ferdig &amp; Trammell, 2004; Wickerson &amp; Chambers, 2006). Since blogs include both written and graphic components where text along with visuals come together to form meaning, blogging may hold an added benefit for deaf students.	f	t
684	Current Practices in Signed Language Interpreting in Legal Settings in North America	f	t	f	1	February 2009	No set date	The purpose of this study was to collect demographic and relevant information on certified sign language interpreters relative to the provision of interpreting services in legal settings, to study what practices are incorporated into the interpreters' work in legal settings, and what professional development related to interpreting in legal settings they have had in the past and what they believe they need.	t	t
689	Cochlear Implants and the Deaf Community	f	f	f	2	August 2008	September 2011	We are co-editing a book titled <i>Cochlear Implants and the Deaf Community</i>. As of now most chapters have been finalized and we are in the last stages of editing the rest of the submitted first drafts. The book will be published by the Gallaudet University Press upon completion. Chapter topics address many of the research priorities listed below.	t	t
685	A Grounded Theory of Deaf Middle School Students' Revision of their Own Writing	f	t	f	2	March 2009	April 2010	This study used a grounded theory methodology to examine the experiences of deaf middle school students attending a program for deaf children in a public school to answer the following question: How do deaf children in middle school construct meaningful texts? The students were in one of two self-contained classes taught by a teacher of the deaf. The eight students and two teachers were each interviewed at least once. Classroom observations of the students engaged in writing an essay were conducted, and writing samples from each student were provided by the teachers.<br><br>All of the data were analyzed, and a grounded theory that describes the experiences of the deaf students emerged. The theory consists of one core category and four key categories, which encompass three parts of writing: Knowing, Experiencing, and Doing. The core category, which captures the essence of what revision is to the students, is Living in Language and is the sole category in Knowing. Three key categories fall under the Experiencing heading: Interacting with the Text, Interacting with Instruction, and Interacting with Self as Reviser. The final key category is the sole category in Doing: Fixing Wrongs. A significant understanding is that the students in this study are not given many opportunities to construct meaningful texts independently in their classes. Despite the lack of control over their own texts, the students have developed strategies to successfully "play the game" of writing in school.<br><br>In addition, recommendations for future research and ways to improve instruction are offered. The greatest implication for instruction is that teachers need to step back and consider how instruction impacts the students. Students especially need to be empowered to control their own writing and develop metacognition of their own work. Future research can be done to test the theory using a broader scope of participants in other settings. It could also examine the writing process from the teachers' perspectives to provide information about what informs their instruction of writing and revising.	f	t
770	Client's Perspective of Therapeutic Alliance When an Interpreter is Involved in Therapy. (Pilot Study)	f	f	f	2	March 31, 2010	June 30, 2010	This pilot study focused on the deaf client's perception of the therapeutic alliance when an interpreter is present in the therapy session. This qualitative study followed the phenomenological approach and utilized a semi-structured interview of one deaf participant who had experienced therapy with an interpreter, one document review of how interpreters are trained to work in a mental health setting, and one observation of a demonstration of a psychiatric appointment with a deaf client using an interpreter.	f	t
705	Rehabilitation Engineering Research Center on Hearing Enhancement	f	f	f	1	October 1, 2008	September 30, 2013	This project builds and tests components of an innovative model of aural rehabilitation (AR) tools, services, and training in order to assure a better match between hearing technologies and individuals in their natural environments.<br><br>Project goals include: (1) improving assessment, fitting, availability, and use of hearing technologies; (2) increasing the quality, availability, and knowledge of AR services; (3) training of consumers, service providers, and future researchers, developers, and practitioners; and (4) transferring technology and knowledge to agencies, standards bodies, consumers, and the professions who influence the communicative effectiveness of those who are deaf or hard of hearing.<br><br>Additional component projects are designed to fall into four areas: (1) Aural Rehabilitation projects improve the assessment and treatment of individuals in need of AR; (2) Hearing Technology addresses the technology challenges of real-life use of assistive technologies, hearing aids, and cochlear implants; (3) Training Programs provide training of individuals who will become the rehabilitation innovators of the future; and (4) Dissemination and Advocacy programs transfer technology and knowledge to agencies, standards bodies, consumers, and hearing professionals.	f	t
847	LEIGH	t	f	f	\N	\N	\N	(product only)	t	f
848	YUKNIS	t	f	f	\N	\N	\N	(product only)	t	f
849	MERTENS	t	f	f	\N	\N	\N	(product only)	t	f
850	GIBBONS	t	f	f	\N	\N	\N	(product only)	t	f
743	A Comparative Analysis of Deaf and Hearing Interpreters in Community Settings	f	f	f	2	August 2009	May 2010	This study compared the work of four interpreters, two hearing and two Deaf. Original data were collected, which consisted of two sight translations performed by each of the four interpreters. The data were used to analyze the interpreters' use of several linguistic features and to elicit qualitative feedback from Deaf consumers. The results of both the linguistic analysis and of the consumer feedback were mixed. It was originally hypothesized that there would be noticeable differences between the hearing interpreters and the Deaf interpreters, but the actual results did not always break down along hearing-deaf lines.<br><br>Results were shared in an in-class presentation in May, 2010.	f	t
744	Treatment Adherence in Adults who are HIV-Positive and Deaf	f	f	f	1	September 2009	September 2010	The study aimed to assess the treatment adherence of individuals who were HIV-Positive and Deaf. Results not yet published.	f	t
745	Signing with an Accent: ASL L2 Phonology and Chinese Signers	f	t	f	1	July 20, 2010	\N	This project investigates the phenomenon of "sign accent", or systematic phonological errors made by signers acquiring ASL as a second language (L2). This topic has been virtually ignored in the sign language literature, despite extensive discussion of accent in spoken L2s and a common assumption that some counterpart exists for signed L2. The investigations will focus on handshape, approaching the phenomenon of L2 signing accent. Native Chinese Sign Language Users in Beijing, China will be recorded signing ASL and data collected will be analyzed for an accent.<br><br>Paper in progress; possible poster.	f	t
758	The Relationship Between Computer Mediated Communication and Employment of Deaf People	f	f	f	\N	\N	\N	James Schiller is working on his dissertation in social policy and planning at Walden University, School of Human Services. He is also a faculty member at Gallaudet University, Dept. of Social Work. The disseration examines the relationship between employment and computer mediated technology for deaf and hard of hearing workers. The study began collecting data on May 1, 2010 using a web survey and will continue until October 1, 2010. At that point, Mr. Schiller will compile the statistics and begin writing his dissertation.	f	t
769	The Effect of Sensory Experience, Language, and Communication Mode on the Neural Substrates of Phonological Processing	f	f	f	1	May 2009	August 2010	Though funding ended in August, 2010, the research continues. The aim of this project is to explore the effects of sensory experience and different communication modes (i.e.-Oral and Cued Speech) on the neural substrates serving phonological processes. When the phonology of a spoken language is acquired visually via manual cues (e.g. Cued Speech) rather than auditory channels, how does this affect the functional neuroanatomy of phonological processing? Will deaf cuers and hearing speakers with comparable phonological experience expressed and acquired in different modalities (visual and auditory) recruit the same neural regions for phonological processing? To address these questions, we have conducted functional magnetic resonance imaging (fMRI) and neuropsychological studies funded by the National Institute of Child Health and Human Development and National Institute of Deafness and Communication Disorders. Most of the data have been acquired, processed and analyzed, with the exception of one study that focuses on phonological awareness in deaf individuals. Research funding for the current project was used to analyze fMRI data on phonological awareness and neuropsychological measures of phonological skills from deaf adults.	f	t
763	Weekend Science Camp for the Deaf/Hard of Hearing at Camp Dreams and Inspirations, East Gull Lake, MN	f	f	f	1	June 16, 2011	June 19,2011	A weekend camp for 8-14 year old deaf/hard of hearing students was held at Camp Dreams and Inspirations in East Gull Lake, MN. The camp is primarily for youth who have an interest in the sciences or are asked by their schools to participate (as part of an individualized lesson plan). Dr. Lundberg (Chemistry faculty at GU), Ms. Gerdts (registered nurse), and Mr. Sherman (pilot) served as deaf/hard of hearing counselors and role models to encourage interest in the science, technology, engineering, medicine, and mathematics fields. Demonstrations and activities were held on the camp's property and in biology and chemistry laboratories at Central Lakes College in Minnesota.	t	t
772	A Note on the Construction of Complex and Quaternionic Vector Fields on Spheres	f	f	f	2	February 2010	June 2011	A relationship between real, complex, and quaternionic vector fields on spheres is given by using a relationship between the corresponding standard inner products. The construction of complex and quaternionic vector fields on spheres is then investigated by using the given relationship and by providing explicit formulas for calculating the maximal number of linearly independent complex and quaternionic vector fields on spheres.<br><br>A manuscript has been accepted for publication in <i>Mathematical Notes</i> and will be published in 2011.	t	t
773	"Deaf and Accessibility" in the Discourse of Hong Kong Rehabilitation Policies	f	f	f	1	April 1, 2010	April 1, 2011	Over the past years, the word "accessibility" has been prominent when promoting the needs of the persons with disabilities in Hong Kong. The concept "access for all" appears to be the goal of the people at stake as nourishing by the government and the non-government organizations (NGOs) ever since the word has been introduced. Certainly, different disability groups have different accessibility needs. This study aims to explore the experience of the signing deaf people in terms of accessibility, and attempt to find its association with the rehabilitation policies.	f	t
774	Oceanmotion Website, a NASA Sponsored Educational Website for Ocean Surface Currents	f	f	f	1	2002	No set date	The oceanmotion website (http://www.oceanmotion.org/) is a NASA-sponsored website that features ocean surface currents. This year I am involved in a major upgrade to higher resolution data and imagery. This requires rewriting code, regenerating visuals (maps, graphs) and posting them on a server at Gallaudet.	f	t
780	The Reliability and Norms of the Leisure Diagnostic Battery for Undergraduate Recreation Majors Whom are Deaf	f	f	f	1	Spring 2010	No set date	Presented normative data, for college students whom are Deaf, on the Leisure Diagnostic Battery (LDB). The LDB measures leisure functioning, barriers to leisure involvement, and leisure preferences.	f	t
852	VAN GILDER	t	f	f	\N	\N	\N	(product only)	t	f
853	HOREJES	t	f	f	\N	\N	\N	(product only)	t	f
854	DUDIS	t	f	f	\N	\N	\N	(product only)	t	f
855	MILLER	t	f	f	\N	\N	\N	(product only)	t	f
856	SASS-LEHRER	t	f	f	\N	\N	\N	(product only)	t	f
781	Increasing International Business Opportunities for Deaf and Hard of Hearing Americans	f	f	f	\N	\N	\N	The project objective was to develop mutually beneficial relationships with business partners to provide students with expert knowledge, mentors, and business networks while providing partners with expanded international business opportunities and an awareness of the abilities of deaf entrepreneurs.<br>1. The project outcome enabled faculty to establish contacts with firms and individuals in all the countries visited and developed potential internship sites for students (China, So. Africa, Botswana, Argentina, Brazil, India).<br>2. DOB members advised firms, the campus community and students about business opportunities available abroad and how economic and business systems in those countries are similar or different to those in the USA.<br>3. Disseminated data through department web page, brochures describing our international activities and programs, the catalog specifying courses, and outreach to other colleges and universities about Gallaudet including ICBER University of Connecticut and CIBER University of Minnesota.<br>4. Developed three new courses with international focus, increased the international content of current courses and established an international business minor. Established a mentoring program.<br>5. Internationalized the business curriculum by increasing faculty knowledge of areas covered in the grant through training, lectures and workshops and increased faculty knowledge of cultures of countries covered.	f	t
792	Die Geschichte Meiner Familie (The History of My Family)	f	t	f	2	August 2009	May 2010	In the era of the National Socialist German Worker's Party (NSDAP), 1933-1945, eugenic policies were combined to create a legal and social structure designed to exclude German citizens judged biologically deficient. One group deemed "undesirabl" was the German deaf community. The Reimanns, deaf members of the author's family, were a part of this community. The purpose of this multimedia project is, first, to document through interviews and personal as well as public photographs to understand the experiences and reflections of these policies on the surviving Reimanns author's deaf family. Second, to provide an additional historical context in which to understand the stories of German deaf survivors of World War II.	f	t
835	Sensory Politics and the Cochlear Implant Debates	f	f	f	2	May 2010	May 2011	This book chapter develops the notion of sensory politics as a variation on identity politics. Sensory politics recognizes that sensory experience is a culturally mediated phenomenon, and that deaf individuals experience the impact of interpretations of the meanings of sound and hearing within a phonocentric orientation. Recognizing the privileged role of vision and visual cognition within Deaf culture raises questions about the need to normalize members of this cultural group to fit the sensory regime of a phonocentric orientation.	t	f
782	An Examination of Literacy Experiences in First-Generation Deaf Latino College Students	f	f	f	1	2008	2010	<br>Dissertation: This qualitative research study examined and described the literacy experiences of first-generation, deaf Latino college students. This was achieved by conducting a series of videophone interviews of first generation deaf Latino students who graduated from college, and those who did not graduate from college. We collected data from eleven participants; six who had graduated from college and five who had withdrawn from college were analyzed and described. Two research questions guided this study. The first question was: Are there specific common literacy experiences in first-generation deaf Latino college students? The second question was: Do academically successful deaf Latino college students, who graduated from college, have similar or different literacy experiences than deaf Latino college students who did not graduate from college?<br><br>Results from this study indicated that the first-generation, deaf Latino college students who participated in this study had similar literacy experiences growing up, in high school, and in college as hearing, first-generation college students. These experiences related to parents' educational levels, the linguistic environment at home, parent-school interactions, preparation for college, and minority status stress. In addition, data from the participants' interviews revealed other variables that may have contributed to the participants' academic success in college. These included expectations of higher education, establishing goals, taking advantage of support services, and the characteristics of assertiveness and independence. The findings of this study could be used in curriculum and program development, and professional development for teachers who will be working with deaf and hard of hearing, at-risk students. Ultimately, this may impact the success that first generation deaf Latino college students have during their college experiences, thus increasing their ability to graduate from college.	f	t
786	Preserving History at Gallaudet University	f	f	f	2	August 10, 2009	\N	A presentation to the Finnish Museum of the Deaf in Helsinki, Finland. The presentation discussed various ways of how deaf history is preserved at Gallaudet and various techniques for dissemination of information.	f	t
787	Taking Stock: Alexander Graham Bell and Eugenics	f	f	f	2	October 1, 2009	No set date	This presentation gives an overview of Alexander Graham Bell's role in the American Eugenics movement, particularly between 1883 and 1920.	t	t
788	Women with Heart	f	f	f	1	February 2010	September 2010	While I was recuperating from a heart attack that I suffered in November of 2009, I was approached by Ms. Lynne Rosental, the Director of Medical Center Development at George Washington University and Ms. Jessica Johnson of the Richard B. and Lynne V. Cheney Cardiovascular Institute to participate in their developing "Women with Heart" initiative.<br><br>The goal of this program is to promote heart health in all women, particularly women in the underserved communities in the Washington metropolitan area and elsewhere. Women who might particularly benefit could include women with disabilities and women living in poverty who have less access to information that might prevent a heart attack and to the funds necessary for care once a heart-attack strikes. Telling my own story, we hope, will aid both in the education of and fundraising for these women.<br><br>An article will appear in a newsletter to be produced by the George Washington Memorial Cardiovascular Institute in September of 2010, titled <i>Profile of a Survivor</i>.<br><br>A video is to be completed in winter 2010.	f	t
791	La Vie et l'Oeuvre de Mademoiselle Yvonne Pitrois, Bienfaitrice de la Communaut&eacute; Sourde (The Life and Work of Miss Yvonne Pitroise, Benefactress of the Deaf Community)	f	f	f	1	July 1, 2009	Fall 2010	This will be the lead article in the fall issue of a periodical for the deaf in France, titled <i>Patrimoine Sourd. (Deaf Patrimony)</i>. The periodical is dedicated to the study of the history and culture of the deaf in France and is published by the Association CLSFB (Culture and Sign Language Ferdinand Berthier Society) in Louhans, under the direction of Yves Delaporte.	f	t
785	Ringing Up Eugenics	f	f	f	2	April 15, 2010	April 15, 2011	This project examined Alexander Graham Bell's role in the American eugenics movement and presents the argument that the eugenics movement did not seek diversity among the American population. One approach to the homogenization of Deaf Americans was achieved through oralism, which sought to normalize deaf Americans during the Progressive Era.	t	t
865	NICKERSON	t	f	f	\N	\N	\N	(product only)	t	f
801	Men Bring Condoms, Women Take Pills: Men's and Women's Roles in Contraceptive Decision-Making	f	f	f	1	May 2007	No set date	The most popular form of reversible contraception in the U.S. is the female-controlled hormonal birth control pill. Consequently, scholars and lay people have typically assumed that women assume primary responsibility for contraceptive decision-making in relationships. Although many studies have shown that men exert strong influence over contraceptive decisions in developing countries, very few studies have considered the gendered dynamic of contraceptive decision-making in developed societies. This study uses in-depth interviews with 30 American opposite-sex couples to show that contraceptive responsibility in long-term relationships often conforms to a gendered division of labor, with women primarily in charge. A substantial minority of men in this study were highly committed contraceptors. However, the social framing of contraception as being primarily in women's "sphere," and the technological constraints on their participation made even these men reluctant to discuss contraception with their female partners.<br><br>A paper is currently under review in <i>Gender and Society</i>.<br>A paper was presented at the Annual Meetings of the Population Association of America in Dallas, TX, April 2010.	f	t
800	Development of Bimodal Bilingualism	f	f	f	1	May 15, 1009	May 14, 2014	Five-year project to compare early language development in hearing bilingual (ASL/English), cochlear implanted monolingual (English or Brazilian Portuguese), and cochlear implanted bilingual (ASL/English) children in the U.S. and Brazil. Includes both longitudinal and experimental components.	t	t
802	Academic Motivation and Well-Being in Deaf and Hard-of-Hearing University Students: Predictors of Academic Success	f	f	f	1	November 2, 2009	June 1, 2011	While there has been considerable research on the academic motivation of hearing students and factors affecting their motivation in multiple educational settings, research on how deaf students maintain academic motivation is lacking. The purpose of the study is to develop a predictive model for what impacts academic motivation and determine which combination of the college admission ACT composite score and general psychological variables (within academic motivation and subjective well-being) best predicts academic success as measured by the current college GPA. Undergraduate students completed a total of 4 measures: the Self-Regulation Questionnaire &mdash; Academic &mdash; and the Perceived Competence Scale (to assess academic motivation components) as well as the Rosenberg Self-Esteem Scale and the Satisfaction with Life Scale (to evaluate well-being components). Multiple regression analyses will be conducted to determine the best predictors of academic success.	f	t
803	Referents in English to ASL Interpretation of a Therapy Session	f	f	f	2	February 24, 2010	May 3, 2010	We used a video text of an interpreted therapy session for the basis of analysis in which video clips of multiple hearing participants' utterances are being interpreted from spoken English to ASL. We viewed them and selected clips to examine how the interpreter indicates participants speaking and how she indicated when a different participant began to speak.	f	t
813	Sign Language Interpreting Studies Reader	f	f	f	1	March 2010	August 2011	We are compiling a reader of classic and/or seminal essays in SL interpreting that are no longer available in print, or were published in obscure journals, etc., as well as essays and articles that have had a major impact on our profession but are no longer read. The reader is adapted from the <i>Interpreting Studies Reade</i>r published by Routledge. Our contract is with John Benjamins. Publication date TBD.	f	t
815	Knowledge of Domestic and Dating Violence Among Deaf College Students	f	f	f	2	January 2008	June 2010	This study surveyed deaf college students at Gallaudet University to explore violence within their intimate relationships.	f	t
821	A Comparison of Two Approaches to State-wide Assessment Accommodations	f	f	f	2	Fall 2010	Spring 2011	While live examiners have historically provided the American Sign Language (ASL) administration accommodation to deaf students enrolled in Georgia's State Schools, this approach is not without limitations. This project involved an experimental study that compared test accommodations provided to deaf students via DVD to those provided by a live examiner. Interestingly, there was little difference in student performance associated with accommodation type. <br><br>Note: Two proposals for paper presentations have been submitted. If accepted, the first presentation will be given at the annual conference of the Georgia Educational Research Association in October 2011 and the second presentation will be given at the National Association of School Psychologists' Annual convention in February 2012.	t	f
822	Deaf Readers as Bilinguals: An Examination of Deaf Readers' Print Comprehension in Light of Current Advances on Bilingualism and Second Language Processing	f	f	f	2	Fall 2010	July 2011	Peer-reviewed survey article on past and state-of-the-art research on deaf readers within the context of new developments in bilingualism research.	t	f
866	Wabash Study - Understanding Gallaudet Students' Literacy Development	f	f	f	1	September 2010	September 2013	This is part of a national study that is using assessment to measure student learning outcomes. The researchers will address literacy skills of students who enter Gallaudet University in the developmental ENG courses and provide them with ongoing intervention to assess and track literacy skills.	t	f
840	The Effects of Electronic Communication on American Sign Language	f	f	f	2	September 2010	May 2011	Self-report survey data were collected at Gallaudet University and via an online survey examining Deaf participants' use, perceptions of use, and attitudes towards the use of electronic communication acronyms in spontaneous production of ASL. Statistical measures were used to explore whether usage among native Deaf ASL users followed variation patterns found in the conversational use of electronic communication acronyms identified in North American English speakers.<br><br>A presentation was made at the Georgetown University Round Table on Languages and Linguistics as part of the panel "Sign Languages and the New Media." 03/11/11<br><br>Paper submitted for publication to Sign Language Studies 09/23/11. Acceptance by SLS is pending.	t	f
621	Parsing Sentences in Two Languages II (Eye-tracking study)	f	f	f	1	July, 2007	No set date	The main goal of this study is to examine what kind of information&mdash;syntactic and semanti&mdash;second language learners utilize when they read in their second language. Using eye-tracking technology, the investigators examine the processing of English relative clauses among different groups of second language learners of English, namely, deaf ASL-English bilinguals, Russian-English bilinguals, and Spanish-English bilinguals. They also investigate how the participants' English proficiency levels and their individual cognitive resources may play a role in how closely second language sentence processing might approximate sentence processing in the first language.	t	t
867	METZGER	t	f	f	\N	\N	\N	(product only)	t	f
868	MATHUR	t	f	f	\N	\N	\N	(product only)	t	f
248	Visual and Haptic Self-Monitoring During Sign Production	f	f	f	2	October 2003	No set date	The role of visual feedback during sign production was experimentally investigated in native and novice signers. Experimentally altered visual feedback was provided to the signer via a pair of goggles that displayed a camera view of him/her from different vantage points. Connected sign utterances were elicited using either the pictures of arranged objects or videos of a person describing those pictures in ASL and were recorded for later analysis. Errors in scene description and changes in linguistic aspects of sign were analyzed in relation to the nature of the visual feedback available during sign production. In addition, some of the recorded utterances were used to investigate whether the observers who were na&iuml;ve to the conditions of signing could perceive changes in sign quality due to the altered visual feedback.	t	t
838	High School Diversity Experiences of Entering Gallaudet Students	f	f	f	2	August 23, 2011	September 30, 2011	This study incorporated data from the GRI 2009-2010 Annual Survey of Deaf and Hard of Hearing Children &amp; Youth and the "Admissions data" on deaf and hard of hearing individuals who applied to the undergraduate college of Gallaudet University in 2009-2010. The goal of this project is to do analyses to gain insights on the school-based ethnoracial and deafness diversity experiences of deaf and hard of hearing high school students, particularly the group of students mentioned above. Included will be information on their educational experiences in similar instructional settings, degree of hearing loss, and the relationship between ethnoracial identity and whether the students are "economically disadvantaged".<br><br>This study will strive to find answers to the following, and more:<br><br>1. How ethnoracially diverse is the deaf and hard of hearing student population within schools?<br>2. Is there a relationship between degree of hearing loss and the instructional setting experienced by deaf and hard of hearing high school students within the same school?<br>3. Is there a relationship between economic disadvantage status and the instructional setting experienced by deaf and hard of hearing high school students within the same school?<br>4. Is there a relationship between ethnoracial identity and the instructional setting experienced by deaf and hard of hearing high school students within the same school?<br>5. What is the relationship between ethnoracial identity and the applicants admitted to Gallaudet?	t	f
273	Science of Learning Center Focusing on Visual Languages and Visual Learning (VL&sup2;)	f	f	f	1	July 1, 2004	No set date	Hosted by Gallaudet University, the Science of Learning Center (SLC) on Visual Language and Visual Learning (VL&sup2;) is one of six SLCs funded by the National Science Foundation (NSF). These Science of Learning Centers were established by NSF to support interdisciplinary and cross-disciplinary research that presents new lines of thinking and inquiry into the science of learning.<br><br>The purpose of the Science of Learning Center on Visual Language and Visual Learning (VL&sup2;) is to advance, fundamentally, the science of learning related to how aspects of human higher cognition are realized through one of our most central senses: vision.<br><br>Of all the SLCs, the VL&sup2; is unique in its focus on the role of visual processes, visual language, and social experience in higher cognition and learning. It seeks to understand more about how visual processes, visual language, and social experience effect the development of cognition and language and also studies these effects for reading and literacy.<br><br>The researchers study these learning processes in monolinguals and bilinguals across the lifespan in order to promote optimal practices in education in both formal and informal settings. The VL&sup2; conducts research across multiple disciplines in order to understand how the visual modality is used in learning, especially language learning, from infancy to adulthood. The research program is structured developmentally (infant/toddler, children, and adults), with specific themes of research addressed at each stage of development.<br><br>The center's research and work is organized around five strategic focus areas. These areas are:<br><br>&bull; <b>Visual and cognitive plasticity:</b><br>Study 1: Gaze-Following in Deaf Infants (Singleton, Brooks, &amp; Meltzoff)<br>Study 2: Visual Processing in Deaf Toddlers (Bosworth &amp; Dobkins) <br>Study 3: Electrophysiological Indices of Visual Language Experience on Auditory and Visual Function (Corina, Sharma, Mitchell, Coffey-Corina, &amp; Gutierrez)<br>Study 4: Optical Imaging of Visual Selective Attention in Deaf Adults (Dye, Gratton, &amp; Fabiani)<br>Study 5: Visual Processing in Deaf Adults (Eden)<br><br>&bull; <b>Language levelopment and bilingualism</b><br>Study 6: Neural Representations of Print, Fingerspelling &amp; Sign in Deaf Readers (Emmorey)<br>Study 7: Cross-Language Activation During Sentence Comprehension in ASL-English Bilinguals (Morford, Dussias, Pi&ntilde;ar, Van Hell, Wilkinson &amp; Kroll)<br>Study 8: Executive Function in Deaf Bilingual School-Aged Signers (Hauser &amp; Bavelier)<br><br>&bull; <b>Reading and literacy in visual learning</b><br>Study 9: VL&sup2; Early Education Longitudinal Study (Allen, Morere, &amp; Clark)<br>Study 10: Individual Differences in Deaf Readers (Traxler, Corina, Morford, &amp; Long)<br>Study 11: Modeling Semantic-Orthographic-Manual Networks with Reduced Auditory Input (Plaut)<br>Study 12: Orthographic Processing Effects on Eye Movements in Deaf Readers (Rayner, B&eacute;langer, &amp; Morford)<br><br>&bull;<b> Translation of research to educational practice </b><br>Study 13: The Role of Gesture in Learning (Padden &amp; Goldin-Meadow)<br>Study 14: Visual Language Training to Enhance Literacy Development (Enns &amp; McQuarrie).<br>Study 15: Fingerspelling Development as Alternative Gateway to Phonological Representations in Deaf Children (Schick).<br><br>&bull; <b>Integration of research and education</b><br>The VL&sup2; is partnered with twelve national and international universities, and staff work closely with affiliated researchers and universities to carry out the research agenda.<br><br>The ultimate mission is to improve the education and lives of all visual learners. Be they deaf or hearing, the researchers hope to benefit the lives of the young visual learner learning to read, the teacher trying to create new teaching strategies for visual learners, the parents of visual learners, and the schools that seek to build programs for visual learners. They also wish to highlight the unique gifts and needs of being a visual learner, especially young deaf children learning natural signed languages.	t	t
826	Senior Language Assessment Project	f	f	f	1	March 2011	No set date	In order to determine the levels of written English and ASL proficiencies of graduating seniors, OBTL and the Faculty Development Office have conducted the Senior Language Assessment Project. Initially beginning with written English assessment in 2009, the ASL component has been added for the 2011 graduating seniors. A final report will be submitted to the Faculty Senate in the fall of 2011.	t	f
827	Body Image, Deaf Identity, and the Tripartite Model: A Preliminary Study	f	t	f	1	March 2011	December 2011	This was a dissertation examining the influence of parents, peers, and the media on body image satisfaction in the deaf population. Contemporary literature has minimal information regarding body image and deaf. Research findings are pending completion of the dissertation.	t	f
570	Comparison of Lexical Versus Morphological Grouping of Graphemes in Learning New Words	f	f	f	2	March 2007	No set date	This project examined a method of teaching words of a new language (L2) to deaf adults by dividing words into meaningful parts (morphemes) that pertain to well-known ASL signs. Such a presentation of meaningful word segments contrasts with the usual method that many deaf readers use to learn new words, namely, by memorizing either whole letter strings or one or more graphical features that they consider salient. The proposed method may prove beneficial to deaf readers because it promises to foster the effective decoding of the meaning of newly encountered words by emphasizing their morphological composition. It may allow deaf readers to at least partly sidestep the memory challenges of learning words as entire unanalyzed units. Once well learned, a morpheme-based strategy could be applied in order to decode the meanings of the many thousands of words composed of these meaningful word parts. In addition, the initial analytical processing of words into their morphemes may allow readers to develop more structured representations of the words that would lead to more efficient activation of their meanings when encountered in the future.<br><br>This study compared the efficacy of three visual word processing strategies in learning new words. The three strategies focused on either the word as a whole, the word's morphemic structure, or its syllabic (i.e., phonologically based) composition. One hundred and five deaf/hard of hearing ASL signers were screened for their English reading ability, language and hearing background, and knowledge of signs used in the study. Based on their screening profiles, 54 were selected and split into three matching groups, each exposed to a different word learning strategy. The study revealed a disadvantage of emphasizing the syllabic structure during word learning, but no advantage from emphasizing word's component morphemes compared to presenting a word as a whole. The tests of the word knowledge (lexical decision and translation identification tasks) pointed to (i) the minimal benefit from the phonological cues that the syllabic decomposition provided; (ii) a greater benefit from a meaning-based word decomposition; and (iii) a possible influence of logographic (whole word) processing that is habitual to many Deaf readers. Thus, despite the theoretical or putative potential that the morphological approach has to best capitalize on the visual capabilities of deaf learners, a well learned logographic strategy also may be effective in the short run.	t	t
3	American Annals of the Deaf: Reference Issue	f	f	f	1	January 1, 1990	No set date	The GRI has been compiling information for the "Schools and Programs for the Deaf in the United States" and "Schools and Programs for the Deaf in Canada" listings in the Reference issue of the <i>American Annals of the Deaf</i> for over 20 years. The 2011 issue includes 554 schools and programs in the United States and 22 schools and programs in Canada. The listings have been used for a variety of purposes by educators and researchers and serve chiefly as a directory of programs and schools and the services these programs provide to deaf children and youth in support of their education.	t	t
830	Training of School Social Workers to Meet the Educational and Emotional Needs of Diverse Deaf and Hard of Hearing Children	f	f	f	2	October 1, 2010	September 30, 2011	U. S. Department of Education training grant to train graduate social work students to work with diverse groups of deaf and hard of hearing children in schools.<br><br>Coordinated a full day faculty development workshop for social work faculty and field instructors: Presented by E. J. Mullen, PhD, Wilma &amp; Albert Musher Professor Emeritus, Columbia University School of Social Work. August 22, 2011. Gallaudet University.	t	f
836	Classroom Discourse Observation Pilot Study	f	f	f	1	September 2010	No set date	In 2008, the Faculty Senate passed a measure requiring the development of multiple measures to evaluate faculty proficiency in American Sign Language. One key aspect is the evaluation of language and discourse within the classroom. After an ad-hoc committee developed the Classroom Discourse checklist, the Office of Bilingual Teaching and Learning and the ASL-Diagnostic and Evaluation Services conducted a pilot study to determine appropriate procedures, protocols and measures involved in the Classroom Discourse Observation. A final report was submitted to the Faculty Welfare Committee in May, 2011. In addition, an OBTL Faculty Fellow is charged with developing workshops and web-based resources to support faculty in the effort at improving their performance on the Classroom Discourse Observation.	t	f
844	THUMANN	t	f	f	\N	\N	\N	(product only)	t	f
783	Exploring Blended Instructional Pedagogy to Enhance Student Learning and Scientific Reasoning Skills in Biology (STEM)	f	f	f	1	September 1, 2009	No set date	This multiple-case study is designed to explore a blended learning pedagogy with deaf and hard of hearing (DHH) students to enhance their comprehension and scientific reasoning skills in biology. Instruction of STEM content presents an additional challenge to DHH students and educators based on literature. The instructional delivery system under study blends e-Learning (simulation, virtual labs, data analysis models, learning portals, etc.) with classroom instruction to optimize learning. It replicates a 2006 research project during which Computer Information Systems (CIS) majors were involved in a limited experiment seeking to enhance student learning through a blended instructional paradigm. Preliminary findings were positive in terms of student engagement, learning gains, and learner satisfaction. The current study intends to collect additional empirical evidence and to broaden the scope to another STEM discipline in order to confirm previous findings and to test the generality that the technology-enabled blended learning paradigm can indeed improve DHH student learning of STEM content. The project also exemplifies cross-disciplinary faculty collaboration in research.	t	t
746	Prevalence and Predictors of Intimate Partner Violence Victimization in the Deaf Community	f	t	f	2	November 2009	November 2010	It has been estimated that roughly 25% of all Deaf women are victims of intimate partner violence. This figure is similar to the annual prevalence rates of 16% to 30% in the hearing community. Using health behavior data collected by the National Center for Deaf Health Research, the current study sought to ascertain the actual prevalence of intimate partner violence victimization in a community sample of Deaf women, as well as investigate the predictors of intimate partner violence against Deaf women. Results indicate that 25.9% of Deaf women and 16.7% of hearing women reported experiences of lifetime physical assault; 29.1% of Deaf women and 10.4% of hearing women reported experiences of lifetime unwanted sex by an intimate partner. Additionally, traditional predictors of intimate partner violence emerged as significant predictors of violence against Deaf women; however, Deaf-specific factors of school background and best language also emerged as significant predictors. This finding has significant implications for clinicians assessing for partner violence. Looking for "red flags" in the traditional predictors of violence is not sufficient when working with Deaf clients. Rather, we must also assess for Deaf-specific demographic predictors, such as school background and preferred/best language, in order to effectively serve Deaf survivors.	t	t
843	Situated Access: Making People Feel Welcome	f	f	f	1	February 1, 2011	No set date	The definition of "access" varies depending upon who is providing the definition. For example, a person who uses a wheelchair may define access as 'the provision of curb cuts or ramps'. For members of the Deaf Community, the provision of communication (via services provided by a Sign Language Interpreter) is considered to be "access". This research addresses the question of HOW Sign Language Interpreters define access and the various social institutions that influence that conception.	t	f
306	Successful Science Teaching: Problem Solving Strategies of Outstanding Science Teachers of the Deaf	f	f	f	1	October 1, 2004	No Set Date	This study involves direct observation of the classrooms of award-winning teachers of science to Deaf students. The study includes: (1) teaching styles of these teachers, (2) their relationships with students, (3) how they solve instructional problems, and (4) what sets outstanding teachers of science to Deaf students apart from their colleagues, including their love of learning, problem-solving skills, and a radar-like 6th sense that scans and interprets the learning environment. The study highlights the common characteristics, philosophies, teaching methods, and behaviors that have helped these teachers of Deaf students win teaching awards and recognition for teaching excellence in their schools.	t	t
810	Early Educational Longitudinal Study (EELS), under VL&sup2; Science of Learning Center	f	f	f	1	October 1, 2009	No set date	In this longitudinal study, information about deaf preschool children is being collected around the nation using parent, teacher, and school surveys, as well as direct educational and psychological assessments in three waves. The information will provide insights about deaf children's cognitive, social, and emotional development, and their learning environment. This study will help develop interventions that benefit deaf children's learning, especially in their literacy development. Data collection for Wave 1 is complete and analysis is underway. A colloquium proposal with four papers has been submitted to AERA. Data collection for Wave 2 is underway. Dr. Allen will be presenting <i> Early language and emerging literacy: Preliminary analysis of the Early Education Longitudinal Study</i> at The Center for Mind and Brain Visual Language Summit, University of California at Davis in November, 2011.	t	t
6	Annual Survey of Deaf and Hard of Hearing Children and Youth	f	f	f	1	1968	No set date	Initiated at the request of educators and researchers in the field of deaf education, the Annual Survey of Deaf and Hard of Hearing Children and Youth (Annual Survey) began in 1968. This broad-based national survey is conducted through special and regular private and public school systems, providing a core set of data from which meaningful research of issues related to educating deaf children may be performed. It continues to provide an ongoing database for research and planning toward bettering the education of deaf and hard of hearing children and youth. This survey collects a wide range of data about deaf and hard of hearing children: who and where they are, what their characteristics are, what changes are taking place in their educational settings, and what trends are occurring in their education. The Annual Survey is the only national database on deaf and hard of hearing children and youth in the U.S.; information collected through this survey&mdash;reported in summary, cumulative form only&mdash;is utilized by many individuals and organizations, within and beyond the Gallaudet community.<br><br>The 2009-10 Annual Survey collected information on 37,828 deaf and hard of hearing students across the United States. Age, sex, ethnicity, etiology, audiological, cochlear implant/hearing aid use, instructional setting/services, communication mode used in teaching the student, home communication, and additional educationally relevant conditions are some of the variables collected on these school-age children and youth. Regional/national/state summaries can be found at the Gallaudet Research web site: http://research.gallaudet.edu/Demographics/	t	t
841	Trilingual Interpreting	f	f	f	2	March 10, 2011	September 30, 2011	This is a collaborative project of the National Consortium of Interpreter Education Centers (NCIEC) between Western Oregon University (WOU), Northeastern University (NU), and the Gallaudet University Regional Interpreter Education Center (GURIEC).<br><br>The overall goal of this particular NCIEC Task Force endeavor was to identify and vet competencies and skills specific to trilingual interpreting. To this end, the Task Force conducted a literature review and a survey (both of which are currently underway) and engaging in approximately 15-20 focus groups nationwide. The information gleaned from these activities will assist the Task Force to 1) identify a set of general competency domains for use in organizing the competencies and skills; and 2) craft a draft set of competencies to be vetted by trilingual stakeholders.	t	f
51	District of Columbia Space Grant Consortium	f	f	f	2	August 26, 2010	August 25, 2011	Gallaudet University, along with other local universities and organizations comprising the District of Columbia Space Grant Consortium, participates in supporting educational and student financial assistance programs that develop infrastructure related to NASA and its strategic missions. During this reporting interval:<br><br>(1) a scholarship competition was conducted and two undergraduates were awarded funds<br>(2) student support funds provided a stipend and room and board for student interns during summer 2011<br>(3) software was maintained supporting NASA educational and research activities and undergraduate teaching by Dr. Snyder<br>(4) support was provided for one MSSD botball team to be trained and compete in the 2011 Botball regional competition. The Model Secondary School for the Deaf (MSSD) teams competed in the 2011 Greater D.C. Regional Botball Tournament on April 9 in Fairfax, Virginia. Teams from the District of Columbia, Virginia, Maryland and the surrounding states brought robots to the competition. Out of 36 teams at the tournament, MSSD Team took 29th place overall.<br>(5) undergraduate Remotely Operated Vehicle (ROV) investigations took place in Spring 2011 in the PHY153 laboratory during the entire semester<br>(6) This year's trip to Space Camp for MSSD students was cancelled due to tornadoes that hit Alabama in May. Funds have been carried over to support the trip in 2012.	t	t
444	Reflective Journal Writing: Deaf Pre-Service Teachers with Hearing Children	f	f	f	2	October 1, 2005	No set date	In this study, the content of reflective journals of Deaf pre-service teachers during student teaching in a classroom with hearing students has been examined. Journal entries were analyzed and compared to the established literature on student teaching. It was found that these student teachers focused on many of the same issues that had been mentioned in the literature on reflective teaching; they often did so by incorporating key elements of Deaf culture. Unlike most student teachers, this cohort placed a primary emphasis on pedagogy, but with a visual emphasis. Implications of the socio-cultural perspectives in teacher education programs are discussed in the researchers' analyses.	t	t
839	Evaluation and Training of Active Sign Language Interpreters in Trinidad and Tobago	f	f	f	2	June 21, 2010	August 3, 2011	The majority of current sign language interpreters in Trinidad and Tobago (T&T) do not have formal training in the profession and do not hold qualifications or certification as an interpreter. At this point, there is no interpreter training program and no interpreter association that evaluates and qualifies individuals. This project articulated a plan for immediate evaluation, training, and qualification of acting interpreters in the field. At the same time, the plan actively involved T&T stakeholders, interpreter leaders, and members of the Deaf community to develop greater capacity among persons of T&T to achieve long-term objectives for training and evaluating interpreters.	t	f
112	Signs of Literacy: A Longitudinal Study of ASL and English Literacy Acquisition	f	f	f	1	October 1, 1993	No set date	This interdisciplinary, longitudinal study examines cultural, linguistic, and cognitive development in deaf and hard of hearing children from diverse backgrounds. The first phase of data collection was carried out from 1994-1996 when 12 teachers and 60 children were videotaped biweekly in their preschool classrooms. In FY2003, follow-up data were collected on six children selected for an in-depth, longitudinal study. The goals were (1) to describe the ASL and English literacy acquisition of six deaf and hard of hearing children in preschool classrooms where ASL and English were the languages of instruction, (2) to describe the pedagogy, including the philosophy, teaching strategies, and classroom literacy environments of nine preschool teachers as well as the early literacy practices in a Deaf home, and (3) to document the ASL, English literacy, and academic achievement of the six target students from the time the classroom data collection ended in 1996 through 2002. The central focus of the ongoing analysis of the videotapes is on how ASL and English literacy are acquired by individual children who differ in theoretically important ways, how the parents', teachers', and children's use of ASL is linked to, and supports, emerging English literacy, and how this linguistic and cultural knowledge contributes to academic achievement during interaction with adults and peers.	t	t
300	Motivations and Goals of Owners, Managers, and Counselors of Planned Recreational Programs for Deaf and Hard of Hearing Children	f	f	f	1	June 2005	No set date	There are approximately 70 known summer camps for deaf and hard of hearing children and youth around the United States. In addition, weekend programs directed at mainstreamed deaf and hard of hearing youth are emerging around the United States as education and mental health professionals strive to provide the crucial social experiences that are frequently lacking in mainstream settings.<br><br>This study is the first to focus on this phenomenon. Given the dearth of research on these programs, the focus is on very foundation of the program&mdash;the administrators, the program staff, and the actual activities offered. What are the motivations and goals of owners, managers, and counselors of summer and weekend programs for deaf and hard of hearing children? How are these motivations and goals reflected in staffing patterns (qualifications, training provided, expectations), actual activities, perception of ongoing challenges, and marketing efforts? To what extent do these patterns, activities and perceptions include sensitivity to, and a special effort towards, solitary and almost solitary children and youth?<br><br>This qualitative study attempts to answer these and other questions, to provide a rich description of the current state of affairs, and promote further study of various elements of this phenomenon.<br><br>A proposal was submitted to the Gallaudet Press in March 2011 and was accepted in May 2011. The manuscript is due January 2013.	t	t
842	Garbage In = Garbage Out: The Importance of Source Text Selection in Assessing Interpretations	f	f	f	1	March 1, 2010	No set date	The choice of source stimulus texts is as fundamental to the assessment and evaluation of interpreting as is choosing the appropriate evaluation rubrics. This discussion of which source texts will most effectively draw out the kind of interpretation that reflects those features the researchers intend to evaluate is essential to interpreting education and assessment. In ASL/English interpretations for example, many features that are heavily weighted in evaluation are found with more or less frequency depending on genre and register in ASL source texts. It is those features that contribute, in part, to the creation of a successfully coherent, dynamic equivalence in the target production. This preliminary report addresses the benefit to educators and evaluators in considering the source as the starting point for assessing a target interpretation. It includes both theoretical and evidence-based practices, and practical applications for educators and evaluators.	t	f
247	Auditory Self-Monitoring	f	f	f	1	October 2003	No set date	A novel approach to investigating self hearing has been developed. It is based on traditional psychophysical techniques, and focuses on the individual's sensitivity to variations in different acoustic properties of his/her speech feedback (e.g., timing, intensity). To date, tests of feedback delay detection and relative loudness of the self-generated speech have been fully automated and applied to investigate the effect of different listening conditions on self- hearing by individuals with different hearing abilities. In addition, a new line of research has been developed that focuses on the acoustic characteristics of the speech signal recorded both in the person's ear canal and at different points on his/her head, for live versus recorded speech, in either open or occluded ear. It is expected that the outcomes of this research program will include both increased understanding of the role that speech feedback plays in speech production, and the guidelines for the design of hearing assistive technology that can better serve self hearing needs of hard of hearing individuals.	t	t
615	Cochlear Implants and Gallaudet University	f	f	f	2	September 19, 2008	September 2011	This project focused on collecting information, via a confidential on-line survey, from a non-random sample of Gallaudet faculty, professional staff, and students on a variety of cochlear implant-related topics. Respondents were asked about their knowledge of the effectiveness of implants, whether implants could threaten the future of the Deaf community, and, if they are deaf, whether they would consider a cochlear implant for themselves.<br><br>The researchers compared the results of this survey with a similar survey conducted eight years ago to determine whether or not attitudes related to cochlear implants have changed on campus since 2000.	t	t
859	Priority Research Fund, 2010-2011	f	f	f	2	October 1, 2010	September 30, 2011	The Priority Research Fund, which provides an amount up to $35,000, was awarded to 1 Gallaudet faculty member and continued supporting two faculty on their 3rd and final year projects:<br><br><b>New grantee:</b><br>Brune, Jeffrey (History) <i>Faking It? Disability Stigma and the Modern American State</i><br><br><b>Continuing grantees:</b><br>Mathur, Gaurav (Linguistics) <i> Perception of Phonological Structure in ASL </i><br>Wang, Qi (Business) &amp; Solomon, Caroline (Biology) <i>Exploring Blended Instructional Pedagogy to Enhance Student Learning and Scientific Reasoning Skills in Biology (STEM)</i>	t	f
447	Signing with an Accent: ASL L2 Phonology	f	f	f	1	November 2005	No set date	This project investigates the phenomenon of "sign accent", or systematic phonological errors made by signers acquiring ASL as a second language (L2). This topic has been virtually ignored in the sign language literature, despite extensive discussion of accent in spoken L2s and a common assumption that some counterpart exists for signed L2. The investigations focus on handshape, approaching the phenomenon of L2 signing accent from two different perspectives. A "production component" explores non-signing subjects' ability to accurately reproduce ASL signs, while a "rating component" compares the ability of native and non-native ASL signers to identify accented L2 signing, based primarily on handshape.	t	t
837	The Depiction Comprehension Test Project	f	f	f	2	October 2007	July 2011	A key need is to develop a theoretical framework that will guide the creation of ASL proficiency tests. The project examines the roles played by physical space and the body in ASL, particularly in the use of the body to represent bodily actions. The researchers are developing a prototype test of ASL fluency based on Dudis' typological study of such phenomena.	t	f
542	Latin American and the Caribbean Newborn Hearing Screening Survey	f	f	f	2	October 1, 2006	No set date	The purpose of the survey is to collect information on infant hearing screening in the Spanish-speaking countries of Latin America and the Caribbean. We also have included Brazil, a Portuguese-speaking country. Latino deaf and hard of hearing children make up 25% of all school-age deaf and hard of hearing children in American schools. The information we collect through this survey can benefit professionals in the United States who work with immigrant Latino deaf and hard of hearing children and their families. Collecting information on services for infants and toddlers also will provide the opportunity for information sharing amongst the countries involved. The researchers planning this project are frequently contacted for technical assistance by educators, parents, and professionals in Latin America. They believe that the most beneficial form of technical assistance is helping to build networks among professionals in the region. These professionals will be able to share resources and solutions that are best suited for their conditions. By fostering the development of such networks, the researchers recognize that technical solutions, as well as expertise, may be available in the region. Finally, the researchers believe that collecting this information serves as a basic needs assessment of the state of infant hearing screening in the Spanish-speaking countries in Latin America and the Caribbean. The information gathered can serve as the basis for improving services in these countries.	t	t
765	Anthropological Genetics of <i>GJB2</i> Deafness	f	f	f	1	January 2010	No end date	The researchers aim to explore two anthropological explanations for the high prevalence and mutational diversity of deafness-causing <i>GJB2</i> mutations in the North American population, as well as the association of specific <i>GJB2</i> mutations with ethnic groups. The first is <b>heterotic balancing selection</b>, in which <i>GJB2</i> heterozygotes may have increased fitness, possibly due to resistance to bacillary dysentery. A second explanation is <b>linguistic homogamy</b>, meaning in this case that deaf individuals have actively sought mates with compatible fluency in signed languages. This mate-selection phenomenon may have begun ~200 years ago with the introduction of signed language in residential schools for the deaf. The significance of linguistic homogamy in deaf communities is that in the broader human population, the same mechanism may have driven the inexplicably rapid evolution of <i>FOXP2</i> and 21 other genes implicated in human speech since their appearance in early humans 100,000-200,000 years ago.	t	t
578	Families Who are Deaf: A Photographic Essay	f	f	f	1	January 2007	No set date	This project is being done by conducting a photo-documentary study of the everyday lives of families who have family members who are deaf and who use ASL. Using semi-structured depth interviews, the researchers collected information directly from families relating to various aspects of their life experience. Family portraits and photographs of the families "in action" were made. An important focus was to document how the families who use ASL, and those whose children also have cochlear implants, move back and forth between Deaf and hearing cultures and ASL and spoken languages in the context of home, school, and community. This project will result in a book that will include summaries/identified themes from the interviews, information from the literature, and family essays&mdash;all juxtaposed with family photographs.<br><br>Seven families with a family member who is deaf and who use American Sign Language were interviewed and photographed. Diversity in family structure, age of children, race and ethnicity was achieved; three of the children also have cochlear implants. The photographs are intended to record the rhythm and pattern of the family's life at home and in other venues as the parents suggest; they are informal and both candid and set-up photographs are made.<br><br>Note: A photo-documentary book was due to Gallaudet University Press September 12, 2011.	t	t
736	Reliability and Validity of the Adapted COPE Scale with Deaf College Students	f	f	f	2	June 2007	June 2011	The purpose of this study was to investigate the reliability and validity of the Adapted Coping Operations Preference Enquiry (COPE) scale with deaf college students. Reliability was obtained by conducting a confirmatory factor analysis of the items to determine if factor structures are similar to those obtained in the original study on the COPE. Internal consistencies and correlations among the scales were obtained as additional measures of reliability. It was concluded that this measure needs ongoing work.	t	t
825	Do Deaf Individuals' Dating Scripts Follow the Traditional Sexual Script?	f	f	f	2	October 5, 2010	August 5, 2011	The current study investigated Deaf individuals' dating expectations. Prior research on dating expectations has identified three common scenes: initiation/meeting, activities, and outcomes/conclusions. Participants were asked to report their expectations for each scene on a typical date. Talking was the most frequently occurring initiation activity. Dinner and a movie were among the top date activities in the activities scene. Activities were often reported as group gatherings. Dating outcomes included a goodnight kiss and making plans for another date. These expectations do not match prior research with hearing participants where the Traditional Sexual Script could be identified. Comparisons and suggestions for future research are discussed.	t	f
824	Parenting Experiences Raising Deaf Children: Understanding Behavior Problems	f	t	f	2	October 1, 2009	August 15, 2011	The focus of the present study was to explore the unique interaction between hearing parents and their deaf children. By utilizing qualitative methodology, the goal specifically was to gain an understanding of the parents' experiences and perceptions of their deaf children's behavior.	t	f
858	Small Research Grants, 2010-2011	f	f	f	2	October 1, 2010	September 30, 2011	A Small Research Grant, which provides up to $5,000 each, was awarded to the following faculty and students: <br><br><b>Faculty/staff recipient(s) [student(s)] (department) project title:</b><br>&bull; Ackley, R. Steven [Martin, Ruth; Moushey, Jamie] (Hearing, Speech and Language Sciences) <i>Vestibular Function in DFNB1 Deafness</i><br>&bull; Benedict, Beth &amp; Bodner-Johnson, Barbara (Communication Studies) <i>Families Who Are Deaf </i><br>&bull; Betman, Beth (Social Work) <i>Exploring the Phenomenological Experience of Child Sexual Abuse in Deaf Women Through the Creation of a Sandtray World</i><br>&bull; Clark, Diane (Educational Foundation Research/VL&sup2;) <i>The Dating Game: Do Deaf Individuals' Dating Scripts Follow the Traditional Sexual Script?</i><br>&bull; Hochgesang, Julie (Linguistics) <i>Representation of Hand Configuration Data in Different Notation Systems for Child Acquisition of ASL</i><br>&bull; Mirus, Gene (ASL/Deaf Studies/General Studies) <i>Face2Face Video Interactions with Iphones</i><br>&bull; Sabila, Paul (Chemistry) <i>Synthesis of Strained Heterocycles</i><br>&bull; Villanueva, Miako (Linguistics) <i>Interpreters' Use of Focus Shifts in ASL</i><br><br><b>Student recipient(s) [faculty/staff] (department) project title:</b><br>&bull; Adam, Mimi Rosa (Linguistics) <i>Facial Expression Acquisition in Children</i><br>&bull; Anderson, Melissa (Psychology) <i>Violence against Deaf Women: Deaf-Deaf Verse Hearing-Deaf Relationships</i><br>&bull; Barron, Malibu (Sociology) <i>Gallaudet University Undergraduates: Attitude and Behavior Toward Budgeting</i><br>&bull; Benedict, Rachel (Linguistics) <i>ASL Sign NOT as a Possible Prefix</i><br>&bull; Burkhardt, Brittany (Hearing, Speech and Language Sciences) <i>The Relationship between Speech Audiometry Performance and Broadband Acoustic Reflexes and Distortion Product Otoacoustic Emissions Results for People with Similar Audiograms in the Moderately-Severe to Profound Hearing Loss Range</i><br>&bull; Cull, Amber (Linguistics) <i>Production of Movement in Users of ASL and its Influence of Being Identified as Non-Native</i><br>&bull; Lovik, Julianna [Roy, Cynthia] (Interpretation) <i>Repair in Interpreted Discourse</i><br>&bull; Mazevski, Annette (Hearing,Speech and Language Sciences) <i>Aging Adults: Changes in Audibility and Memory, and their Effects on Speech Task Performance</i><br>&bull; Nead, Daniel (Psychology) <i>The Use of the Trauma Symptom Inventory and Brief Symptom Inventory with Deaf and Hard Hearing Israelis.</i><br>&bull; Peschiera, Meghan (Hearing, Speech and Language Sciences) <i>Hearing Loss in Dentists</i><br>&bull; Soje, Gabriel (Supervision/Administration) <i>Interpreters' Use of Focus shifts in ASL</i><br>&bull; Tyler, Allison (ASL and Deaf Studies) <i>Parallelism between Native Americans at the Boarding Schools during the Late 1800's and Deaf People during the Oral Method Movement</i><br>&bull; Wafa, Talah (Hearing, Speech and Language Sciences) <i>The Vestibular Effects of Cochlear Implantation in Deaf Adults</i><br>&bull; Wolf Craig, Kelly (Psychohlogy) <i>Body Image, Deaf Identity and the Tripartite Model: A preliminary Study</i><br>&bull; Yates, Michael; Crisologo, Anna; Hall, Wyatte; Schmitt Nelson, Shawn; Plotkin, Rachael; Witkin, Greg; Zodda, Jason (Psychology) <i>Signing While Driving: An Investigation of Divided Attention Resources Among Deaf Drivers</i><br>&bull; Zodda, Jason (Psychology) <i>Prevalence and Predicators of Risky Sexual Behavior in Young Adults who are Deaf </i><br>	t	f
819	Parenting Stress in Raising Deaf Children and the Role of Parental Personality in Coping	f	t	f	1	May 15, 2011	August 15, 2013	Within any family, parenting is a challenging process. For a variety of reasons, however, parents may be more or less reactive to the challenges of raising children. In general, parents of children with disabilities report greater parenting stress than do parents of children without disabilities (Beckman, 1991); parents of a deaf child are generally no exception (Feher-Prout, 1996). Parents of newly - diagnosed deaf children have a persistent feeling of being overwhelmed and inadequate to the task of raising a deaf child. 95% of babies with hearing loss are born to hearing parents (Marschark &amp; Hauser, 2008) who had no reason to suspect their child would have any degree of hearing loss. Many parents have had little if any experience with anyone who is deaf, so there is no frame of reference. At the time of diagnosis parents are presented with technical information and the need to make decisions about sensory devices, early intervention, and modality and communication choices. The decision-making process can be emotional, challenging, and stressful. Despite the common assumption that rearing a child with a disability such as hearing loss may put strain on parents, some studies have shown that not all families are at risk. This may suggest that parental factors, such as personality, may influence their approach to coping with the stressors of raising a deaf child.	t	t
823	Crying &amp; Helping Behavior: Gender Differences with Deaf Adults	f	t	f	2	October 1, 2009	October 1, 2010	There has been a significant amount of research addressing the crying and helping behaviors of hearing people. There has been little to no literature that examines the crying behaviors or helping behaviors of deaf people. The goal of this research was to study the opinions surrounding crying behaviors between men and women as well as the helping behaviors of deaf men and deaf women. In this study, deaf students were asked to report on their opinions surrounding the crying behaviors of men and women in very obvious, obvious, and non-obvious (ambiguous) situations. The participants were also asked to read about several different situations involving men and women crying in very obvious, obvious, and ambiguous circumstances and determine how helpful they would be towards the person in distress. Lastly, participants were told that there was a student on Gallaudet University's campus who was injured and home bound and needed help getting his notes and materials from his classes. The participants were then asked if they would be willing to volunteer some time to assist this student.<br><br>Results of the research confirmed that there is no difference in the acceptability of deaf men and deaf women crying. In addition, it was found that deaf women were more likely to help in ambiguous situations then deaf men were, while deaf men were more likely to help in the very obvious situation with the male victim. Lastly, it was found that roughly half of the participants were willing to volunteer their time to the injured student who was home bound and needed help.	t	f
828	Exploring the Phenomenological Experience of Child Sexual Abuse in Deaf Women Through the Creation of a Sandplay World	f	f	f	\N	September 2011	May 2011	The purpose of this study is to understand the phenomenological experience of Deaf women who experienced child sexual abuse and to understand what strengths may have been acquired through this experience.<br><br>Research Questions:<br><br>1. What is the phenomenological (lived) experience of deaf women who were sexually abused as children?<br>2. What is the perceived post-traumatic growth that may have come out of the abuse experience?<br><br>The participants will create a sandtray world that reflects their experience.	t	f
742	Hearing Mothers' Resolution of the Diagnosis of Child Deafness: An Exploration of the Reaction to Diagnosis Interview	f	t	f	2	October 30, 2009	April 30, 2011	The study explored the use of the Reaction to Diagnosis Interview (RDI) with parents of children diagnosed with hearing loss. Based on the tenets of attachment theory, the RDI measures parental resolution of the potential adjustment associated with their child receiving a life changing "medical diagnosis". Parents who are able to successfully cope with the emotional responses of receiving a diagnosis for their children, and can corporate this into a new understanding of their child, are considered to have "resolved" the difficulty associated with that event. The current study will explore characteristics of parents that may influence resolution of the diagnosis of hearing loss. Specifically, this study will examine how stress, psychological adjustment, attitudes about deaf people, and family social support contribute to parental resolution of the diagnosis of hearing loss. The study also explored the relationship between parental resolution and functioning/development of the D/deaf child.	t	t
777	Synthesis of Strained Heterocycles	f	f	f	1	October 1, 2010	No set date	The focus of the current project has been to develop a plausible methodology for the synthesis of biologically important tetrahydrofuran molecules like pachastrissamine using a ring expansion-silyl group migration protocol that was discovered during the researcher's graduate school research. This strategy has been successfully used to prepare some 2,3- and 3,4-disubstituted tetrahydrofurans in a stereoselective fashion. However, the general application of this strategy is limited in that it can only be applied for trans-epoxide substrates.<br><br>The focus of the Gallaudet Small Grant was to enable the researcher to (1) set up a functioning Organic Chemistry Research Laboratory at Gallaudet University, (2) contribute positively to the field of organic synthesis while teaching at Gallaudet University; and new data could be published and/or presented at different forums, and (3) eventually train Gallaudet students to be research scientists.<br><br><b><u>Objectives</u></b><br><br>1. The researcher's initial objective will be to investigate the effects of using different silyl(silicon)-protecting groups (TBDPS, TES, TIPS, TMS) on the selectivity and rate of ring expansion and on the migration of the silyl group.<br><br><img src="http://testgspp.gallaudet.edu/annual_reports/images/molecules1.png" /><br><br>2. The researcher will explore different ring activation conditions using various Lewis acids with the aim of developing a more versatile ring expansion-silyl migration conditions applicable to different classes of epoxides.<br><br>3. The researcher hopes to use the findings to design new methodologies that will enable the application of the ring expansion-silyl migration protocol to <i>trans</i>-epoxides 4, 5 and 6.<br><br><img src="http://testgspp.gallaudet.edu/annual_reports/images/molecules2.png" /><br />4. In the future, the new methodologies will be applied in the synthesis of biologically active natural products.	t	t
812	Black Deaf Administrators: Leadership Issues and Perceived Challenges to Organizational Advancement	f	f	f	2	June 2010	May 2011	This qualitative, phenomenological case study examined the perceptions and experiences of six Black Deaf administrators working in government, education, and human service settings from a transformative perspective in relation to leadership, mentoring opportunities, leadership development opportunities, and organizational impediments to advancement. Data collection methods included in-depth face-to-face interviews, recorded observations in the workplaces, and participant journals. The Moustakas' modified van Kaam method was applied to collect, analyze, and interpret the data through the combined lens of the Critical Race theory and the Disability Discrimination model. Themes that emerged in the study were race and disability; racism; prejudice; White privilege; tokenism; audism; the triple challenges of race, disability, and gender; and resiliency. In conclusion, the experiences of Black Deaf administrators were not found to be comparable to experiences of Caucasian Deaf, Caucasian hearing, and Black hearing counterparts, which reaffirmed the value of human diversity for the enrichment of the community.	t	t
834	Strategies Used by Trilingual Interpreters in VRS	f	f	f	1	September 2010	No set date	This study aims to investigate linguistic strategies that trilingual (English-Spanish-American Sign Language) VRS interpreters employ when faced with instances where a decision needs to be made about source material that is potentially ambiguous. The linguistic focus is on items that are unique to Spanish within these trilingual settings (formal vs. informal variants of Spanish pronouns, regional Spanish vocabulary, and gendered variants of some nouns). The researchers' hypothesis is that interpreters will have a variety of methods for handling ambiguous forms in the input and meet with a myriad of success in the resulting interpretations. The general goal is to document strategies that are commonly employed in order to systematically investigate whether such strategies are optimal.	t	f
658	SCAN - A Competing Words Subtest: Effect of Asynchronous Word Alignment on Test Performance in Children with Learning Disabilities	f	f	f	2	March 2008	No set date	Eighteen students at a Washington, DC metropolitan independent school for children with learning disabilities participated in a study designed to look at dichotic listening and the effect of stimulus onset asynchrony. The purpose of this study was to determine if this population (children and adolescents with a known learning disability), who are at a high risk for a central auditory processing disorder (CAPD), would perform differently on the competing words (CW) subtest of the SCAN-A: A Test for Auditory Processing Disorders in Adults and Adolescents. The SCAN-A is a widely used clinical assessment tool that can be used as a screener for both adults and adolescents to determine their central auditory processing abilities. At the time of development, the SCAN-A CW stimuli was controlled for intensity and duration. However, due to technical limitations, a true simultaneous onset of 0.00 ms could not be controlled for. At the time of testing, it was unknown how the asynchrony within the word pairs would affect test performance specifically in this population. Generally, there is limited information regarding CAPD and dichotic listening in children with learning disabilities except for the fact that these children may have more of a difficult time with the said auditory task. The current study attempted to compare performance of this specific population on the original recording, a true simultaneous onset of 0.00 msec and a recording in which there was a 90 msec offset between word pairs.	t	t
629	Perception of Phonological Structure in American Sign Language	f	f	f	2	April 2008	December 2010	The study investigates how language experience and parameters of phonological structure affect perception in American Sign Language (ASL). To examine perception, the study uses a number of experimental techniques in psycholinguistics. One technique&mdash;primed lexical decision&mdash;determines whether one sign facilitates the recognition of another sign if they share a parameter in common. Another technique, primed phonological matching, determines whether participants can detect a slight phonological difference between the two signs produced by different signers. To evaluate the effects of language experience, performance on these tasks are compared across both Deaf and hearing individuals in several groups: those exposed to ASL from birth; those exposed to ASL after five years of age; and those with no prior ASL exposure. The study helps to identify aspects of linguistic structure prominent in perception and to determine the degrees of signed language fluency with respect to perception, which can be applied toward language assessment.	t	t
704	Site-directed Mutagenesis of RasGRP2	f	f	f	1	October 1, 2008	No set date	Extracellular ligands, such as drugs, tumor promoters, and natural ligands, activate receptors located on the cellular membrane to elicit intracellular responses. This leads to a multitude of downstream signaling cascades, modulated by intracellular proteins. The researchers' project focuses on the Ras guanyl nucleotide-releasing protein (RasGRP). After activation of receptors located at the cellular membrane, the activated RasGRP "turns on" the Ras protein "switch." Activated Ras then broadcasts signals from the cell surface to other parts of the cell, such as downstream signaling on the ras gene, triggering cell proliferation and differentiation, essential for sustaining life. However, mutations on proteins that activate this pathway can stimulate cell division inappropriately, promoting the development of cancer. In collaboration with the National Cancer Institute, the Gallaudet University Molecular Genetics Laboratory utilizes site-directed mutagenesis to mutate specific residues of RasGRP1 and RasGRP2 isoforms to identify the reasons behind different binding affinities of the isoforms to phorbol esters, which are tumor-promoting ligands. Mutations of the RasGRP isoforms, DNA, and protein purification are performed in the Gallaudet University Molecular Genetics Laboratory. The National Cancer Institute then performs radioligand binding assays with phorbol esters to determine the binding affinities (increased or decreased Ras activation) of the mutated RasGRP. The long-term goal of this project is to develop novel strategies for manipulation of signaling pathways that involve RasGRP.	t	t
747	Kindergartens for the Deaf in Three Countries: United States, France, and Japan	f	f	f	1	July 2010	June 2013	This sociological and anthropological project examines the acculturation of young Deaf children in kindergartens from three countries: the United States, Japan, and France. It also analyzes the culture of Deafness within their larger cultures and socio-political contexts. This is the first cross-comparative international ethnographic study of kindergartens in schools for the deaf and, as such, it has the potential to open up new lines of scholarly inquiry via video-cued multivocal comparative ethnography. New lines of inquiry include varying pedagogy, curriculum, and goals of early childhood education from nation to nation as well as its national and cultural variation in Deaf education.	t	t
784	How the Past Informs the Present: Intersections of Deaf History with Deaf Studies	f	f	f	2	April 9, 2010	April 12, 2011	How is Deaf History used within the field of Deaf Studies? Deaf Studies scholars engage with deaf history to inform and frame their studies&mdash;theoretical and applied. However, this intersection has received little attention to date. In this presentation, two historians take a critical look at how past and current understandings of Deaf History have influenced Deaf Studies scholarship. They seek to explain the meaning and value of Deaf History for Deaf Studies. Finally, they point out myths in deaf history and outline theories and methods within history which Deaf Studies scholars may find beneficial.<br><br>The presentation cited below will be published in the conference proceedings.	t	t
869	Research-based Tools for Monitoring and Evaluating a Family-centered Early Education Program: The Case of Vietnam.	f	f	f	1	September 1, 2011	June 1, 2015	The study will develop appropriate and culturally-relevant indicators for measuring changes in knowledge, attitudes skills, and aspirations experienced by participants in a nascent early education program for families with deaf children in Vietnam. The overall objectives of the Intergenerational Deaf Education Outreach Project( IDEO) is to assist young deaf children to integrate into mainstream society, by piloting an innovative joint family and institution-based delivery system comprising screening, family support, and preschool services. Employing the indigenous sign language and local deaf adults in early education is an innovation for Vietnam, and given the government's interest in this pilot as a national model, it is imperative to document the efficacy of the teacher training program and the teaching/mentoring itself in stimulating deaf children's learning of language, as well as the response of participating families and schools. This provides an opportunity to apply the prototype of a "family assessment toolkit" developed earlier (Bowman, Reilly, Weber, 2009). This program has a deaf community involvement component that means documenting change on non-traditional matters in education, such as community engagement with deaf children. As the documentation needs to be done by local Vietnamese educators and deaf community members on an ongoing basis, there is need to develop and deliver training in applied research by use of videotaping and analyzing sessions with children, survey, interview and observation. The product from this study will include a comprehensive guide to developing an approach to monitoring the essential aspects of knowledge, behavioral and attitudinal change in an early deaf education program, and in training local people to do the data collection and review of data. The findings from data collection in Vietnam will be used regularly by the technical working groups that steer the IDEO program and be made available for other research-guided programs attempting to develop effective deaf early childhood programs in developing nations.	t	f
832	Collaborative Research: CI-ADDO-EN: Development of Publicly Available, Easily Searchable, Linguistically Analyzed, Video Corpora for Sign Language and Gesture Research	f	f	f	1	August 1, 2011	July 31, 2014	The goal of this project is to create a linguistically annotated, publicly available, and easily searchable corpus of videos from American Sign Language (ASL), which is being made available on the Web. This will constitute an important piece of infrastructure, enabling new kinds of research in both linguistics and vision-based recognition of ASL. In addition, a key goal is to make this corpus easily accessible to the broader ASL community, including users and learners of ASL.<br><br>This project draws on data and annotations collected in previous projects during the past decade, and will make them available on the web for the first time. In addition, a pilot study will incorporate a very rich set of ASL data contained in the Gallaudet University Deaf Studies Digital Journal into the searchable interface. The annotations of the journal will be carried out at the ASL &amp; Deaf Studies department.<br><br>The current state of the project can be viewed at http://secrets.rutgers.edu/dai/queryPages/	t	f
809	Rehabilitation Engineering Research Center on Telecommunications Access (RERC-TA)	f	f	f	1	October 1, 2009	September 30, 2014	The primary mission of the Telecommunications Access RERC is to advance accessibility and usability of telecommunications for people with all types of disabilities. The current program of the RERC is designed both to lay the foundation for access in next generation technologies and to create the bridge technologies needed to allow users to migrate to new technologies without losing access to emergency services or the ability to communicate with colleagues and family who are still on older telecommunications networks.<br><br>The research and development program of this RERC covers four areas:<br>1. to ensure that people with disabilities have effective communication for an emergency (and every day) when using new and emerging telecommunication technologies.<br>2. to ensure interoperable real-time text for people who depend on text for communication (deaf, hard of hearing, physical disability ,and speech disability).<br>3. to ensure the availability of accessible telecollaboration solutions for employment and participation.<br>4. to increase the impact of research through better guidelines, standards, tools, sample codes, and other resources that enable more companies to implement accessibility in their telecommunication technologies.<br><br>Notes: Williams, N. <i>Proof-of-concept application for finding the location of a user who sends an SMS to an emergency service</i>.<br>Because SMS do not encode the sender's location, unlike cell phone calls to 911, there must be an alternate means of finding the sender's location in an emergency. TAP has developed and deployed a proof-of-concept application, where an automated system receives the initial emergency SMS, and then responds with an SMS with an inline link to the sender. The sender clicks on the link, which takes him to a web page. This page finds the sender's location, using HTML 5 standard geolocation techniques. The automated response system takes note when the sender opens that page, and displays the user's map location to the emergency responder. This proof-of-concept application has been demonstrated to NENA and several companies that are interested in providing 911 emergency text services.<br><br>Christian Vogler and Norman Williams are co-chairs of the technical recommendations committee of the Emergency Access Advisory Committee in 2011.<br><br>Christian Vogler was appointed to the Third Communications Security, Reliability, and Interoperability Council at the FCC, for an 18-month term until March 2013.	t	t
833	Gallaudet Scholarship of Teaching and Learning Initiative (GSTLI)	f	f	f	1	August 2011	August 2013	The Gallaudet Scholarship of Teaching and Learning Initiative (GTSLI) is designed to create a learning community of teacher-scholars who, over a period of two years, will investigate, reflect upon, document, and enhance teaching practices designed to meet the needs of visually oriented and linguistically diverse learners in Gallaudet classrooms. Six faculty participants will be given one course release each semester for the two-year period of the initiative and will receive special project pay for two summers to work on their GTSLI projects. GSTLI activities include bi-weekly, 90-minute group meetings to discuss selected readings, individual project ideas and plans, and video samples of classroom teaching and learning. Participants will have the opportunity to meet with nationally recognized experts in the Scholarship of Teaching and Learning and to attend the annual meeting of the International Society for the Scholarship of Teaching and Learning. Each participant's GSTLI project will become part of a website entitled <i>Hands-on Learning: The Gallaudet Gallery of Engaged Teaching and Learning</i>.	t	f
576	An Automatic Fitting Algorithm for Cochlear Implants	f	f	f	1	October 1, 2006	September 30, 2010	The purpose of this study is to design and evaluate an automatic cochlear implant fitting algorithm based on a paired comparison adaptive approach to guide audiologists in choosing the best frequency allocation for the individual client. Frequency allocation can impact speech recognition abilities and, in turn, communication. This work entails a systematic search for an optimum frequency allocation using a modified Simplex procedure.<br><br>This study consists of three experiments: One is the discrimination of frequency analysis band wherein minimally detectable differences in frequency shifts along the electrode array will be identified. In experiment two, subjects will be computer-guided to search for an optimal frequency allocation among cells in a matrix, with the results from experiment one defining the cell content. The third experiment, the speech battery test, consists of speech perception experiments with the new map using nonsense syllable, phoneme, and sentence stimuli. Experiment 2 and Experiment 3 will be recursively conducted until the results converge with up to six sessions per subject required to finish the experiment. During the subject's first and last visits, he/she will complete the Communication Profile for the Hearing Impaired (CPHI) and a questionnaire similar to the Abbreviated Profile of Hearing Aid Benefit (APHAB). Four normal hearing native English speakers will be recruited to evaluate the experimental procedure and the speech processing algorithm. Fifteen post-linguistically deafened Nucleus 24 cochlear implant users will then be recruited to complete the study.<br><br>The successful completion of this research will not only result in better quality of life for cochlear implant users, but will form the basis for future research into the adaptive fitting of auditory prostheses.	f	t
829	Comparison of Astronomy Teaching Strategies for Deaf and Hard of Hearing Students in the Elementary Classrooms	f	f	f	1	August 8, 2011	No set date	The study will report summaries of Astronomy teaching strategies of those teaching Deaf and Hard of Hearing students. Specifically it will compare visual, captioned, and ASL teaching strategies in both the classrooms and laboratory settings and will look at the impact of planetarium visits on children's learning and behavior. The study will also report any similarities and differences in the astronomy curriculum used by the schools.	t	f
610	Teacher/Parent Reading Study [VL&sup2; Research/Practice Integration Study]	f	f	f	2	September 2007	No set date	The goal of this project was to obtain an insider's perspective on how deaf children, reared and educated in an ASL/English bilingual learning environment, become proficient readers. To learn how children who acquire language primarily by eye, rather than by ear, become competent readers, the investigators interviewed 12 mostly deaf ASL-English bilingual teachers and parents of deaf children. The intent was to explore their insights, experiences, and beliefs about this process, focusing on students who have moved beyond learning to read and are now reading to learn. The researchers identified recurring themes regarding essential foundations for fostering and skills associated with bilingual ASL/English competence and reading proficiency. They were planning to use these factors to create an online survey with VL&sup2;, but due to restructuring, will no longer be collaborating with VL&sup2; in this effort.<br><br>Update September, 2011: Two publications are being prepared: a written article for the <i>Journal of Deaf Studies and Deaf Education</i> and an ASL article for the <i>Deaf Studies Digital Journal</i>.	t	t
820	ASL Co-activation Study	f	f	f	1	2008	No set date	The researchers are conducting several studies testing whether deaf and hearing ASL bilinguals at different levels of proficiency activate ASL signs when they read English words. The purpose is to gain a better understanding of the lexical architecture of ASL-English bilinguals and how this might affect their literacy development.	t	f
857	GERNER DE GARCIA	t	f	f	\N	\N	\N	(product only)	t	f
860	MOORE	t	f	f	\N	\N	\N	(product only)	t	f
861	FENNELL	t	f	f	\N	\N	\N	(product only)	t	f
766	EARTH, REILLY, HARRIS, GARATE	t	f	f	2	October 2009	June 2010	(product only)	t	t
863	CONLEY	t	f	f	\N	\N	\N	(product only)	t	f
846	ROY	t	f	f	\N	\N	\N	(product only)	t	f
\.


--
-- Name: agency_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY agency
    ADD CONSTRAINT agency_pkey PRIMARY KEY (id);


--
-- Name: classification_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY classification
    ADD CONSTRAINT classification_pkey PRIMARY KEY (id);


--
-- Name: department_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY department
    ADD CONSTRAINT department_pkey PRIMARY KEY (id);


--
-- Name: focus_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY focus
    ADD CONSTRAINT focus_pkey PRIMARY KEY (id);


--
-- Name: funder_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY funder
    ADD CONSTRAINT funder_pkey PRIMARY KEY (id);


--
-- Name: funding_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY funding
    ADD CONSTRAINT funding_pkey PRIMARY KEY (id);


--
-- Name: investigator_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY investigator
    ADD CONSTRAINT investigator_pkey PRIMARY KEY (id);


--
-- Name: person_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- Name: priority_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY priority
    ADD CONSTRAINT priority_pkey PRIMARY KEY (id);


--
-- Name: product_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: profession_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY profession
    ADD CONSTRAINT profession_pkey PRIMARY KEY (id);


--
-- Name: program_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY program
    ADD CONSTRAINT program_pkey PRIMARY KEY (id);


--
-- Name: project_pkey; Type: CONSTRAINT; Schema: public; Owner: kjcole; Tablespace: 
--

ALTER TABLE ONLY project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);


--
-- Name: focus_priority_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY focus
    ADD CONSTRAINT focus_priority_fkey FOREIGN KEY (priority) REFERENCES priority(id);


--
-- Name: focus_project_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY focus
    ADD CONSTRAINT focus_project_fkey FOREIGN KEY (project) REFERENCES project(id);


--
-- Name: funding_funder_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY funding
    ADD CONSTRAINT funding_funder_fkey FOREIGN KEY (funder) REFERENCES funder(id);


--
-- Name: funding_project_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY funding
    ADD CONSTRAINT funding_project_fkey FOREIGN KEY (project) REFERENCES project(id);


--
-- Name: investigator_agency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY investigator
    ADD CONSTRAINT investigator_agency_fkey FOREIGN KEY (agency) REFERENCES agency(id);


--
-- Name: investigator_department_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY investigator
    ADD CONSTRAINT investigator_department_fkey FOREIGN KEY (department) REFERENCES department(id);


--
-- Name: investigator_person_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY investigator
    ADD CONSTRAINT investigator_person_fkey FOREIGN KEY (person) REFERENCES person(id);


--
-- Name: investigator_profession_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY investigator
    ADD CONSTRAINT investigator_profession_fkey FOREIGN KEY (profession) REFERENCES profession(id);


--
-- Name: investigator_program_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY investigator
    ADD CONSTRAINT investigator_program_fkey FOREIGN KEY (program) REFERENCES program(id);


--
-- Name: investigator_project_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY investigator
    ADD CONSTRAINT investigator_project_fkey FOREIGN KEY (project) REFERENCES project(id);


--
-- Name: product_classification_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_classification_fkey FOREIGN KEY (classification) REFERENCES classification(id);


--
-- Name: product_project_fkey; Type: FK CONSTRAINT; Schema: public; Owner: kjcole
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_project_fkey FOREIGN KEY (project) REFERENCES project(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

